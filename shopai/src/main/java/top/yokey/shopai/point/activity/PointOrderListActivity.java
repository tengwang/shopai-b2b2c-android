package top.yokey.shopai.point.activity;

import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.jeremyliao.liveeventbus.LiveEventBus;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.point.adapter.PointsOrderListAdapter;
import top.yokey.shopai.point.viewmodel.PointOrderListVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.PointOrderListBean;

@Route(path = ARoutePath.POINT_ORDER_LIST)
public class PointOrderListActivity extends BaseActivity {

    private final ArrayList<PointOrderListBean> arrayList = new ArrayList<>();
    private final PointsOrderListAdapter adapter = new PointsOrderListAdapter(arrayList);
    private Toolbar mainToolbar = null;
    private PullRefreshView mainPullRefreshView = null;
    private int page = 1;
    private PointOrderListVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_pull_refresh_view);
        mainToolbar = findViewById(R.id.mainToolbar);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.pointOrder);
        observeKeyborad(R.id.mainLinearLayout);
        mainPullRefreshView.setAdapter(adapter);
        mainPullRefreshView.setItemDecoration(new LineDecoration(App.get().dp2Px(16), true));
        vm = getVM(PointOrderListVM.class);
        page = 1;
        getData();

    }

    @Override
    public void initEvent() {

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                page = 1;
                getData();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getData();
            }

            @Override
            public void onLoadMore() {
                getData();
            }
        });

        adapter.setOnItemClickListener(new PointsOrderListAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, PointOrderListBean bean) {
                App.get().start(ARoutePath.POINT_ORDER, Constant.DATA_ID, bean.getPointOrderid());
            }

            @Override
            public void onOpera(int position, PointOrderListBean bean) {
                switch (bean.getPointOrderstate()) {
                    case "20":
                        DialogHelp.get().query(get(), R.string.confirmSelection, R.string.cancelOrder, null, view -> vm.cancel(bean.getPointOrderid()));
                        break;
                    case "30":
                        DialogHelp.get().query(get(), R.string.confirmSelection, R.string.confirmReceive, null, view -> vm.receive(bean.getPointOrderid()));
                        break;
                    default:
                        App.get().start(ARoutePath.POINT_ORDER, Constant.DATA_ID, bean.getPointOrderid());
                        break;
                }
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getOrderLiveData().observe(this, list -> {
            if (page == 1) arrayList.clear();
            page = page + 1;
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getHasMoreLiveData().observe(this, bool -> mainPullRefreshView.setCanLoadMore(bool));

        vm.getCancelLiveData().observe(this, string -> {
            ToastHelp.get().showSuccess();
            page = 1;
            getData();
        });

        vm.getReceiveLiveData().observe(this, string -> {
            ToastHelp.get().showSuccess();
            page = 1;
            getData();
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                if (arrayList.size() != 0) {
                    ToastHelp.get().show(bean.getReason());
                } else {
                    mainPullRefreshView.setError(bean.getReason());
                }
            } else {
                ToastHelp.get().show(bean.getReason());
            }
        });

        LiveEventBus.get(Constant.DATA_REFRESH, boolean.class).observe(this, bool -> {
            page = 1;
            getData();
        });

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView.setLoad();
        vm.getList(page + "");

    }

}
