package top.yokey.shopai.point.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.PointGoodsBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.PointCartController;
import top.yokey.shopai.zsdk.controller.PointprodController;

public class PointVM extends BaseViewModel {

    private final MutableLiveData<PointGoodsBean> goodsLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> addCartLiveData = new MutableLiveData<>();

    public PointVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<PointGoodsBean> getGoodsLiveData() {

        return goodsLiveData;

    }

    public MutableLiveData<String> getAddCartLiveData() {

        return addCartLiveData;

    }

    public void getGoods(String id) {

        PointprodController.pinfo(id, new HttpCallBack<PointGoodsBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, PointGoodsBean bean) {
                goodsLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void addCart(String pgId, String quantity) {

        PointCartController.add(pgId, quantity, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                addCartLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
