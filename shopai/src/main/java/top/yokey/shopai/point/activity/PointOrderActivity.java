package top.yokey.shopai.point.activity;

import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.jeremyliao.liveeventbus.LiveEventBus;

import top.yokey.shopai.R;
import top.yokey.shopai.point.viewmodel.PointOrderVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zsdk.bean.PointOrderBean;

@Route(path = ARoutePath.POINT_ORDER)
public class PointOrderActivity extends BaseActivity {

    @Autowired(name = Constant.DATA_ID)
    String orderId;
    private Toolbar mainToolbar = null;
    private AppCompatTextView toolbarTextView = null;
    private AppCompatTextView stateTextView = null;
    private AppCompatTextView stateTipsTextView = null;
    private AppCompatImageView stateImageView = null;
    private RelativeLayout logisticsRelativeLayout = null;
    private AppCompatTextView logisticsTextView = null;
    private AppCompatTextView logisticsTimeTextView = null;
    private AppCompatTextView addressNameTextView = null;
    private AppCompatTextView addressAreaTextView = null;
    private RelativeLayout messageRelativeLayout = null;
    private AppCompatTextView messageContentTextView = null;
    private AppCompatImageView goodsImageView = null;
    private AppCompatTextView nameTextView = null;
    private AppCompatTextView pointTextView = null;
    private AppCompatTextView numberTextView = null;
    private AppCompatTextView timeTextView = null;
    private AppCompatTextView operaTextView = null;
    private PointOrderBean bean = null;
    private PointOrderVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_point_order);
        mainToolbar = findViewById(R.id.mainToolbar);
        toolbarTextView = findViewById(R.id.toolbarTextView);
        stateTextView = findViewById(R.id.stateTextView);
        stateTipsTextView = findViewById(R.id.stateTipsTextView);
        stateImageView = findViewById(R.id.stateImageView);
        logisticsRelativeLayout = findViewById(R.id.logisticsRelativeLayout);
        logisticsTextView = findViewById(R.id.logisticsTextView);
        logisticsTimeTextView = findViewById(R.id.logisticsTimeTextView);
        addressNameTextView = findViewById(R.id.addressNameTextView);
        addressAreaTextView = findViewById(R.id.addressAreaTextView);
        messageRelativeLayout = findViewById(R.id.messageRelativeLayout);
        messageContentTextView = findViewById(R.id.messageContentTextView);
        goodsImageView = findViewById(R.id.goodsImageView);
        nameTextView = findViewById(R.id.nameTextView);
        pointTextView = findViewById(R.id.pointTextView);
        numberTextView = findViewById(R.id.numberTextView);
        timeTextView = findViewById(R.id.timeTextView);
        operaTextView = findViewById(R.id.operaTextView);

    }

    @Override
    public void initData() {

        if (VerifyUtil.isEmpty(orderId)) {
            ToastHelp.get().showDataError();
            onReturn(false);
            return;
        }
        setStateBarColor(R.color.yellow);
        setToolbar(mainToolbar, R.string.orderDetailed);
        observeKeyborad(R.id.mainLinearLayout);
        toolbarTextView.setTextColor(App.get().getColors(R.color.primary));
        mainToolbar.setBackgroundResource(R.drawable.selector_yellow);
        mainToolbar.setNavigationIcon(R.drawable.ic_action_back_primary);
        vm = getVM(PointOrderVM.class);

    }

    @Override
    public void initEvent() {

        logisticsRelativeLayout.setOnClickListener(view -> {

        });

        operaTextView.setOnClickListener(view -> {
            switch (bean.getOrderInfo().getPointOrderstate()) {
                case "20":
                    DialogHelp.get().query(get(), R.string.confirmSelection, R.string.cancelOrder, null, v -> vm.cancel(orderId));
                    break;
                case "30":
                    DialogHelp.get().query(get(), R.string.confirmSelection, R.string.confirmReceive, null, v -> vm.receive(orderId));
                    break;
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getOrderLiveData().observe(this, bean -> {
            PointOrderActivity.this.bean = bean;
            String time = String.format(getString(R.string.orderPaySn), bean.getOrderInfo().getPointOrdersn());
            stateTextView.setText(bean.getOrderInfo().getPointOrderstatetext());
            operaTextView.setVisibility(View.VISIBLE);
            switch (bean.getOrderInfo().getPointOrderstate()) {
                case "2":
                    stateImageView.setImageResource(R.mipmap.ic_order_detailed_cancel);
                    stateTipsTextView.setText(R.string.addTime);
                    stateTipsTextView.append("：" + bean.getOrderInfo().getPointAddtime());
                    time = time + "\n" + getString(R.string.addTime) + "：" + bean.getOrderInfo().getPointAddtime();
                    operaTextView.setVisibility(View.GONE);
                    break;
                case "20":
                    stateImageView.setImageResource(R.mipmap.ic_order_detailed_wait_deliver);
                    stateTipsTextView.setText(R.string.addTime);
                    stateTipsTextView.append("：" + bean.getOrderInfo().getPointAddtime());
                    time = time + "\n" + getString(R.string.addTime) + "：" + bean.getOrderInfo().getPointAddtime();
                    operaTextView.setText(R.string.cancelExchange);
                    break;
                case "30":
                    stateImageView.setImageResource(R.mipmap.ic_order_detailed_wait_receive);
                    stateTipsTextView.setText(R.string.deliverTime);
                    stateTipsTextView.append("：" + bean.getOrderInfo().getPointShippingtime());
                    time = time + "\n" + getString(R.string.addTime) + "：" + bean.getOrderInfo().getPointAddtime();
                    time = time + "\n" + getString(R.string.deliverTime) + "：" + bean.getOrderInfo().getPointShippingtime();
                    operaTextView.setText(R.string.confirmReceive);
                    break;
                case "40":
                    stateImageView.setImageResource(R.mipmap.ic_order_detailed_finish);
                    stateTipsTextView.setText(R.string.finishTime);
                    stateTipsTextView.append("：" + bean.getOrderInfo().getPointFinnshedtime());
                    time = time + "\n" + getString(R.string.addTime) + "：" + bean.getOrderInfo().getPointAddtime();
                    time = time + "\n" + getString(R.string.deliverTime) + "：" + bean.getOrderInfo().getPointShippingtime();
                    time = time + "\n" + getString(R.string.finishTime) + "：" + bean.getOrderInfo().getPointFinnshedtime();
                    operaTextView.setVisibility(View.GONE);
                    break;
            }
            if (bean.getOrderInfo().getDeliverInfo().size() == 0) {
                logisticsRelativeLayout.setVisibility(View.GONE);
            } else {
                logisticsRelativeLayout.setVisibility(View.VISIBLE);
                String deliver = bean.getOrderInfo().getDeliverInfo().get(bean.getOrderInfo().getDeliverInfo().size() - 1);
                logisticsTextView.setText(deliver.substring(deliver.indexOf("&nbsp;")).replace("&nbsp;", ""));
                logisticsTimeTextView.setText(deliver.substring(0, deliver.indexOf("&nbsp;")).replace("&nbsp;", " "));
            }
            addressNameTextView.setText(String.format(getString(R.string.goodsBuyReceiveName), bean.getOrderaddressInfo().getPointTruename()));
            addressNameTextView.append(" " + bean.getOrderaddressInfo().getPointMobphone());
            addressAreaTextView.setText(bean.getOrderaddressInfo().getPointAddress());
            if (!VerifyUtil.isEmpty(bean.getOrderInfo().getPointOrdermessage())) {
                messageRelativeLayout.setVisibility(View.VISIBLE);
                messageContentTextView.setText(bean.getOrderInfo().getPointOrdermessage());
            } else {
                messageRelativeLayout.setVisibility(View.GONE);
            }
            ImageHelp.get().displayRadius(bean.getProdList().get(0).getPointGoodsimage(), goodsImageView);
            nameTextView.setText(bean.getProdList().get(0).getPointGoodsname());
            pointTextView.setText(R.string.exchangePoint);
            pointTextView.append("：" + bean.getProdList().get(0).getPointGoodspoints());
            numberTextView.setText("x");
            numberTextView.append(bean.getProdList().get(0).getPointGoodsnum());
            timeTextView.setText(time);
        });

        vm.getCancelLiveData().observe(this, string -> {
            ToastHelp.get().showSuccess();
            vm.orderInfo(orderId);
            LiveEventBus.get(Constant.DATA_REFRESH).post(true);
        });

        vm.getReceiveLiveData().observe(this, string -> {
            ToastHelp.get().showSuccess();
            vm.orderInfo(orderId);
            LiveEventBus.get(Constant.DATA_REFRESH).post(true);
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.orderInfo(orderId));
                return;
            }
            ToastHelp.get().show(bean.getReason());
        });

    }

    @Override
    public void onResume() {

        super.onResume();
        vm.orderInfo(orderId);

    }

}
