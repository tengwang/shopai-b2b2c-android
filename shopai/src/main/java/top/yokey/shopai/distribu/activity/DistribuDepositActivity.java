package top.yokey.shopai.distribu.activity;

import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.distribu.adapter.DistribuDepositAdapter;
import top.yokey.shopai.distribu.viewmodel.DistribuDepositVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.util.AnimUtil;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.MemberFXDepositListBean;

@Route(path = ARoutePath.DISTRIBU_DEPOSIT)
public class DistribuDepositActivity extends BaseActivity {

    private final ArrayList<MemberFXDepositListBean> arrayList = new ArrayList<>();
    private final DistribuDepositAdapter adapter = new DistribuDepositAdapter(arrayList);
    private Toolbar mainToolbar = null;
    private AppCompatTextView priceTextView = null;
    private AppCompatTextView cashTextView = null;
    private AppCompatTextView detailedTextView = null;
    private AppCompatTextView nightTextView = null;
    private LinearLayoutCompat recordLinearLayout = null;
    private PullRefreshView recordPullRefreshView = null;
    private int page = 1;
    private boolean isAnim = false;
    private DistribuDepositVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_distribu_deposit);
        mainToolbar = findViewById(R.id.mainToolbar);
        priceTextView = findViewById(R.id.priceTextView);
        cashTextView = findViewById(R.id.cashTextView);
        detailedTextView = findViewById(R.id.detailedTextView);
        nightTextView = findViewById(R.id.nightTextView);
        recordLinearLayout = findViewById(R.id.recordLinearLayout);
        recordPullRefreshView = findViewById(R.id.recordPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.accountBalance);
        observeKeyborad(R.id.mainRelativeLayout);
        recordPullRefreshView.setAdapter(adapter);
        recordPullRefreshView.setItemDecoration(new LineDecoration(1, App.get().getColors(R.color.divider), true));
        vm = getVM(DistribuDepositVM.class);
        vm.getAsset();
        getLog();

    }

    @Override
    public void initEvent() {

        cashTextView.setOnClickListener(view -> App.get().start(ARoutePath.DISTRIBU_CASH_APPLY));

        detailedTextView.setOnClickListener(view -> showRecord());

        nightTextView.setOnClickListener(view -> goneAll());

        recordPullRefreshView.setOnClickListener(view -> {
            if (recordPullRefreshView.isError() || arrayList.size() == 0) {
                getLog();
            }
        });

        recordPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getLog();
            }

            @Override
            public void onLoadMore() {
                getLog();
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getAssetLiveData().observe(this, bean -> {
            priceTextView.setText("￥");
            priceTextView.append(bean.getAvailableFxTrad());
        });

        vm.getLogLiveData().observe(this, list -> {
            if (page == 1) arrayList.clear();
            page = page + 1;
            arrayList.addAll(list);
            recordPullRefreshView.setComplete();
        });

        vm.getHasMoreLiveData().observe(this, bool -> recordPullRefreshView.setCanLoadMore(bool));

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getAsset());
                return;
            }
            if (bean.getCode() == 2) {
                if (arrayList.size() != 0) {
                    ToastHelp.get().show(bean.getReason());
                } else {
                    recordPullRefreshView.setError(bean.getReason());
                }
                return;
            }
            ToastHelp.get().show(bean.getReason());
        });

    }

    @Override
    public void onReturn(boolean handler) {

        if (nightTextView.getVisibility() == View.VISIBLE) {
            goneAll();
            return;
        }
        if (handler && isShowKeyboard()) {
            hideKeyboard();
            return;
        }
        finish();

    }

    //自定义方法

    private void goneAll() {

        if (isAnim) {
            return;
        }
        if (nightTextView.getVisibility() == View.VISIBLE) {
            isAnim = true;
            AnimUtil.objectAnimator(nightTextView, AnimUtil.ALPHA, () -> {
                isAnim = false;
                nightTextView.setVisibility(View.GONE);
            }, 1.0f, 0);
        }
        if (recordLinearLayout.getVisibility() == View.VISIBLE) {
            isAnim = true;
            AnimUtil.objectAnimator(recordLinearLayout, AnimUtil.TRABSLATION_Y, () -> {
                isAnim = false;
                recordLinearLayout.setVisibility(View.GONE);
            }, 0, App.get().getHeight());
        }

    }

    private void showRecord() {

        if (isAnim) {
            return;
        }
        if (nightTextView.getVisibility() == View.GONE) {
            isAnim = true;
            nightTextView.setVisibility(View.VISIBLE);
            AnimUtil.objectAnimator(nightTextView, AnimUtil.ALPHA, () -> isAnim = false, 0, 1.0f);
        }
        if (recordLinearLayout.getVisibility() == View.GONE) {
            isAnim = true;
            recordLinearLayout.setVisibility(View.VISIBLE);
            AnimUtil.objectAnimator(recordLinearLayout, AnimUtil.TRABSLATION_Y, () -> isAnim = false, App.get().getHeight(), 0);
        }

    }

    private void getLog() {

        recordPullRefreshView.setLoad();
        vm.getLog(page + "");

    }

}
