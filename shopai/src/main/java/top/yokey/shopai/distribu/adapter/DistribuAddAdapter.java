package top.yokey.shopai.distribu.adapter;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zsdk.bean.GoodsFXAddBean;

public class DistribuAddAdapter extends RecyclerView.Adapter<DistribuAddAdapter.ViewHolder> {

    private final ArrayList<GoodsFXAddBean> arrayList;
    private boolean isGrid;
    private OnItemClickListener onItemClickListener = null;

    public DistribuAddAdapter(ArrayList<GoodsFXAddBean> arrayList, boolean isGrid) {

        this.isGrid = isGrid;
        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        GoodsFXAddBean bean = arrayList.get(position);
        ImageHelp.get().displayRadius(bean.getGoodsImageUrl(), holder.mainImageView);
        int height = (App.get().getWidth() - 32) / 2;
        ViewGroup.LayoutParams layoutParams = holder.mainImageView.getLayoutParams();
        if (isGrid) layoutParams.height = height;
        holder.mainImageView.setLayoutParams(layoutParams);
        holder.nameTextView.setText(bean.getGoodsName());
        holder.descTextView.setText(bean.getGoodsJingle());
        holder.priceTextView.setText("￥");
        holder.priceTextView.append(bean.getGoodsPrice());
        holder.rateTextView.setText(String.format(App.get().getString(R.string.distribuCommissionRate), bean.getFxCommisRate()));
        holder.rateTextView.append("%");
        holder.saleTextView.setText(R.string.saleNumber);
        holder.saleTextView.append("：");
        holder.saleTextView.append(bean.getSaleCount());
        if (isGrid) {
            holder.descTextView.setVisibility(View.GONE);
            holder.descTextView.setText("");
        } else {
            holder.descTextView.setVisibility(View.VISIBLE);
            holder.descTextView.setText(bean.getGoodsJingle());
        }
        if (bean.getIsOwnShop().equals("1")) {
            String name = App.get().handlerHtml(App.get().getString(R.string.htmlOwn), "#EE0000");
            name = name + " " + holder.nameTextView.getText().toString();
            holder.nameTextView.setText(Html.fromHtml(name));
        }

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

        holder.addImageView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickAdd(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view;
        if (isGrid) {
            view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_distribu_add_list, group, false);
        } else {
            view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_distribu_add_ver, group, false);
        }
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setGrid(boolean grid) {

        isGrid = grid;

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, GoodsFXAddBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickAdd(int position, GoodsFXAddBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatImageView mainImageView;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView descTextView;
        private final AppCompatTextView priceTextView;
        private final AppCompatTextView saleTextView;
        private final AppCompatTextView rateTextView;
        private final AppCompatImageView addImageView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainImageView = view.findViewById(R.id.mainImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            descTextView = view.findViewById(R.id.descTextView);
            priceTextView = view.findViewById(R.id.priceTextView);
            saleTextView = view.findViewById(R.id.saleTextView);
            rateTextView = view.findViewById(R.id.rateTextView);
            addImageView = view.findViewById(R.id.addImageView);

        }

    }

}
