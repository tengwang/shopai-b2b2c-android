package top.yokey.shopai.distribu.activity;

import android.view.View;
import android.view.inputmethod.EditorInfo;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.distribu.adapter.DistribuBillAdapter;
import top.yokey.shopai.distribu.viewmodel.DistribuBillVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.adapter.ViewPagerAdapter;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.DistribuBillListBean;
import top.yokey.shopai.zsdk.data.GoodsData;

@Route(path = ARoutePath.DISTRIBU_BILL)
public class DistribuBillActivity extends BaseActivity {

    private final int[] page = new int[3];
    private final String[] state = new String[3];
    @SuppressWarnings("unchecked")
    private final ArrayList<DistribuBillListBean>[] arrayList = new ArrayList[3];
    private final DistribuBillAdapter[] adapter = new DistribuBillAdapter[3];
    private final PullRefreshView[] mainPullRefreshView = new PullRefreshView[3];
    private Toolbar mainToolbar = null;
    private AppCompatEditText toolbarEditText = null;
    private AppCompatImageView toolbarImageView = null;
    private TabLayout mainTabLayout = null;
    private ViewPager mainViewPager = null;
    private int position;
    private String keyword = "";
    private DistribuBillVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_distribu_bill);
        mainToolbar = findViewById(R.id.mainToolbar);
        toolbarEditText = findViewById(R.id.toolbarEditText);
        toolbarImageView = findViewById(R.id.toolbarImageView);
        mainTabLayout = findViewById(R.id.mainTabLayout);
        mainViewPager = findViewById(R.id.mainViewPager);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar);
        observeKeyborad(R.id.mainLinearLayout);
        position = getIntent().getIntExtra(Constant.DATA_POSITION, 0);
        toolbarEditText.setHint(R.string.pleaseInputOrderSnOrTitle);
        toolbarImageView.setImageResource(R.drawable.ic_action_search);
        state[0] = "";
        state[1] = "0";
        state[2] = "1";
        List<String> titleList = new ArrayList<>();
        titleList.add(getString(R.string.all));
        titleList.add(getString(R.string.unSettlement));
        titleList.add(getString(R.string.alreadySettlement));
        List<View> viewList = new ArrayList<>();
        viewList.add(getLayoutInflater().inflate(R.layout.include_pull_refresh_view, null));
        viewList.add(getLayoutInflater().inflate(R.layout.include_pull_refresh_view, null));
        viewList.add(getLayoutInflater().inflate(R.layout.include_pull_refresh_view, null));
        for (int i = 0; i < viewList.size(); i++) {
            page[i] = 1;
            arrayList[i] = new ArrayList<>();
            adapter[i] = new DistribuBillAdapter(arrayList[i]);
            mainPullRefreshView[i] = viewList.get(i).findViewById(R.id.mainPullRefreshView);
            mainPullRefreshView[i].setItemDecoration(new LineDecoration(App.get().dp2Px(12), true));
            mainTabLayout.addTab(mainTabLayout.newTab().setText(titleList.get(i)));
            mainPullRefreshView[i].setAdapter(adapter[i]);
        }
        App.get().setTabLayout(mainTabLayout, mainViewPager, new ViewPagerAdapter(viewList, titleList));
        mainTabLayout.setTabMode(TabLayout.MODE_FIXED);
        mainViewPager.setCurrentItem(position);
        vm = getVM(DistribuBillVM.class);
        page[position] = 1;
        getData();

    }

    @Override
    public void initEvent() {

        toolbarImageView.setOnClickListener(view -> {
            keyword = Objects.requireNonNull(toolbarEditText.getText()).toString();
            page[position] = 1;
            hideKeyboard();
            getData();
        });

        toolbarEditText.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                keyword = Objects.requireNonNull(toolbarEditText.getText()).toString();
                page[position] = 1;
                hideKeyboard();
                getData();
            }
            return false;
        });

        mainViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position1) {
                position = position1;
                if (arrayList[position].size() == 0) {
                    page[position] = 1;
                    getData();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        for (int i = 0; i < mainPullRefreshView.length; i++) {
            final int pos = i;
            mainPullRefreshView[pos].setOnClickListener(view -> {
                if (mainPullRefreshView[pos].isError() || arrayList[pos].size() == 0) {
                    page[pos] = 1;
                    getData();
                }
            });
        }

        for (PullRefreshView pullRefreshView : mainPullRefreshView) {
            pullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    page[position] = 1;
                    getData();
                }

                @Override
                public void onLoadMore() {
                    getData();
                }
            });
        }

        for (DistribuBillAdapter listAdapter : adapter) {
            listAdapter.setOnItemClickListener(new DistribuBillAdapter.OnItemClickListener() {
                @Override
                public void onClick(int position, DistribuBillListBean bean) {

                }

                @Override
                public void onClickGoods(int position, DistribuBillListBean bean) {
                    App.get().startGoods(new GoodsData(bean.getGoodsId()));
                }
            });
        }

    }

    @Override
    public void initObserve() {

        vm.getBillLiveData().observe(this, list -> {
            if (page[position] == 1) arrayList[position].clear();
            page[position] = page[position] + 1;
            arrayList[position].addAll(list);
            mainPullRefreshView[position].setComplete();
        });

        vm.getHasMoreLiveData().observe(this, bool -> mainPullRefreshView[position].setCanLoadMore(bool));

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                if (arrayList[position] != null && arrayList[position].size() != 0) {
                    ToastHelp.get().show(bean.getReason());
                } else {
                    mainPullRefreshView[position].setError(bean.getReason());
                }
            } else {
                ToastHelp.get().show(bean.getReason());
            }
        });

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView[position].setLoad();
        vm.getBillList(state[position], keyword, page[position] + "");

    }

}
