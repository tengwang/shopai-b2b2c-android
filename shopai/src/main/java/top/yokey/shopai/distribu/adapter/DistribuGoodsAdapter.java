package top.yokey.shopai.distribu.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zsdk.bean.GoodsFXListBean;

public class DistribuGoodsAdapter extends RecyclerView.Adapter<DistribuGoodsAdapter.ViewHolder> {

    private final ArrayList<GoodsFXListBean> arrayList;
    private onItemClickListener onItemClickListener = null;

    public DistribuGoodsAdapter(ArrayList<GoodsFXListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        GoodsFXListBean bean = arrayList.get(position);
        holder.storeTextView.setText(bean.getStoreName());
        holder.descTextView.setText(String.format(App.get().getString(R.string.distribuCommissionRate), bean.getFxCommisRate()));
        holder.descTextView.append("%");
        ImageHelp.get().displayRadius(bean.getGoodsImageUrl(), holder.goodsImageView);
        holder.nameTextView.setText(bean.getGoodsName());
        holder.timeTextView.setText(String.format(App.get().getString(R.string.distribuAuthorizeTime), bean.getFxTime()));
        holder.priceTextView.setText("￥");
        holder.priceTextView.append(bean.getGoodsPrice());
        String number = "x1";
        holder.numberTextView.setText(number);

        holder.removeTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickDrop(position, bean);
            }
        });

        holder.shareTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickShare(position, bean);
            }
        });

        holder.mainLinearLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_distribu_goods, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(onItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface onItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, GoodsFXListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickDrop(int position, GoodsFXListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickShare(int position, GoodsFXListBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayoutCompat mainLinearLayout;
        private final AppCompatTextView storeTextView;
        private final AppCompatTextView descTextView;
        private final AppCompatImageView goodsImageView;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView timeTextView;
        private final AppCompatTextView priceTextView;
        private final AppCompatTextView numberTextView;
        private final AppCompatTextView removeTextView;
        private final AppCompatTextView shareTextView;

        private ViewHolder(View view) {

            super(view);
            mainLinearLayout = view.findViewById(R.id.mainLinearLayout);
            storeTextView = view.findViewById(R.id.storeTextView);
            descTextView = view.findViewById(R.id.descTextView);
            goodsImageView = view.findViewById(R.id.goodsImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            timeTextView = view.findViewById(R.id.timeTextView);
            priceTextView = view.findViewById(R.id.priceTextView);
            numberTextView = view.findViewById(R.id.numberTextView);
            removeTextView = view.findViewById(R.id.removeTextView);
            shareTextView = view.findViewById(R.id.shareTextView);

        }

    }

}
