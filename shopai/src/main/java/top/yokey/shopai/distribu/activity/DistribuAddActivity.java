package top.yokey.shopai.distribu.activity;

import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Vector;

import top.yokey.shopai.R;
import top.yokey.shopai.distribu.adapter.DistribuAddAdapter;
import top.yokey.shopai.distribu.adapter.DistribuContractAdapter;
import top.yokey.shopai.distribu.viewmodel.DistribuAddVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.recycler.GridDecoration;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.recycler.SpaceDecoration;
import top.yokey.shopai.zcom.util.AnimUtil;
import top.yokey.shopai.zcom.util.ConvertUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.GoodsFXAddBean;
import top.yokey.shopai.zsdk.bean.SearchShowBean;
import top.yokey.shopai.zsdk.data.GoodsData;
import top.yokey.shopai.zsdk.data.GoodsDistribuData;

@Route(path = ARoutePath.DISTRIBU_ADD)
public class DistribuAddActivity extends BaseActivity {

    private final Vector<String> gcId = new Vector<>();
    private final Vector<String> gcName = new Vector<>();
    private final GoodsDistribuData data = new GoodsDistribuData();
    private final ArrayList<GoodsFXAddBean> arrayList = new ArrayList<>();
    private final DistribuAddAdapter adapter = new DistribuAddAdapter(arrayList, true);
    private final ArrayList<SearchShowBean.ContractListBean> contractArrayList = new ArrayList<>();
    private final DistribuContractAdapter contractAdapter = new DistribuContractAdapter(contractArrayList);
    private final LineDecoration lineDecoration = new LineDecoration(1, App.get().getColors(R.color.divider), false);
    private final SpaceDecoration spaceDecoration = new SpaceDecoration(App.get().dp2Px(4));
    private Toolbar mainToolbar = null;
    private AppCompatEditText toolbarEditText = null;
    private AppCompatImageView toolbarImageView = null;
    private AppCompatTextView screenOrderTextView = null;
    private AppCompatTextView screenCommissionTextView = null;
    private AppCompatTextView screenScreenTextView = null;
    private AppCompatImageView screenOrientationImageView = null;
    private AppCompatTextView screenNightTextView = null;
    private LinearLayoutCompat screenOrderLinearLayout = null;
    private AppCompatTextView screenOrderSynthesizeTextView = null;
    private AppCompatTextView screenOrderHighTextView = null;
    private AppCompatTextView screenOrderLowTextView = null;
    private AppCompatTextView screenOrderPopularityTextView = null;
    private LinearLayoutCompat screenConditionLinearLayout = null;
    private AppCompatEditText screenPriceFromEditText = null;
    private AppCompatEditText screenPriceToEditText = null;
    private AppCompatSpinner screenCateSpinner = null;
    private AppCompatTextView screenOwnTextView = null;
    private RecyclerView screenContractRecyclerView = null;
    private AppCompatTextView screenConfirmTextView = null;
    private AppCompatTextView screenResetTextView = null;
    private PullRefreshView mainPullRefreshView = null;
    private boolean isGrid = true;
    private DistribuAddVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_distribu_add);
        mainToolbar = findViewById(R.id.mainToolbar);
        toolbarEditText = findViewById(R.id.toolbarEditText);
        toolbarImageView = findViewById(R.id.toolbarImageView);
        screenOrderTextView = findViewById(R.id.screenOrderTextView);
        screenCommissionTextView = findViewById(R.id.screenCommissionTextView);
        screenScreenTextView = findViewById(R.id.screenScreenTextView);
        screenOrientationImageView = findViewById(R.id.screenOrientationImageView);
        screenNightTextView = findViewById(R.id.screenNightTextView);
        screenOrderLinearLayout = findViewById(R.id.screenOrderLinearLayout);
        screenOrderSynthesizeTextView = findViewById(R.id.screenOrderSynthesizeTextView);
        screenOrderHighTextView = findViewById(R.id.screenOrderHighTextView);
        screenOrderLowTextView = findViewById(R.id.screenOrderLowTextView);
        screenOrderPopularityTextView = findViewById(R.id.screenOrderPopularityTextView);
        screenConditionLinearLayout = findViewById(R.id.screenConditionLinearLayout);
        screenPriceFromEditText = findViewById(R.id.screenPriceFromEditText);
        screenPriceToEditText = findViewById(R.id.screenPriceToEditText);
        screenCateSpinner = findViewById(R.id.screenCateSpinner);
        screenOwnTextView = findViewById(R.id.screenOwnTextView);
        screenContractRecyclerView = findViewById(R.id.screenContractRecyclerView);
        screenConfirmTextView = findViewById(R.id.screenConfirmTextView);
        screenResetTextView = findViewById(R.id.screenResetTextView);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar);
        observeKeyborad(R.id.mainRelativeLayout);
        toolbarEditText.setHint(R.string.pleaseInputKeyword);
        screenScreenTextView.setEnabled(false);
        data.setOrder("");
        data.setPage("1");
        vm = getVM(DistribuAddVM.class);
        vm.getSearchShow();
        setGridModel();
        getData();

    }

    @Override
    public void initEvent() {

        toolbarEditText.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                data.setKeyword(Objects.requireNonNull(toolbarEditText.getText()).toString());
                data.setPage("1");
                getData();
            }
            return false;
        });

        toolbarImageView.setOnClickListener(view -> {
            data.setKeyword(Objects.requireNonNull(toolbarEditText.getText()).toString());
            data.setPage("1");
            getData();
        });

        screenOrderTextView.setOnClickListener(view -> {
            screenConditionLinearLayout.setVisibility(View.GONE);
            screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
            if (screenOrderLinearLayout.getVisibility() == View.VISIBLE) {
                screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
                goneScreenCondition();
            } else {
                screenNightTextView.setVisibility(View.VISIBLE);
                screenOrderLinearLayout.setVisibility(View.VISIBLE);
                screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_up, 0);
                AnimUtil.objectAnimator(screenOrderLinearLayout, AnimUtil.TRABSLATION_Y, -screenOrderLinearLayout.getHeight(), 0);
                AnimUtil.objectAnimator(screenNightTextView, AnimUtil.ALPHA, 0, 1.0f);
            }
        });

        screenCommissionTextView.setOnClickListener(view -> order(2, "4", "2"));

        screenScreenTextView.setOnClickListener(view -> {
            screenOrderLinearLayout.setVisibility(View.GONE);
            screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
            if (screenConditionLinearLayout.getVisibility() == View.VISIBLE) {
                screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
                goneScreenCondition();
            } else {
                screenNightTextView.setVisibility(View.VISIBLE);
                screenConditionLinearLayout.setVisibility(View.VISIBLE);
                screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_up, 0);
                AnimUtil.objectAnimator(screenConditionLinearLayout, AnimUtil.TRABSLATION_X, App.get().getWidth(), 0);
                AnimUtil.objectAnimator(screenNightTextView, AnimUtil.ALPHA, 0, 1.0f);
            }
        });

        screenOrientationImageView.setOnClickListener(view -> {
            if (isGrid) {
                setVerModel();
                return;
            }
            setGridModel();
        });

        screenNightTextView.setOnClickListener(view -> goneScreenCondition());

        screenOrderSynthesizeTextView.setOnClickListener(view -> {
            screenOrderSynthesizeTextView.setTextColor(App.get().getColors(R.color.accent));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderPopularityTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderTextView.setText(R.string.synthesizeSort);
            order(1, "", "");
        });

        screenOrderHighTextView.setOnClickListener(view -> {
            screenOrderSynthesizeTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.accent));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderPopularityTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderTextView.setText(R.string.priceHighToLow);
            order(1, "3", "2");
        });

        screenOrderLowTextView.setOnClickListener(view -> {
            screenOrderSynthesizeTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.accent));
            screenOrderPopularityTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderTextView.setText(R.string.priceLowToHigh);
            order(1, "3", "1");
        });

        screenOrderPopularityTextView.setOnClickListener(view -> {
            screenOrderSynthesizeTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderPopularityTextView.setTextColor(App.get().getColors(R.color.accent));
            screenOrderTextView.setText(R.string.popularitySort);
            order(1, "2", "2");
        });

        screenOwnTextView.setOnClickListener(view -> {
            if (VerifyUtil.isEmpty(data.getOwnShop())) {
                data.setOwnShop("1");
                screenOwnTextView.setTextColor(App.get().getColors(R.color.white));
                screenOwnTextView.setBackgroundResource(R.drawable.selector_accent_4dp);
            } else {
                data.setOwnShop("");
                screenOwnTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenOwnTextView.setBackgroundResource(R.drawable.selector_primary_dark_4dp);
            }
        });

        screenConfirmTextView.setOnClickListener(view -> {
            screenOrderSynthesizeTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderPopularityTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderTextView.setText(R.string.synthesizeSort);
            order(1, "", "");
        });

        screenResetTextView.setOnClickListener(view -> {
            data.setGcId("");
            data.setPriceFrom("");
            data.setPriceTo("");
            data.setOwnShop("");
            data.setCi("");
            screenPriceFromEditText.setText("");
            screenPriceToEditText.setText("");
            screenCateSpinner.setSelection(0);
            screenOwnTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOwnTextView.setBackgroundResource(R.drawable.selector_primary_dark_4dp);
            ToastHelp.get().show(R.string.success);
        });

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                data.setPage("1");
                getData();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                data.setPage("1");
                getData();
            }

            @Override
            public void onLoadMore() {
                getData();
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getSearchShowLiveData().observe(this, bean -> {
            gcId.clear();
            gcName.clear();
            gcId.add("");
            gcName.add("不限");
            for (int i = 0; i < bean.getGcList().size(); i++) {
                gcId.add(bean.getGcList().get(i).getGcId());
                gcName.add(bean.getGcList().get(i).getGcName());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<>(get(), R.layout.spinner_simple_small, gcName);
            adapter.setDropDownViewResource(R.layout.spinner_item_small);
            screenCateSpinner.setAdapter(adapter);
            screenCateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    data.setGcId(gcId.get(position));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            screenScreenTextView.setEnabled(true);
            contractArrayList.clear();
            contractArrayList.addAll(bean.getContractList());
            contractAdapter.notifyDataSetChanged();
            App.get().setRecyclerView(screenContractRecyclerView, contractAdapter);
            screenContractRecyclerView.addItemDecoration(new GridDecoration(3, App.get().dp2Px(8), false));
            screenContractRecyclerView.setLayoutManager(new GridLayoutManager(get(), 3));
            contractAdapter.notifyDataSetChanged();
            contractAdapter.setOnItemClickListener((position, bean1) -> {
                String value = bean1.getId() + "_";
                if (bean1.isSelect()) {
                    data.setCi(data.getCi().replace(value, ""));
                    contractArrayList.get(position).setSelect(false);
                } else {
                    data.setCi(data.getCi() + value);
                    contractArrayList.get(position).setSelect(true);
                }
                contractAdapter.notifyDataSetChanged();
            });
        });

        vm.getGoodsLiveData().observe(this, list -> {
            if (data.getPage().equals("1")) arrayList.clear();
            data.setPage((ConvertUtil.string2Int(data.getPage()) + 1) + "");
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getHasMoreLiveData().observe(this, bool -> mainPullRefreshView.setCanLoadMore(bool));

        vm.getAddLiveData().observe(this, bean -> ToastHelp.get().showSuccess());

        vm.getErrorLiveData().observe(this, bean -> {
            switch (bean.getCode()) {
                case 1:
                    DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getSearchShow());
                    break;
                case 2:
                    if (arrayList.size() == 0) {
                        mainPullRefreshView.setError(bean.getReason());
                    } else {
                        ToastHelp.get().show(bean.getReason());
                    }
                    break;
                default:
                    ToastHelp.get().show(bean.getReason());
                    break;
            }
        });

    }

    @Override
    public void onReturn(boolean handler) {

        if (screenNightTextView.getVisibility() == View.VISIBLE) {
            goneScreenCondition();
            return;
        }
        if (handler && isShowKeyboard()) {
            hideKeyboard();
            return;
        }
        finish();

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView.setLoad();
        vm.getList(data);

    }

    private void setVerModel() {

        isGrid = false;
        screenOrientationImageView.setImageResource(R.mipmap.ic_orientation_ver);
        mainPullRefreshView.setLayoutManager(new LinearLayoutManager(get()));
        mainPullRefreshView.getRecyclerView().setPadding(0, 0, 0, 0);
        mainPullRefreshView.removeItemDecoration(lineDecoration);
        mainPullRefreshView.removeItemDecoration(spaceDecoration);
        mainPullRefreshView.setItemDecoration(lineDecoration);
        adapter.setGrid(isGrid);
        mainPullRefreshView.setAdapter(adapter);
        mainPullRefreshView.setComplete();
        setAdapterEvent();

    }

    private void setGridModel() {

        isGrid = true;
        screenOrientationImageView.setImageResource(R.mipmap.ic_orientation_grid);
        mainPullRefreshView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        mainPullRefreshView.getRecyclerView().setPadding(App.get().dp2Px(4), 0, App.get().dp2Px(4), 0);
        mainPullRefreshView.removeItemDecoration(lineDecoration);
        mainPullRefreshView.removeItemDecoration(spaceDecoration);
        mainPullRefreshView.setItemDecoration(spaceDecoration);
        adapter.setGrid(isGrid);
        mainPullRefreshView.setAdapter(adapter);
        mainPullRefreshView.setComplete();
        setAdapterEvent();

    }

    private void setAdapterEvent() {

        adapter.setOnItemClickListener(new DistribuAddAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, GoodsFXAddBean bean) {
                App.get().startGoods(new GoodsData(bean.getGoodsId()));
            }

            @Override
            public void onClickAdd(int position, GoodsFXAddBean bean) {
                vm.add(bean.getGoodsCommonid());
            }
        });

    }

    private void goneScreenCondition() {

        if (screenNightTextView.getVisibility() == View.VISIBLE) {
            AnimUtil.objectAnimator(screenNightTextView, AnimUtil.ALPHA, () -> screenNightTextView.setVisibility(View.GONE), 1.0f, 0);
        }
        if (screenOrderLinearLayout.getVisibility() == View.VISIBLE) {
            screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
            AnimUtil.objectAnimator(screenOrderLinearLayout, AnimUtil.TRABSLATION_Y, () -> screenOrderLinearLayout.setVisibility(View.GONE), 0, -screenOrderLinearLayout.getHeight());
        }
        if (screenConditionLinearLayout.getVisibility() == View.VISIBLE) {
            screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
            AnimUtil.objectAnimator(screenConditionLinearLayout, AnimUtil.TRABSLATION_X, () -> screenConditionLinearLayout.setVisibility(View.GONE), 0, App.get().getWidth());
        }

    }

    private void order(int type, String sort, String order) {

        hideKeyboard();
        goneScreenCondition();
        data.setPage("1");
        data.setSort(sort);
        data.setOrder(order);
        data.setPriceTo(Objects.requireNonNull(screenPriceToEditText.getText()).toString());
        data.setPriceFrom(Objects.requireNonNull(screenPriceFromEditText.getText()).toString());
        screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
        screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
        switch (type) {
            case 1:
                screenOrderTextView.setTextColor(App.get().getColors(R.color.accent));
                screenCommissionTextView.setTextColor(App.get().getColors(R.color.textTwo));
                break;
            case 2:
                screenOrderTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenCommissionTextView.setTextColor(App.get().getColors(R.color.accent));
                screenOrderSynthesizeTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenOrderPopularityTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenOrderTextView.setText(R.string.synthesizeSort);
                break;
        }
        if (!VerifyUtil.isEmpty(data.getPriceFrom()) || !VerifyUtil.isEmpty(data.getPriceTo())
                || !VerifyUtil.isEmpty(data.getGcId()) || !VerifyUtil.isEmpty(data.getOwnShop())
                || !VerifyUtil.isEmpty(data.getCi())) {
            screenScreenTextView.setTextColor(App.get().getColors(R.color.accentDark));
        } else {
            screenScreenTextView.setTextColor(App.get().getColors(R.color.textTwo));
        }
        getData();

    }

}
