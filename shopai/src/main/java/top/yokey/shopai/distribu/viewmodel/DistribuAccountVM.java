package top.yokey.shopai.distribu.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.MemberFXAssetBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberFXController;
import top.yokey.shopai.zsdk.data.DistribuAccountData;

public class DistribuAccountVM extends BaseViewModel {

    private final MutableLiveData<MemberFXAssetBean> assetLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> saveLiveData = new MutableLiveData<>();

    public DistribuAccountVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<MemberFXAssetBean> getAssetLiveData() {

        return assetLiveData;

    }

    public MutableLiveData<String> getSaveLiveData() {

        return saveLiveData;

    }

    public void getAsset() {

        MemberFXController.myAsset(new HttpCallBack<MemberFXAssetBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, MemberFXAssetBean bean) {
                assetLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void save(DistribuAccountData data) {

        MemberFXController.saveMember(data, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                saveLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
