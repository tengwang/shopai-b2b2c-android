package top.yokey.shopai.distribu.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.MemberFXAssetBean;
import top.yokey.shopai.zsdk.bean.MemberFXCashBean;
import top.yokey.shopai.zsdk.bean.MemberFXCashListBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberFXController;

public class DistribuCashVM extends BaseViewModel {

    private final MutableLiveData<MemberFXAssetBean> assetLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> cashLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<MemberFXCashListBean>> logLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();
    private final MutableLiveData<MemberFXCashBean> infoLiveData = new MutableLiveData<>();

    public DistribuCashVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<MemberFXAssetBean> getAssetLiveData() {

        return assetLiveData;

    }

    public MutableLiveData<String> getCashLiveData() {

        return cashLiveData;

    }

    public MutableLiveData<ArrayList<MemberFXCashListBean>> getLogLiveData() {

        return logLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public MutableLiveData<MemberFXCashBean> getInfoLiveData() {

        return infoLiveData;

    }

    public void getAsset() {

        MemberFXController.myAsset(new HttpCallBack<MemberFXAssetBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, MemberFXAssetBean bean) {
                assetLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void cash(String amount, String payPwd) {

        MemberFXController.cashApply(amount, payPwd, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                cashLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void getLog(String page) {

        MemberFXController.fxCash(page, new HttpCallBack<ArrayList<MemberFXCashListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<MemberFXCashListBean> list) {
                logLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

    public void getInfo(String id) {

        MemberFXController.cashInfo(id, new HttpCallBack<MemberFXCashBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, MemberFXCashBean bean) {
                infoLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(4, reason));
            }
        });

    }

}
