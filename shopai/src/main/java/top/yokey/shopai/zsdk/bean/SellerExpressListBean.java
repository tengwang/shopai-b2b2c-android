package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class SellerExpressListBean implements Serializable {

    @SerializedName("id")
    private String id = "";
    @SerializedName("e_name")
    private String eName = "";
    @SerializedName("e_code")
    private String eCode = "";
    @SerializedName("e_letter")
    private String eLetter = "";
    @SerializedName("e_order")
    private String eOrder = "";
    @SerializedName("e_url")
    private String eUrl = "";
    @SerializedName("e_zt_state")
    private String eZtState = "";
    @SerializedName("is_check")
    private String isChecke = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String geteName() {
        return eName;
    }

    public void seteName(String eName) {
        this.eName = eName;
    }

    public String geteCode() {
        return eCode;
    }

    public void seteCode(String eCode) {
        this.eCode = eCode;
    }

    public String geteLetter() {
        return eLetter;
    }

    public void seteLetter(String eLetter) {
        this.eLetter = eLetter;
    }

    public String geteOrder() {
        return eOrder;
    }

    public void seteOrder(String eOrder) {
        this.eOrder = eOrder;
    }

    public String geteUrl() {
        return eUrl;
    }

    public void seteUrl(String eUrl) {
        this.eUrl = eUrl;
    }

    public String geteZtState() {
        return eZtState;
    }

    public void seteZtState(String eZtState) {
        this.eZtState = eZtState;
    }

    public String getIsChecke() {
        return isChecke;
    }

    public void setIsChecke(String isChecke) {
        this.isChecke = isChecke;
    }

}
