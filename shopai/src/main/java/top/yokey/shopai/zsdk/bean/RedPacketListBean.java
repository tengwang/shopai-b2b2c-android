package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class RedPacketListBean implements Serializable {

    @SerializedName("id")
    private String id = "";
    @SerializedName("packet_id")
    private String packetId = "";
    @SerializedName("packet_name")
    private String packetName = "";
    @SerializedName("member_id")
    private String memberId = "";
    @SerializedName("member_name")
    private String memberName = "";
    @SerializedName("packet_price")
    private String packetPrice = "";
    @SerializedName("add_time")
    private String addTime = "";
    @SerializedName("valid_date")
    private String validDate = "";
    @SerializedName("is_use")
    private String isUse = "";
    @SerializedName("use_time")
    private String useTime = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPacketId() {
        return packetId;
    }

    public void setPacketId(String packetId) {
        this.packetId = packetId;
    }

    public String getPacketName() {
        return packetName;
    }

    public void setPacketName(String packetName) {
        this.packetName = packetName;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getPacketPrice() {
        return packetPrice;
    }

    public void setPacketPrice(String packetPrice) {
        this.packetPrice = packetPrice;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getValidDate() {
        return validDate;
    }

    public void setValidDate(String validDate) {
        this.validDate = validDate;
    }

    public String getIsUse() {
        return isUse;
    }

    public void setIsUse(String isUse) {
        this.isUse = isUse;
    }

    public String getUseTime() {
        return useTime;
    }

    public void setUseTime(String useTime) {
        this.useTime = useTime;
    }

}
