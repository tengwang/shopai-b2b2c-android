package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.DistribuBillListBean;
import top.yokey.shopai.zsdk.bean.GoodsFXAddBean;
import top.yokey.shopai.zsdk.bean.GoodsFXListBean;
import top.yokey.shopai.zsdk.bean.MemberFXAddBean;
import top.yokey.shopai.zsdk.bean.MemberFXAssetBean;
import top.yokey.shopai.zsdk.bean.MemberFXCashBean;
import top.yokey.shopai.zsdk.bean.MemberFXCashListBean;
import top.yokey.shopai.zsdk.bean.MemberFXDepositListBean;
import top.yokey.shopai.zsdk.bean.MemberFXIndexBean;
import top.yokey.shopai.zsdk.bean.OrderFXListBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.data.DistribuAccountData;
import top.yokey.shopai.zsdk.data.GoodsDistribuData;

@SuppressWarnings("ALL")
public class MemberFXController {

    private static final String ACT = "member_fx";

    public static void index(HttpCallBack<MemberFXIndexBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "index")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "member_info");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(data, MemberFXIndexBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void myAsset(HttpCallBack<MemberFXAssetBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "my_asset")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), MemberFXAssetBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void dropGoods(String fxId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "drop_goods")
                .add("fx_id", fxId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void fxAdd(String id, HttpCallBack<MemberFXAddBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "fx_add")
                .add("id", id)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "data");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(data, MemberFXAddBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void fxGoods(String page, HttpCallBack<ArrayList<GoodsFXListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "fx_goods")
                .add("curpage", page)
                .add("page", ShopAISdk.get().getPageNumber())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "goods_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, GoodsFXListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void goodsList(GoodsDistribuData data, HttpCallBack<ArrayList<GoodsFXAddBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "goods_list")
                .add("page", ShopAISdk.get().getPageNumber())
                .add("sort", data.getSort())
                .add("order", data.getOrder())
                .add("gc_id", data.getGcId())
                .add("keyword", data.getKeyword())
                .add("price_from", data.getPriceFrom())
                .add("price_to", data.getPriceTo())
                .add("own_shop", data.getOwnShop())
                .add("ci", data.getCi())
                .add("curpage", data.getPage())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "goods_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, GoodsFXAddBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void fxOrder(String type, String key, String page, HttpCallBack<ArrayList<OrderFXListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "fx_order")
                .add("state_type", type)
                .add("order_key", key)
                .add("curpage", page)
                .add("page", ShopAISdk.get().getPageNumber())
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "order_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, OrderFXListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void fxBill(String state, String key, String page, HttpCallBack<ArrayList<DistribuBillListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "fx_bill")
                .add("bill_state", state)
                .add("goods_name", key)
                .add("curpage", page)
                .add("page", ShopAISdk.get().getPageNumber())
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "bill_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, DistribuBillListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void saveMember(DistribuAccountData data, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "save_member")
                .add("bill_user_name", data.getBillUserName())
                .add("bill_type_number", data.getBillTypeNumber())
                .add("bill_bank_name", data.getBillBankName())
                .add("bill_type_code", data.getBillTypeCode())
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void commissionInfo(String page, HttpCallBack<ArrayList<MemberFXDepositListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "commission_info")
                .add("curpage", page)
                .add("page", ShopAISdk.get().getPageNumber())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, MemberFXDepositListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void cashApply(String amount, String pwd, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "cash_apply")
                .add("tradc_amount", amount)
                .add("pay_pwd", pwd)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void fxCash(String page, HttpCallBack<ArrayList<MemberFXCashListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "fx_cash")
                .add("curpage", page)
                .add("page", ShopAISdk.get().getPageNumber())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "cash_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, MemberFXCashListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void cashInfo(String id, HttpCallBack<MemberFXCashBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "cash_info")
                .add("tradc_id", id)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), MemberFXCashBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
