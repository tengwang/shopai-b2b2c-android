package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class ChatBean implements Serializable {

    @SerializedName("m_id")
    private String mId;
    @SerializedName("f_id")
    private String fId;
    @SerializedName("f_name")
    private String fName;
    @SerializedName("f_ip")
    private String fIp;
    @SerializedName("t_id")
    private String tId;
    @SerializedName("t_name")
    private String tName;
    @SerializedName("t_msg")
    private String tMsg;
    @SerializedName("add_time")
    private String addTime;
    @SerializedName("time")
    private String time;

    public String getMId() {
        return mId;
    }

    public void setMId(String mId) {
        this.mId = mId;
    }

    public String getFId() {
        return fId;
    }

    public void setFId(String fId) {
        this.fId = fId;
    }

    public String getFName() {
        return fName;
    }

    public void setFName(String fName) {
        this.fName = fName;
    }

    public String getFIp() {
        return fIp;
    }

    public void setFIp(String fIp) {
        this.fIp = fIp;
    }

    public String getTId() {
        return tId;
    }

    public void setTId(String tId) {
        this.tId = tId;
    }

    public String getTName() {
        return tName;
    }

    public void setTName(String tName) {
        this.tName = tName;
    }

    public String getTMsg() {
        return tMsg;
    }

    public void setTMsg(String tMsg) {
        this.tMsg = tMsg;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
