package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class StoreFavoriteBean implements Serializable {

    @SerializedName("store_id")
    private String storeId = "";
    @SerializedName("store_name")
    private String storeName = "";
    @SerializedName("fav_time")
    private String favTime = "";
    @SerializedName("fav_time_text")
    private String favTimeText = "";
    @SerializedName("goods_count")
    private String goodsCount = "";
    @SerializedName("store_collect")
    private String storeCollect = "";
    @SerializedName("store_avatar")
    private String storeAvatar = "";
    @SerializedName("store_avatar_url")
    private String storeAvatarUrl = "";

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getFavTime() {
        return favTime;
    }

    public void setFavTime(String favTime) {
        this.favTime = favTime;
    }

    public String getFavTimeText() {
        return favTimeText;
    }

    public void setFavTimeText(String favTimeText) {
        this.favTimeText = favTimeText;
    }

    public String getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(String goodsCount) {
        this.goodsCount = goodsCount;
    }

    public String getStoreCollect() {
        return storeCollect;
    }

    public void setStoreCollect(String storeCollect) {
        this.storeCollect = storeCollect;
    }

    public String getStoreAvatar() {
        return storeAvatar;
    }

    public void setStoreAvatar(String storeAvatar) {
        this.storeAvatar = storeAvatar;
    }

    public String getStoreAvatarUrl() {
        return storeAvatarUrl;
    }

    public void setStoreAvatarUrl(String storeAvatarUrl) {
        this.storeAvatarUrl = storeAvatarUrl;
    }

}
