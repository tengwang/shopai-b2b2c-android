package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class GoodsXianshiBean implements Serializable {

    @SerializedName("xianshi_goods_id")
    private String xianshiGoodsId = "";
    @SerializedName("xianshi_id")
    private String xianshiId = "";
    @SerializedName("xianshi_name")
    private String xianshiName = "";
    @SerializedName("xianshi_title")
    private String xianshiTitle = "";
    @SerializedName("xianshi_explain")
    private String xianshiExplain = "";
    @SerializedName("goods_id")
    private String goodsId = "";
    @SerializedName("store_id")
    private String storeId = "";
    @SerializedName("goods_name")
    private String goodsName = "";
    @SerializedName("goods_price")
    private String goodsPrice = "";
    @SerializedName("xianshi_price")
    private String xianshiPrice = "";
    @SerializedName("goods_image")
    private String goodsImage = "";
    @SerializedName("start_time")
    private String startTime = "";
    @SerializedName("end_time")
    private String endTime = "";
    @SerializedName("lower_limit")
    private String lowerLimit = "";
    @SerializedName("state")
    private String state = "";
    @SerializedName("xianshi_recommend")
    private String xianshiRecommend = "";
    @SerializedName("gc_id_1")
    private String gcId1 = "";
    @SerializedName("goods_url")
    private String goodsUrl = "";
    @SerializedName("image_url")
    private String imageUrl = "";
    @SerializedName("xianshi_discount")
    private String xianshiDiscount = "";
    @SerializedName("image_url_240")
    private String imageUrl240 = "";
    @SerializedName("down_price")
    private String downPrice = "";
    @SerializedName("endtime")
    private String endtime = "";

    public String getXianshiGoodsId() {
        return xianshiGoodsId;
    }

    public void setXianshiGoodsId(String xianshiGoodsId) {
        this.xianshiGoodsId = xianshiGoodsId;
    }

    public String getXianshiId() {
        return xianshiId;
    }

    public void setXianshiId(String xianshiId) {
        this.xianshiId = xianshiId;
    }

    public String getXianshiName() {
        return xianshiName;
    }

    public void setXianshiName(String xianshiName) {
        this.xianshiName = xianshiName;
    }

    public String getXianshiTitle() {
        return xianshiTitle;
    }

    public void setXianshiTitle(String xianshiTitle) {
        this.xianshiTitle = xianshiTitle;
    }

    public String getXianshiExplain() {
        return xianshiExplain;
    }

    public void setXianshiExplain(String xianshiExplain) {
        this.xianshiExplain = xianshiExplain;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(String goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public String getXianshiPrice() {
        return xianshiPrice;
    }

    public void setXianshiPrice(String xianshiPrice) {
        this.xianshiPrice = xianshiPrice;
    }

    public String getGoodsImage() {
        return goodsImage;
    }

    public void setGoodsImage(String goodsImage) {
        this.goodsImage = goodsImage;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(String lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getXianshiRecommend() {
        return xianshiRecommend;
    }

    public void setXianshiRecommend(String xianshiRecommend) {
        this.xianshiRecommend = xianshiRecommend;
    }

    public String getGcId1() {
        return gcId1;
    }

    public void setGcId1(String gcId1) {
        this.gcId1 = gcId1;
    }

    public String getGoodsUrl() {
        return goodsUrl;
    }

    public void setGoodsUrl(String goodsUrl) {
        this.goodsUrl = goodsUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getXianshiDiscount() {
        return xianshiDiscount;
    }

    public void setXianshiDiscount(String xianshiDiscount) {
        this.xianshiDiscount = xianshiDiscount;
    }

    public String getImageUrl240() {
        return imageUrl240;
    }

    public void setImageUrl240(String imageUrl240) {
        this.imageUrl240 = imageUrl240;
    }

    public String getDownPrice() {
        return downPrice;
    }

    public void setDownPrice(String downPrice) {
        this.downPrice = downPrice;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

}
