package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class PointLogBean implements Serializable {

    @SerializedName("pl_id")
    private String plId = "";
    @SerializedName("pl_memberid")
    private String plMemberid = "";
    @SerializedName("pl_membername")
    private String plMembername = "";
    @SerializedName("pl_adminid")
    private String plAdminid = "";
    @SerializedName("pl_adminname")
    private String plAdminname = "";
    @SerializedName("pl_points")
    private String plPoints = "";
    @SerializedName("pl_addtime")
    private String plAddtime = "";
    @SerializedName("pl_desc")
    private String plDesc = "";
    @SerializedName("pl_stage")
    private String plStage = "";
    @SerializedName("stagetext")
    private String stagetext = "";
    @SerializedName("addtimetext")
    private String addtimetext = "";

    public String getPlId() {
        return plId;
    }

    public void setPlId(String plId) {
        this.plId = plId;
    }

    public String getPlMemberid() {
        return plMemberid;
    }

    public void setPlMemberid(String plMemberid) {
        this.plMemberid = plMemberid;
    }

    public String getPlMembername() {
        return plMembername;
    }

    public void setPlMembername(String plMembername) {
        this.plMembername = plMembername;
    }

    public String getPlAdminid() {
        return plAdminid;
    }

    public void setPlAdminid(String plAdminid) {
        this.plAdminid = plAdminid;
    }

    public String getPlAdminname() {
        return plAdminname;
    }

    public void setPlAdminname(String plAdminname) {
        this.plAdminname = plAdminname;
    }

    public String getPlPoints() {
        return plPoints;
    }

    public void setPlPoints(String plPoints) {
        this.plPoints = plPoints;
    }

    public String getPlAddtime() {
        return plAddtime;
    }

    public void setPlAddtime(String plAddtime) {
        this.plAddtime = plAddtime;
    }

    public String getPlDesc() {
        return plDesc;
    }

    public void setPlDesc(String plDesc) {
        this.plDesc = plDesc;
    }

    public String getPlStage() {
        return plStage;
    }

    public void setPlStage(String plStage) {
        this.plStage = plStage;
    }

    public String getStagetext() {
        return stagetext;
    }

    public void setStagetext(String stagetext) {
        this.stagetext = stagetext;
    }

    public String getAddtimetext() {
        return addtimetext;
    }

    public void setAddtimetext(String addtimetext) {
        this.addtimetext = addtimetext;
    }

}
