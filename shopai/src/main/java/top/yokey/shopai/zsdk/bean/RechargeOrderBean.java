package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class RechargeOrderBean implements Serializable {

    @SerializedName("pdinfo")
    private PdinfoBean pdinfo = null;
    @SerializedName("payment_list")
    private ArrayList<PaymentListBean> paymentList = new ArrayList<>();

    public PdinfoBean getPdinfo() {
        return pdinfo;
    }

    public void setPdinfo(PdinfoBean pdinfo) {
        this.pdinfo = pdinfo;
    }

    public ArrayList<PaymentListBean> getPaymentList() {
        return paymentList;
    }

    public void setPaymentList(ArrayList<PaymentListBean> paymentList) {
        this.paymentList = paymentList;
    }

    public static class PdinfoBean {

        @SerializedName("pdr_id")
        private String pdrId = "";
        @SerializedName("pdr_sn")
        private String pdrSn = "";
        @SerializedName("pdr_member_id")
        private String pdrMemberId = "";
        @SerializedName("pdr_member_name")
        private String pdrMemberName = "";
        @SerializedName("pdr_amount")
        private String pdrAmount = "";
        @SerializedName("pdr_add_time")
        private String pdrAddTime = "";

        public String getPdrId() {
            return pdrId;
        }

        public void setPdrId(String pdrId) {
            this.pdrId = pdrId;
        }

        public String getPdrSn() {
            return pdrSn;
        }

        public void setPdrSn(String pdrSn) {
            this.pdrSn = pdrSn;
        }

        public String getPdrMemberId() {
            return pdrMemberId;
        }

        public void setPdrMemberId(String pdrMemberId) {
            this.pdrMemberId = pdrMemberId;
        }

        public String getPdrMemberName() {
            return pdrMemberName;
        }

        public void setPdrMemberName(String pdrMemberName) {
            this.pdrMemberName = pdrMemberName;
        }

        public String getPdrAmount() {
            return pdrAmount;
        }

        public void setPdrAmount(String pdrAmount) {
            this.pdrAmount = pdrAmount;
        }

        public String getPdrAddTime() {
            return pdrAddTime;
        }

        public void setPdrAddTime(String pdrAddTime) {
            this.pdrAddTime = pdrAddTime;
        }
    }

    public static class PaymentListBean {

        @SerializedName("payment_code")
        private String paymentCode = "";
        @SerializedName("payment_name")
        private String paymentName = "";

        public String getPaymentCode() {
            return paymentCode;
        }

        public void setPaymentCode(String paymentCode) {
            this.paymentCode = paymentCode;
        }

        public String getPaymentName() {
            return paymentName;
        }

        public void setPaymentName(String paymentName) {
            this.paymentName = paymentName;
        }

    }

}
