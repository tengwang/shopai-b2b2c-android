package top.yokey.shopai.zsdk.controller;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.OrderPayBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.data.GoodsBuyData;

@SuppressWarnings("ALL")
public class MemberBuyController {

    private static final String ACT = "member_buy";

    public static void pay(String paySn, HttpCallBack<OrderPayBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "pay")
                .add("pay_sn", paySn)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "pay_info");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(data, OrderPayBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void buyStep1(GoodsBuyData data, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "buy_step1")
                .add("cart_id", data.getCartId())
                .add("ifcart", data.getIfcart())
                .add("address_id", data.getAddressId())
                .add("pingou", data.getPingou())
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void buyStep2(GoodsBuyData data, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "buy_step2")
                .add("ifcart", data.getIfcart())
                .add("cart_id", data.getCartId())
                .add("address_id", data.getAddressId())
                .add("pingou", data.getPingou())
                .add("log_id", data.getLogId())
                .add("buyer_id", data.getBuyerId())
                .add("vat_hash", data.getVatHash())
                .add("offpay_hash", data.getOffpayHash())
                .add("offpay_hash_batch", data.getOffpayHashBatch())
                .add("pay_name", data.getPayName())
                .add("invoice_id", data.getInvoiceId())
                .add("voucher", data.getVoucher())
                .add("pd_pay", data.getPdPay())
                .add("password", data.getPassword())
                .add("fcode", data.getFcode())
                .add("rcb_pay", data.getRcbPay())
                .add("rpt", data.getRpt())
                .add("pay_message", data.getPayMessage())
                .add("J_PointInput", data.getjPointInput())
                .add("isPoint", data.getIsPoint())
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void checkPdPwd(String password, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "check_pd_pwd")
                .add("password", password)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
