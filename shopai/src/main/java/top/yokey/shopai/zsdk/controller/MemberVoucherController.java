package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.VoucherBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class MemberVoucherController {

    private static final String ACT = "member_voucher";

    public static void voucherFreeex(String tid, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "voucher_freeex")
                .add("tid", tid)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void voucherList(HttpCallBack<ArrayList<VoucherBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "voucher_list")
                .add("page", "999")
                .add("curpage", "1")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "voucher_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, VoucherBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void voucherPwex(String pwdCode, String captcha, String codeKey, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "voucher_pwex")
                .add("pwd_code", pwdCode)
                .add("captcha", captcha)
                .add("codekey", codeKey)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
