package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("All")
public class OrderEvaluateAgainBean implements Serializable {

    @SerializedName("store_info")
    private StoreInfoBean storeInfo = null;
    @SerializedName("evaluate_goods")
    private ArrayList<EvaluateGoodsBean> evaluateGoods = new ArrayList<>();

    public StoreInfoBean getStoreInfo() {
        return storeInfo;
    }

    public void setStoreInfo(StoreInfoBean storeInfo) {
        this.storeInfo = storeInfo;
    }

    public ArrayList<EvaluateGoodsBean> getEvaluateGoods() {
        return evaluateGoods;
    }

    public void setEvaluateGoods(ArrayList<EvaluateGoodsBean> evaluateGoods) {
        this.evaluateGoods = evaluateGoods;
    }

    public static class StoreInfoBean {

        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("store_name")
        private String storeName = "";
        @SerializedName("is_own_shop")
        private String isOwnShop = "";

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getIsOwnShop() {
            return isOwnShop;
        }

        public void setIsOwnShop(String isOwnShop) {
            this.isOwnShop = isOwnShop;
        }

    }

    public static class EvaluateGoodsBean {

        @SerializedName("geval_id")
        private String gevalId = "";
        @SerializedName("geval_orderid")
        private String gevalOrderid = "";
        @SerializedName("geval_orderno")
        private String gevalOrderno = "";
        @SerializedName("geval_ordergoodsid")
        private String gevalOrdergoodsid = "";
        @SerializedName("geval_goodsid")
        private String gevalGoodsid = "";
        @SerializedName("geval_goodsname")
        private String gevalGoodsname = "";
        @SerializedName("geval_goodsprice")
        private String gevalGoodsprice = "";
        @SerializedName("geval_goodsimage")
        private String gevalGoodsimage = "";
        @SerializedName("geval_scores")
        private String gevalScores = "";
        @SerializedName("geval_content")
        private String gevalContent = "";
        @SerializedName("geval_isanonymous")
        private String gevalIsanonymous = "";
        @SerializedName("geval_addtime")
        private String gevalAddtime = "";
        @SerializedName("geval_storeid")
        private String gevalStoreid = "";
        @SerializedName("geval_storename")
        private String gevalStorename = "";
        @SerializedName("geval_frommemberid")
        private String gevalFrommemberid = "";
        @SerializedName("geval_frommembername")
        private String gevalFrommembername = "";
        @SerializedName("geval_explain")
        private String gevalExplain = "";
        @SerializedName("geval_image")
        private String gevalImage = "";
        @SerializedName("geval_content_again")
        private String gevalContentAgain = "";
        @SerializedName("geval_addtime_again")
        private String gevalAddtimeAgain = "";
        @SerializedName("geval_image_again")
        private String gevalImageAgain = "";
        @SerializedName("geval_explain_again")
        private String gevalExplainAgain = "";

        private String evaluateContent = "";
        private String evaluateImage0 = "";
        private String evaluateImage1 = "";
        private String evaluateImage2 = "";
        private String evaluateImage3 = "";
        private String evaluateImage4 = "";
        private String evaluateImage0Name = "";
        private String evaluateImage1Name = "";
        private String evaluateImage2Name = "";
        private String evaluateImage3Name = "";
        private String evaluateImage4Name = "";

        public String getGevalId() {
            return gevalId;
        }

        public void setGevalId(String gevalId) {
            this.gevalId = gevalId;
        }

        public String getGevalOrderid() {
            return gevalOrderid;
        }

        public void setGevalOrderid(String gevalOrderid) {
            this.gevalOrderid = gevalOrderid;
        }

        public String getGevalOrderno() {
            return gevalOrderno;
        }

        public void setGevalOrderno(String gevalOrderno) {
            this.gevalOrderno = gevalOrderno;
        }

        public String getGevalOrdergoodsid() {
            return gevalOrdergoodsid;
        }

        public void setGevalOrdergoodsid(String gevalOrdergoodsid) {
            this.gevalOrdergoodsid = gevalOrdergoodsid;
        }

        public String getGevalGoodsid() {
            return gevalGoodsid;
        }

        public void setGevalGoodsid(String gevalGoodsid) {
            this.gevalGoodsid = gevalGoodsid;
        }

        public String getGevalGoodsname() {
            return gevalGoodsname;
        }

        public void setGevalGoodsname(String gevalGoodsname) {
            this.gevalGoodsname = gevalGoodsname;
        }

        public String getGevalGoodsprice() {
            return gevalGoodsprice;
        }

        public void setGevalGoodsprice(String gevalGoodsprice) {
            this.gevalGoodsprice = gevalGoodsprice;
        }

        public String getGevalGoodsimage() {
            return gevalGoodsimage;
        }

        public void setGevalGoodsimage(String gevalGoodsimage) {
            this.gevalGoodsimage = gevalGoodsimage;
        }

        public String getGevalScores() {
            return gevalScores;
        }

        public void setGevalScores(String gevalScores) {
            this.gevalScores = gevalScores;
        }

        public String getGevalContent() {
            return gevalContent;
        }

        public void setGevalContent(String gevalContent) {
            this.gevalContent = gevalContent;
        }

        public String getGevalIsanonymous() {
            return gevalIsanonymous;
        }

        public void setGevalIsanonymous(String gevalIsanonymous) {
            this.gevalIsanonymous = gevalIsanonymous;
        }

        public String getGevalAddtime() {
            return gevalAddtime;
        }

        public void setGevalAddtime(String gevalAddtime) {
            this.gevalAddtime = gevalAddtime;
        }

        public String getGevalStoreid() {
            return gevalStoreid;
        }

        public void setGevalStoreid(String gevalStoreid) {
            this.gevalStoreid = gevalStoreid;
        }

        public String getGevalStorename() {
            return gevalStorename;
        }

        public void setGevalStorename(String gevalStorename) {
            this.gevalStorename = gevalStorename;
        }

        public String getGevalFrommemberid() {
            return gevalFrommemberid;
        }

        public void setGevalFrommemberid(String gevalFrommemberid) {
            this.gevalFrommemberid = gevalFrommemberid;
        }

        public String getGevalFrommembername() {
            return gevalFrommembername;
        }

        public void setGevalFrommembername(String gevalFrommembername) {
            this.gevalFrommembername = gevalFrommembername;
        }

        public String getGevalExplain() {
            return gevalExplain;
        }

        public void setGevalExplain(String gevalExplain) {
            this.gevalExplain = gevalExplain;
        }

        public String getGevalImage() {
            return gevalImage;
        }

        public void setGevalImage(String gevalImage) {
            this.gevalImage = gevalImage;
        }

        public String getGevalContentAgain() {
            return gevalContentAgain;
        }

        public void setGevalContentAgain(String gevalContentAgain) {
            this.gevalContentAgain = gevalContentAgain;
        }

        public String getGevalAddtimeAgain() {
            return gevalAddtimeAgain;
        }

        public void setGevalAddtimeAgain(String gevalAddtimeAgain) {
            this.gevalAddtimeAgain = gevalAddtimeAgain;
        }

        public String getGevalImageAgain() {
            return gevalImageAgain;
        }

        public void setGevalImageAgain(String gevalImageAgain) {
            this.gevalImageAgain = gevalImageAgain;
        }

        public String getGevalExplainAgain() {
            return gevalExplainAgain;
        }

        public void setGevalExplainAgain(String gevalExplainAgain) {
            this.gevalExplainAgain = gevalExplainAgain;
        }

        public String getEvaluateContent() {
            return evaluateContent;
        }

        public void setEvaluateContent(String evaluateContent) {
            this.evaluateContent = evaluateContent;
        }

        public String getEvaluateImage0() {
            return evaluateImage0;
        }

        public void setEvaluateImage0(String evaluateImage0) {
            this.evaluateImage0 = evaluateImage0;
        }

        public String getEvaluateImage1() {
            return evaluateImage1;
        }

        public void setEvaluateImage1(String evaluateImage1) {
            this.evaluateImage1 = evaluateImage1;
        }

        public String getEvaluateImage2() {
            return evaluateImage2;
        }

        public void setEvaluateImage2(String evaluateImage2) {
            this.evaluateImage2 = evaluateImage2;
        }

        public String getEvaluateImage3() {
            return evaluateImage3;
        }

        public void setEvaluateImage3(String evaluateImage3) {
            this.evaluateImage3 = evaluateImage3;
        }

        public String getEvaluateImage4() {
            return evaluateImage4;
        }

        public void setEvaluateImage4(String evaluateImage4) {
            this.evaluateImage4 = evaluateImage4;
        }

        public String getEvaluateImage0Name() {
            return evaluateImage0Name;
        }

        public void setEvaluateImage0Name(String evaluateImage0Name) {
            this.evaluateImage0Name = evaluateImage0Name;
        }

        public String getEvaluateImage1Name() {
            return evaluateImage1Name;
        }

        public void setEvaluateImage1Name(String evaluateImage1Name) {
            this.evaluateImage1Name = evaluateImage1Name;
        }

        public String getEvaluateImage2Name() {
            return evaluateImage2Name;
        }

        public void setEvaluateImage2Name(String evaluateImage2Name) {
            this.evaluateImage2Name = evaluateImage2Name;
        }

        public String getEvaluateImage3Name() {
            return evaluateImage3Name;
        }

        public void setEvaluateImage3Name(String evaluateImage3Name) {
            this.evaluateImage3Name = evaluateImage3Name;
        }

        public String getEvaluateImage4Name() {
            return evaluateImage4Name;
        }

        public void setEvaluateImage4Name(String evaluateImage4Name) {
            this.evaluateImage4Name = evaluateImage4Name;
        }

    }

}
