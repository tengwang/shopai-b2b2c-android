package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ALL")
public class OrderPayBean implements Serializable {

    @SerializedName("pay_amount")
    private String payAmount = "";
    @SerializedName("member_available_pd")
    private String memberAvailablePd = "";
    @SerializedName("member_available_rcb")
    private String memberAvailableRcb = "";
    @SerializedName("member_paypwd")
    private boolean memberPaypwd = false;
    @SerializedName("pay_sn")
    private String paySn = "";
    @SerializedName("payed_amount")
    private String payedAmount = "";
    @SerializedName("payment_list")
    private ArrayList<PaymentListBean> paymentList;

    public String getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }

    public String getMemberAvailablePd() {
        return memberAvailablePd;
    }

    public void setMemberAvailablePd(String memberAvailablePd) {
        this.memberAvailablePd = memberAvailablePd;
    }

    public String getMemberAvailableRcb() {
        return memberAvailableRcb;
    }

    public void setMemberAvailableRcb(String memberAvailableRcb) {
        this.memberAvailableRcb = memberAvailableRcb;
    }

    public boolean isMemberPaypwd() {
        return memberPaypwd;
    }

    public void setMemberPaypwd(boolean memberPaypwd) {
        this.memberPaypwd = memberPaypwd;
    }

    public String getPaySn() {
        return paySn;
    }

    public void setPaySn(String paySn) {
        this.paySn = paySn;
    }

    public String getPayedAmount() {
        return payedAmount;
    }

    public void setPayedAmount(String payedAmount) {
        this.payedAmount = payedAmount;
    }

    public List<PaymentListBean> getPaymentList() {
        return paymentList;
    }

    public void setPaymentList(ArrayList<PaymentListBean> paymentList) {
        this.paymentList = paymentList;
    }

    public static class PaymentListBean {

        @SerializedName("payment_code")
        private String paymentCode = "";
        @SerializedName("payment_name")
        private String paymentName = "";

        public String getPaymentCode() {
            return paymentCode;
        }

        public void setPaymentCode(String paymentCode) {
            this.paymentCode = paymentCode;
        }

        public String getPaymentName() {
            return paymentName;
        }

        public void setPaymentName(String paymentName) {
            this.paymentName = paymentName;
        }

    }

}
