package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class StoreGoodsRankBean implements Serializable {

    @SerializedName("goods_id")
    private String goodsId = "";
    @SerializedName("store_id")
    private String storeId = "";
    @SerializedName("store_name")
    private String storeName = "";
    @SerializedName("goods_name")
    private String goodsName = "";
    @SerializedName("goods_price")
    private String goodsPrice = "";
    @SerializedName("goods_marketprice")
    private String goodsMarketprice = "";
    @SerializedName("goods_image")
    private String goodsImage = "";
    @SerializedName("goods_salenum")
    private String goodsSalenum = "";
    @SerializedName("evaluation_good_star")
    private String evaluationGoodStar = "";
    @SerializedName("evaluation_count")
    private String evaluationCount = "";
    @SerializedName("is_virtual")
    private String isVirtual = "";
    @SerializedName("is_presell")
    private String isPresell = "";
    @SerializedName("is_fcode")
    private String isFcode = "";
    @SerializedName("have_gift")
    private String haveGift = "";
    @SerializedName("goods_addtime")
    private String goodsAddtime = "";
    @SerializedName("sole_flag")
    private boolean soleFlag = false;
    @SerializedName("group_flag")
    private boolean groupFlag = false;
    @SerializedName("xianshi_flag")
    private boolean xianshiFlag = false;
    @SerializedName("goods_image_url")
    private String goodsImageUrl = "";

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(String goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public String getGoodsMarketprice() {
        return goodsMarketprice;
    }

    public void setGoodsMarketprice(String goodsMarketprice) {
        this.goodsMarketprice = goodsMarketprice;
    }

    public String getGoodsImage() {
        return goodsImage;
    }

    public void setGoodsImage(String goodsImage) {
        this.goodsImage = goodsImage;
    }

    public String getGoodsSalenum() {
        return goodsSalenum;
    }

    public void setGoodsSalenum(String goodsSalenum) {
        this.goodsSalenum = goodsSalenum;
    }

    public String getEvaluationGoodStar() {
        return evaluationGoodStar;
    }

    public void setEvaluationGoodStar(String evaluationGoodStar) {
        this.evaluationGoodStar = evaluationGoodStar;
    }

    public String getEvaluationCount() {
        return evaluationCount;
    }

    public void setEvaluationCount(String evaluationCount) {
        this.evaluationCount = evaluationCount;
    }

    public String getIsVirtual() {
        return isVirtual;
    }

    public void setIsVirtual(String isVirtual) {
        this.isVirtual = isVirtual;
    }

    public String getIsPresell() {
        return isPresell;
    }

    public void setIsPresell(String isPresell) {
        this.isPresell = isPresell;
    }

    public String getIsFcode() {
        return isFcode;
    }

    public void setIsFcode(String isFcode) {
        this.isFcode = isFcode;
    }

    public String getHaveGift() {
        return haveGift;
    }

    public void setHaveGift(String haveGift) {
        this.haveGift = haveGift;
    }

    public String getGoodsAddtime() {
        return goodsAddtime;
    }

    public void setGoodsAddtime(String goodsAddtime) {
        this.goodsAddtime = goodsAddtime;
    }

    public boolean isSoleFlag() {
        return soleFlag;
    }

    public void setSoleFlag(boolean soleFlag) {
        this.soleFlag = soleFlag;
    }

    public boolean isGroupFlag() {
        return groupFlag;
    }

    public void setGroupFlag(boolean groupFlag) {
        this.groupFlag = groupFlag;
    }

    public boolean isXianshiFlag() {
        return xianshiFlag;
    }

    public void setXianshiFlag(boolean xianshiFlag) {
        this.xianshiFlag = xianshiFlag;
    }

    public String getGoodsImageUrl() {
        return goodsImageUrl;
    }

    public void setGoodsImageUrl(String goodsImageUrl) {
        this.goodsImageUrl = goodsImageUrl;
    }

}
