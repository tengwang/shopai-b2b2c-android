package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class RetreatRefundAllBean implements Serializable {

    @SerializedName("order")
    private OrderBean order = null;
    @SerializedName("goods")
    private GoodsBean goods = null;
    @SerializedName("goods_list")
    private ArrayList<GoodsListBean> goodsList = new ArrayList<>();
    @SerializedName("gift_list")
    private ArrayList<GiftListBean> giftList = new ArrayList<>();
    @SerializedName("reason_list")
    private ArrayList<ReasonList> reasonList = new ArrayList<>();

    public OrderBean getOrder() {
        return order;
    }

    public void setOrder(OrderBean order) {
        this.order = order;
    }

    public ArrayList<GoodsListBean> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(ArrayList<GoodsListBean> goodsList) {
        this.goodsList = goodsList;
    }

    public ArrayList<GiftListBean> getGiftList() {
        return giftList;
    }

    public void setGiftList(ArrayList<GiftListBean> giftList) {
        this.giftList = giftList;
    }

    public GoodsBean getGoods() {
        return goods;
    }

    public void setGoods(GoodsBean goods) {
        this.goods = goods;
    }

    public ArrayList<ReasonList> getReasonList() {
        return reasonList;
    }

    public void setReasonList(ArrayList<ReasonList> reasonList) {
        this.reasonList = reasonList;
    }

    public static class OrderBean {

        @SerializedName("order_id")
        private String orderId = "";
        @SerializedName("order_type")
        private String orderType = "";
        @SerializedName("order_amount")
        private String orderAmount = "";
        @SerializedName("order_sn")
        private String orderSn = "";
        @SerializedName("store_name")
        private String storeName = "";
        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("allow_refund_amount")
        private String allowRefundAmount = "";
        @SerializedName("book_amount")
        private String bookAmount = "";

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public String getOrderAmount() {
            return orderAmount;
        }

        public void setOrderAmount(String orderAmount) {
            this.orderAmount = orderAmount;
        }

        public String getOrderSn() {
            return orderSn;
        }

        public void setOrderSn(String orderSn) {
            this.orderSn = orderSn;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getAllowRefundAmount() {
            return allowRefundAmount;
        }

        public void setAllowRefundAmount(String allowRefundAmount) {
            this.allowRefundAmount = allowRefundAmount;
        }

        public String getBookAmount() {
            return bookAmount;
        }

        public void setBookAmount(String bookAmount) {
            this.bookAmount = bookAmount;
        }

    }

    public static class GoodsBean {

        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("order_goods_id")
        private String orderGoodsId = "";
        @SerializedName("goods_id")
        private String goodsId = "";
        @SerializedName("goods_name")
        private String goodsName = "";
        @SerializedName("goods_type")
        private String goodsType = "";
        @SerializedName("goods_img_360")
        private String goodsImg360 = "";
        @SerializedName("goods_price")
        private String goodsPrice = "";
        @SerializedName("goods_spec")
        private String goodsSpec = "";
        @SerializedName("goods_num")
        private String goodsNum = "";
        @SerializedName("goods_pay_price")
        private String goodsPayPrice = "";

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getOrderGoodsId() {
            return orderGoodsId;
        }

        public void setOrderGoodsId(String orderGoodsId) {
            this.orderGoodsId = orderGoodsId;
        }

        public String getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(String goodsId) {
            this.goodsId = goodsId;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public String getGoodsType() {
            return goodsType;
        }

        public void setGoodsType(String goodsType) {
            this.goodsType = goodsType;
        }

        public String getGoodsImg360() {
            return goodsImg360;
        }

        public void setGoodsImg360(String goodsImg360) {
            this.goodsImg360 = goodsImg360;
        }

        public String getGoodsPrice() {
            return goodsPrice;
        }

        public void setGoodsPrice(String goodsPrice) {
            this.goodsPrice = goodsPrice;
        }

        public String getGoodsSpec() {
            return goodsSpec;
        }

        public void setGoodsSpec(String goodsSpec) {
            this.goodsSpec = goodsSpec;
        }

        public String getGoodsNum() {
            return goodsNum;
        }

        public void setGoodsNum(String goodsNum) {
            this.goodsNum = goodsNum;
        }

        public String getGoodsPayPrice() {
            return goodsPayPrice;
        }

        public void setGoodsPayPrice(String goodsPayPrice) {
            this.goodsPayPrice = goodsPayPrice;
        }

    }

    public static class GoodsListBean {

        @SerializedName("goods_id")
        private String goodsId = "";
        @SerializedName("goods_name")
        private String goodsName = "";
        @SerializedName("goods_price")
        private String goodsPrice = "";
        @SerializedName("goods_num")
        private String goodsNum = "";
        @SerializedName("goods_spec")
        private String goodsSpec = "";
        @SerializedName("goods_img_360")
        private String goodsImg360 = "";
        @SerializedName("goods_type")
        private String goodsType = "";

        public String getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(String goodsId) {
            this.goodsId = goodsId;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public String getGoodsPrice() {
            return goodsPrice;
        }

        public void setGoodsPrice(String goodsPrice) {
            this.goodsPrice = goodsPrice;
        }

        public String getGoodsNum() {
            return goodsNum;
        }

        public void setGoodsNum(String goodsNum) {
            this.goodsNum = goodsNum;
        }

        public String getGoodsSpec() {
            return goodsSpec;
        }

        public void setGoodsSpec(String goodsSpec) {
            this.goodsSpec = goodsSpec;
        }

        public String getGoodsImg360() {
            return goodsImg360;
        }

        public void setGoodsImg360(String goodsImg360) {
            this.goodsImg360 = goodsImg360;
        }

        public String getGoodsType() {
            return goodsType;
        }

        public void setGoodsType(String goodsType) {
            this.goodsType = goodsType;
        }

    }

    public static class GiftListBean {

        @SerializedName("goods_id")
        private String goodsId = "";
        @SerializedName("goods_name")
        private String goodsName = "";
        @SerializedName("goods_price")
        private String goodsPrice = "";
        @SerializedName("goods_num")
        private String goodsNum = "";
        @SerializedName("goods_spec")
        private String goodsSpec = "";
        @SerializedName("goods_img_360")
        private String goodsImg360 = "";
        @SerializedName("goods_type")
        private String goodsType = "";

        public String getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(String goodsId) {
            this.goodsId = goodsId;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public String getGoodsPrice() {
            return goodsPrice;
        }

        public void setGoodsPrice(String goodsPrice) {
            this.goodsPrice = goodsPrice;
        }

        public String getGoodsNum() {
            return goodsNum;
        }

        public void setGoodsNum(String goodsNum) {
            this.goodsNum = goodsNum;
        }

        public String getGoodsSpec() {
            return goodsSpec;
        }

        public void setGoodsSpec(String goodsSpec) {
            this.goodsSpec = goodsSpec;
        }

        public String getGoodsImg360() {
            return goodsImg360;
        }

        public void setGoodsImg360(String goodsImg360) {
            this.goodsImg360 = goodsImg360;
        }

        public String getGoodsType() {
            return goodsType;
        }

        public void setGoodsType(String goodsType) {
            this.goodsType = goodsType;
        }

    }

    public static class ReasonList {

        @SerializedName("reason_id")
        private String reasonId = "";
        @SerializedName("reason_info")
        private String reasonInfo = "";

        public String getReasonId() {
            return reasonId;
        }

        public void setReasonId(String reasonId) {
            this.reasonId = reasonId;
        }

        public String getReasonInfo() {
            return reasonInfo;
        }

        public void setReasonInfo(String reasonInfo) {
            this.reasonInfo = reasonInfo;
        }

    }

}
