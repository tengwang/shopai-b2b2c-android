package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class MemberFXCashBean implements Serializable {

    @SerializedName("tradc_id")
    private String tradcId = "";
    @SerializedName("tradc_sn")
    private String tradcSn = "";
    @SerializedName("tradc_member_id")
    private String tradcMemberId = "";
    @SerializedName("tradc_member_name")
    private String tradcMemberName = "";
    @SerializedName("tradc_amount")
    private String tradcAmount = "";
    @SerializedName("tradc_bank_name")
    private String tradcBankName = "";
    @SerializedName("tradc_bank_no")
    private String tradcBankNo = "";
    @SerializedName("tradc_bank_user")
    private String tradcBankUser = "";
    @SerializedName("tradc_add_time")
    private String tradcAddTime = "";
    @SerializedName("tradc_payment_time")
    private String tradcPaymentTime = "";
    @SerializedName("tradc_payment_state")
    private String tradcPaymentState = "";
    @SerializedName("tradc_payment_admin")
    private String tradcPaymentAdmin = "";
    @SerializedName("tradc_add_time_text")
    private String tradcAddTimeText = "";
    @SerializedName("tradc_payment_state_txt")
    private String tradcPaymentStateTxt = "";
    @SerializedName("tradc_payment_time_text")
    private String tradcPaymentTimeText = "";

    public String getTradcId() {
        return tradcId;
    }

    public void setTradcId(String tradcId) {
        this.tradcId = tradcId;
    }

    public String getTradcSn() {
        return tradcSn;
    }

    public void setTradcSn(String tradcSn) {
        this.tradcSn = tradcSn;
    }

    public String getTradcMemberId() {
        return tradcMemberId;
    }

    public void setTradcMemberId(String tradcMemberId) {
        this.tradcMemberId = tradcMemberId;
    }

    public String getTradcMemberName() {
        return tradcMemberName;
    }

    public void setTradcMemberName(String tradcMemberName) {
        this.tradcMemberName = tradcMemberName;
    }

    public String getTradcAmount() {
        return tradcAmount;
    }

    public void setTradcAmount(String tradcAmount) {
        this.tradcAmount = tradcAmount;
    }

    public String getTradcBankName() {
        return tradcBankName;
    }

    public void setTradcBankName(String tradcBankName) {
        this.tradcBankName = tradcBankName;
    }

    public String getTradcBankNo() {
        return tradcBankNo;
    }

    public void setTradcBankNo(String tradcBankNo) {
        this.tradcBankNo = tradcBankNo;
    }

    public String getTradcBankUser() {
        return tradcBankUser;
    }

    public void setTradcBankUser(String tradcBankUser) {
        this.tradcBankUser = tradcBankUser;
    }

    public String getTradcAddTime() {
        return tradcAddTime;
    }

    public void setTradcAddTime(String tradcAddTime) {
        this.tradcAddTime = tradcAddTime;
    }

    public String getTradcPaymentTime() {
        return tradcPaymentTime;
    }

    public void setTradcPaymentTime(String tradcPaymentTime) {
        this.tradcPaymentTime = tradcPaymentTime;
    }

    public String getTradcPaymentState() {
        return tradcPaymentState;
    }

    public void setTradcPaymentState(String tradcPaymentState) {
        this.tradcPaymentState = tradcPaymentState;
    }

    public String getTradcPaymentAdmin() {
        return tradcPaymentAdmin;
    }

    public void setTradcPaymentAdmin(String tradcPaymentAdmin) {
        this.tradcPaymentAdmin = tradcPaymentAdmin;
    }

    public String getTradcAddTimeText() {
        return tradcAddTimeText;
    }

    public void setTradcAddTimeText(String tradcAddTimeText) {
        this.tradcAddTimeText = tradcAddTimeText;
    }

    public String getTradcPaymentStateTxt() {
        return tradcPaymentStateTxt;
    }

    public void setTradcPaymentStateTxt(String tradcPaymentStateTxt) {
        this.tradcPaymentStateTxt = tradcPaymentStateTxt;
    }

    public String getTradcPaymentTimeText() {
        return tradcPaymentTimeText;
    }

    public void setTradcPaymentTimeText(String tradcPaymentTimeText) {
        this.tradcPaymentTimeText = tradcPaymentTimeText;
    }

}
