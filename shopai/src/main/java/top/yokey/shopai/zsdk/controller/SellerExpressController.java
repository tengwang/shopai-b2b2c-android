package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.SellerDeliverBean;
import top.yokey.shopai.zsdk.bean.SellerExpressListBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class SellerExpressController {

    private static final String ACT = "seller_express";

    public static void saveDefault(String id, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "savedefault")
                .add("expresslists", id)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void getMylist(String orderId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "get_mylist")
                .add("order_id", orderId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void getList(HttpCallBack<ArrayList<SellerExpressListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "get_list")
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "express_array");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.object2ArrayList(data, SellerExpressListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void getDeliverExpress(String orderId, HttpCallBack<SellerDeliverBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "get_defaultexpress")
                .add("order_id", orderId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), SellerDeliverBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
