package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class MemberFXDepositListBean implements Serializable {

    @SerializedName("lg_id")
    private String lgId;
    @SerializedName("lg_member_id")
    private String lgMemberId;
    @SerializedName("lg_member_name")
    private String lgMemberName;
    @SerializedName("lg_admin_name")
    private String lgAdminName;
    @SerializedName("lg_type")
    private String lgType;
    @SerializedName("lg_av_amount")
    private String lgAvAmount;
    @SerializedName("lg_freeze_amount")
    private String lgFreezeAmount;
    @SerializedName("lg_add_time")
    private String lgAddTime;
    @SerializedName("lg_desc")
    private String lgDesc;

    public String getLgId() {
        return lgId;
    }

    public void setLgId(String lgId) {
        this.lgId = lgId;
    }

    public String getLgMemberId() {
        return lgMemberId;
    }

    public void setLgMemberId(String lgMemberId) {
        this.lgMemberId = lgMemberId;
    }

    public String getLgMemberName() {
        return lgMemberName;
    }

    public void setLgMemberName(String lgMemberName) {
        this.lgMemberName = lgMemberName;
    }

    public String getLgAdminName() {
        return lgAdminName;
    }

    public void setLgAdminName(String lgAdminName) {
        this.lgAdminName = lgAdminName;
    }

    public String getLgType() {
        return lgType;
    }

    public void setLgType(String lgType) {
        this.lgType = lgType;
    }

    public String getLgAvAmount() {
        return lgAvAmount;
    }

    public void setLgAvAmount(String lgAvAmount) {
        this.lgAvAmount = lgAvAmount;
    }

    public String getLgFreezeAmount() {
        return lgFreezeAmount;
    }

    public void setLgFreezeAmount(String lgFreezeAmount) {
        this.lgFreezeAmount = lgFreezeAmount;
    }

    public String getLgAddTime() {
        return lgAddTime;
    }

    public void setLgAddTime(String lgAddTime) {
        this.lgAddTime = lgAddTime;
    }

    public String getLgDesc() {
        return lgDesc;
    }

    public void setLgDesc(String lgDesc) {
        this.lgDesc = lgDesc;
    }

}
