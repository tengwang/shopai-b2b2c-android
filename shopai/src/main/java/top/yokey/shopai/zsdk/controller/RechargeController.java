package top.yokey.shopai.zsdk.controller;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.RechargeOrderBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.data.PreDepositCashData;

@SuppressWarnings("ALL")
public class RechargeController {

    private static final String ACT = "recharge";

    public static void index(String pdrAmount, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "index")
                .add("pdr_amount", pdrAmount)
                .add("client", "android")
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "pay_sn");
                        httpCallBack.onSuccess(result, baseBean, data);
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void pdCashAdd(PreDepositCashData data, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "pd_cash_add")
                .add("pdc_amount", data.getAmount())
                .add("pdc_bank_name", data.getBankName())
                .add("pdc_bank_no", data.getBankNo())
                .add("pdc_bank_user", data.getBankUser())
                .add("mobilenum", data.getMobile())
                .add("password", data.getPassword())
                .add("client", "android")
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void rechargeOrder(String paySn, HttpCallBack<RechargeOrderBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "recharge_order")
                .add("pay_sn", paySn)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), RechargeOrderBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
