package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class StoreClassBean implements Serializable {

    @SerializedName("sc_id")
    private String scId = "";
    @SerializedName("sc_name")
    private String scName = "";
    @SerializedName("sc_bail")
    private String scBail = "";
    @SerializedName("sc_sort")
    private String scSort = "";

    public String getScId() {
        return scId;
    }

    public void setScId(String scId) {
        this.scId = scId;
    }

    public String getScName() {
        return scName;
    }

    public void setScName(String scName) {
        this.scName = scName;
    }

    public String getScBail() {
        return scBail;
    }

    public void setScBail(String scBail) {
        this.scBail = scBail;
    }

    public String getScSort() {
        return scSort;
    }

    public void setScSort(String scSort) {
        this.scSort = scSort;
    }

}
