package top.yokey.shopai.zsdk.controller;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.LoginBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class LoginController {

    private static final String ACT = "login";

    public static void index(String username, String password, HttpCallBack<LoginBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "index")
                .add("username", username)
                .add("password", password)
                .add("client", "android")
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = baseBean.getDatas().replace("[]", "{}");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(data, LoginBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
