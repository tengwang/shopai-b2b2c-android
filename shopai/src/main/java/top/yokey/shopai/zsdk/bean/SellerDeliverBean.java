package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class SellerDeliverBean implements Serializable {

    @SerializedName("daddress_info")
    private DaddressInfoBean daddressInfo = null;
    @SerializedName("orderinfo")
    private OrderinfoBean orderinfo = null;

    public DaddressInfoBean getDaddressInfo() {
        return daddressInfo;
    }

    public void setDaddressInfo(DaddressInfoBean daddressInfo) {
        this.daddressInfo = daddressInfo;
    }

    public OrderinfoBean getOrderinfo() {
        return orderinfo;
    }

    public void setOrderinfo(OrderinfoBean orderinfo) {
        this.orderinfo = orderinfo;
    }

    public static class DaddressInfoBean {

        @SerializedName("address_id")
        private String addressId = "";
        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("seller_name")
        private String sellerName = "";
        @SerializedName("area_id")
        private String areaId = "";
        @SerializedName("city_id")
        private String cityId = "";
        @SerializedName("area_info")
        private String areaInfo = "";
        @SerializedName("address")
        private String address = "";
        @SerializedName("telphone")
        private String telphone = "";
        @SerializedName("company")
        private String company = "";
        @SerializedName("is_default")
        private String isDefault = "";

        public String getAddressId() {
            return addressId;
        }

        public void setAddressId(String addressId) {
            this.addressId = addressId;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getSellerName() {
            return sellerName;
        }

        public void setSellerName(String sellerName) {
            this.sellerName = sellerName;
        }

        public String getAreaId() {
            return areaId;
        }

        public void setAreaId(String areaId) {
            this.areaId = areaId;
        }

        public String getCityId() {
            return cityId;
        }

        public void setCityId(String cityId) {
            this.cityId = cityId;
        }

        public String getAreaInfo() {
            return areaInfo;
        }

        public void setAreaInfo(String areaInfo) {
            this.areaInfo = areaInfo;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getTelphone() {
            return telphone;
        }

        public void setTelphone(String telphone) {
            this.telphone = telphone;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getIsDefault() {
            return isDefault;
        }

        public void setIsDefault(String isDefault) {
            this.isDefault = isDefault;
        }

    }

    public static class OrderinfoBean {

        @SerializedName("order_id")
        private String orderId = "";
        @SerializedName("order_sn")
        private String orderSn = "";
        @SerializedName("pay_sn")
        private String paySn = "";
        @SerializedName("pay_sn1")
        private String paySn1 = "";
        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("store_name")
        private String storeName = "";
        @SerializedName("buyer_id")
        private String buyerId = "";
        @SerializedName("buyer_name")
        private String buyerName = "";
        @SerializedName("buyer_email")
        private String buyerEmail = "";
        @SerializedName("buyer_phone")
        private String buyerPhone = "";
        @SerializedName("add_time")
        private String addTime = "";
        @SerializedName("payment_code")
        private String paymentCode = "";
        @SerializedName("payment_time")
        private String paymentTime = "";
        @SerializedName("finnshed_time")
        private String finnshedTime = "";
        @SerializedName("goods_amount")
        private String goodsAmount = "";
        @SerializedName("order_amount")
        private String orderAmount = "";
        @SerializedName("rcb_amount")
        private String rcbAmount = "";
        @SerializedName("pd_amount")
        private String pdAmount = "";
        @SerializedName("points_money")
        private String pointsMoney = "";
        @SerializedName("points_number")
        private String pointsNumber = "";
        @SerializedName("shipping_fee")
        private String shippingFee = "";
        @SerializedName("evaluation_state")
        private String evaluationState = "";
        @SerializedName("evaluation_again_state")
        private String evaluationAgainState = "";
        @SerializedName("order_state")
        private String orderState = "";
        @SerializedName("refund_state")
        private String refundState = "";
        @SerializedName("lock_state")
        private String lockState = "";
        @SerializedName("delete_state")
        private String deleteState = "";
        @SerializedName("refund_amount")
        private String refundAmount = "";
        @SerializedName("delay_time")
        private String delayTime = "";
        @SerializedName("order_from")
        private String orderFrom = "";
        @SerializedName("shipping_code")
        private String shippingCode = "";
        @SerializedName("order_type")
        private String orderType = "";
        @SerializedName("api_pay_time")
        private String apiPayTime = "";
        @SerializedName("chain_id")
        private String chainId = "";
        @SerializedName("chain_code")
        private String chainCode = "";
        @SerializedName("rpt_amount")
        private String rptAmount = "";
        @SerializedName("trade_no")
        private String tradeNo = "";
        @SerializedName("is_fx")
        private String isFx = "";
        @SerializedName("state_desc")
        private String stateDesc = "";
        @SerializedName("payment_name")
        private String paymentName = "";
        @SerializedName("extend_order_common")
        private ExtendOrderCommonBean extendOrderCommon = null;
        @SerializedName("extend_order_goods")
        private ArrayList<ExtendOrderGoodsBean> extendOrderGoods = new ArrayList<>();

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getOrderSn() {
            return orderSn;
        }

        public void setOrderSn(String orderSn) {
            this.orderSn = orderSn;
        }

        public String getPaySn() {
            return paySn;
        }

        public void setPaySn(String paySn) {
            this.paySn = paySn;
        }

        public String getPaySn1() {
            return paySn1;
        }

        public void setPaySn1(String paySn1) {
            this.paySn1 = paySn1;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getBuyerId() {
            return buyerId;
        }

        public void setBuyerId(String buyerId) {
            this.buyerId = buyerId;
        }

        public String getBuyerName() {
            return buyerName;
        }

        public void setBuyerName(String buyerName) {
            this.buyerName = buyerName;
        }

        public String getBuyerEmail() {
            return buyerEmail;
        }

        public void setBuyerEmail(String buyerEmail) {
            this.buyerEmail = buyerEmail;
        }

        public String getBuyerPhone() {
            return buyerPhone;
        }

        public void setBuyerPhone(String buyerPhone) {
            this.buyerPhone = buyerPhone;
        }

        public String getAddTime() {
            return addTime;
        }

        public void setAddTime(String addTime) {
            this.addTime = addTime;
        }

        public String getPaymentCode() {
            return paymentCode;
        }

        public void setPaymentCode(String paymentCode) {
            this.paymentCode = paymentCode;
        }

        public String getPaymentTime() {
            return paymentTime;
        }

        public void setPaymentTime(String paymentTime) {
            this.paymentTime = paymentTime;
        }

        public String getFinnshedTime() {
            return finnshedTime;
        }

        public void setFinnshedTime(String finnshedTime) {
            this.finnshedTime = finnshedTime;
        }

        public String getGoodsAmount() {
            return goodsAmount;
        }

        public void setGoodsAmount(String goodsAmount) {
            this.goodsAmount = goodsAmount;
        }

        public String getOrderAmount() {
            return orderAmount;
        }

        public void setOrderAmount(String orderAmount) {
            this.orderAmount = orderAmount;
        }

        public String getRcbAmount() {
            return rcbAmount;
        }

        public void setRcbAmount(String rcbAmount) {
            this.rcbAmount = rcbAmount;
        }

        public String getPdAmount() {
            return pdAmount;
        }

        public void setPdAmount(String pdAmount) {
            this.pdAmount = pdAmount;
        }

        public String getPointsMoney() {
            return pointsMoney;
        }

        public void setPointsMoney(String pointsMoney) {
            this.pointsMoney = pointsMoney;
        }

        public String getPointsNumber() {
            return pointsNumber;
        }

        public void setPointsNumber(String pointsNumber) {
            this.pointsNumber = pointsNumber;
        }

        public String getShippingFee() {
            return shippingFee;
        }

        public void setShippingFee(String shippingFee) {
            this.shippingFee = shippingFee;
        }

        public String getEvaluationState() {
            return evaluationState;
        }

        public void setEvaluationState(String evaluationState) {
            this.evaluationState = evaluationState;
        }

        public String getEvaluationAgainState() {
            return evaluationAgainState;
        }

        public void setEvaluationAgainState(String evaluationAgainState) {
            this.evaluationAgainState = evaluationAgainState;
        }

        public String getOrderState() {
            return orderState;
        }

        public void setOrderState(String orderState) {
            this.orderState = orderState;
        }

        public String getRefundState() {
            return refundState;
        }

        public void setRefundState(String refundState) {
            this.refundState = refundState;
        }

        public String getLockState() {
            return lockState;
        }

        public void setLockState(String lockState) {
            this.lockState = lockState;
        }

        public String getDeleteState() {
            return deleteState;
        }

        public void setDeleteState(String deleteState) {
            this.deleteState = deleteState;
        }

        public String getRefundAmount() {
            return refundAmount;
        }

        public void setRefundAmount(String refundAmount) {
            this.refundAmount = refundAmount;
        }

        public String getDelayTime() {
            return delayTime;
        }

        public void setDelayTime(String delayTime) {
            this.delayTime = delayTime;
        }

        public String getOrderFrom() {
            return orderFrom;
        }

        public void setOrderFrom(String orderFrom) {
            this.orderFrom = orderFrom;
        }

        public String getShippingCode() {
            return shippingCode;
        }

        public void setShippingCode(String shippingCode) {
            this.shippingCode = shippingCode;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public String getApiPayTime() {
            return apiPayTime;
        }

        public void setApiPayTime(String apiPayTime) {
            this.apiPayTime = apiPayTime;
        }

        public String getChainId() {
            return chainId;
        }

        public void setChainId(String chainId) {
            this.chainId = chainId;
        }

        public String getChainCode() {
            return chainCode;
        }

        public void setChainCode(String chainCode) {
            this.chainCode = chainCode;
        }

        public String getRptAmount() {
            return rptAmount;
        }

        public void setRptAmount(String rptAmount) {
            this.rptAmount = rptAmount;
        }

        public String getTradeNo() {
            return tradeNo;
        }

        public void setTradeNo(String tradeNo) {
            this.tradeNo = tradeNo;
        }

        public String getIsFx() {
            return isFx;
        }

        public void setIsFx(String isFx) {
            this.isFx = isFx;
        }

        public String getStateDesc() {
            return stateDesc;
        }

        public void setStateDesc(String stateDesc) {
            this.stateDesc = stateDesc;
        }

        public String getPaymentName() {
            return paymentName;
        }

        public void setPaymentName(String paymentName) {
            this.paymentName = paymentName;
        }

        public ExtendOrderCommonBean getExtendOrderCommon() {
            return extendOrderCommon;
        }

        public void setExtendOrderCommon(ExtendOrderCommonBean extendOrderCommon) {
            this.extendOrderCommon = extendOrderCommon;
        }

        public ArrayList<ExtendOrderGoodsBean> getExtendOrderGoods() {
            return extendOrderGoods;
        }

        public void setExtendOrderGoods(ArrayList<ExtendOrderGoodsBean> extendOrderGoods) {
            this.extendOrderGoods = extendOrderGoods;
        }

        public static class ExtendOrderCommonBean {

            @SerializedName("order_id")
            private String orderId = "";
            @SerializedName("store_id")
            private String storeId = "";
            @SerializedName("shipping_time")
            private String shippingTime = "";
            @SerializedName("shipping_express_id")
            private String shippingExpressId = "";
            @SerializedName("evaluation_time")
            private String evaluationTime = "";
            @SerializedName("order_message")
            private String orderMessage = "";
            @SerializedName("order_pointscount")
            private String orderPointscount = "";
            @SerializedName("voucher_price")
            private String voucherPrice = "";
            @SerializedName("voucher_code")
            private String voucherCode = "";
            @SerializedName("deliver_explain")
            private String deliverExplain = "";
            @SerializedName("daddress_id")
            private String daddressId = "";
            @SerializedName("reciver_name")
            private String reciverName = "";
            @SerializedName("reciver_info")
            private ReciverInfoBean reciverInfo = null;
            @SerializedName("reciver_province_id")
            private String reciverProvinceId = "";
            @SerializedName("reciver_city_id")
            private String reciverCityId = "";
            @SerializedName("invoice_info")
            private InvoiceInfoBean invoiceInfo = null;
            @SerializedName("sale_info")
            private String saleInfo = "";
            @SerializedName("dlyo_pickup_code")
            private String dlyoPickupCode = "";
            @SerializedName("sale_total")
            private String saleTotal = "";
            @SerializedName("discount")
            private String discount = "";

            public String getOrderId() {
                return orderId;
            }

            public void setOrderId(String orderId) {
                this.orderId = orderId;
            }

            public String getStoreId() {
                return storeId;
            }

            public void setStoreId(String storeId) {
                this.storeId = storeId;
            }

            public String getShippingTime() {
                return shippingTime;
            }

            public void setShippingTime(String shippingTime) {
                this.shippingTime = shippingTime;
            }

            public String getShippingExpressId() {
                return shippingExpressId;
            }

            public void setShippingExpressId(String shippingExpressId) {
                this.shippingExpressId = shippingExpressId;
            }

            public String getEvaluationTime() {
                return evaluationTime;
            }

            public void setEvaluationTime(String evaluationTime) {
                this.evaluationTime = evaluationTime;
            }

            public String getOrderMessage() {
                return orderMessage;
            }

            public void setOrderMessage(String orderMessage) {
                this.orderMessage = orderMessage;
            }

            public String getOrderPointscount() {
                return orderPointscount;
            }

            public void setOrderPointscount(String orderPointscount) {
                this.orderPointscount = orderPointscount;
            }

            public String getVoucherPrice() {
                return voucherPrice;
            }

            public void setVoucherPrice(String voucherPrice) {
                this.voucherPrice = voucherPrice;
            }

            public String getVoucherCode() {
                return voucherCode;
            }

            public void setVoucherCode(String voucherCode) {
                this.voucherCode = voucherCode;
            }

            public String getDeliverExplain() {
                return deliverExplain;
            }

            public void setDeliverExplain(String deliverExplain) {
                this.deliverExplain = deliverExplain;
            }

            public String getDaddressId() {
                return daddressId;
            }

            public void setDaddressId(String daddressId) {
                this.daddressId = daddressId;
            }

            public String getReciverName() {
                return reciverName;
            }

            public void setReciverName(String reciverName) {
                this.reciverName = reciverName;
            }

            public ReciverInfoBean getReciverInfo() {
                return reciverInfo;
            }

            public void setReciverInfo(ReciverInfoBean reciverInfo) {
                this.reciverInfo = reciverInfo;
            }

            public String getReciverProvinceId() {
                return reciverProvinceId;
            }

            public void setReciverProvinceId(String reciverProvinceId) {
                this.reciverProvinceId = reciverProvinceId;
            }

            public String getReciverCityId() {
                return reciverCityId;
            }

            public void setReciverCityId(String reciverCityId) {
                this.reciverCityId = reciverCityId;
            }

            public InvoiceInfoBean getInvoiceInfo() {
                return invoiceInfo;
            }

            public void setInvoiceInfo(InvoiceInfoBean invoiceInfo) {
                this.invoiceInfo = invoiceInfo;
            }

            public String getSaleInfo() {
                return saleInfo;
            }

            public void setSaleInfo(String saleInfo) {
                this.saleInfo = saleInfo;
            }

            public String getDlyoPickupCode() {
                return dlyoPickupCode;
            }

            public void setDlyoPickupCode(String dlyoPickupCode) {
                this.dlyoPickupCode = dlyoPickupCode;
            }

            public String getSaleTotal() {
                return saleTotal;
            }

            public void setSaleTotal(String saleTotal) {
                this.saleTotal = saleTotal;
            }

            public String getDiscount() {
                return discount;
            }

            public void setDiscount(String discount) {
                this.discount = discount;
            }

            public static class ReciverInfoBean {

                @SerializedName("phone")
                private String phone = "";
                @SerializedName("mob_phone")
                private String mobPhone = "";
                @SerializedName("tel_phone")
                private String telPhone = "";
                @SerializedName("address")
                private String address = "";
                @SerializedName("area")
                private String area = "";
                @SerializedName("street")
                private String street = "";

                public String getPhone() {
                    return phone;
                }

                public void setPhone(String phone) {
                    this.phone = phone;
                }

                public String getMobPhone() {
                    return mobPhone;
                }

                public void setMobPhone(String mobPhone) {
                    this.mobPhone = mobPhone;
                }

                public String getTelPhone() {
                    return telPhone;
                }

                public void setTelPhone(String telPhone) {
                    this.telPhone = telPhone;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public String getArea() {
                    return area;
                }

                public void setArea(String area) {
                    this.area = area;
                }

                public String getStreet() {
                    return street;
                }

                public void setStreet(String street) {
                    this.street = street;
                }

            }

            public static class InvoiceInfoBean {

                @SerializedName("类型")
                private String type = "";
                @SerializedName("抬头")
                private String hand = "";
                @SerializedName("内容")
                private String content = "";

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public String getHand() {
                    return hand;
                }

                public void setHand(String hand) {
                    this.hand = hand;
                }

                public String getContent() {
                    return content;
                }

                public void setContent(String content) {
                    this.content = content;
                }

            }

        }

        public static class ExtendOrderGoodsBean {

            @SerializedName("rec_id")
            private String recId = "";
            @SerializedName("order_id")
            private String orderId = "";
            @SerializedName("goods_id")
            private String goodsId = "";
            @SerializedName("goods_name")
            private String goodsName = "";
            @SerializedName("goods_price")
            private String goodsPrice = "";
            @SerializedName("goods_num")
            private String goodsNum = "";
            @SerializedName("goods_image")
            private String goodsImage = "";
            @SerializedName("goods_pay_price")
            private String goodsPayPrice = "";
            @SerializedName("store_id")
            private String storeId = "";
            @SerializedName("buyer_id")
            private String buyerId = "";
            @SerializedName("goods_type")
            private String goodsType = "";
            @SerializedName("sales_id")
            private String salesId = "";
            @SerializedName("commis_rate")
            private String commisRate = "";
            @SerializedName("gc_id")
            private String gcId = "";
            @SerializedName("goods_spec")
            private String goodsSpec = "";
            @SerializedName("goods_contractid")
            private String goodsContractid = "";
            @SerializedName("invite_rates")
            private String inviteRates = "";
            @SerializedName("goods_serial")
            private String goodsSerial = "";
            @SerializedName("goods_commonid")
            private String goodsCommonid = "";
            @SerializedName("add_time")
            private String addTime = "";
            @SerializedName("is_fx")
            private String isFx = "";
            @SerializedName("fx_commis_rate")
            private String fxCommisRate = "";
            @SerializedName("fx_member_id")
            private String fxMemberId = "";

            public String getRecId() {
                return recId;
            }

            public void setRecId(String recId) {
                this.recId = recId;
            }

            public String getOrderId() {
                return orderId;
            }

            public void setOrderId(String orderId) {
                this.orderId = orderId;
            }

            public String getGoodsId() {
                return goodsId;
            }

            public void setGoodsId(String goodsId) {
                this.goodsId = goodsId;
            }

            public String getGoodsName() {
                return goodsName;
            }

            public void setGoodsName(String goodsName) {
                this.goodsName = goodsName;
            }

            public String getGoodsPrice() {
                return goodsPrice;
            }

            public void setGoodsPrice(String goodsPrice) {
                this.goodsPrice = goodsPrice;
            }

            public String getGoodsNum() {
                return goodsNum;
            }

            public void setGoodsNum(String goodsNum) {
                this.goodsNum = goodsNum;
            }

            public String getGoodsImage() {
                return goodsImage;
            }

            public void setGoodsImage(String goodsImage) {
                this.goodsImage = goodsImage;
            }

            public String getGoodsPayPrice() {
                return goodsPayPrice;
            }

            public void setGoodsPayPrice(String goodsPayPrice) {
                this.goodsPayPrice = goodsPayPrice;
            }

            public String getStoreId() {
                return storeId;
            }

            public void setStoreId(String storeId) {
                this.storeId = storeId;
            }

            public String getBuyerId() {
                return buyerId;
            }

            public void setBuyerId(String buyerId) {
                this.buyerId = buyerId;
            }

            public String getGoodsType() {
                return goodsType;
            }

            public void setGoodsType(String goodsType) {
                this.goodsType = goodsType;
            }

            public String getSalesId() {
                return salesId;
            }

            public void setSalesId(String salesId) {
                this.salesId = salesId;
            }

            public String getCommisRate() {
                return commisRate;
            }

            public void setCommisRate(String commisRate) {
                this.commisRate = commisRate;
            }

            public String getGcId() {
                return gcId;
            }

            public void setGcId(String gcId) {
                this.gcId = gcId;
            }

            public String getGoodsSpec() {
                return goodsSpec;
            }

            public void setGoodsSpec(String goodsSpec) {
                this.goodsSpec = goodsSpec;
            }

            public String getGoodsContractid() {
                return goodsContractid;
            }

            public void setGoodsContractid(String goodsContractid) {
                this.goodsContractid = goodsContractid;
            }

            public String getInviteRates() {
                return inviteRates;
            }

            public void setInviteRates(String inviteRates) {
                this.inviteRates = inviteRates;
            }

            public String getGoodsSerial() {
                return goodsSerial;
            }

            public void setGoodsSerial(String goodsSerial) {
                this.goodsSerial = goodsSerial;
            }

            public String getGoodsCommonid() {
                return goodsCommonid;
            }

            public void setGoodsCommonid(String goodsCommonid) {
                this.goodsCommonid = goodsCommonid;
            }

            public String getAddTime() {
                return addTime;
            }

            public void setAddTime(String addTime) {
                this.addTime = addTime;
            }

            public String getIsFx() {
                return isFx;
            }

            public void setIsFx(String isFx) {
                this.isFx = isFx;
            }

            public String getFxCommisRate() {
                return fxCommisRate;
            }

            public void setFxCommisRate(String fxCommisRate) {
                this.fxCommisRate = fxCommisRate;
            }

            public String getFxMemberId() {
                return fxMemberId;
            }

            public void setFxMemberId(String fxMemberId) {
                this.fxMemberId = fxMemberId;
            }

        }

    }

}
