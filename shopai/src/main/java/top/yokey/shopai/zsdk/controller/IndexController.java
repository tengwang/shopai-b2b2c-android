package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.ApkVersionBean;
import top.yokey.shopai.zsdk.bean.ArticleListBean;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.SearchKeyBean;
import top.yokey.shopai.zsdk.bean.SearchShowBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class IndexController {

    private static final String ACT = "index";

    public static void index(HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "index")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.getString(baseBean.getDatas(), "index"));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void apkVersion(HttpCallBack<ApkVersionBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "apk_version")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), ApkVersionBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void searchKeyList(HttpCallBack<SearchKeyBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "search_key_list")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), SearchKeyBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void searchShow(HttpCallBack<SearchShowBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "search_show")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), SearchShowBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void special(String specialId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "special")
                .add("special_id", specialId)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.getString(baseBean.getDatas(), "list"));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void getGG(String acId, HttpCallBack<ArrayList<ArticleListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "getgg")
                .add("ac_id", acId)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "article_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, ArticleListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
