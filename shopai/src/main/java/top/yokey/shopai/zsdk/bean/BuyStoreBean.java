package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class BuyStoreBean implements Serializable {

    private String key = "";
    private String message = "";
    private String total = "";
    private String logistics = "";
    @SerializedName("store_goods_total")
    private String storeGoodsTotal = "";
    @SerializedName("store_mansong_rule_list")
    private StoreMansongRuleListBean storeMansongRuleList = null;
    @SerializedName("store_voucher_info")
    private StoreVoucherInfoBean storeVoucherInfo = null;
    @SerializedName("freight")
    private String freight = "";
    @SerializedName("store_name")
    private String storeName = "";
    @SerializedName("goods_list")
    private ArrayList<GoodsListBean> goodsList = new ArrayList<>();

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getLogistics() {
        return logistics;
    }

    public void setLogistics(String logistics) {
        this.logistics = logistics;
    }

    public String getStoreGoodsTotal() {
        return storeGoodsTotal;
    }

    public void setStoreGoodsTotal(String storeGoodsTotal) {
        this.storeGoodsTotal = storeGoodsTotal;
    }

    public StoreMansongRuleListBean getStoreMansongRuleList() {
        return storeMansongRuleList;
    }

    public void setStoreMansongRuleList(StoreMansongRuleListBean storeMansongRuleList) {
        this.storeMansongRuleList = storeMansongRuleList;
    }

    public StoreVoucherInfoBean getStoreVoucherInfo() {
        return storeVoucherInfo;
    }

    public void setStoreVoucherInfo(StoreVoucherInfoBean storeVoucherInfo) {
        this.storeVoucherInfo = storeVoucherInfo;
    }

    public String getFreight() {
        return freight;
    }

    public void setFreight(String freight) {
        this.freight = freight;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public ArrayList<GoodsListBean> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(ArrayList<GoodsListBean> goodsList) {
        this.goodsList = goodsList;
    }

    public static class StoreMansongRuleListBean {

        @SerializedName("rule_id")
        private String ruleId = "";
        @SerializedName("mansong_id")
        private String mansongId = "";
        @SerializedName("price")
        private String price = "";
        @SerializedName("discount")
        private String discount = "";
        @SerializedName("mansong_goods_name")
        private String mansongGoodsName = "";
        @SerializedName("goods_id")
        private String goodsId = "";
        @SerializedName("goods_image")
        private String goodsImage = "";
        @SerializedName("goods_image_url")
        private String goodsImageUrl = "";
        @SerializedName("goods_storage")
        private String goodsStorage = "";
        @SerializedName("goods_url")
        private String goodsUrl = "";
        @SerializedName("mansong_name")
        private String mansongName = "";
        @SerializedName("start_time")
        private String startTime = "";
        @SerializedName("end_time")
        private String endTime = "";
        @SerializedName("desc")
        private DescBean desc = null;

        public String getRuleId() {
            return ruleId;
        }

        public void setRuleId(String ruleId) {
            this.ruleId = ruleId;
        }

        public String getMansongId() {
            return mansongId;
        }

        public void setMansongId(String mansongId) {
            this.mansongId = mansongId;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getMansongGoodsName() {
            return mansongGoodsName;
        }

        public void setMansongGoodsName(String mansongGoodsName) {
            this.mansongGoodsName = mansongGoodsName;
        }

        public String getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(String goodsId) {
            this.goodsId = goodsId;
        }

        public String getGoodsImage() {
            return goodsImage;
        }

        public void setGoodsImage(String goodsImage) {
            this.goodsImage = goodsImage;
        }

        public String getGoodsImageUrl() {
            return goodsImageUrl;
        }

        public void setGoodsImageUrl(String goodsImageUrl) {
            this.goodsImageUrl = goodsImageUrl;
        }

        public String getGoodsStorage() {
            return goodsStorage;
        }

        public void setGoodsStorage(String goodsStorage) {
            this.goodsStorage = goodsStorage;
        }

        public String getGoodsUrl() {
            return goodsUrl;
        }

        public void setGoodsUrl(String goodsUrl) {
            this.goodsUrl = goodsUrl;
        }

        public String getMansongName() {
            return mansongName;
        }

        public void setMansongName(String mansongName) {
            this.mansongName = mansongName;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public DescBean getDesc() {
            return desc;
        }

        public void setDesc(DescBean desc) {
            this.desc = desc;
        }

        public static class DescBean {

            @SerializedName("desc")
            private String desc = "";
            @SerializedName("goods_name")
            private String goodsName = "";
            @SerializedName("url")
            private String url = "";

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getGoodsName() {
                return goodsName;
            }

            public void setGoodsName(String goodsName) {
                this.goodsName = goodsName;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

        }

    }

    public static class StoreVoucherInfoBean {

        @SerializedName("voucher_id")
        private String voucherId = "";
        @SerializedName("voucher_code")
        private String voucherCode = "";
        @SerializedName("voucher_t_id")
        private String voucherTId = "";
        @SerializedName("voucher_title")
        private String voucherTitle = "";
        @SerializedName("voucher_desc")
        private String voucherDesc = "";
        @SerializedName("voucher_start_date")
        private String voucherStartDate = "";
        @SerializedName("voucher_end_date")
        private String voucherEndDate = "";
        @SerializedName("voucher_price")
        private String voucherPrice = "";
        @SerializedName("voucher_limit")
        private String voucherLimit = "";
        @SerializedName("voucher_store_id")
        private String voucherStoreId = "";
        @SerializedName("voucher_state")
        private String voucherState = "";
        @SerializedName("voucher_active_date")
        private String voucherActiveDate = "";
        @SerializedName("voucher_type")
        private String voucherType = "";
        @SerializedName("voucher_owner_id")
        private String voucherOwnerId = "";
        @SerializedName("voucher_owner_name")
        private String voucherOwnerName = "";
        @SerializedName("voucher_order_id")
        private String voucherOrderId = "";
        @SerializedName("voucher_pwd")
        private String voucherPwd = "";
        @SerializedName("voucher_pwd2")
        private String voucherPwd2 = "";
        @SerializedName("desc")
        private String desc = "";

        public String getVoucherId() {
            return voucherId;
        }

        public void setVoucherId(String voucherId) {
            this.voucherId = voucherId;
        }

        public String getVoucherCode() {
            return voucherCode;
        }

        public void setVoucherCode(String voucherCode) {
            this.voucherCode = voucherCode;
        }

        public String getVoucherTId() {
            return voucherTId;
        }

        public void setVoucherTId(String voucherTId) {
            this.voucherTId = voucherTId;
        }

        public String getVoucherTitle() {
            return voucherTitle;
        }

        public void setVoucherTitle(String voucherTitle) {
            this.voucherTitle = voucherTitle;
        }

        public String getVoucherDesc() {
            return voucherDesc;
        }

        public void setVoucherDesc(String voucherDesc) {
            this.voucherDesc = voucherDesc;
        }

        public String getVoucherStartDate() {
            return voucherStartDate;
        }

        public void setVoucherStartDate(String voucherStartDate) {
            this.voucherStartDate = voucherStartDate;
        }

        public String getVoucherEndDate() {
            return voucherEndDate;
        }

        public void setVoucherEndDate(String voucherEndDate) {
            this.voucherEndDate = voucherEndDate;
        }

        public String getVoucherPrice() {
            return voucherPrice;
        }

        public void setVoucherPrice(String voucherPrice) {
            this.voucherPrice = voucherPrice;
        }

        public String getVoucherLimit() {
            return voucherLimit;
        }

        public void setVoucherLimit(String voucherLimit) {
            this.voucherLimit = voucherLimit;
        }

        public String getVoucherStoreId() {
            return voucherStoreId;
        }

        public void setVoucherStoreId(String voucherStoreId) {
            this.voucherStoreId = voucherStoreId;
        }

        public String getVoucherState() {
            return voucherState;
        }

        public void setVoucherState(String voucherState) {
            this.voucherState = voucherState;
        }

        public String getVoucherActiveDate() {
            return voucherActiveDate;
        }

        public void setVoucherActiveDate(String voucherActiveDate) {
            this.voucherActiveDate = voucherActiveDate;
        }

        public String getVoucherType() {
            return voucherType;
        }

        public void setVoucherType(String voucherType) {
            this.voucherType = voucherType;
        }

        public String getVoucherOwnerId() {
            return voucherOwnerId;
        }

        public void setVoucherOwnerId(String voucherOwnerId) {
            this.voucherOwnerId = voucherOwnerId;
        }

        public String getVoucherOwnerName() {
            return voucherOwnerName;
        }

        public void setVoucherOwnerName(String voucherOwnerName) {
            this.voucherOwnerName = voucherOwnerName;
        }

        public String getVoucherOrderId() {
            return voucherOrderId;
        }

        public void setVoucherOrderId(String voucherOrderId) {
            this.voucherOrderId = voucherOrderId;
        }

        public String getVoucherPwd() {
            return voucherPwd;
        }

        public void setVoucherPwd(String voucherPwd) {
            this.voucherPwd = voucherPwd;
        }

        public String getVoucherPwd2() {
            return voucherPwd2;
        }

        public void setVoucherPwd2(String voucherPwd2) {
            this.voucherPwd2 = voucherPwd2;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

    }

    public static class GoodsListBean {

        @SerializedName("cart_id")
        private String cartId = "";
        @SerializedName("buyer_id")
        private String buyerId = "";
        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("store_name")
        private String storeName = "";
        @SerializedName("goods_id")
        private String goodsId = "";
        @SerializedName("goods_name")
        private String goodsName = "";
        @SerializedName("goods_price")
        private String goodsPrice = "";
        @SerializedName("goods_num")
        private String goodsNum = "";
        @SerializedName("goods_image")
        private String goodsImage = "";
        @SerializedName("bl_id")
        private String blId = "";
        @SerializedName("state")
        private boolean state = false;
        @SerializedName("storage_state")
        private boolean storageState = false;
        @SerializedName("goods_commonid")
        private String goodsCommonid = "";
        @SerializedName("gc_id")
        private String gcId = "";
        @SerializedName("transport_id")
        private String transportId = "";
        @SerializedName("goods_freight")
        private String goodsFreight = "";
        @SerializedName("goods_trans_v")
        private String goodsTransV = "";
        @SerializedName("goods_vat")
        private String goodsVat = "";
        @SerializedName("goods_storage")
        private String goodsStorage = "";
        @SerializedName("goods_storage_alarm")
        private String goodsStorageAlarm = "";
        @SerializedName("is_fcode")
        private String isFcode = "";
        @SerializedName("have_gift")
        private String haveGift = "";
        @SerializedName("is_book")
        private String isBook = "";
        @SerializedName("book_down_payment")
        private String bookDownPayment = "";
        @SerializedName("book_final_payment")
        private String bookFinalPayment = "";
        @SerializedName("book_down_time")
        private String bookDownTime = "";
        @SerializedName("is_chain")
        private String isChain = "";
        @SerializedName("goods_total")
        private String goodsTotal = "";
        @SerializedName("goods_image_url")
        private String goodsImageUrl = "";
        @SerializedName("goods_spec")
        private String goodsSpec = "";

        public String getCartId() {
            return cartId;
        }

        public void setCartId(String cartId) {
            this.cartId = cartId;
        }

        public String getBuyerId() {
            return buyerId;
        }

        public void setBuyerId(String buyerId) {
            this.buyerId = buyerId;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(String goodsId) {
            this.goodsId = goodsId;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public String getGoodsPrice() {
            return goodsPrice;
        }

        public void setGoodsPrice(String goodsPrice) {
            this.goodsPrice = goodsPrice;
        }

        public String getGoodsNum() {
            return goodsNum;
        }

        public void setGoodsNum(String goodsNum) {
            this.goodsNum = goodsNum;
        }

        public String getGoodsImage() {
            return goodsImage;
        }

        public void setGoodsImage(String goodsImage) {
            this.goodsImage = goodsImage;
        }

        public String getBlId() {
            return blId;
        }

        public void setBlId(String blId) {
            this.blId = blId;
        }

        public boolean isState() {
            return state;
        }

        public void setState(boolean state) {
            this.state = state;
        }

        public boolean isStorageState() {
            return storageState;
        }

        public void setStorageState(boolean storageState) {
            this.storageState = storageState;
        }

        public String getGoodsCommonid() {
            return goodsCommonid;
        }

        public void setGoodsCommonid(String goodsCommonid) {
            this.goodsCommonid = goodsCommonid;
        }

        public String getGcId() {
            return gcId;
        }

        public void setGcId(String gcId) {
            this.gcId = gcId;
        }

        public String getTransportId() {
            return transportId;
        }

        public void setTransportId(String transportId) {
            this.transportId = transportId;
        }

        public String getGoodsFreight() {
            return goodsFreight;
        }

        public void setGoodsFreight(String goodsFreight) {
            this.goodsFreight = goodsFreight;
        }

        public String getGoodsTransV() {
            return goodsTransV;
        }

        public void setGoodsTransV(String goodsTransV) {
            this.goodsTransV = goodsTransV;
        }

        public String getGoodsVat() {
            return goodsVat;
        }

        public void setGoodsVat(String goodsVat) {
            this.goodsVat = goodsVat;
        }

        public String getGoodsStorage() {
            return goodsStorage;
        }

        public void setGoodsStorage(String goodsStorage) {
            this.goodsStorage = goodsStorage;
        }

        public String getGoodsStorageAlarm() {
            return goodsStorageAlarm;
        }

        public void setGoodsStorageAlarm(String goodsStorageAlarm) {
            this.goodsStorageAlarm = goodsStorageAlarm;
        }

        public String getIsFcode() {
            return isFcode;
        }

        public void setIsFcode(String isFcode) {
            this.isFcode = isFcode;
        }

        public String getHaveGift() {
            return haveGift;
        }

        public void setHaveGift(String haveGift) {
            this.haveGift = haveGift;
        }

        public String getIsBook() {
            return isBook;
        }

        public void setIsBook(String isBook) {
            this.isBook = isBook;
        }

        public String getBookDownPayment() {
            return bookDownPayment;
        }

        public void setBookDownPayment(String bookDownPayment) {
            this.bookDownPayment = bookDownPayment;
        }

        public String getBookFinalPayment() {
            return bookFinalPayment;
        }

        public void setBookFinalPayment(String bookFinalPayment) {
            this.bookFinalPayment = bookFinalPayment;
        }

        public String getBookDownTime() {
            return bookDownTime;
        }

        public void setBookDownTime(String bookDownTime) {
            this.bookDownTime = bookDownTime;
        }

        public String getIsChain() {
            return isChain;
        }

        public void setIsChain(String isChain) {
            this.isChain = isChain;
        }

        public String getGoodsTotal() {
            return goodsTotal;
        }

        public void setGoodsTotal(String goodsTotal) {
            this.goodsTotal = goodsTotal;
        }

        public String getGoodsImageUrl() {
            return goodsImageUrl;
        }

        public void setGoodsImageUrl(String goodsImageUrl) {
            this.goodsImageUrl = goodsImageUrl;
        }

        public String getGoodsSpec() {
            return goodsSpec;
        }

        public void setGoodsSpec(String goodsSpec) {
            this.goodsSpec = goodsSpec;
        }

    }

}
