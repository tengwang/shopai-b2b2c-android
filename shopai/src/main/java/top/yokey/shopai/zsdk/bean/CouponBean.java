package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class CouponBean implements Serializable {

    @SerializedName("coupon_id")
    private String couponId = "";
    @SerializedName("coupon_code")
    private String couponCode = "";
    @SerializedName("coupon_t_id")
    private String couponTId = "";
    @SerializedName("coupon_title")
    private String couponTitle = "";
    @SerializedName("coupon_desc")
    private String couponDesc = "";
    @SerializedName("coupon_start_date")
    private String couponStartDate = "";
    @SerializedName("coupon_end_date")
    private String couponEndDate = "";
    @SerializedName("coupon_price")
    private String couponPrice = "";
    @SerializedName("coupon_limit")
    private String couponLimit = "";
    @SerializedName("coupon_state")
    private String couponState = "";
    @SerializedName("coupon_active_date")
    private String couponActiveDate = "";
    @SerializedName("coupon_owner_id")
    private String couponOwnerId = "";
    @SerializedName("coupon_owner_name")
    private String couponOwnerName = "";
    @SerializedName("coupon_order_id")
    private String couponOrderId = "";
    @SerializedName("coupon_pwd")
    private String couponPwd = "";
    @SerializedName("coupon_pwd2")
    private String couponPwd2 = "";
    @SerializedName("coupon_customimg")
    private String couponCustomimg = "";
    @SerializedName("coupon_customimg_url")
    private String couponCustomimgUrl = "";
    @SerializedName("coupon_state_text")
    private String couponStateText = "";
    @SerializedName("coupon_state_key")
    private String couponStateKey = "";
    @SerializedName("coupon_start_date_text")
    private String couponStartDateText = "";
    @SerializedName("coupon_end_date_text")
    private String couponEndDateText = "";

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getCouponTId() {
        return couponTId;
    }

    public void setCouponTId(String couponTId) {
        this.couponTId = couponTId;
    }

    public String getCouponTitle() {
        return couponTitle;
    }

    public void setCouponTitle(String couponTitle) {
        this.couponTitle = couponTitle;
    }

    public String getCouponDesc() {
        return couponDesc;
    }

    public void setCouponDesc(String couponDesc) {
        this.couponDesc = couponDesc;
    }

    public String getCouponStartDate() {
        return couponStartDate;
    }

    public void setCouponStartDate(String couponStartDate) {
        this.couponStartDate = couponStartDate;
    }

    public String getCouponEndDate() {
        return couponEndDate;
    }

    public void setCouponEndDate(String couponEndDate) {
        this.couponEndDate = couponEndDate;
    }

    public String getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(String couponPrice) {
        this.couponPrice = couponPrice;
    }

    public String getCouponLimit() {
        return couponLimit;
    }

    public void setCouponLimit(String couponLimit) {
        this.couponLimit = couponLimit;
    }

    public String getCouponState() {
        return couponState;
    }

    public void setCouponState(String couponState) {
        this.couponState = couponState;
    }

    public String getCouponActiveDate() {
        return couponActiveDate;
    }

    public void setCouponActiveDate(String couponActiveDate) {
        this.couponActiveDate = couponActiveDate;
    }

    public String getCouponOwnerId() {
        return couponOwnerId;
    }

    public void setCouponOwnerId(String couponOwnerId) {
        this.couponOwnerId = couponOwnerId;
    }

    public String getCouponOwnerName() {
        return couponOwnerName;
    }

    public void setCouponOwnerName(String couponOwnerName) {
        this.couponOwnerName = couponOwnerName;
    }

    public String getCouponOrderId() {
        return couponOrderId;
    }

    public void setCouponOrderId(String couponOrderId) {
        this.couponOrderId = couponOrderId;
    }

    public String getCouponPwd() {
        return couponPwd;
    }

    public void setCouponPwd(String couponPwd) {
        this.couponPwd = couponPwd;
    }

    public String getCouponPwd2() {
        return couponPwd2;
    }

    public void setCouponPwd2(String couponPwd2) {
        this.couponPwd2 = couponPwd2;
    }

    public String getCouponCustomimg() {
        return couponCustomimg;
    }

    public void setCouponCustomimg(String couponCustomimg) {
        this.couponCustomimg = couponCustomimg;
    }

    public String getCouponCustomimgUrl() {
        return couponCustomimgUrl;
    }

    public void setCouponCustomimgUrl(String couponCustomimgUrl) {
        this.couponCustomimgUrl = couponCustomimgUrl;
    }

    public String getCouponStateText() {
        return couponStateText;
    }

    public void setCouponStateText(String couponStateText) {
        this.couponStateText = couponStateText;
    }

    public String getCouponStateKey() {
        return couponStateKey;
    }

    public void setCouponStateKey(String couponStateKey) {
        this.couponStateKey = couponStateKey;
    }

    public String getCouponStartDateText() {
        return couponStartDateText;
    }

    public void setCouponStartDateText(String couponStartDateText) {
        this.couponStartDateText = couponStartDateText;
    }

    public String getCouponEndDateText() {
        return couponEndDateText;
    }

    public void setCouponEndDateText(String couponEndDateText) {
        this.couponEndDateText = couponEndDateText;
    }

}
