package top.yokey.shopai.zsdk.controller;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.RedPacketDetailBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class RedPacketController {

    private static final String ACT = "red_packet";

    public static void index(String id, HttpCallBack<RedPacketDetailBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "index")
                .add("id", id)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "packet_detail");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(data, RedPacketDetailBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
