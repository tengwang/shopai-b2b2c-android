package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class SellerStoreBean implements Serializable {

    @SerializedName("store_id")
    private String storeId = "";
    @SerializedName("store_name")
    private String storeName = "";
    @SerializedName("grade_id")
    private String gradeId = "";
    @SerializedName("member_id")
    private String memberId = "";
    @SerializedName("member_name")
    private String memberName = "";
    @SerializedName("seller_name")
    private String sellerName = "";
    @SerializedName("sc_id")
    private String scId = "";
    @SerializedName("store_company_name")
    private String storeCompanyName = "";
    @SerializedName("province_id")
    private String provinceId = "";
    @SerializedName("area_info")
    private String areaInfo = "";
    @SerializedName("store_address")
    private String storeAddress = "";
    @SerializedName("store_zip")
    private String storeZip = "";
    @SerializedName("store_state")
    private String storeState = "";
    @SerializedName("store_close_info")
    private String storeCloseInfo = "";
    @SerializedName("store_sort")
    private String storeSort = "";
    @SerializedName("store_time")
    private String storeTime = "";
    @SerializedName("store_end_time")
    private String storeEndTime = "";
    @SerializedName("store_label")
    private String storeLabel = "";
    @SerializedName("store_banner")
    private String storeBanner = "";
    @SerializedName("store_avatar")
    private String storeAvatar = "";
    @SerializedName("store_keywords")
    private String storeKeywords = "";
    @SerializedName("store_description")
    private String storeDescription = "";
    @SerializedName("store_qq")
    private String storeQq = "";
    @SerializedName("store_ww")
    private String storeWw = "";
    @SerializedName("store_phone")
    private String storePhone = "";
    @SerializedName("store_zy")
    private String storeZy = "";
    @SerializedName("store_domain")
    private String storeDomain = "";
    @SerializedName("store_domain_times")
    private String storeDomainTimes = "";
    @SerializedName("store_recommend")
    private String storeRecommend = "";
    @SerializedName("store_theme")
    private String storeTheme = "";
    @SerializedName("store_credit")
    private StoreCreditBean storeCredit = null;
    @SerializedName("store_desccredit")
    private String storeDesccredit = "";
    @SerializedName("store_servicecredit")
    private String storeServicecredit = "";
    @SerializedName("store_deliverycredit")
    private String storeDeliverycredit = "";
    @SerializedName("store_collect")
    private String storeCollect = "";
    @SerializedName("store_slide")
    private String storeSlide = "";
    @SerializedName("store_slide_url")
    private String storeSlideUrl = "";
    @SerializedName("store_stamp")
    private String storeStamp = "";
    @SerializedName("store_printdesc")
    private String storePrintdesc = "";
    @SerializedName("store_sales")
    private String storeSales = "";
    @SerializedName("store_presales")
    private String storePresales = "";
    @SerializedName("store_aftersales")
    private String storeAftersales = "";
    @SerializedName("store_workingtime")
    private String storeWorkingtime = "";
    @SerializedName("store_free_price")
    private String storeFreePrice = "";
    @SerializedName("store_free_time")
    private String storeFreeTime = "";
    @SerializedName("store_decoration_switch")
    private String storeDecorationSwitch = "";
    @SerializedName("store_decoration_only")
    private String storeDecorationOnly = "";
    @SerializedName("store_decoration_image_count")
    private String storeDecorationImageCount = "";
    @SerializedName("is_own_shop")
    private String isOwnShop = "";
    @SerializedName("bind_all_gc")
    private String bindAllGc = "";
    @SerializedName("store_vrcode_prefix")
    private String storeVrcodePrefix = "";
    @SerializedName("mb_title_img")
    private String mbTitleImg = "";
    @SerializedName("mb_sliders")
    private String mbSliders = "";
    @SerializedName("left_bar_type")
    private String leftBarType = "";
    @SerializedName("deliver_region")
    private String deliverRegion = "";
    @SerializedName("is_distribution")
    private String isDistribution = "";
    @SerializedName("is_person")
    private String isPerson = "";
    @SerializedName("mb_store_decoration_switch")
    private String mbStoreDecorationSwitch = "";
    @SerializedName("goods_count")
    private String goodsCount = "";
    @SerializedName("store_credit_average")
    private String storeCreditAverage = "";
    @SerializedName("store_credit_percent")
    private String storeCreditPercent = "";

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getScId() {
        return scId;
    }

    public void setScId(String scId) {
        this.scId = scId;
    }

    public String getStoreCompanyName() {
        return storeCompanyName;
    }

    public void setStoreCompanyName(String storeCompanyName) {
        this.storeCompanyName = storeCompanyName;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getAreaInfo() {
        return areaInfo;
    }

    public void setAreaInfo(String areaInfo) {
        this.areaInfo = areaInfo;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getStoreZip() {
        return storeZip;
    }

    public void setStoreZip(String storeZip) {
        this.storeZip = storeZip;
    }

    public String getStoreState() {
        return storeState;
    }

    public void setStoreState(String storeState) {
        this.storeState = storeState;
    }

    public String getStoreCloseInfo() {
        return storeCloseInfo;
    }

    public void setStoreCloseInfo(String storeCloseInfo) {
        this.storeCloseInfo = storeCloseInfo;
    }

    public String getStoreSort() {
        return storeSort;
    }

    public void setStoreSort(String storeSort) {
        this.storeSort = storeSort;
    }

    public String getStoreTime() {
        return storeTime;
    }

    public void setStoreTime(String storeTime) {
        this.storeTime = storeTime;
    }

    public String getStoreEndTime() {
        return storeEndTime;
    }

    public void setStoreEndTime(String storeEndTime) {
        this.storeEndTime = storeEndTime;
    }

    public String getStoreLabel() {
        return storeLabel;
    }

    public void setStoreLabel(String storeLabel) {
        this.storeLabel = storeLabel;
    }

    public String getStoreBanner() {
        return storeBanner;
    }

    public void setStoreBanner(String storeBanner) {
        this.storeBanner = storeBanner;
    }

    public String getStoreAvatar() {
        return storeAvatar;
    }

    public void setStoreAvatar(String storeAvatar) {
        this.storeAvatar = storeAvatar;
    }

    public String getStoreKeywords() {
        return storeKeywords;
    }

    public void setStoreKeywords(String storeKeywords) {
        this.storeKeywords = storeKeywords;
    }

    public String getStoreDescription() {
        return storeDescription;
    }

    public void setStoreDescription(String storeDescription) {
        this.storeDescription = storeDescription;
    }

    public String getStoreQq() {
        return storeQq;
    }

    public void setStoreQq(String storeQq) {
        this.storeQq = storeQq;
    }

    public String getStoreWw() {
        return storeWw;
    }

    public void setStoreWw(String storeWw) {
        this.storeWw = storeWw;
    }

    public String getStorePhone() {
        return storePhone;
    }

    public void setStorePhone(String storePhone) {
        this.storePhone = storePhone;
    }

    public String getStoreZy() {
        return storeZy;
    }

    public void setStoreZy(String storeZy) {
        this.storeZy = storeZy;
    }

    public String getStoreDomain() {
        return storeDomain;
    }

    public void setStoreDomain(String storeDomain) {
        this.storeDomain = storeDomain;
    }

    public String getStoreDomainTimes() {
        return storeDomainTimes;
    }

    public void setStoreDomainTimes(String storeDomainTimes) {
        this.storeDomainTimes = storeDomainTimes;
    }

    public String getStoreRecommend() {
        return storeRecommend;
    }

    public void setStoreRecommend(String storeRecommend) {
        this.storeRecommend = storeRecommend;
    }

    public String getStoreTheme() {
        return storeTheme;
    }

    public void setStoreTheme(String storeTheme) {
        this.storeTheme = storeTheme;
    }

    public StoreCreditBean getStoreCredit() {
        return storeCredit;
    }

    public void setStoreCredit(StoreCreditBean storeCredit) {
        this.storeCredit = storeCredit;
    }

    public String getStoreDesccredit() {
        return storeDesccredit;
    }

    public void setStoreDesccredit(String storeDesccredit) {
        this.storeDesccredit = storeDesccredit;
    }

    public String getStoreServicecredit() {
        return storeServicecredit;
    }

    public void setStoreServicecredit(String storeServicecredit) {
        this.storeServicecredit = storeServicecredit;
    }

    public String getStoreDeliverycredit() {
        return storeDeliverycredit;
    }

    public void setStoreDeliverycredit(String storeDeliverycredit) {
        this.storeDeliverycredit = storeDeliverycredit;
    }

    public String getStoreCollect() {
        return storeCollect;
    }

    public void setStoreCollect(String storeCollect) {
        this.storeCollect = storeCollect;
    }

    public String getStoreSlide() {
        return storeSlide;
    }

    public void setStoreSlide(String storeSlide) {
        this.storeSlide = storeSlide;
    }

    public String getStoreSlideUrl() {
        return storeSlideUrl;
    }

    public void setStoreSlideUrl(String storeSlideUrl) {
        this.storeSlideUrl = storeSlideUrl;
    }

    public String getStoreStamp() {
        return storeStamp;
    }

    public void setStoreStamp(String storeStamp) {
        this.storeStamp = storeStamp;
    }

    public String getStorePrintdesc() {
        return storePrintdesc;
    }

    public void setStorePrintdesc(String storePrintdesc) {
        this.storePrintdesc = storePrintdesc;
    }

    public String getStoreSales() {
        return storeSales;
    }

    public void setStoreSales(String storeSales) {
        this.storeSales = storeSales;
    }

    public String getStorePresales() {
        return storePresales;
    }

    public void setStorePresales(String storePresales) {
        this.storePresales = storePresales;
    }

    public String getStoreAftersales() {
        return storeAftersales;
    }

    public void setStoreAftersales(String storeAftersales) {
        this.storeAftersales = storeAftersales;
    }

    public String getStoreWorkingtime() {
        return storeWorkingtime;
    }

    public void setStoreWorkingtime(String storeWorkingtime) {
        this.storeWorkingtime = storeWorkingtime;
    }

    public String getStoreFreePrice() {
        return storeFreePrice;
    }

    public void setStoreFreePrice(String storeFreePrice) {
        this.storeFreePrice = storeFreePrice;
    }

    public String getStoreFreeTime() {
        return storeFreeTime;
    }

    public void setStoreFreeTime(String storeFreeTime) {
        this.storeFreeTime = storeFreeTime;
    }

    public String getStoreDecorationSwitch() {
        return storeDecorationSwitch;
    }

    public void setStoreDecorationSwitch(String storeDecorationSwitch) {
        this.storeDecorationSwitch = storeDecorationSwitch;
    }

    public String getStoreDecorationOnly() {
        return storeDecorationOnly;
    }

    public void setStoreDecorationOnly(String storeDecorationOnly) {
        this.storeDecorationOnly = storeDecorationOnly;
    }

    public String getStoreDecorationImageCount() {
        return storeDecorationImageCount;
    }

    public void setStoreDecorationImageCount(String storeDecorationImageCount) {
        this.storeDecorationImageCount = storeDecorationImageCount;
    }

    public String getIsOwnShop() {
        return isOwnShop;
    }

    public void setIsOwnShop(String isOwnShop) {
        this.isOwnShop = isOwnShop;
    }

    public String getBindAllGc() {
        return bindAllGc;
    }

    public void setBindAllGc(String bindAllGc) {
        this.bindAllGc = bindAllGc;
    }

    public String getStoreVrcodePrefix() {
        return storeVrcodePrefix;
    }

    public void setStoreVrcodePrefix(String storeVrcodePrefix) {
        this.storeVrcodePrefix = storeVrcodePrefix;
    }

    public String getMbTitleImg() {
        return mbTitleImg;
    }

    public void setMbTitleImg(String mbTitleImg) {
        this.mbTitleImg = mbTitleImg;
    }

    public String getMbSliders() {
        return mbSliders;
    }

    public void setMbSliders(String mbSliders) {
        this.mbSliders = mbSliders;
    }

    public String getLeftBarType() {
        return leftBarType;
    }

    public void setLeftBarType(String leftBarType) {
        this.leftBarType = leftBarType;
    }

    public String getDeliverRegion() {
        return deliverRegion;
    }

    public void setDeliverRegion(String deliverRegion) {
        this.deliverRegion = deliverRegion;
    }

    public String getIsDistribution() {
        return isDistribution;
    }

    public void setIsDistribution(String isDistribution) {
        this.isDistribution = isDistribution;
    }

    public String getIsPerson() {
        return isPerson;
    }

    public void setIsPerson(String isPerson) {
        this.isPerson = isPerson;
    }

    public String getMbStoreDecorationSwitch() {
        return mbStoreDecorationSwitch;
    }

    public void setMbStoreDecorationSwitch(String mbStoreDecorationSwitch) {
        this.mbStoreDecorationSwitch = mbStoreDecorationSwitch;
    }

    public String getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(String goodsCount) {
        this.goodsCount = goodsCount;
    }

    public String getStoreCreditAverage() {
        return storeCreditAverage;
    }

    public void setStoreCreditAverage(String storeCreditAverage) {
        this.storeCreditAverage = storeCreditAverage;
    }

    public String getStoreCreditPercent() {
        return storeCreditPercent;
    }

    public void setStoreCreditPercent(String storeCreditPercent) {
        this.storeCreditPercent = storeCreditPercent;
    }

    public static class StoreCreditBean {

        @SerializedName("store_desccredit")
        private StoreDesccreditBean storeDesccredit = null;
        @SerializedName("store_servicecredit")
        private StoreServicecreditBean storeServicecredit = null;
        @SerializedName("store_deliverycredit")
        private StoreDeliverycreditBean storeDeliverycredit = null;

        public StoreDesccreditBean getStoreDesccredit() {
            return storeDesccredit;
        }

        public void setStoreDesccredit(StoreDesccreditBean storeDesccredit) {
            this.storeDesccredit = storeDesccredit;
        }

        public StoreServicecreditBean getStoreServicecredit() {
            return storeServicecredit;
        }

        public void setStoreServicecredit(StoreServicecreditBean storeServicecredit) {
            this.storeServicecredit = storeServicecredit;
        }

        public StoreDeliverycreditBean getStoreDeliverycredit() {
            return storeDeliverycredit;
        }

        public void setStoreDeliverycredit(StoreDeliverycreditBean storeDeliverycredit) {
            this.storeDeliverycredit = storeDeliverycredit;
        }

        public static class StoreDesccreditBean {

            @SerializedName("text")
            private String text = "";
            @SerializedName("credit")
            private String credit = "";

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }

            public String getCredit() {
                return credit;
            }

            public void setCredit(String credit) {
                this.credit = credit;
            }

        }

        public static class StoreServicecreditBean {

            @SerializedName("text")
            private String text = "";
            @SerializedName("credit")
            private String credit = "";

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }

            public String getCredit() {
                return credit;
            }

            public void setCredit(String credit) {
                this.credit = credit;
            }

        }

        public static class StoreDeliverycreditBean {

            @SerializedName("text")
            private String text = "";
            @SerializedName("credit")
            private String credit = "";

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }

            public String getCredit() {
                return credit;
            }

            public void setCredit(String credit) {
                this.credit = credit;
            }

        }

    }

}
