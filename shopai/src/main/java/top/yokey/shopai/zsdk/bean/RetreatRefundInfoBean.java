package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class RetreatRefundInfoBean implements Serializable {

    @SerializedName("refund")
    private RefundBean refund = null;
    @SerializedName("detail_array")
    private DetailArrayBean detailArray = null;
    @SerializedName("pic_list")
    private ArrayList<String> picList = new ArrayList<>();

    public RefundBean getRefund() {
        return refund;
    }

    public void setRefund(RefundBean refund) {
        this.refund = refund;
    }

    public DetailArrayBean getDetailArray() {
        return detailArray;
    }

    public void setDetailArray(DetailArrayBean detailArray) {
        this.detailArray = detailArray;
    }

    public ArrayList<String> getPicList() {
        return picList;
    }

    public void setPicList(ArrayList<String> picList) {
        this.picList = picList;
    }

    public static class RefundBean {

        @SerializedName("refund_id")
        private String refundId = "";
        @SerializedName("goods_id")
        private String goodsId = "";
        @SerializedName("goods_name")
        private String goodsName = "";
        @SerializedName("order_id")
        private String orderId = "";
        @SerializedName("refund_amount")
        private String refundAmount = "";
        @SerializedName("refund_sn")
        private String refundSn = "";
        @SerializedName("order_sn")
        private String orderSn = "";
        @SerializedName("add_time")
        private String addTime = "";
        @SerializedName("goods_img_360")
        private String goodsImg360 = "";
        @SerializedName("seller_state")
        private String sellerState = "";
        @SerializedName("admin_state")
        private String adminState = "";
        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("store_name")
        private String storeName = "";
        @SerializedName("reason_info")
        private String reasonInfo = "";
        @SerializedName("buyer_message")
        private String buyerMessage = "";
        @SerializedName("seller_message")
        private String sellerMessage = "";
        @SerializedName("admin_message")
        private String adminMessage = "";

        public String getRefundId() {
            return refundId;
        }

        public void setRefundId(String refundId) {
            this.refundId = refundId;
        }

        public String getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(String goodsId) {
            this.goodsId = goodsId;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getRefundAmount() {
            return refundAmount;
        }

        public void setRefundAmount(String refundAmount) {
            this.refundAmount = refundAmount;
        }

        public String getRefundSn() {
            return refundSn;
        }

        public void setRefundSn(String refundSn) {
            this.refundSn = refundSn;
        }

        public String getOrderSn() {
            return orderSn;
        }

        public void setOrderSn(String orderSn) {
            this.orderSn = orderSn;
        }

        public String getAddTime() {
            return addTime;
        }

        public void setAddTime(String addTime) {
            this.addTime = addTime;
        }

        public String getGoodsImg360() {
            return goodsImg360;
        }

        public void setGoodsImg360(String goodsImg360) {
            this.goodsImg360 = goodsImg360;
        }

        public String getSellerState() {
            return sellerState;
        }

        public void setSellerState(String sellerState) {
            this.sellerState = sellerState;
        }

        public String getAdminState() {
            return adminState;
        }

        public void setAdminState(String adminState) {
            this.adminState = adminState;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getReasonInfo() {
            return reasonInfo;
        }

        public void setReasonInfo(String reasonInfo) {
            this.reasonInfo = reasonInfo;
        }

        public String getBuyerMessage() {
            return buyerMessage;
        }

        public void setBuyerMessage(String buyerMessage) {
            this.buyerMessage = buyerMessage;
        }

        public String getSellerMessage() {
            return sellerMessage;
        }

        public void setSellerMessage(String sellerMessage) {
            this.sellerMessage = sellerMessage;
        }

        public String getAdminMessage() {
            return adminMessage;
        }

        public void setAdminMessage(String adminMessage) {
            this.adminMessage = adminMessage;
        }

    }

    public static class DetailArrayBean {

        @SerializedName("refund_code")
        private String refundCode = "";
        @SerializedName("pay_amount")
        private String payAmount = "";
        @SerializedName("pd_amount")
        private String pdAmount = "";
        @SerializedName("rcb_amount")
        private String rcbAmount = "";

        public String getRefundCode() {
            return refundCode;
        }

        public void setRefundCode(String refundCode) {
            this.refundCode = refundCode;
        }

        public String getPayAmount() {
            return payAmount;
        }

        public void setPayAmount(String payAmount) {
            this.payAmount = payAmount;
        }

        public String getPdAmount() {
            return pdAmount;
        }

        public void setPdAmount(String pdAmount) {
            this.pdAmount = pdAmount;
        }

        public String getRcbAmount() {
            return rcbAmount;
        }

        public void setRcbAmount(String rcbAmount) {
            this.rcbAmount = rcbAmount;
        }

    }

}
