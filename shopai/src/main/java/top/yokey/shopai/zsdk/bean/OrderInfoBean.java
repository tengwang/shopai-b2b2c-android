package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class OrderInfoBean implements Serializable {

    @SerializedName("order_id")
    private String orderId = "";
    @SerializedName("order_sn")
    private String orderSn = "";
    @SerializedName("store_id")
    private String storeId = "";
    @SerializedName("store_name")
    private String storeName = "";
    @SerializedName("add_time")
    private String addTime = "";
    @SerializedName("payment_time")
    private String paymentTime = "";
    @SerializedName("shipping_time")
    private String shippingTime = "";
    @SerializedName("finnshed_time")
    private String finnshedTime = "";
    @SerializedName("order_amount")
    private String orderAmount = "";
    @SerializedName("shipping_fee")
    private String shippingFee = "";
    @SerializedName("real_pay_amount")
    private String realPayAmount = "";
    @SerializedName("state_desc")
    private String stateDesc = "";
    @SerializedName("payment_name")
    private String paymentName = "";
    @SerializedName("order_message")
    private String orderMessage = "";
    @SerializedName("reciver_phone")
    private String reciverPhone = "";
    @SerializedName("reciver_name")
    private String reciverName = "";
    @SerializedName("reciver_addr")
    private String reciverAddr = "";
    @SerializedName("store_member_id")
    private String storeMemberId = "";
    @SerializedName("store_qq")
    private String storeQq = "";
    @SerializedName("node_chat")
    private boolean nodeChat = false;
    @SerializedName("store_phone")
    private String storePhone = "";
    @SerializedName("order_tips")
    private String orderTips = "";
    @SerializedName("invoice")
    private String invoice = "";
    @SerializedName("if_deliver")
    private boolean ifDeliver = false;
    @SerializedName("if_buyer_cancel")
    private boolean ifBuyerCancel = false;
    @SerializedName("if_refund_cancel")
    private boolean ifRefundCancel = false;
    @SerializedName("if_receive")
    private boolean ifReceive = false;
    @SerializedName("if_evaluation")
    private boolean ifEvaluation = false;
    @SerializedName("if_lock")
    private boolean ifLock = false;
    @SerializedName("order_type")
    private String orderType = "";
    @SerializedName("ownshop")
    private boolean ownshop = false;
    @SerializedName("pingou_info")
    private PinGouInfoBean pinGouInfo = null;
    @SerializedName("sale")
    private ArrayList<ArrayList<String>> sale = new ArrayList<>();
    @SerializedName("goods_list")
    private ArrayList<GoodsListBean> goodsList = new ArrayList<>();
    @SerializedName("zengpin_list")
    private ArrayList<ZengpinListBean> zengpinList = new ArrayList<>();

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(String paymentTime) {
        this.paymentTime = paymentTime;
    }

    public String getShippingTime() {
        return shippingTime;
    }

    public void setShippingTime(String shippingTime) {
        this.shippingTime = shippingTime;
    }

    public String getFinnshedTime() {
        return finnshedTime;
    }

    public void setFinnshedTime(String finnshedTime) {
        this.finnshedTime = finnshedTime;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(String shippingFee) {
        this.shippingFee = shippingFee;
    }

    public String getRealPayAmount() {
        return realPayAmount;
    }

    public void setRealPayAmount(String realPayAmount) {
        this.realPayAmount = realPayAmount;
    }

    public String getStateDesc() {
        return stateDesc;
    }

    public void setStateDesc(String stateDesc) {
        this.stateDesc = stateDesc;
    }

    public String getPaymentName() {
        return paymentName;
    }

    public void setPaymentName(String paymentName) {
        this.paymentName = paymentName;
    }

    public String getOrderMessage() {
        return orderMessage;
    }

    public void setOrderMessage(String orderMessage) {
        this.orderMessage = orderMessage;
    }

    public String getReciverPhone() {
        return reciverPhone;
    }

    public void setReciverPhone(String reciverPhone) {
        this.reciverPhone = reciverPhone;
    }

    public String getReciverName() {
        return reciverName;
    }

    public void setReciverName(String reciverName) {
        this.reciverName = reciverName;
    }

    public String getReciverAddr() {
        return reciverAddr;
    }

    public void setReciverAddr(String reciverAddr) {
        this.reciverAddr = reciverAddr;
    }

    public String getStoreMemberId() {
        return storeMemberId;
    }

    public void setStoreMemberId(String storeMemberId) {
        this.storeMemberId = storeMemberId;
    }

    public String getStoreQq() {
        return storeQq;
    }

    public void setStoreQq(String storeQq) {
        this.storeQq = storeQq;
    }

    public boolean isNodeChat() {
        return nodeChat;
    }

    public void setNodeChat(boolean nodeChat) {
        this.nodeChat = nodeChat;
    }

    public String getStorePhone() {
        return storePhone;
    }

    public void setStorePhone(String storePhone) {
        this.storePhone = storePhone;
    }

    public String getOrderTips() {
        return orderTips;
    }

    public void setOrderTips(String orderTips) {
        this.orderTips = orderTips;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public boolean isIfDeliver() {
        return ifDeliver;
    }

    public void setIfDeliver(boolean ifDeliver) {
        this.ifDeliver = ifDeliver;
    }

    public boolean isIfBuyerCancel() {
        return ifBuyerCancel;
    }

    public void setIfBuyerCancel(boolean ifBuyerCancel) {
        this.ifBuyerCancel = ifBuyerCancel;
    }

    public boolean isIfRefundCancel() {
        return ifRefundCancel;
    }

    public void setIfRefundCancel(boolean ifRefundCancel) {
        this.ifRefundCancel = ifRefundCancel;
    }

    public boolean isIfReceive() {
        return ifReceive;
    }

    public void setIfReceive(boolean ifReceive) {
        this.ifReceive = ifReceive;
    }

    public boolean isIfEvaluation() {
        return ifEvaluation;
    }

    public void setIfEvaluation(boolean ifEvaluation) {
        this.ifEvaluation = ifEvaluation;
    }

    public boolean isIfLock() {
        return ifLock;
    }

    public void setIfLock(boolean ifLock) {
        this.ifLock = ifLock;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public boolean isOwnshop() {
        return ownshop;
    }

    public void setOwnshop(boolean ownshop) {
        this.ownshop = ownshop;
    }

    public ArrayList<ArrayList<String>> getSale() {
        return sale;
    }

    public void setSale(ArrayList<ArrayList<String>> sale) {
        this.sale = sale;
    }

    public ArrayList<GoodsListBean> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(ArrayList<GoodsListBean> goodsList) {
        this.goodsList = goodsList;
    }

    public ArrayList<ZengpinListBean> getZengpinList() {
        return zengpinList;
    }

    public void setZengpinList(ArrayList<ZengpinListBean> zengpinList) {
        this.zengpinList = zengpinList;
    }

    public PinGouInfoBean getPinGouInfo() {
        return pinGouInfo;
    }

    public void setPinGouInfo(PinGouInfoBean pinGouInfo) {
        this.pinGouInfo = pinGouInfo;
    }

    public static class GoodsListBean {

        @SerializedName("rec_id")
        private String recId = "";
        @SerializedName("goods_id")
        private String goodsId = "";
        @SerializedName("goods_name")
        private String goodsName = "";
        @SerializedName("goods_price")
        private String goodsPrice = "";
        @SerializedName("goods_num")
        private String goodsNum = "";
        @SerializedName("goods_spec")
        private String goodsSpec = "";
        @SerializedName("image_url")
        private String imageUrl = "";
        @SerializedName("refund")
        private String refund = "";

        public String getRecId() {
            return recId;
        }

        public void setRecId(String recId) {
            this.recId = recId;
        }

        public String getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(String goodsId) {
            this.goodsId = goodsId;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public String getGoodsPrice() {
            return goodsPrice;
        }

        public void setGoodsPrice(String goodsPrice) {
            this.goodsPrice = goodsPrice;
        }

        public String getGoodsNum() {
            return goodsNum;
        }

        public void setGoodsNum(String goodsNum) {
            this.goodsNum = goodsNum;
        }

        public String getGoodsSpec() {
            return goodsSpec;
        }

        public void setGoodsSpec(String goodsSpec) {
            this.goodsSpec = goodsSpec;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getRefund() {
            return refund;
        }

        public void setRefund(String refund) {
            this.refund = refund;
        }

    }

    public static class ZengpinListBean {

        @SerializedName("goods_name")
        private String goodsName = "";
        @SerializedName("goods_num")
        private String goodsNum = "";

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public String getGoodsNum() {
            return goodsNum;
        }

        public void setGoodsNum(String goodsNum) {
            this.goodsNum = goodsNum;
        }

    }

    public static class PinGouInfoBean {

        @SerializedName("log_id")
        private String logId = "";
        @SerializedName("order_id")
        private String orderId = "";
        @SerializedName("order_sn")
        private String orderSn = "";
        @SerializedName("buyer_type")
        private String buyerType = "";
        @SerializedName("buyer_id")
        private String buyerId = "";
        @SerializedName("buyer_name")
        private String buyerName = "";
        @SerializedName("goods_id")
        private String goodsId = "";
        @SerializedName("goods_commonid")
        private String goodsCommonid = "";
        @SerializedName("min_num")
        private String minNum = "";
        @SerializedName("add_time")
        private String addTime = "";
        @SerializedName("pay_time")
        private String payTime = "";
        @SerializedName("end_time")
        private String endTime = "";
        @SerializedName("cancel_time")
        private String cancelTime = "";
        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("lock_state")
        private String lockState = "";
        @SerializedName("goods_num")
        private String goodsNum = "";
        @SerializedName("pingou_id")
        private String pingouId = "";

        public String getLogId() {
            return logId;
        }

        public void setLogId(String logId) {
            this.logId = logId;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getOrderSn() {
            return orderSn;
        }

        public void setOrderSn(String orderSn) {
            this.orderSn = orderSn;
        }

        public String getBuyerType() {
            return buyerType;
        }

        public void setBuyerType(String buyerType) {
            this.buyerType = buyerType;
        }

        public String getBuyerId() {
            return buyerId;
        }

        public void setBuyerId(String buyerId) {
            this.buyerId = buyerId;
        }

        public String getBuyerName() {
            return buyerName;
        }

        public void setBuyerName(String buyerName) {
            this.buyerName = buyerName;
        }

        public String getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(String goodsId) {
            this.goodsId = goodsId;
        }

        public String getGoodsCommonid() {
            return goodsCommonid;
        }

        public void setGoodsCommonid(String goodsCommonid) {
            this.goodsCommonid = goodsCommonid;
        }

        public String getMinNum() {
            return minNum;
        }

        public void setMinNum(String minNum) {
            this.minNum = minNum;
        }

        public String getAddTime() {
            return addTime;
        }

        public void setAddTime(String addTime) {
            this.addTime = addTime;
        }

        public String getPayTime() {
            return payTime;
        }

        public void setPayTime(String payTime) {
            this.payTime = payTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getCancelTime() {
            return cancelTime;
        }

        public void setCancelTime(String cancelTime) {
            this.cancelTime = cancelTime;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getLockState() {
            return lockState;
        }

        public void setLockState(String lockState) {
            this.lockState = lockState;
        }

        public String getGoodsNum() {
            return goodsNum;
        }

        public void setGoodsNum(String goodsNum) {
            this.goodsNum = goodsNum;
        }

        public String getPingouId() {
            return pingouId;
        }

        public void setPingouId(String pingouId) {
            this.pingouId = pingouId;
        }

    }

}
