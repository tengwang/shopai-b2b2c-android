package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.CartBean;
import top.yokey.shopai.zsdk.bean.CartEditQuantityBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class MemberCartController {

    private static final String ACT = "member_cart";

    public static void cartCount(HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "cart_count")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.getString(baseBean.getDatas(), "cart_count"));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void cartList(HttpCallBack<ArrayList<CartBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "cart_list")
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "cart_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, CartBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void cartDel(String cartId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "cart_del")
                .add("cart_id", cartId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void cartAdd(String goodsId, String quantity, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "cart_add")
                .add("goods_id", goodsId)
                .add("quantity", quantity)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void cartEditQuantity(String cartId, String goodsId, String quantity, HttpCallBack<CartEditQuantityBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "cart_edit_quantity")
                .add("cart_id", cartId)
                .add("goods_id", goodsId)
                .add("quantity", quantity)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), CartEditQuantityBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
