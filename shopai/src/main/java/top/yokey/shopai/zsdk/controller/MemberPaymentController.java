package top.yokey.shopai.zsdk.controller;

import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.data.OrderPayData;

@SuppressWarnings("ALL")
public class MemberPaymentController {

    private static final String ACT = "member_payment";

    public static void payNew(OrderPayData data, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "pay_new")
                .add("pay_sn", data.getPaySn())
                .add("password", data.getPassword())
                .add("rcb_pay", data.getRcbPay())
                .add("pd_pay", data.getPdPay())
                .add("payment_code", data.getPaymentCode())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
