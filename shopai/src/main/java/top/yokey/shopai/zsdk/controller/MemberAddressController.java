package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.AddressBean;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.data.AddressData;

@SuppressWarnings("ALL")
public class MemberAddressController {

    private static final String ACT = "member_address";

    public static void addressList(HttpCallBack<ArrayList<AddressBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "address_list")
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "address_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, AddressBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void addressDel(String addressId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "address_del")
                .add("address_id", addressId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void addressAdd(AddressData data, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "address_add")
                .add("true_name", data.getTrueName())
                .add("mob_phone", data.getMobPhone())
                .add("city_id", data.getCityId())
                .add("area_id", data.getAreaId())
                .add("address", data.getAddress())
                .add("area_info", data.getAreaInfo())
                .add("is_default", data.getIsDefault())
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void addressEdit(AddressData data, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "address_edit")
                .add("address_id", data.getAddressId())
                .add("true_name", data.getTrueName())
                .add("mob_phone", data.getMobPhone())
                .add("city_id", data.getCityId())
                .add("area_id", data.getAreaId())
                .add("address", data.getAddress())
                .add("area_info", data.getAreaInfo())
                .add("is_default", data.getIsDefault())
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
