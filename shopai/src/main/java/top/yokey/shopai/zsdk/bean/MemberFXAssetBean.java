package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class MemberFXAssetBean implements Serializable {

    @SerializedName("member_name")
    private String memberName = "";
    @SerializedName("member_email")
    private String memberEmail = "";
    @SerializedName("bill_user_name")
    private String billUserName = "";
    @SerializedName("bill_type_code")
    private String billTypeCode = "";
    @SerializedName("bill_type_number")
    private String billTypeNumber = "";
    @SerializedName("bill_bank_name")
    private String billBankName = "";
    @SerializedName("available_fx_trad")
    private String availableFxTrad = "";
    @SerializedName("freeze_fx_trad")
    private String freezeFxTrad = "";

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberEmail() {
        return memberEmail;
    }

    public void setMemberEmail(String memberEmail) {
        this.memberEmail = memberEmail;
    }

    public String getBillUserName() {
        return billUserName;
    }

    public void setBillUserName(String billUserName) {
        this.billUserName = billUserName;
    }

    public String getBillTypeCode() {
        return billTypeCode;
    }

    public void setBillTypeCode(String billTypeCode) {
        this.billTypeCode = billTypeCode;
    }

    public String getBillTypeNumber() {
        return billTypeNumber;
    }

    public void setBillTypeNumber(String billTypeNumber) {
        this.billTypeNumber = billTypeNumber;
    }

    public String getBillBankName() {
        return billBankName;
    }

    public void setBillBankName(String billBankName) {
        this.billBankName = billBankName;
    }

    public String getAvailableFxTrad() {
        return availableFxTrad;
    }

    public void setAvailableFxTrad(String availableFxTrad) {
        this.availableFxTrad = availableFxTrad;
    }

    public String getFreezeFxTrad() {
        return freezeFxTrad;
    }

    public void setFreezeFxTrad(String freezeFxTrad) {
        this.freezeFxTrad = freezeFxTrad;
    }

}
