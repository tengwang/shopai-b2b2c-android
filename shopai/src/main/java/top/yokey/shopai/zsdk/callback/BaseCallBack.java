package top.yokey.shopai.zsdk.callback;

import top.yokey.shopai.zsdk.bean.BaseBean;

@SuppressWarnings("ALL")
public interface BaseCallBack {

    void onSuccess(String result, BaseBean baseBean);

    void onFailure(String reason);

}
