package top.yokey.shopai.zsdk.controller;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.SellerStoreBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.data.SellerStoreData;

@SuppressWarnings("ALL")
public class SellerStoreController {

    private static final String ACT = "seller_store";

    public static void storeInfo(HttpCallBack<SellerStoreBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "store_info")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "store_info");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(data, SellerStoreBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void storeEdit(SellerStoreData data, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "store_edit")
                .add("store_qq", data.getQq())
                .add("store_ww", data.getWw())
                .add("store_phone", data.getPhone())
                .add("store_zy", data.getZy())
                .add("seo_keywords", data.getKeyword())
                .add("seo_description", data.getDescription())
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
