package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.StoreListBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.data.ShopSearchData;

@SuppressWarnings("ALL")
public class ShopController {

    private static final String ACT = "shop";

    public static void shopList(ShopSearchData data, HttpCallBack<ArrayList<StoreListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "shop_list")
                .add("key", data.getKey())
                .add("page", ShopAISdk.get().getPageNumber())
                .add("order", data.getOrder())
                .add("keyword", data.getKeyword())
                .add("area_info", data.getAreaInfo())
                .add("sc_id", data.getScId())
                .add("curpage", data.getPage())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "store_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, StoreListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
