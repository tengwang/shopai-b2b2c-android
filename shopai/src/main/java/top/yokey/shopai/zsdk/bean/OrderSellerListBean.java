package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class OrderSellerListBean implements Serializable {

    @SerializedName("order_id")
    private String orderId = "";
    @SerializedName("order_sn")
    private String orderSn = "";
    @SerializedName("pay_sn")
    private String paySn = "";
    @SerializedName("pay_sn1")
    private String paySn1 = "";
    @SerializedName("store_id")
    private String storeId = "";
    @SerializedName("store_name")
    private String storeName = "";
    @SerializedName("buyer_id")
    private String buyerId = "";
    @SerializedName("buyer_name")
    private String buyerName = "";
    @SerializedName("buyer_email")
    private String buyerEmail = "";
    @SerializedName("buyer_phone")
    private String buyerPhone = "";
    @SerializedName("add_time")
    private String addTime = "";
    @SerializedName("payment_code")
    private String paymentCode = "";
    @SerializedName("payment_time")
    private String paymentTime = "";
    @SerializedName("finnshed_time")
    private String finnshedTime = "";
    @SerializedName("goods_amount")
    private String goodsAmount = "";
    @SerializedName("order_amount")
    private String orderAmount = "";
    @SerializedName("rcb_amount")
    private String rcbAmount = "";
    @SerializedName("pd_amount")
    private String pdAmount = "";
    @SerializedName("points_money")
    private String pointsMoney = "";
    @SerializedName("points_number")
    private String pointsNumber = "";
    @SerializedName("shipping_fee")
    private String shippingFee = "";
    @SerializedName("evaluation_state")
    private String evaluationState = "";
    @SerializedName("evaluation_again_state")
    private String evaluationAgainState = "";
    @SerializedName("order_state")
    private String orderState = "";
    @SerializedName("refund_state")
    private String refundState = "";
    @SerializedName("lock_state")
    private String lockState = "";
    @SerializedName("delete_state")
    private String deleteState = "";
    @SerializedName("refund_amount")
    private String refundAmount = "";
    @SerializedName("delay_time")
    private String delayTime = "";
    @SerializedName("order_from")
    private String orderFrom = "";
    @SerializedName("shipping_code")
    private String shippingCode = "";
    @SerializedName("order_type")
    private String orderType = "";
    @SerializedName("api_pay_time")
    private String apiPayTime = "";
    @SerializedName("chain_id")
    private String chainId = "";
    @SerializedName("chain_code")
    private String chainCode = "";
    @SerializedName("rpt_amount")
    private String rptAmount = "";
    @SerializedName("trade_no")
    private String tradeNo = "";
    @SerializedName("is_fx")
    private String isFx = "";
    @SerializedName("state_desc")
    private String stateDesc = "";
    @SerializedName("payment_name")
    private String paymentName = "";
    @SerializedName("add_time_text")
    private String addTimeText = "";
    @SerializedName("if_store_cancel")
    private String ifStoreCancel = "";
    @SerializedName("if_modify_price")
    private String ifModifyPrice = "";
    @SerializedName("if_spay_price")
    private String ifSpayPrice = "";
    @SerializedName("if_store_send")
    private String ifStoreSend = "";
    @SerializedName("if_lock")
    private String ifLock = "";
    @SerializedName("if_deliver")
    private String ifDeliver = "";
    @SerializedName("if_chain_receive")
    private String ifChainReceive = "";
    @SerializedName("goods_count")
    private String goodsCount = "";
    @SerializedName("extend_order_goods")
    private ArrayList<ExtendOrderGoodsBean> extendOrderGoods = new ArrayList<>();
    @SerializedName("zengpin_list")
    private ArrayList<ZengpinListBean> zengpinList = new ArrayList<>();
    @SerializedName("goods_list")
    private ArrayList<GoodsListBean> goodsList = new ArrayList<>();

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getPaySn() {
        return paySn;
    }

    public void setPaySn(String paySn) {
        this.paySn = paySn;
    }

    public String getPaySn1() {
        return paySn1;
    }

    public void setPaySn1(String paySn1) {
        this.paySn1 = paySn1;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerEmail() {
        return buyerEmail;
    }

    public void setBuyerEmail(String buyerEmail) {
        this.buyerEmail = buyerEmail;
    }

    public String getBuyerPhone() {
        return buyerPhone;
    }

    public void setBuyerPhone(String buyerPhone) {
        this.buyerPhone = buyerPhone;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public String getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(String paymentTime) {
        this.paymentTime = paymentTime;
    }

    public String getFinnshedTime() {
        return finnshedTime;
    }

    public void setFinnshedTime(String finnshedTime) {
        this.finnshedTime = finnshedTime;
    }

    public String getGoodsAmount() {
        return goodsAmount;
    }

    public void setGoodsAmount(String goodsAmount) {
        this.goodsAmount = goodsAmount;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getRcbAmount() {
        return rcbAmount;
    }

    public void setRcbAmount(String rcbAmount) {
        this.rcbAmount = rcbAmount;
    }

    public String getPdAmount() {
        return pdAmount;
    }

    public void setPdAmount(String pdAmount) {
        this.pdAmount = pdAmount;
    }

    public String getPointsMoney() {
        return pointsMoney;
    }

    public void setPointsMoney(String pointsMoney) {
        this.pointsMoney = pointsMoney;
    }

    public String getPointsNumber() {
        return pointsNumber;
    }

    public void setPointsNumber(String pointsNumber) {
        this.pointsNumber = pointsNumber;
    }

    public String getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(String shippingFee) {
        this.shippingFee = shippingFee;
    }

    public String getEvaluationState() {
        return evaluationState;
    }

    public void setEvaluationState(String evaluationState) {
        this.evaluationState = evaluationState;
    }

    public String getEvaluationAgainState() {
        return evaluationAgainState;
    }

    public void setEvaluationAgainState(String evaluationAgainState) {
        this.evaluationAgainState = evaluationAgainState;
    }

    public String getOrderState() {
        return orderState;
    }

    public void setOrderState(String orderState) {
        this.orderState = orderState;
    }

    public String getRefundState() {
        return refundState;
    }

    public void setRefundState(String refundState) {
        this.refundState = refundState;
    }

    public String getLockState() {
        return lockState;
    }

    public void setLockState(String lockState) {
        this.lockState = lockState;
    }

    public String getDeleteState() {
        return deleteState;
    }

    public void setDeleteState(String deleteState) {
        this.deleteState = deleteState;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(String delayTime) {
        this.delayTime = delayTime;
    }

    public String getOrderFrom() {
        return orderFrom;
    }

    public void setOrderFrom(String orderFrom) {
        this.orderFrom = orderFrom;
    }

    public String getShippingCode() {
        return shippingCode;
    }

    public void setShippingCode(String shippingCode) {
        this.shippingCode = shippingCode;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getApiPayTime() {
        return apiPayTime;
    }

    public void setApiPayTime(String apiPayTime) {
        this.apiPayTime = apiPayTime;
    }

    public String getChainId() {
        return chainId;
    }

    public void setChainId(String chainId) {
        this.chainId = chainId;
    }

    public String getChainCode() {
        return chainCode;
    }

    public void setChainCode(String chainCode) {
        this.chainCode = chainCode;
    }

    public String getRptAmount() {
        return rptAmount;
    }

    public void setRptAmount(String rptAmount) {
        this.rptAmount = rptAmount;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getIsFx() {
        return isFx;
    }

    public void setIsFx(String isFx) {
        this.isFx = isFx;
    }

    public String getStateDesc() {
        return stateDesc;
    }

    public void setStateDesc(String stateDesc) {
        this.stateDesc = stateDesc;
    }

    public String getPaymentName() {
        return paymentName;
    }

    public void setPaymentName(String paymentName) {
        this.paymentName = paymentName;
    }

    public String getAddTimeText() {
        return addTimeText;
    }

    public void setAddTimeText(String addTimeText) {
        this.addTimeText = addTimeText;
    }

    public String getIfStoreCancel() {
        return ifStoreCancel;
    }

    public void setIfStoreCancel(String ifStoreCancel) {
        this.ifStoreCancel = ifStoreCancel;
    }

    public String getIfModifyPrice() {
        return ifModifyPrice;
    }

    public void setIfModifyPrice(String ifModifyPrice) {
        this.ifModifyPrice = ifModifyPrice;
    }

    public String getIfSpayPrice() {
        return ifSpayPrice;
    }

    public void setIfSpayPrice(String ifSpayPrice) {
        this.ifSpayPrice = ifSpayPrice;
    }

    public String getIfStoreSend() {
        return ifStoreSend;
    }

    public void setIfStoreSend(String ifStoreSend) {
        this.ifStoreSend = ifStoreSend;
    }

    public String getIfLock() {
        return ifLock;
    }

    public void setIfLock(String ifLock) {
        this.ifLock = ifLock;
    }

    public String getIfDeliver() {
        return ifDeliver;
    }

    public void setIfDeliver(String ifDeliver) {
        this.ifDeliver = ifDeliver;
    }

    public String getIfChainReceive() {
        return ifChainReceive;
    }

    public void setIfChainReceive(String ifChainReceive) {
        this.ifChainReceive = ifChainReceive;
    }

    public String getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(String goodsCount) {
        this.goodsCount = goodsCount;
    }

    public ArrayList<ExtendOrderGoodsBean> getExtendOrderGoods() {
        return extendOrderGoods;
    }

    public void setExtendOrderGoods(ArrayList<ExtendOrderGoodsBean> extendOrderGoods) {
        this.extendOrderGoods = extendOrderGoods;
    }

    public ArrayList<ZengpinListBean> getZengpinList() {
        return zengpinList;
    }

    public void setZengpinList(ArrayList<ZengpinListBean> zengpinList) {
        this.zengpinList = zengpinList;
    }

    public ArrayList<GoodsListBean> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(ArrayList<GoodsListBean> goodsList) {
        this.goodsList = goodsList;
    }

    public static class ExtendOrderGoodsBean {

        @SerializedName("rec_id")
        private String recId = "";
        @SerializedName("order_id")
        private String orderId = "";
        @SerializedName("goods_id")
        private String goodsId = "";
        @SerializedName("goods_name")
        private String goodsName = "";
        @SerializedName("goods_price")
        private String goodsPrice = "";
        @SerializedName("goods_num")
        private String goodsNum = "";
        @SerializedName("goods_image")
        private String goodsImage = "";
        @SerializedName("goods_pay_price")
        private String goodsPayPrice = "";
        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("buyer_id")
        private String buyerId = "";
        @SerializedName("goods_type")
        private String goodsType = "";
        @SerializedName("sales_id")
        private String salesId = "";
        @SerializedName("commis_rate")
        private String commisRate = "";
        @SerializedName("gc_id")
        private String gcId = "";
        @SerializedName("goods_spec")
        private String goodsSpec = "";
        @SerializedName("goods_contractid")
        private String goodsContractid = "";
        @SerializedName("invite_rates")
        private String inviteRates = "";
        @SerializedName("goods_serial")
        private String goodsSerial = "";
        @SerializedName("goods_commonid")
        private String goodsCommonid = "";
        @SerializedName("add_time")
        private String addTime = "";
        @SerializedName("is_fx")
        private String isFx = "";
        @SerializedName("fx_commis_rate")
        private String fxCommisRate = "";
        @SerializedName("fx_member_id")
        private String fxMemberId = "";

        public String getRecId() {
            return recId;
        }

        public void setRecId(String recId) {
            this.recId = recId;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(String goodsId) {
            this.goodsId = goodsId;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public String getGoodsPrice() {
            return goodsPrice;
        }

        public void setGoodsPrice(String goodsPrice) {
            this.goodsPrice = goodsPrice;
        }

        public String getGoodsNum() {
            return goodsNum;
        }

        public void setGoodsNum(String goodsNum) {
            this.goodsNum = goodsNum;
        }

        public String getGoodsImage() {
            return goodsImage;
        }

        public void setGoodsImage(String goodsImage) {
            this.goodsImage = goodsImage;
        }

        public String getGoodsPayPrice() {
            return goodsPayPrice;
        }

        public void setGoodsPayPrice(String goodsPayPrice) {
            this.goodsPayPrice = goodsPayPrice;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getBuyerId() {
            return buyerId;
        }

        public void setBuyerId(String buyerId) {
            this.buyerId = buyerId;
        }

        public String getGoodsType() {
            return goodsType;
        }

        public void setGoodsType(String goodsType) {
            this.goodsType = goodsType;
        }

        public String getSalesId() {
            return salesId;
        }

        public void setSalesId(String salesId) {
            this.salesId = salesId;
        }

        public String getCommisRate() {
            return commisRate;
        }

        public void setCommisRate(String commisRate) {
            this.commisRate = commisRate;
        }

        public String getGcId() {
            return gcId;
        }

        public void setGcId(String gcId) {
            this.gcId = gcId;
        }

        public String getGoodsSpec() {
            return goodsSpec;
        }

        public void setGoodsSpec(String goodsSpec) {
            this.goodsSpec = goodsSpec;
        }

        public String getGoodsContractid() {
            return goodsContractid;
        }

        public void setGoodsContractid(String goodsContractid) {
            this.goodsContractid = goodsContractid;
        }

        public String getInviteRates() {
            return inviteRates;
        }

        public void setInviteRates(String inviteRates) {
            this.inviteRates = inviteRates;
        }

        public String getGoodsSerial() {
            return goodsSerial;
        }

        public void setGoodsSerial(String goodsSerial) {
            this.goodsSerial = goodsSerial;
        }

        public String getGoodsCommonid() {
            return goodsCommonid;
        }

        public void setGoodsCommonid(String goodsCommonid) {
            this.goodsCommonid = goodsCommonid;
        }

        public String getAddTime() {
            return addTime;
        }

        public void setAddTime(String addTime) {
            this.addTime = addTime;
        }

        public String getIsFx() {
            return isFx;
        }

        public void setIsFx(String isFx) {
            this.isFx = isFx;
        }

        public String getFxCommisRate() {
            return fxCommisRate;
        }

        public void setFxCommisRate(String fxCommisRate) {
            this.fxCommisRate = fxCommisRate;
        }

        public String getFxMemberId() {
            return fxMemberId;
        }

        public void setFxMemberId(String fxMemberId) {
            this.fxMemberId = fxMemberId;
        }

    }

    public static class ZengpinListBean {

        @SerializedName("rec_id")
        private String recId = "";
        @SerializedName("order_id")
        private String orderId = "";
        @SerializedName("goods_id")
        private String goodsId = "";
        @SerializedName("goods_name")
        private String goodsName = "";
        @SerializedName("goods_price")
        private String goodsPrice = "";
        @SerializedName("goods_num")
        private String goodsNum = "";
        @SerializedName("goods_image")
        private String goodsImage = "";
        @SerializedName("goods_pay_price")
        private String goodsPayPrice = "";
        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("buyer_id")
        private String buyerId = "";
        @SerializedName("goods_type")
        private String goodsType = "";
        @SerializedName("sales_id")
        private String salesId = "";
        @SerializedName("commis_rate")
        private String commisRate = "";
        @SerializedName("gc_id")
        private String gcId = "";
        @SerializedName("goods_spec")
        private String goodsSpec = "";
        @SerializedName("goods_contractid")
        private String goodsContractid = "";
        @SerializedName("invite_rates")
        private String inviteRates = "";
        @SerializedName("goods_serial")
        private String goodsSerial = "";
        @SerializedName("goods_commonid")
        private String goodsCommonid = "";
        @SerializedName("add_time")
        private String addTime = "";
        @SerializedName("is_fx")
        private String isFx = "";
        @SerializedName("fx_commis_rate")
        private String fxCommisRate = "";
        @SerializedName("fx_member_id")
        private String fxMemberId = "";
        @SerializedName("image_60_url")
        private String image60Url = "";
        @SerializedName("image_240_url")
        private String image240Url = "";
        @SerializedName("goods_type_cn")
        private String goodsTypeCn = "";
        @SerializedName("goods_url")
        private String goodsUrl = "";

        public String getRecId() {
            return recId;
        }

        public void setRecId(String recId) {
            this.recId = recId;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(String goodsId) {
            this.goodsId = goodsId;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public String getGoodsPrice() {
            return goodsPrice;
        }

        public void setGoodsPrice(String goodsPrice) {
            this.goodsPrice = goodsPrice;
        }

        public String getGoodsNum() {
            return goodsNum;
        }

        public void setGoodsNum(String goodsNum) {
            this.goodsNum = goodsNum;
        }

        public String getGoodsImage() {
            return goodsImage;
        }

        public void setGoodsImage(String goodsImage) {
            this.goodsImage = goodsImage;
        }

        public String getGoodsPayPrice() {
            return goodsPayPrice;
        }

        public void setGoodsPayPrice(String goodsPayPrice) {
            this.goodsPayPrice = goodsPayPrice;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getBuyerId() {
            return buyerId;
        }

        public void setBuyerId(String buyerId) {
            this.buyerId = buyerId;
        }

        public String getGoodsType() {
            return goodsType;
        }

        public void setGoodsType(String goodsType) {
            this.goodsType = goodsType;
        }

        public String getSalesId() {
            return salesId;
        }

        public void setSalesId(String salesId) {
            this.salesId = salesId;
        }

        public String getCommisRate() {
            return commisRate;
        }

        public void setCommisRate(String commisRate) {
            this.commisRate = commisRate;
        }

        public String getGcId() {
            return gcId;
        }

        public void setGcId(String gcId) {
            this.gcId = gcId;
        }

        public String getGoodsSpec() {
            return goodsSpec;
        }

        public void setGoodsSpec(String goodsSpec) {
            this.goodsSpec = goodsSpec;
        }

        public String getGoodsContractid() {
            return goodsContractid;
        }

        public void setGoodsContractid(String goodsContractid) {
            this.goodsContractid = goodsContractid;
        }

        public String getInviteRates() {
            return inviteRates;
        }

        public void setInviteRates(String inviteRates) {
            this.inviteRates = inviteRates;
        }

        public String getGoodsSerial() {
            return goodsSerial;
        }

        public void setGoodsSerial(String goodsSerial) {
            this.goodsSerial = goodsSerial;
        }

        public String getGoodsCommonid() {
            return goodsCommonid;
        }

        public void setGoodsCommonid(String goodsCommonid) {
            this.goodsCommonid = goodsCommonid;
        }

        public String getAddTime() {
            return addTime;
        }

        public void setAddTime(String addTime) {
            this.addTime = addTime;
        }

        public String getIsFx() {
            return isFx;
        }

        public void setIsFx(String isFx) {
            this.isFx = isFx;
        }

        public String getFxCommisRate() {
            return fxCommisRate;
        }

        public void setFxCommisRate(String fxCommisRate) {
            this.fxCommisRate = fxCommisRate;
        }

        public String getFxMemberId() {
            return fxMemberId;
        }

        public void setFxMemberId(String fxMemberId) {
            this.fxMemberId = fxMemberId;
        }

        public String getImage60Url() {
            return image60Url;
        }

        public void setImage60Url(String image60Url) {
            this.image60Url = image60Url;
        }

        public String getImage240Url() {
            return image240Url;
        }

        public void setImage240Url(String image240Url) {
            this.image240Url = image240Url;
        }

        public String getGoodsTypeCn() {
            return goodsTypeCn;
        }

        public void setGoodsTypeCn(String goodsTypeCn) {
            this.goodsTypeCn = goodsTypeCn;
        }

        public String getGoodsUrl() {
            return goodsUrl;
        }

        public void setGoodsUrl(String goodsUrl) {
            this.goodsUrl = goodsUrl;
        }

    }

    public static class GoodsListBean {

        @SerializedName("rec_id")
        private String recId = "";
        @SerializedName("order_id")
        private String orderId = "";
        @SerializedName("goods_id")
        private String goodsId = "";
        @SerializedName("goods_name")
        private String goodsName = "";
        @SerializedName("goods_price")
        private String goodsPrice = "";
        @SerializedName("goods_num")
        private String goodsNum = "";
        @SerializedName("goods_image")
        private String goodsImage = "";
        @SerializedName("goods_pay_price")
        private String goodsPayPrice = "";
        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("buyer_id")
        private String buyerId = "";
        @SerializedName("goods_type")
        private String goodsType = "";
        @SerializedName("sales_id")
        private String salesId = "";
        @SerializedName("commis_rate")
        private String commisRate = "";
        @SerializedName("gc_id")
        private String gcId = "";
        @SerializedName("goods_spec")
        private String goodsSpec = "";
        @SerializedName("goods_contractid")
        private String goodsContractid = "";
        @SerializedName("invite_rates")
        private String inviteRates = "";
        @SerializedName("goods_serial")
        private String goodsSerial = "";
        @SerializedName("goods_commonid")
        private String goodsCommonid = "";
        @SerializedName("add_time")
        private String addTime = "";
        @SerializedName("is_fx")
        private String isFx = "";
        @SerializedName("fx_commis_rate")
        private String fxCommisRate = "";
        @SerializedName("fx_member_id")
        private String fxMemberId = "";
        @SerializedName("image_60_url")
        private String image60Url = "";
        @SerializedName("image_240_url")
        private String image240Url = "";
        @SerializedName("goods_type_cn")
        private String goodsTypeCn = "";
        @SerializedName("goods_url")
        private String goodsUrl = "";
        @SerializedName("contractlist")
        private ArrayList<ContractlistBean> contractlist = new ArrayList<>();

        public String getRecId() {
            return recId;
        }

        public void setRecId(String recId) {
            this.recId = recId;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(String goodsId) {
            this.goodsId = goodsId;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public String getGoodsPrice() {
            return goodsPrice;
        }

        public void setGoodsPrice(String goodsPrice) {
            this.goodsPrice = goodsPrice;
        }

        public String getGoodsNum() {
            return goodsNum;
        }

        public void setGoodsNum(String goodsNum) {
            this.goodsNum = goodsNum;
        }

        public String getGoodsImage() {
            return goodsImage;
        }

        public void setGoodsImage(String goodsImage) {
            this.goodsImage = goodsImage;
        }

        public String getGoodsPayPrice() {
            return goodsPayPrice;
        }

        public void setGoodsPayPrice(String goodsPayPrice) {
            this.goodsPayPrice = goodsPayPrice;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getBuyerId() {
            return buyerId;
        }

        public void setBuyerId(String buyerId) {
            this.buyerId = buyerId;
        }

        public String getGoodsType() {
            return goodsType;
        }

        public void setGoodsType(String goodsType) {
            this.goodsType = goodsType;
        }

        public String getSalesId() {
            return salesId;
        }

        public void setSalesId(String salesId) {
            this.salesId = salesId;
        }

        public String getCommisRate() {
            return commisRate;
        }

        public void setCommisRate(String commisRate) {
            this.commisRate = commisRate;
        }

        public String getGcId() {
            return gcId;
        }

        public void setGcId(String gcId) {
            this.gcId = gcId;
        }

        public String getGoodsSpec() {
            return goodsSpec;
        }

        public void setGoodsSpec(String goodsSpec) {
            this.goodsSpec = goodsSpec;
        }

        public String getGoodsContractid() {
            return goodsContractid;
        }

        public void setGoodsContractid(String goodsContractid) {
            this.goodsContractid = goodsContractid;
        }

        public String getInviteRates() {
            return inviteRates;
        }

        public void setInviteRates(String inviteRates) {
            this.inviteRates = inviteRates;
        }

        public String getGoodsSerial() {
            return goodsSerial;
        }

        public void setGoodsSerial(String goodsSerial) {
            this.goodsSerial = goodsSerial;
        }

        public String getGoodsCommonid() {
            return goodsCommonid;
        }

        public void setGoodsCommonid(String goodsCommonid) {
            this.goodsCommonid = goodsCommonid;
        }

        public String getAddTime() {
            return addTime;
        }

        public void setAddTime(String addTime) {
            this.addTime = addTime;
        }

        public String getIsFx() {
            return isFx;
        }

        public void setIsFx(String isFx) {
            this.isFx = isFx;
        }

        public String getFxCommisRate() {
            return fxCommisRate;
        }

        public void setFxCommisRate(String fxCommisRate) {
            this.fxCommisRate = fxCommisRate;
        }

        public String getFxMemberId() {
            return fxMemberId;
        }

        public void setFxMemberId(String fxMemberId) {
            this.fxMemberId = fxMemberId;
        }

        public String getImage60Url() {
            return image60Url;
        }

        public void setImage60Url(String image60Url) {
            this.image60Url = image60Url;
        }

        public String getImage240Url() {
            return image240Url;
        }

        public void setImage240Url(String image240Url) {
            this.image240Url = image240Url;
        }

        public String getGoodsTypeCn() {
            return goodsTypeCn;
        }

        public void setGoodsTypeCn(String goodsTypeCn) {
            this.goodsTypeCn = goodsTypeCn;
        }

        public String getGoodsUrl() {
            return goodsUrl;
        }

        public void setGoodsUrl(String goodsUrl) {
            this.goodsUrl = goodsUrl;
        }

        public ArrayList<ContractlistBean> getContractlist() {
            return contractlist;
        }

        public void setContractlist(ArrayList<ContractlistBean> contractlist) {
            this.contractlist = contractlist;
        }

        public static class ContractlistBean {

            @SerializedName("cti_id")
            private String ctiId = "";
            @SerializedName("cti_name")
            private String ctiName = "";
            @SerializedName("cti_describe")
            private String ctiDescribe = "";
            @SerializedName("cti_cost")
            private String ctiCost = "";
            @SerializedName("cti_icon")
            private String ctiIcon = "";
            @SerializedName("cti_descurl")
            private String ctiDescurl = "";
            @SerializedName("cti_state")
            private String ctiState = "";
            @SerializedName("cti_sort")
            private String ctiSort = "";
            @SerializedName("cti_state_key")
            private String ctiStateKey = "";
            @SerializedName("cti_state_text")
            private String ctiStateText = "";
            @SerializedName("cti_icon_url")
            private String ctiIconUrl = "";
            @SerializedName("cti_icon_url_60")
            private String ctiIconUrl60 = "";
            @SerializedName("cti_state_name")
            private String ctiStateName = "";

            public String getCtiId() {
                return ctiId;
            }

            public void setCtiId(String ctiId) {
                this.ctiId = ctiId;
            }

            public String getCtiName() {
                return ctiName;
            }

            public void setCtiName(String ctiName) {
                this.ctiName = ctiName;
            }

            public String getCtiDescribe() {
                return ctiDescribe;
            }

            public void setCtiDescribe(String ctiDescribe) {
                this.ctiDescribe = ctiDescribe;
            }

            public String getCtiCost() {
                return ctiCost;
            }

            public void setCtiCost(String ctiCost) {
                this.ctiCost = ctiCost;
            }

            public String getCtiIcon() {
                return ctiIcon;
            }

            public void setCtiIcon(String ctiIcon) {
                this.ctiIcon = ctiIcon;
            }

            public String getCtiDescurl() {
                return ctiDescurl;
            }

            public void setCtiDescurl(String ctiDescurl) {
                this.ctiDescurl = ctiDescurl;
            }

            public String getCtiState() {
                return ctiState;
            }

            public void setCtiState(String ctiState) {
                this.ctiState = ctiState;
            }

            public String getCtiSort() {
                return ctiSort;
            }

            public void setCtiSort(String ctiSort) {
                this.ctiSort = ctiSort;
            }

            public String getCtiStateKey() {
                return ctiStateKey;
            }

            public void setCtiStateKey(String ctiStateKey) {
                this.ctiStateKey = ctiStateKey;
            }

            public String getCtiStateText() {
                return ctiStateText;
            }

            public void setCtiStateText(String ctiStateText) {
                this.ctiStateText = ctiStateText;
            }

            public String getCtiIconUrl() {
                return ctiIconUrl;
            }

            public void setCtiIconUrl(String ctiIconUrl) {
                this.ctiIconUrl = ctiIconUrl;
            }

            public String getCtiIconUrl60() {
                return ctiIconUrl60;
            }

            public void setCtiIconUrl60(String ctiIconUrl60) {
                this.ctiIconUrl60 = ctiIconUrl60;
            }

            public String getCtiStateName() {
                return ctiStateName;
            }

            public void setCtiStateName(String ctiStateName) {
                this.ctiStateName = ctiStateName;
            }

        }

    }

}
