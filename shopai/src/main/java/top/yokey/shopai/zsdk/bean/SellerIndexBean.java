package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class SellerIndexBean implements Serializable {

    @SerializedName("seller_info")
    private SellerInfoBean sellerInfo = null;
    @SerializedName("store_info")
    private StoreInfoBean storeInfo = null;
    @SerializedName("statics")
    private StaticsBean statics = null;

    public SellerInfoBean getSellerInfo() {
        return sellerInfo;
    }

    public void setSellerInfo(SellerInfoBean sellerInfo) {
        this.sellerInfo = sellerInfo;
    }

    public StoreInfoBean getStoreInfo() {
        return storeInfo;
    }

    public void setStoreInfo(StoreInfoBean storeInfo) {
        this.storeInfo = storeInfo;
    }

    public StaticsBean getStatics() {
        return statics;
    }

    public void setStatics(StaticsBean statics) {
        this.statics = statics;
    }

    public static class SellerInfoBean {

        @SerializedName("seller_id")
        private String sellerId = "";
        @SerializedName("seller_name")
        private String sellerName = "";
        @SerializedName("member_id")
        private String memberId = "";
        @SerializedName("seller_group_id")
        private String sellerGroupId = "";
        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("is_admin")
        private String isAdmin = "";
        @SerializedName("seller_quicklink")
        private String sellerQuicklink = "";
        @SerializedName("last_login_time")
        private String lastLoginTime = "";
        @SerializedName("is_client")
        private String isClient = "";
        @SerializedName("client_type")
        private String clientType = "";
        @SerializedName("last_login_time_fmt")
        private String lastLoginTimeFmt = "";
        @SerializedName("order_nopay_count")
        private String orderNopayCount = "";
        @SerializedName("order_noreceipt_count")
        private String orderNoreceiptCount = "";

        public String getSellerId() {
            return sellerId;
        }

        public void setSellerId(String sellerId) {
            this.sellerId = sellerId;
        }

        public String getSellerName() {
            return sellerName;
        }

        public void setSellerName(String sellerName) {
            this.sellerName = sellerName;
        }

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public String getSellerGroupId() {
            return sellerGroupId;
        }

        public void setSellerGroupId(String sellerGroupId) {
            this.sellerGroupId = sellerGroupId;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getIsAdmin() {
            return isAdmin;
        }

        public void setIsAdmin(String isAdmin) {
            this.isAdmin = isAdmin;
        }

        public String getSellerQuicklink() {
            return sellerQuicklink;
        }

        public void setSellerQuicklink(String sellerQuicklink) {
            this.sellerQuicklink = sellerQuicklink;
        }

        public String getLastLoginTime() {
            return lastLoginTime;
        }

        public void setLastLoginTime(String lastLoginTime) {
            this.lastLoginTime = lastLoginTime;
        }

        public String getIsClient() {
            return isClient;
        }

        public void setIsClient(String isClient) {
            this.isClient = isClient;
        }

        public String getClientType() {
            return clientType;
        }

        public void setClientType(String clientType) {
            this.clientType = clientType;
        }

        public String getLastLoginTimeFmt() {
            return lastLoginTimeFmt;
        }

        public void setLastLoginTimeFmt(String lastLoginTimeFmt) {
            this.lastLoginTimeFmt = lastLoginTimeFmt;
        }

        public String getOrderNopayCount() {
            return orderNopayCount;
        }

        public void setOrderNopayCount(String orderNopayCount) {
            this.orderNopayCount = orderNopayCount;
        }

        public String getOrderNoreceiptCount() {
            return orderNoreceiptCount;
        }

        public void setOrderNoreceiptCount(String orderNoreceiptCount) {
            this.orderNoreceiptCount = orderNoreceiptCount;
        }

    }

    public static class StoreInfoBean {

        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("store_name")
        private String storeName = "";
        @SerializedName("grade_id")
        private String gradeId = "";
        @SerializedName("member_id")
        private String memberId = "";
        @SerializedName("member_name")
        private String memberName = "";
        @SerializedName("seller_name")
        private String sellerName = "";
        @SerializedName("sc_id")
        private String scId = "";
        @SerializedName("store_company_name")
        private String storeCompanyName = "";
        @SerializedName("province_id")
        private String provinceId = "";
        @SerializedName("area_info")
        private String areaInfo = "";
        @SerializedName("store_address")
        private String storeAddress = "";
        @SerializedName("store_zip")
        private String storeZip = "";
        @SerializedName("store_state")
        private String storeState = "";
        @SerializedName("store_close_info")
        private String storeCloseInfo = "";
        @SerializedName("store_sort")
        private String storeSort = "";
        @SerializedName("store_time")
        private String storeTime = "";
        @SerializedName("store_end_time")
        private String storeEndTime = "";
        @SerializedName("store_label")
        private String storeLabel = "";
        @SerializedName("store_banner")
        private String storeBanner = "";
        @SerializedName("store_avatar")
        private String storeAvatar = "";
        @SerializedName("store_keywords")
        private String storeKeywords = "";
        @SerializedName("store_description")
        private String storeDescription = "";
        @SerializedName("store_qq")
        private String storeQq = "";
        @SerializedName("store_ww")
        private String storeWw = "";
        @SerializedName("store_phone")
        private String storePhone = "";
        @SerializedName("store_zy")
        private String storeZy = "";
        @SerializedName("store_domain")
        private String storeDomain = "";
        @SerializedName("store_domain_times")
        private String storeDomainTimes = "";
        @SerializedName("store_recommend")
        private String storeRecommend = "";
        @SerializedName("store_theme")
        private String storeTheme = "";
        @SerializedName("store_credit")
        private StoreCreditBean storeCredit = null;
        @SerializedName("store_desccredit")
        private String storeDesccredit = "";
        @SerializedName("store_servicecredit")
        private String storeServicecredit = "";
        @SerializedName("store_deliverycredit")
        private String storeDeliverycredit = "";
        @SerializedName("store_collect")
        private String storeCollect = "";
        @SerializedName("store_slide")
        private String storeSlide = "";
        @SerializedName("store_slide_url")
        private String storeSlideUrl = "";
        @SerializedName("store_stamp")
        private String storeStamp = "";
        @SerializedName("store_printdesc")
        private String storePrintdesc = "";
        @SerializedName("store_sales")
        private String storeSales = "";
        @SerializedName("store_workingtime")
        private String storeWorkingtime = "";
        @SerializedName("store_free_price")
        private String storeFreePrice = "";
        @SerializedName("store_free_time")
        private String storeFreeTime = "";
        @SerializedName("store_decoration_switch")
        private String storeDecorationSwitch = "";
        @SerializedName("store_decoration_only")
        private String storeDecorationOnly = "";
        @SerializedName("store_decoration_image_count")
        private String storeDecorationImageCount = "";
        @SerializedName("is_own_shop")
        private String isOwnShop = "";
        @SerializedName("bind_all_gc")
        private String bindAllGc = "";
        @SerializedName("store_vrcode_prefix")
        private String storeVrcodePrefix = "";
        @SerializedName("mb_title_img")
        private String mbTitleImg = "";
        @SerializedName("mb_sliders")
        private String mbSliders = "";
        @SerializedName("left_bar_type")
        private String leftBarType = "";
        @SerializedName("deliver_region")
        private String deliverRegion = "";
        @SerializedName("is_distribution")
        private String isDistribution = "";
        @SerializedName("is_person")
        private String isPerson = "";
        @SerializedName("mb_store_decoration_switch")
        private String mbStoreDecorationSwitch = "";
        @SerializedName("goods_count")
        private String goodsCount = "";
        @SerializedName("store_credit_average")
        private String storeCreditAverage = "";
        @SerializedName("store_credit_percent")
        private String storeCreditPercent = "";
        @SerializedName("grade_name")
        private String gradeName = "";
        @SerializedName("grade_goodslimit")
        private String gradeGoodslimit = "";
        @SerializedName("grade_albumlimit")
        private String gradeAlbumlimit = "";
        @SerializedName("daily_sales")
        private DailySalesBean dailySales = null;
        @SerializedName("monthly_sales")
        private MonthlySalesBean monthlySales = null;

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public String getMemberName() {
            return memberName;
        }

        public void setMemberName(String memberName) {
            this.memberName = memberName;
        }

        public String getSellerName() {
            return sellerName;
        }

        public void setSellerName(String sellerName) {
            this.sellerName = sellerName;
        }

        public String getScId() {
            return scId;
        }

        public void setScId(String scId) {
            this.scId = scId;
        }

        public String getStoreCompanyName() {
            return storeCompanyName;
        }

        public void setStoreCompanyName(String storeCompanyName) {
            this.storeCompanyName = storeCompanyName;
        }

        public String getProvinceId() {
            return provinceId;
        }

        public void setProvinceId(String provinceId) {
            this.provinceId = provinceId;
        }

        public String getAreaInfo() {
            return areaInfo;
        }

        public void setAreaInfo(String areaInfo) {
            this.areaInfo = areaInfo;
        }

        public String getStoreAddress() {
            return storeAddress;
        }

        public void setStoreAddress(String storeAddress) {
            this.storeAddress = storeAddress;
        }

        public String getStoreZip() {
            return storeZip;
        }

        public void setStoreZip(String storeZip) {
            this.storeZip = storeZip;
        }

        public String getStoreState() {
            return storeState;
        }

        public void setStoreState(String storeState) {
            this.storeState = storeState;
        }

        public String getStoreCloseInfo() {
            return storeCloseInfo;
        }

        public void setStoreCloseInfo(String storeCloseInfo) {
            this.storeCloseInfo = storeCloseInfo;
        }

        public String getStoreSort() {
            return storeSort;
        }

        public void setStoreSort(String storeSort) {
            this.storeSort = storeSort;
        }

        public String getStoreTime() {
            return storeTime;
        }

        public void setStoreTime(String storeTime) {
            this.storeTime = storeTime;
        }

        public String getStoreEndTime() {
            return storeEndTime;
        }

        public void setStoreEndTime(String storeEndTime) {
            this.storeEndTime = storeEndTime;
        }

        public String getStoreLabel() {
            return storeLabel;
        }

        public void setStoreLabel(String storeLabel) {
            this.storeLabel = storeLabel;
        }

        public String getStoreBanner() {
            return storeBanner;
        }

        public void setStoreBanner(String storeBanner) {
            this.storeBanner = storeBanner;
        }

        public String getStoreAvatar() {
            return storeAvatar;
        }

        public void setStoreAvatar(String storeAvatar) {
            this.storeAvatar = storeAvatar;
        }

        public String getStoreKeywords() {
            return storeKeywords;
        }

        public void setStoreKeywords(String storeKeywords) {
            this.storeKeywords = storeKeywords;
        }

        public String getStoreDescription() {
            return storeDescription;
        }

        public void setStoreDescription(String storeDescription) {
            this.storeDescription = storeDescription;
        }

        public String getStoreQq() {
            return storeQq;
        }

        public void setStoreQq(String storeQq) {
            this.storeQq = storeQq;
        }

        public String getStoreWw() {
            return storeWw;
        }

        public void setStoreWw(String storeWw) {
            this.storeWw = storeWw;
        }

        public String getStorePhone() {
            return storePhone;
        }

        public void setStorePhone(String storePhone) {
            this.storePhone = storePhone;
        }

        public String getStoreZy() {
            return storeZy;
        }

        public void setStoreZy(String storeZy) {
            this.storeZy = storeZy;
        }

        public String getStoreDomain() {
            return storeDomain;
        }

        public void setStoreDomain(String storeDomain) {
            this.storeDomain = storeDomain;
        }

        public String getStoreDomainTimes() {
            return storeDomainTimes;
        }

        public void setStoreDomainTimes(String storeDomainTimes) {
            this.storeDomainTimes = storeDomainTimes;
        }

        public String getStoreRecommend() {
            return storeRecommend;
        }

        public void setStoreRecommend(String storeRecommend) {
            this.storeRecommend = storeRecommend;
        }

        public String getStoreTheme() {
            return storeTheme;
        }

        public void setStoreTheme(String storeTheme) {
            this.storeTheme = storeTheme;
        }

        public StoreCreditBean getStoreCredit() {
            return storeCredit;
        }

        public void setStoreCredit(StoreCreditBean storeCredit) {
            this.storeCredit = storeCredit;
        }

        public String getStoreDesccredit() {
            return storeDesccredit;
        }

        public void setStoreDesccredit(String storeDesccredit) {
            this.storeDesccredit = storeDesccredit;
        }

        public String getStoreServicecredit() {
            return storeServicecredit;
        }

        public void setStoreServicecredit(String storeServicecredit) {
            this.storeServicecredit = storeServicecredit;
        }

        public String getStoreDeliverycredit() {
            return storeDeliverycredit;
        }

        public void setStoreDeliverycredit(String storeDeliverycredit) {
            this.storeDeliverycredit = storeDeliverycredit;
        }

        public String getStoreCollect() {
            return storeCollect;
        }

        public void setStoreCollect(String storeCollect) {
            this.storeCollect = storeCollect;
        }

        public String getStoreSlide() {
            return storeSlide;
        }

        public void setStoreSlide(String storeSlide) {
            this.storeSlide = storeSlide;
        }

        public String getStoreSlideUrl() {
            return storeSlideUrl;
        }

        public void setStoreSlideUrl(String storeSlideUrl) {
            this.storeSlideUrl = storeSlideUrl;
        }

        public String getStoreStamp() {
            return storeStamp;
        }

        public void setStoreStamp(String storeStamp) {
            this.storeStamp = storeStamp;
        }

        public String getStorePrintdesc() {
            return storePrintdesc;
        }

        public void setStorePrintdesc(String storePrintdesc) {
            this.storePrintdesc = storePrintdesc;
        }

        public String getStoreSales() {
            return storeSales;
        }

        public void setStoreSales(String storeSales) {
            this.storeSales = storeSales;
        }

        public String getStoreWorkingtime() {
            return storeWorkingtime;
        }

        public void setStoreWorkingtime(String storeWorkingtime) {
            this.storeWorkingtime = storeWorkingtime;
        }

        public String getStoreFreePrice() {
            return storeFreePrice;
        }

        public void setStoreFreePrice(String storeFreePrice) {
            this.storeFreePrice = storeFreePrice;
        }

        public String getStoreFreeTime() {
            return storeFreeTime;
        }

        public void setStoreFreeTime(String storeFreeTime) {
            this.storeFreeTime = storeFreeTime;
        }

        public String getStoreDecorationSwitch() {
            return storeDecorationSwitch;
        }

        public void setStoreDecorationSwitch(String storeDecorationSwitch) {
            this.storeDecorationSwitch = storeDecorationSwitch;
        }

        public String getStoreDecorationOnly() {
            return storeDecorationOnly;
        }

        public void setStoreDecorationOnly(String storeDecorationOnly) {
            this.storeDecorationOnly = storeDecorationOnly;
        }

        public String getStoreDecorationImageCount() {
            return storeDecorationImageCount;
        }

        public void setStoreDecorationImageCount(String storeDecorationImageCount) {
            this.storeDecorationImageCount = storeDecorationImageCount;
        }

        public String getIsOwnShop() {
            return isOwnShop;
        }

        public void setIsOwnShop(String isOwnShop) {
            this.isOwnShop = isOwnShop;
        }

        public String getBindAllGc() {
            return bindAllGc;
        }

        public void setBindAllGc(String bindAllGc) {
            this.bindAllGc = bindAllGc;
        }

        public String getStoreVrcodePrefix() {
            return storeVrcodePrefix;
        }

        public void setStoreVrcodePrefix(String storeVrcodePrefix) {
            this.storeVrcodePrefix = storeVrcodePrefix;
        }

        public String getMbTitleImg() {
            return mbTitleImg;
        }

        public void setMbTitleImg(String mbTitleImg) {
            this.mbTitleImg = mbTitleImg;
        }

        public String getMbSliders() {
            return mbSliders;
        }

        public void setMbSliders(String mbSliders) {
            this.mbSliders = mbSliders;
        }

        public String getLeftBarType() {
            return leftBarType;
        }

        public void setLeftBarType(String leftBarType) {
            this.leftBarType = leftBarType;
        }

        public String getDeliverRegion() {
            return deliverRegion;
        }

        public void setDeliverRegion(String deliverRegion) {
            this.deliverRegion = deliverRegion;
        }

        public String getIsDistribution() {
            return isDistribution;
        }

        public void setIsDistribution(String isDistribution) {
            this.isDistribution = isDistribution;
        }

        public String getIsPerson() {
            return isPerson;
        }

        public void setIsPerson(String isPerson) {
            this.isPerson = isPerson;
        }

        public String getMbStoreDecorationSwitch() {
            return mbStoreDecorationSwitch;
        }

        public void setMbStoreDecorationSwitch(String mbStoreDecorationSwitch) {
            this.mbStoreDecorationSwitch = mbStoreDecorationSwitch;
        }

        public String getGoodsCount() {
            return goodsCount;
        }

        public void setGoodsCount(String goodsCount) {
            this.goodsCount = goodsCount;
        }

        public String getStoreCreditAverage() {
            return storeCreditAverage;
        }

        public void setStoreCreditAverage(String storeCreditAverage) {
            this.storeCreditAverage = storeCreditAverage;
        }

        public String getStoreCreditPercent() {
            return storeCreditPercent;
        }

        public void setStoreCreditPercent(String storeCreditPercent) {
            this.storeCreditPercent = storeCreditPercent;
        }

        public String getGradeName() {
            return gradeName;
        }

        public void setGradeName(String gradeName) {
            this.gradeName = gradeName;
        }

        public String getGradeGoodslimit() {
            return gradeGoodslimit;
        }

        public void setGradeGoodslimit(String gradeGoodslimit) {
            this.gradeGoodslimit = gradeGoodslimit;
        }

        public String getGradeAlbumlimit() {
            return gradeAlbumlimit;
        }

        public void setGradeAlbumlimit(String gradeAlbumlimit) {
            this.gradeAlbumlimit = gradeAlbumlimit;
        }

        public DailySalesBean getDailySales() {
            return dailySales;
        }

        public void setDailySales(DailySalesBean dailySales) {
            this.dailySales = dailySales;
        }

        public MonthlySalesBean getMonthlySales() {
            return monthlySales;
        }

        public void setMonthlySales(MonthlySalesBean monthlySales) {
            this.monthlySales = monthlySales;
        }

        public static class StoreCreditBean {

            @SerializedName("store_desccredit")
            private StoreDesccreditBean storeDesccredit = null;
            @SerializedName("store_servicecredit")
            private StoreServicecreditBean storeServicecredit = null;
            @SerializedName("store_deliverycredit")
            private StoreDeliverycreditBean storeDeliverycredit = null;

            public StoreDesccreditBean getStoreDesccredit() {
                return storeDesccredit;
            }

            public void setStoreDesccredit(StoreDesccreditBean storeDesccredit) {
                this.storeDesccredit = storeDesccredit;
            }

            public StoreServicecreditBean getStoreServicecredit() {
                return storeServicecredit;
            }

            public void setStoreServicecredit(StoreServicecreditBean storeServicecredit) {
                this.storeServicecredit = storeServicecredit;
            }

            public StoreDeliverycreditBean getStoreDeliverycredit() {
                return storeDeliverycredit;
            }

            public void setStoreDeliverycredit(StoreDeliverycreditBean storeDeliverycredit) {
                this.storeDeliverycredit = storeDeliverycredit;
            }

            public static class StoreDesccreditBean {

                @SerializedName("text")
                private String text = "";
                @SerializedName("credit")
                private String credit = "";

                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }

                public String getCredit() {
                    return credit;
                }

                public void setCredit(String credit) {
                    this.credit = credit;
                }

            }

            public static class StoreServicecreditBean {

                @SerializedName("text")
                private String text = "";
                @SerializedName("credit")
                private String credit = "";

                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }

                public String getCredit() {
                    return credit;
                }

                public void setCredit(String credit) {
                    this.credit = credit;
                }

            }

            public static class StoreDeliverycreditBean {

                @SerializedName("text")
                private String text = "";
                @SerializedName("credit")
                private String credit = "";

                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }

                public String getCredit() {
                    return credit;
                }

                public void setCredit(String credit) {
                    this.credit = credit;
                }

            }

        }

        public static class DailySalesBean {

            @SerializedName("ordernum")
            private String ordernum = "";
            @SerializedName("orderamount")
            private String orderamount = "";

            public String getOrdernum() {
                return ordernum;
            }

            public void setOrdernum(String ordernum) {
                this.ordernum = ordernum;
            }

            public String getOrderamount() {
                return orderamount;
            }

            public void setOrderamount(String orderamount) {
                this.orderamount = orderamount;
            }

        }

        public static class MonthlySalesBean {

            @SerializedName("ordernum")
            private String ordernum = "";
            @SerializedName("orderamount")
            private String orderamount = "";

            public String getOrdernum() {
                return ordernum;
            }

            public void setOrdernum(String ordernum) {
                this.ordernum = ordernum;
            }

            public String getOrderamount() {
                return orderamount;
            }

            public void setOrderamount(String orderamount) {
                this.orderamount = orderamount;
            }

        }

    }

    public static class StaticsBean {

        @SerializedName("goodscount")
        private String goodscount = "";
        @SerializedName("online")
        private String online = "";
        @SerializedName("waitverify")
        private String waitverify = "";
        @SerializedName("verifyfail")
        private String verifyfail = "";
        @SerializedName("offline")
        private String offline = "";
        @SerializedName("lockup")
        private String lockup = "";
        @SerializedName("imagecount")
        private String imagecount = "";
        @SerializedName("consult")
        private String consult = "";
        @SerializedName("progressing")
        private String progressing = "";
        @SerializedName("payment")
        private String payment = "";
        @SerializedName("delivery")
        private String delivery = "";
        @SerializedName("refund_lock")
        private String refundLock = "";
        @SerializedName("refund")
        private String refund = "";
        @SerializedName("return_lock")
        private String returnLock = "";
        @SerializedName("return")
        private String returnX = "";
        @SerializedName("complain")
        private String complain = "";
        @SerializedName("bill_confirm")
        private String billConfirm = "";

        public String getGoodscount() {
            return goodscount;
        }

        public void setGoodscount(String goodscount) {
            this.goodscount = goodscount;
        }

        public String getOnline() {
            return online;
        }

        public void setOnline(String online) {
            this.online = online;
        }

        public String getWaitverify() {
            return waitverify;
        }

        public void setWaitverify(String waitverify) {
            this.waitverify = waitverify;
        }

        public String getVerifyfail() {
            return verifyfail;
        }

        public void setVerifyfail(String verifyfail) {
            this.verifyfail = verifyfail;
        }

        public String getOffline() {
            return offline;
        }

        public void setOffline(String offline) {
            this.offline = offline;
        }

        public String getLockup() {
            return lockup;
        }

        public void setLockup(String lockup) {
            this.lockup = lockup;
        }

        public String getImagecount() {
            return imagecount;
        }

        public void setImagecount(String imagecount) {
            this.imagecount = imagecount;
        }

        public String getConsult() {
            return consult;
        }

        public void setConsult(String consult) {
            this.consult = consult;
        }

        public String getProgressing() {
            return progressing;
        }

        public void setProgressing(String progressing) {
            this.progressing = progressing;
        }

        public String getPayment() {
            return payment;
        }

        public void setPayment(String payment) {
            this.payment = payment;
        }

        public String getDelivery() {
            return delivery;
        }

        public void setDelivery(String delivery) {
            this.delivery = delivery;
        }

        public String getRefundLock() {
            return refundLock;
        }

        public void setRefundLock(String refundLock) {
            this.refundLock = refundLock;
        }

        public String getRefund() {
            return refund;
        }

        public void setRefund(String refund) {
            this.refund = refund;
        }

        public String getReturnLock() {
            return returnLock;
        }

        public void setReturnLock(String returnLock) {
            this.returnLock = returnLock;
        }

        public String getReturnX() {
            return returnX;
        }

        public void setReturnX(String returnX) {
            this.returnX = returnX;
        }

        public String getComplain() {
            return complain;
        }

        public void setComplain(String complain) {
            this.complain = complain;
        }

        public String getBillConfirm() {
            return billConfirm;
        }

        public void setBillConfirm(String billConfirm) {
            this.billConfirm = billConfirm;
        }

    }

}
