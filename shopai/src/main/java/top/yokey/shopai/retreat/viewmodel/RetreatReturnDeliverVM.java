package top.yokey.shopai.retreat.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.RetreatReturnDeliverBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberReturnController;

public class RetreatReturnDeliverVM extends BaseViewModel {

    private final MutableLiveData<RetreatReturnDeliverBean> deliverLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> postLiveData = new MutableLiveData<>();

    public RetreatReturnDeliverVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<RetreatReturnDeliverBean> getDeliverLiveData() {

        return deliverLiveData;

    }

    public MutableLiveData<String> getPostLiveData() {

        return postLiveData;

    }

    public void getDeliver(String returnId) {

        MemberReturnController.shipForm(returnId, new HttpCallBack<RetreatReturnDeliverBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, RetreatReturnDeliverBean bean) {
                deliverLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void post(String returnId, String expressId, String invoiceNo) {

        MemberReturnController.shipPost(returnId, expressId, invoiceNo, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                postLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
