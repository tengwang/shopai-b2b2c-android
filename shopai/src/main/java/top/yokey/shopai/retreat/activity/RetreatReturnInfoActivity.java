package top.yokey.shopai.retreat.activity;

import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import top.yokey.shopai.R;
import top.yokey.shopai.retreat.viewmodel.RetreatRefundInfoVM;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.VerifyUtil;

@Route(path = ARoutePath.RETREAT_RETURN_INFO)
public class RetreatReturnInfoActivity extends BaseActivity {

    private final AppCompatImageView[] mainImageView = new AppCompatImageView[3];
    @Autowired(name = Constant.DATA_ID)
    String refundId;
    private Toolbar mainToolbar = null;
    private AppCompatTextView snTextView = null;
    private AppCompatTextView reasonTextView = null;
    private AppCompatTextView priceTextView = null;
    private AppCompatTextView remarkTextView = null;
    private AppCompatTextView storeStateTextView = null;
    private AppCompatTextView storeRemarkTextView = null;
    private AppCompatTextView platformStateTextView = null;
    private AppCompatTextView platformRemarkTextView = null;
    private AppCompatTextView paymentTextView = null;
    private AppCompatTextView onlinePriceTextView = null;
    private AppCompatTextView preDepositPriceTextView = null;
    private AppCompatTextView rechargeCardPriceTextView = null;
    private RetreatRefundInfoVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_retreat_return_info);
        mainToolbar = findViewById(R.id.mainToolbar);
        snTextView = findViewById(R.id.snTextView);
        reasonTextView = findViewById(R.id.reasonTextView);
        priceTextView = findViewById(R.id.priceTextView);
        remarkTextView = findViewById(R.id.remarkTextView);
        mainImageView[0] = findViewById(R.id.zeroImageView);
        mainImageView[1] = findViewById(R.id.oneImageView);
        mainImageView[2] = findViewById(R.id.twoImageView);
        storeStateTextView = findViewById(R.id.storeStateTextView);
        storeRemarkTextView = findViewById(R.id.storeRemarkTextView);
        platformStateTextView = findViewById(R.id.platformStateTextView);
        platformRemarkTextView = findViewById(R.id.platformRemarkTextView);
        paymentTextView = findViewById(R.id.paymentTextView);
        onlinePriceTextView = findViewById(R.id.onlinePriceTextView);
        preDepositPriceTextView = findViewById(R.id.preDepositPriceTextView);
        rechargeCardPriceTextView = findViewById(R.id.rechargeCardPriceTextView);

    }

    @Override
    public void initData() {

        if (VerifyUtil.isEmpty(refundId)) {
            ToastHelp.get().showDataError();
            onReturn(false);
            return;
        }
        mainImageView[0].setVisibility(View.GONE);
        mainImageView[1].setVisibility(View.GONE);
        mainImageView[2].setVisibility(View.GONE);
        setToolbar(mainToolbar, R.string.refundDetailed);
        observeKeyborad(R.id.mainLinearLayout);
        vm = getVM(RetreatRefundInfoVM.class);
        vm.getInfo(refundId);

    }

    @Override
    public void initEvent() {

    }

    @Override
    public void initObserve() {

        vm.getInfoLiveData().observe(this, bean -> {
            snTextView.setText(bean.getRefund().getRefundSn());
            reasonTextView.setText(bean.getRefund().getReasonInfo());
            priceTextView.setText("￥");
            priceTextView.append(bean.getRefund().getRefundAmount());
            remarkTextView.setText(bean.getRefund().getBuyerMessage());
            storeStateTextView.setText(bean.getRefund().getSellerState());
            storeRemarkTextView.setText(bean.getRefund().getSellerMessage());
            platformStateTextView.setText(bean.getRefund().getAdminState());
            platformRemarkTextView.setText(bean.getRefund().getAdminMessage());
            if (bean.getDetailArray() != null) {
                paymentTextView.setText(bean.getDetailArray().getRefundCode());
                onlinePriceTextView.setText("￥");
                onlinePriceTextView.append(bean.getDetailArray().getPayAmount());
                preDepositPriceTextView.setText("￥");
                preDepositPriceTextView.append(bean.getDetailArray().getPdAmount());
                rechargeCardPriceTextView.setText("￥");
                rechargeCardPriceTextView.append(bean.getDetailArray().getRcbAmount());
            }
            if (bean.getPicList() != null) {
                for (int i = 0; i < bean.getPicList().size(); i++) {
                    mainImageView[i].setVisibility(View.VISIBLE);
                    ImageHelp.get().display(bean.getPicList().get(i), mainImageView[i]);
                }
            }
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getInfo(refundId));
            }
        });

    }

}
