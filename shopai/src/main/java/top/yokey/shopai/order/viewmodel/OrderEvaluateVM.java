package top.yokey.shopai.order.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.io.File;
import java.util.HashMap;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.FileUploadBean;
import top.yokey.shopai.zsdk.bean.OrderEvaluateBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberEvaluateController;
import top.yokey.shopai.zsdk.controller.SnsAlbumController;

public class OrderEvaluateVM extends BaseViewModel {

    private final MutableLiveData<OrderEvaluateBean> evaluateLiveData = new MutableLiveData<>();
    private final MutableLiveData<FileUploadBean> fileUploadLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> saveLiveData = new MutableLiveData<>();

    public OrderEvaluateVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<OrderEvaluateBean> getEvaluateLiveData() {

        return evaluateLiveData;

    }

    public MutableLiveData<FileUploadBean> getFileUploadLiveData() {

        return fileUploadLiveData;

    }

    public MutableLiveData<String> getSaveLiveData() {

        return saveLiveData;

    }

    public void getEvaluate(String orderId) {

        MemberEvaluateController.index(orderId, new HttpCallBack<OrderEvaluateBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, OrderEvaluateBean bean) {
                evaluateLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void fileUpload(File file) {

        SnsAlbumController.fileUpload(file, new HttpCallBack<FileUploadBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, FileUploadBean bean) {
                fileUploadLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void save(HashMap<String, String> hashMap) {

        MemberEvaluateController.save(hashMap, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                saveLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

}
