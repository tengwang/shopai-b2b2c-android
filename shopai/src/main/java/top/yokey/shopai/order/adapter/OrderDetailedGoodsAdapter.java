package top.yokey.shopai.order.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zsdk.bean.OrderInfoBean;

public class OrderDetailedGoodsAdapter extends RecyclerView.Adapter<OrderDetailedGoodsAdapter.ViewHolder> {

    private final ArrayList<OrderInfoBean.GoodsListBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public OrderDetailedGoodsAdapter(ArrayList<OrderInfoBean.GoodsListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        OrderInfoBean.GoodsListBean bean = arrayList.get(position);
        ImageHelp.get().displayRadius(bean.getImageUrl(), holder.mainImageView);
        holder.nameTextView.setText(bean.getGoodsName());
        holder.specTextView.setText(bean.getGoodsSpec());
        holder.priceTextView.setText("￥");
        holder.priceTextView.append(bean.getGoodsPrice());
        holder.numberTextView.setText("x");
        holder.numberTextView.append(bean.getGoodsNum());
        if (Integer.parseInt(bean.getRefund()) == 0) {
            holder.refundTextView.setVisibility(View.GONE);
            holder.returnTextView.setVisibility(View.GONE);
        } else {
            holder.refundTextView.setVisibility(View.VISIBLE);
            holder.returnTextView.setVisibility(View.VISIBLE);
        }

        holder.refundTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onRefund(position, bean);
            }
        });

        holder.returnTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onReturn(position, bean);
            }
        });

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_order_detailed_goods, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, OrderInfoBean.GoodsListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onRefund(int position, OrderInfoBean.GoodsListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onReturn(int position, OrderInfoBean.GoodsListBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatImageView mainImageView;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView specTextView;
        private final AppCompatTextView priceTextView;
        private final AppCompatTextView numberTextView;
        private final AppCompatTextView refundTextView;
        private final AppCompatTextView returnTextView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainImageView = view.findViewById(R.id.mainImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            specTextView = view.findViewById(R.id.specTextView);
            priceTextView = view.findViewById(R.id.priceTextView);
            numberTextView = view.findViewById(R.id.numberTextView);
            refundTextView = view.findViewById(R.id.refundTextView);
            returnTextView = view.findViewById(R.id.returnTextView);

        }

    }

}
