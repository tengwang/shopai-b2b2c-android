package top.yokey.shopai.order.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.OrderPinGouBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.PinGouController;
import top.yokey.shopai.zsdk.data.OrderPinGouData;

public class OrderPinGouVM extends BaseViewModel {

    private final MutableLiveData<OrderPinGouBean> orderLiveData = new MutableLiveData<>();

    public OrderPinGouVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<OrderPinGouBean> getOrderLiveData() {

        return orderLiveData;

    }

    public void getOrder(OrderPinGouData data) {

        PinGouController.info(data, new HttpCallBack<OrderPinGouBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, OrderPinGouBean bean) {
                orderLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
