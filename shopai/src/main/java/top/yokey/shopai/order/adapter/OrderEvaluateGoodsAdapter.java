package top.yokey.shopai.order.adapter;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zsdk.bean.OrderEvaluateBean;

public class OrderEvaluateGoodsAdapter extends RecyclerView.Adapter<OrderEvaluateGoodsAdapter.ViewHolder> {

    private final ArrayList<OrderEvaluateBean.OrderGoodsBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public OrderEvaluateGoodsAdapter(ArrayList<OrderEvaluateBean.OrderGoodsBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        OrderEvaluateBean.OrderGoodsBean bean = arrayList.get(position);
        ImageHelp.get().displayRadius(bean.getGoodsImageUrl(), holder.mainImageView);
        holder.nameTextView.setText(bean.getGoodsName());
        holder.specTextView.setText(bean.getGoodsSpec());
        if (VerifyUtil.isEmpty(bean.getEvaluateImage0())) {
            holder.zeroImageView.setBackgroundResource(R.mipmap.ic_add_img);
        } else {
            ImageHelp.get().displayRadius(bean.getEvaluateImage0(), holder.zeroImageView);
        }
        if (VerifyUtil.isEmpty(bean.getEvaluateImage1())) {
            holder.oneImageView.setBackgroundResource(R.mipmap.ic_add_img);
        } else {
            ImageHelp.get().displayRadius(bean.getEvaluateImage1(), holder.oneImageView);
        }
        if (VerifyUtil.isEmpty(bean.getEvaluateImage2())) {
            holder.twoImageView.setBackgroundResource(R.mipmap.ic_add_img);
        } else {
            ImageHelp.get().displayRadius(bean.getEvaluateImage2(), holder.twoImageView);
        }
        if (VerifyUtil.isEmpty(bean.getEvaluateImage3())) {
            holder.thrImageView.setBackgroundResource(R.mipmap.ic_add_img);
        } else {
            ImageHelp.get().displayRadius(bean.getEvaluateImage3(), holder.thrImageView);
        }
        if (VerifyUtil.isEmpty(bean.getEvaluateImage4())) {
            holder.fouImageView.setBackgroundResource(R.mipmap.ic_add_img);
        } else {
            ImageHelp.get().displayRadius(bean.getEvaluateImage4(), holder.fouImageView);
        }

        holder.mainRatingBar.setOnRatingBarChangeListener((ratingBar, v, b) -> arrayList.get(position).setEvaluateRating(v + ""));

        holder.evaluateEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                arrayList.get(position).setEvaluateContent(Objects.requireNonNull(holder.evaluateEditText.getText()).toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        holder.zeroImageView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickImage(position, 0, bean);
            }
        });

        holder.oneImageView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickImage(position, 1, bean);
            }
        });

        holder.twoImageView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickImage(position, 2, bean);
            }
        });

        holder.thrImageView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickImage(position, 3, bean);
            }
        });

        holder.fouImageView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickImage(position, 4, bean);
            }
        });

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_order_evaluate_goods, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, OrderEvaluateBean.OrderGoodsBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickImage(int position, int positionImage, OrderEvaluateBean.OrderGoodsBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatImageView mainImageView;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView specTextView;
        private final AppCompatRatingBar mainRatingBar;
        private final AppCompatEditText evaluateEditText;
        private final AppCompatImageView zeroImageView;
        private final AppCompatImageView oneImageView;
        private final AppCompatImageView twoImageView;
        private final AppCompatImageView thrImageView;
        private final AppCompatImageView fouImageView;

        private ViewHolder(View view) {
            super(view);

            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainImageView = view.findViewById(R.id.mainImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            specTextView = view.findViewById(R.id.specTextView);
            mainRatingBar = view.findViewById(R.id.mainRatingBar);
            evaluateEditText = view.findViewById(R.id.evaluateEditText);
            zeroImageView = view.findViewById(R.id.zeroImageView);
            oneImageView = view.findViewById(R.id.oneImageView);
            twoImageView = view.findViewById(R.id.twoImageView);
            thrImageView = view.findViewById(R.id.thrImageView);
            fouImageView = view.findViewById(R.id.fouImageView);

        }

    }

}
