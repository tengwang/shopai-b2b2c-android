package top.yokey.shopai.store.fragment;

import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.jeremyliao.liveeventbus.LiveEventBus;

import java.util.ArrayList;
import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.store.adapter.StoreGoodsAdapter;
import top.yokey.shopai.store.viewmodel.StoreGoodsVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.base.BaseFragment;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.recycler.SpaceDecoration;
import top.yokey.shopai.zcom.util.AnimUtil;
import top.yokey.shopai.zcom.util.ConvertUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.GoodsListBean;
import top.yokey.shopai.zsdk.bean.StoreBean;
import top.yokey.shopai.zsdk.data.GoodsData;
import top.yokey.shopai.zsdk.data.GoodsSearchData;

public class StoreGoodsFragment extends BaseFragment {

    private final ArrayList<GoodsListBean> arrayList = new ArrayList<>();
    private final StoreGoodsAdapter adapter = new StoreGoodsAdapter(arrayList, true);
    private final LineDecoration lineDecoration = new LineDecoration(1, App.get().getColors(R.color.divider), false);
    private final SpaceDecoration spaceDecoration = new SpaceDecoration(App.get().dp2Px(4));
    private AppCompatTextView screenOrderTextView = null;
    private AppCompatTextView screenSaleTextView = null;
    private AppCompatTextView screenScreenTextView = null;
    private AppCompatImageView screenOrientationImageView = null;
    private AppCompatTextView screenNightTextView = null;
    private LinearLayoutCompat screenOrderLinearLayout = null;
    private AppCompatTextView screenOrderSynthesizeTextView = null;
    private AppCompatTextView screenOrderHighTextView = null;
    private AppCompatTextView screenOrderLowTextView = null;
    private AppCompatTextView screenOrderPopularityTextView = null;
    private LinearLayoutCompat screenConditionLinearLayout = null;
    private AppCompatEditText screenPriceFromEditText = null;
    private AppCompatEditText screenPriceToEditText = null;
    private AppCompatTextView screenConfirmTextView = null;
    private AppCompatTextView screenResetTextView = null;
    private PullRefreshView mainPullRefreshView = null;
    private boolean isGrid = true;
    private GoodsSearchData data = null;
    private StoreGoodsVM vm = null;

    @Override
    public View initView() {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_store_goods, null);
        screenOrderTextView = view.findViewById(R.id.screenOrderTextView);
        screenSaleTextView = view.findViewById(R.id.screenSaleTextView);
        screenScreenTextView = view.findViewById(R.id.screenScreenTextView);
        screenOrientationImageView = view.findViewById(R.id.screenOrientationImageView);
        screenNightTextView = view.findViewById(R.id.screenNightTextView);
        screenOrderLinearLayout = view.findViewById(R.id.screenOrderLinearLayout);
        screenOrderSynthesizeTextView = view.findViewById(R.id.screenOrderSynthesizeTextView);
        screenOrderHighTextView = view.findViewById(R.id.screenOrderHighTextView);
        screenOrderLowTextView = view.findViewById(R.id.screenOrderLowTextView);
        screenOrderPopularityTextView = view.findViewById(R.id.screenOrderPopularityTextView);
        screenConditionLinearLayout = view.findViewById(R.id.screenConditionLinearLayout);
        screenPriceFromEditText = view.findViewById(R.id.screenPriceFromEditText);
        screenPriceToEditText = view.findViewById(R.id.screenPriceToEditText);
        screenConfirmTextView = view.findViewById(R.id.screenConfirmTextView);
        screenResetTextView = view.findViewById(R.id.screenResetTextView);
        mainPullRefreshView = view.findViewById(R.id.mainPullRefreshView);
        return view;

    }

    @Override
    public void initData() {

        data = new GoodsSearchData();
        vm = getVM(StoreGoodsVM.class);

    }

    @Override
    public void initEvent() {

        screenOrderTextView.setOnClickListener(view -> {
            screenConditionLinearLayout.setVisibility(View.GONE);
            screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
            if (screenOrderLinearLayout.getVisibility() == View.VISIBLE) {
                screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
                goneScreenCondition();
            } else {
                screenNightTextView.setVisibility(View.VISIBLE);
                screenOrderLinearLayout.setVisibility(View.VISIBLE);
                screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_up, 0);
                AnimUtil.objectAnimator(screenOrderLinearLayout, AnimUtil.TRABSLATION_Y, -screenOrderLinearLayout.getHeight(), 0);
                AnimUtil.objectAnimator(screenNightTextView, AnimUtil.ALPHA, 0, 1.0f);
            }
        });

        screenSaleTextView.setOnClickListener(view -> order(2, "3", "2"));

        screenScreenTextView.setOnClickListener(view -> {
            screenOrderLinearLayout.setVisibility(View.GONE);
            screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
            if (screenConditionLinearLayout.getVisibility() == View.VISIBLE) {
                screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
                goneScreenCondition();
            } else {
                screenNightTextView.setVisibility(View.VISIBLE);
                screenConditionLinearLayout.setVisibility(View.VISIBLE);
                screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_up, 0);
                AnimUtil.objectAnimator(screenConditionLinearLayout, AnimUtil.TRABSLATION_X, App.get().getWidth(), 0);
                AnimUtil.objectAnimator(screenNightTextView, AnimUtil.ALPHA, 0, 1.0f);
            }
        });

        screenOrientationImageView.setOnClickListener(view -> {
            if (isGrid) {
                setVerModel();
                return;
            }
            setGridModel();
        });

        screenNightTextView.setOnClickListener(view -> goneScreenCondition());

        screenOrderSynthesizeTextView.setOnClickListener(view -> {
            screenOrderSynthesizeTextView.setTextColor(App.get().getColors(R.color.accent));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderPopularityTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderTextView.setText(R.string.synthesizeSort);
            order(1, "0", "0");
        });

        screenOrderHighTextView.setOnClickListener(view -> {
            screenOrderSynthesizeTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.accent));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderPopularityTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderTextView.setText(R.string.priceHighToLow);
            order(1, "2", "2");
        });

        screenOrderLowTextView.setOnClickListener(view -> {
            screenOrderSynthesizeTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.accent));
            screenOrderPopularityTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderTextView.setText(R.string.priceLowToHigh);
            order(1, "2", "1");
        });

        screenOrderPopularityTextView.setOnClickListener(view -> {
            screenOrderSynthesizeTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderPopularityTextView.setTextColor(App.get().getColors(R.color.accent));
            screenOrderTextView.setText(R.string.popularitySort);
            order(1, "5", "2");
        });

        screenConfirmTextView.setOnClickListener(view -> {
            screenOrderSynthesizeTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderPopularityTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderTextView.setText(R.string.synthesizeSort);
            order(1, "", "");
        });

        screenResetTextView.setOnClickListener(view -> {
            data.setPriceFrom("");
            data.setPriceTo("");
            screenPriceFromEditText.setText("");
            screenPriceToEditText.setText("");
            ToastHelp.get().show(R.string.success);
        });

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                data.setPage("1");
                getData();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                data.setPage("1");
                getData();
            }

            @Override
            public void onLoadMore() {
                getData();
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getGoodsLiveData().observe(this, list -> {
            if (data.getPage().equals("1")) arrayList.clear();
            data.setPage((ConvertUtil.string2Int(data.getPage()) + 1) + "");
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getHasMoreLiveData().observe(this, bool -> mainPullRefreshView.setCanLoadMore(bool));

        vm.getAddCartLiveData().observe(this, string -> ToastHelp.get().show(R.string.tipsAlreadyAddCart));

        vm.getFavoriteLiveData().observe(this, string -> ToastHelp.get().show(R.string.tipsAlreadyFavorite));

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                if (arrayList.size() == 0) {
                    mainPullRefreshView.setError(bean.getReason());
                } else {
                    ToastHelp.get().show(bean.getReason());
                }
            } else {
                ToastHelp.get().show(bean.getReason());
            }
        });

        LiveEventBus.get(Constant.DATA_BEAN, StoreBean.class).observe(this, bean -> {
            data.setKey("");
            data.setOrder("");
            data.setPage("1");
            data.setStoreId(bean.getStoreInfo().getStoreId());
            setGridModel();
            getData();
        });

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView.setLoad();
        vm.getList(data);

    }

    private void setVerModel() {

        isGrid = false;
        screenOrientationImageView.setImageResource(R.mipmap.ic_orientation_ver);
        mainPullRefreshView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mainPullRefreshView.getRecyclerView().setPadding(0, 0, 0, 0);
        mainPullRefreshView.removeItemDecoration(lineDecoration);
        mainPullRefreshView.removeItemDecoration(spaceDecoration);
        mainPullRefreshView.setItemDecoration(lineDecoration);
        adapter.setGrid(isGrid);
        mainPullRefreshView.setAdapter(adapter);
        mainPullRefreshView.setComplete();
        setAdapterEvent();

    }

    private void setGridModel() {

        isGrid = true;
        screenOrientationImageView.setImageResource(R.mipmap.ic_orientation_grid);
        mainPullRefreshView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        mainPullRefreshView.getRecyclerView().setPadding(App.get().dp2Px(4), 0, App.get().dp2Px(4), 0);
        mainPullRefreshView.removeItemDecoration(lineDecoration);
        mainPullRefreshView.removeItemDecoration(spaceDecoration);
        mainPullRefreshView.setItemDecoration(spaceDecoration);
        adapter.setGrid(isGrid);
        mainPullRefreshView.setAdapter(adapter);
        mainPullRefreshView.setComplete();
        setAdapterEvent();

    }

    private void setAdapterEvent() {

        adapter.setOnItemClickListener(new StoreGoodsAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, GoodsListBean bean) {
                App.get().startGoods(new GoodsData(bean.getGoodsId()));
            }

            @Override
            public void onClickCart(int position, GoodsListBean bean) {
                if (App.get().isMemberLogin()) {
                    vm.addCart(bean.getGoodsId(), "1");
                } else {
                    App.get().startMemberLogin();
                }
            }

            @Override
            public void onClickFavorate(int position, GoodsListBean bean) {
                if (App.get().isMemberLogin()) {
                    vm.favorite(bean.getGoodsId());
                } else {
                    App.get().startMemberLogin();
                }
            }

            @Override
            public void onClickStore(int position, GoodsListBean bean) {
                App.get().startStore(bean.getStoreId());
            }
        });

    }

    private void goneScreenCondition() {

        if (screenNightTextView.getVisibility() == View.VISIBLE) {
            AnimUtil.objectAnimator(screenNightTextView, AnimUtil.ALPHA, () -> screenNightTextView.setVisibility(View.GONE), 1.0f, 0);
        }
        if (screenOrderLinearLayout.getVisibility() == View.VISIBLE) {
            screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
            AnimUtil.objectAnimator(screenOrderLinearLayout, AnimUtil.TRABSLATION_Y, () -> screenOrderLinearLayout.setVisibility(View.GONE), 0, -screenOrderLinearLayout.getHeight());
        }
        if (screenConditionLinearLayout.getVisibility() == View.VISIBLE) {
            screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
            AnimUtil.objectAnimator(screenConditionLinearLayout, AnimUtil.TRABSLATION_X, () -> screenConditionLinearLayout.setVisibility(View.GONE), 0, App.get().getWidth());
        }

    }

    private void order(int type, String key, String order) {

        hideKeyboard();
        goneScreenCondition();
        data.setPage("1");
        data.setKey(key);
        data.setOrder(order);
        data.setPriceTo(Objects.requireNonNull(screenPriceToEditText.getText()).toString());
        data.setPriceFrom(Objects.requireNonNull(screenPriceFromEditText.getText()).toString());
        screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
        screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
        switch (type) {
            case 1:
                screenOrderTextView.setTextColor(App.get().getColors(R.color.accent));
                screenSaleTextView.setTextColor(App.get().getColors(R.color.textTwo));
                break;
            case 2:
                screenOrderTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenSaleTextView.setTextColor(App.get().getColors(R.color.accent));
                screenOrderSynthesizeTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenOrderPopularityTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenOrderTextView.setText(R.string.synthesizeSort);
                break;
        }
        if (!VerifyUtil.isEmpty(data.getPriceFrom()) || !VerifyUtil.isEmpty(data.getPriceTo())) {
            screenScreenTextView.setTextColor(App.get().getColors(R.color.accentDark));
        } else {
            screenScreenTextView.setTextColor(App.get().getColors(R.color.textTwo));
        }
        getData();

    }

}
