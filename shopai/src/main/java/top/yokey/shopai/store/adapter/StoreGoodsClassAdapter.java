package top.yokey.shopai.store.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zsdk.bean.StoreGoodsClassBean;

public class StoreGoodsClassAdapter extends RecyclerView.Adapter<StoreGoodsClassAdapter.ViewHolder> {

    private final ArrayList<StoreGoodsClassBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public StoreGoodsClassAdapter(ArrayList<StoreGoodsClassBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        StoreGoodsClassBean bean = arrayList.get(position);
        if (bean.getLevel().equals("1")) {
            holder.nameTextView.setText(bean.getName());
        } else {
            holder.nameTextView.setText("  -  ");
            holder.nameTextView.append(bean.getName());
        }
        if (bean.getChild().size() != 0) {
            holder.mainRecyclerView.setVisibility(View.VISIBLE);
        } else {
            holder.mainRecyclerView.setVisibility(View.GONE);
        }
        StoreGoodsClassAdapter adapter = new StoreGoodsClassAdapter(bean.getChild());
        App.get().setRecyclerView(holder.mainRecyclerView, adapter);

        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onClick(int positionChild, StoreGoodsClassBean classStoreBean) {
                if (onItemClickListener != null) {
                    onItemClickListener.onChildClick(position, classStoreBean);
                }
            }

            @Override
            public void onChildClick(int position, StoreGoodsClassBean classStoreBean) {

            }
        });

        holder.mainLinearLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_store_goods_class, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, StoreGoodsClassBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onChildClick(int position, StoreGoodsClassBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayoutCompat mainLinearLayout;
        private final AppCompatTextView nameTextView;
        private final RecyclerView mainRecyclerView;

        private ViewHolder(View view) {

            super(view);
            mainLinearLayout = view.findViewById(R.id.mainLinearLayout);
            nameTextView = view.findViewById(R.id.nameTextView);
            mainRecyclerView = view.findViewById(R.id.mainRecyclerView);

        }

    }

}
