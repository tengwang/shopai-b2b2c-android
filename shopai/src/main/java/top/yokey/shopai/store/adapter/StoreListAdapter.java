package top.yokey.shopai.store.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.recycler.GridDecoration;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zsdk.bean.StoreListBean;

public class StoreListAdapter extends RecyclerView.Adapter<StoreListAdapter.ViewHolder> {

    private final ArrayList<StoreListBean> arrayList;
    private final GridDecoration gridDecoration = new GridDecoration(4, App.get().dp2Px(8), false);
    private OnItemClickListener onItemClickListener = null;

    public StoreListAdapter(ArrayList<StoreListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        StoreListBean bean = arrayList.get(position);
        ImageHelp.get().displayRadius(bean.getStoreAvatar(), holder.avatarImageView);
        holder.nameTextView.setText(bean.getStoreName());
        holder.ownTextView.setText(bean.getGradeId());
        holder.collectTextView.setText(String.format(App.get().getString(R.string.storeGoodsAndCount), bean.getStoreCreditPercent(), bean.getGoodsCount()));
        holder.dealTextView.setText(String.format(App.get().getString(R.string.storeDeal), VerifyUtil.isEmpty(bean.getNumSalesJq()) ? "0" : bean.getNumSalesJq()));
        holder.descTextView.setText(bean.getStoreCredit().getStoreDesccredit().getText());
        holder.descTextView.append("：" + bean.getStoreCredit().getStoreDesccredit().getCredit());
        holder.serviceTextView.setText(bean.getStoreCredit().getStoreServicecredit().getText());
        holder.serviceTextView.append("：" + bean.getStoreCredit().getStoreServicecredit().getCredit());
        holder.deliveryTextView.setText(bean.getStoreCredit().getStoreDelivercredit().getText());
        holder.deliveryTextView.append("：" + bean.getStoreCredit().getStoreDelivercredit().getCredit());
        StoreListGoodsAdapter adapter = new StoreListGoodsAdapter(bean.getSearchListGoods());
        App.get().setRecyclerView(holder.goodsRecyclerView, adapter);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(App.get(), 4);
        holder.goodsRecyclerView.setLayoutManager(gridLayoutManager);
        holder.goodsRecyclerView.removeItemDecoration(gridDecoration);
        holder.goodsRecyclerView.addItemDecoration(gridDecoration);

        adapter.setOnItemClickListener((position1, bean1) -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickGoods(position, position1, bean1);
            }
        });

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_store_list, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, StoreListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickGoods(int position, int itemPosition, StoreListBean.SearchListGoodsBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatImageView avatarImageView;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView ownTextView;
        private final AppCompatTextView collectTextView;
        private final AppCompatTextView dealTextView;
        private final AppCompatTextView descTextView;
        private final AppCompatTextView serviceTextView;
        private final AppCompatTextView deliveryTextView;
        private final RecyclerView goodsRecyclerView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            avatarImageView = view.findViewById(R.id.avatarImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            ownTextView = view.findViewById(R.id.ownTextView);
            collectTextView = view.findViewById(R.id.collectTextView);
            dealTextView = view.findViewById(R.id.dealTextView);
            descTextView = view.findViewById(R.id.descTextView);
            serviceTextView = view.findViewById(R.id.serviceTextView);
            deliveryTextView = view.findViewById(R.id.deliveryTextView);
            goodsRecyclerView = view.findViewById(R.id.goodsRecyclerView);

        }

    }

}
