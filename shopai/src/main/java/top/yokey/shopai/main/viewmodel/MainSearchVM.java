package top.yokey.shopai.main.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.SearchKeyBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.IndexController;

public class MainSearchVM extends BaseViewModel {

    private final MutableLiveData<SearchKeyBean> searchLiveData = new MutableLiveData<>();

    public MainSearchVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<SearchKeyBean> getSearchLiveData() {

        return searchLiveData;

    }

    public void getSearch() {

        IndexController.searchKeyList(new HttpCallBack<SearchKeyBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, SearchKeyBean bean) {
                searchLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
