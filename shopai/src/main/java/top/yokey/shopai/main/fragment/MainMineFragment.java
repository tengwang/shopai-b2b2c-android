package top.yokey.shopai.main.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.jeremyliao.liveeventbus.LiveEventBus;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.main.viewmodel.MainMineVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseFragment;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.SpaceDecoration;
import top.yokey.shopai.zcom.util.TimeUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.AppAdvertBean;
import top.yokey.shopai.zsdk.bean.GoodsListBean;

public class MainMineFragment extends BaseFragment {

    private final RelativeLayout[] orderRelativeLayout = new RelativeLayout[4];
    private final AppCompatTextView[] orderNumTextView = new AppCompatTextView[4];
    private final LinearLayoutCompat[] propertyLinearLayout = new LinearLayoutCompat[5];
    private final AppCompatTextView[] propertyTextView = new AppCompatTextView[5];
    private final AppCompatTextView[] functionTextView = new AppCompatTextView[12];
    private final ArrayList<GoodsListBean> arrayList = new ArrayList<>();
    //private final GoodsListAdapter adapter = new GoodsListAdapter(arrayList, true);
    private RelativeLayout headerRelativeLayout = null;
    private AppCompatImageView settingImageView = null;
    private AppCompatImageView messageImageView = null;
    private AppCompatTextView messageDotTextView = null;
    private AppCompatImageView avatarImageView = null;
    private AppCompatTextView nameTextView = null;
    private AppCompatTextView levelTextView = null;
    private AppCompatTextView allOrderTextView = null;
    private AppCompatImageView advertImageView = null;
    private RecyclerView mainRecyclerView = null;
    private MainMineVM vm = null;

    @Override
    public View initView() {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_main_mine, null);
        headerRelativeLayout = view.findViewById(R.id.headerRelativeLayout);
        settingImageView = view.findViewById(R.id.settingImageView);
        messageImageView = view.findViewById(R.id.messageImageView);
        messageDotTextView = view.findViewById(R.id.messageDotTextView);
        avatarImageView = view.findViewById(R.id.avatarImageView);
        nameTextView = view.findViewById(R.id.nameTextView);
        levelTextView = view.findViewById(R.id.levelTextView);
        allOrderTextView = view.findViewById(R.id.allOrderTextView);
        orderRelativeLayout[0] = view.findViewById(R.id.waitPayRelativeLayout);
        orderRelativeLayout[1] = view.findViewById(R.id.waitReceiveRelativeLayout);
        orderRelativeLayout[2] = view.findViewById(R.id.waitEvaluateRelativeLayout);
        orderRelativeLayout[3] = view.findViewById(R.id.returnRefundRelativeLayout);
        orderNumTextView[0] = view.findViewById(R.id.waitPayNumTextView);
        orderNumTextView[1] = view.findViewById(R.id.waitReceiveNumTextView);
        orderNumTextView[2] = view.findViewById(R.id.waitEvaluateNumTextView);
        orderNumTextView[3] = view.findViewById(R.id.returnRefundNumTextView);
        advertImageView = view.findViewById(R.id.advertImageView);
        propertyLinearLayout[0] = view.findViewById(R.id.preDepositLinearLayout);
        propertyLinearLayout[1] = view.findViewById(R.id.rechargeCardLinearLayout);
        propertyLinearLayout[2] = view.findViewById(R.id.voucherLinearLayout);
        propertyLinearLayout[3] = view.findViewById(R.id.couponLinearLayout);
        propertyLinearLayout[4] = view.findViewById(R.id.pointLinearLayout);
        propertyTextView[0] = view.findViewById(R.id.preDepositTextView);
        propertyTextView[1] = view.findViewById(R.id.rechargeCardTextView);
        propertyTextView[2] = view.findViewById(R.id.voucherTextView);
        propertyTextView[3] = view.findViewById(R.id.couponTextView);
        propertyTextView[4] = view.findViewById(R.id.pointTextView);
        functionTextView[0] = view.findViewById(R.id.favoriteTextView);
        functionTextView[1] = view.findViewById(R.id.footprintTextView);
        functionTextView[2] = view.findViewById(R.id.pointShopTextView);
        functionTextView[3] = view.findViewById(R.id.pointOrderTextView);
        functionTextView[4] = view.findViewById(R.id.addressTextView);
        functionTextView[5] = view.findViewById(R.id.invoiceTextView);
        functionTextView[6] = view.findViewById(R.id.inviteTextView);
        functionTextView[7] = view.findViewById(R.id.signTextView);
        functionTextView[8] = view.findViewById(R.id.sellerTextView);
        functionTextView[9] = view.findViewById(R.id.distribuTextView);
        functionTextView[10] = view.findViewById(R.id.feedbackTextView);
        functionTextView[11] = view.findViewById(R.id.customerTextView);
        mainRecyclerView = view.findViewById(R.id.mainRecyclerView);
        return view;

    }

    @Override
    public void initData() {

        //App.get().setRecyclerView(mainRecyclerView, adapter);
        mainRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        mainRecyclerView.addItemDecoration(new SpaceDecoration(App.get().dp2Px(4)));
        vm = getVM(MainMineVM.class);
        vm.getGoods();

    }

    @Override
    public void initEvent() {

        headerRelativeLayout.setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_CENTER));

        settingImageView.setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_SETTING));

        messageImageView.setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_MESSAGE));

        allOrderTextView.setOnClickListener(view -> App.get().start(ARoutePath.ORDER_LIST, Constant.DATA_POSITION, 0));

        orderRelativeLayout[0].setOnClickListener(view -> App.get().start(ARoutePath.ORDER_LIST, Constant.DATA_POSITION, 1));

        orderRelativeLayout[1].setOnClickListener(view -> App.get().start(ARoutePath.ORDER_LIST, Constant.DATA_POSITION, 3));

        orderRelativeLayout[2].setOnClickListener(view -> App.get().start(ARoutePath.ORDER_LIST, Constant.DATA_POSITION, 4));

        orderRelativeLayout[3].setOnClickListener(view -> App.get().start(ARoutePath.RETREAT_LIST));

        propertyLinearLayout[0].setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_PRE_DEPOSIT));

        propertyLinearLayout[1].setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_RECHARGE_CARD));

        propertyLinearLayout[2].setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_VOUCHER));

        propertyLinearLayout[3].setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_COUPON));

        propertyLinearLayout[4].setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_POINT));

        functionTextView[0].setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_FAVORITE));

        functionTextView[1].setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_FOOTPRINT));

        functionTextView[2].setOnClickListener(view -> App.get().start(ARoutePath.POINT_LIST));

        functionTextView[3].setOnClickListener(view -> App.get().start(ARoutePath.POINT_ORDER_LIST));

        functionTextView[4].setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_ADDRESS));

        functionTextView[5].setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_INVOICE));

        functionTextView[6].setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_INVITE));

        functionTextView[7].setOnClickListener(view -> App.get().start(ARoutePath.MAIN_SIGN));

        functionTextView[8].setOnClickListener(view -> App.get().start(ARoutePath.SELLER_INDEX));

        functionTextView[9].setOnClickListener(view -> App.get().start(ARoutePath.DISTRIBU_INDEX));

        functionTextView[10].setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_FEEDBACK));

        functionTextView[11].setOnClickListener(view -> App.get().startChat(ShopAISdk.get().getChatUrl("1", "")));

        /*
        adapter.setOnItemClickListener(new GoodsListAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, GoodsListBean bean) {
                App.get().startGoods(new GoodsData(bean.getGoodsId()));
            }

            @Override
            public void onClickFavorate(int position, GoodsListBean bean) {
                if (App.get().isMemberLogin()) {
                    vm.favorite(bean.getGoodsId());
                } else {
                    App.get().startMemberLogin();
                }
            }

            @Override
            public void onClickCart(int position, GoodsListBean bean) {
                if (App.get().isMemberLogin()) {
                    vm.addCart(bean.getGoodsId(), "1");
                } else {
                    App.get().startMemberLogin();
                }
            }

            @Override
            public void onClickStore(int position, GoodsListBean bean) {
                App.get().startStore(bean.getStoreId());
            }
        });
        */

    }

    @Override
    public void initObserve() {

        vm.getMemberLiveData().observe(this, bean -> {
            App.get().setMemberBean(bean);
            getAsset();
        });

        vm.getAssetLiveData().observe(this, bean -> {
            propertyTextView[0].setText("￥");
            propertyTextView[0].append(bean.getPredepoit());
            propertyTextView[1].setText("￥");
            propertyTextView[1].append(bean.getAvailableRcBalance());
            propertyTextView[2].setText(bean.getVoucher());
            propertyTextView[2].append(getString(R.string.zhang));
            propertyTextView[3].setText(bean.getCoupon());
            propertyTextView[3].append(getString(R.string.zhang));
            propertyTextView[4].setText(bean.getPoint());
        });

        vm.getGoodsLiveData().observe(this, list -> {
            arrayList.clear();
            arrayList.addAll(list);
            //adapter.notifyDataSetChanged();
        });

        vm.getAddCartLiveData().observe(this, string -> ToastHelp.get().show(R.string.tipsAlreadyAddCart));

        vm.getFavoriteLiveData().observe(this, string -> ToastHelp.get().show(R.string.tipsAlreadyFavorite));

        vm.getErrorLiveData().observe(this, bean -> ToastHelp.get().show(bean.getReason()));

        LiveEventBus.get(Constant.DATA_MSG_COUNT, String.class).observe(this, string -> {
            messageDotTextView.setVisibility(string.equals("0") ? View.GONE : View.VISIBLE);
            messageDotTextView.setText(string);
        });

        LiveEventBus.get(Constant.DATA_ADVERT, AppAdvertBean.class).observe(this, bean -> {
            if (App.get().getAppAdvertBean() != null) {
                advertImageView.setVisibility(VerifyUtil.isEmpty(App.get().getAppAdvertBean().getAppMineAdvertLink()) ? View.GONE : View.VISIBLE);
                ImageHelp.get().displayRadius(App.get().getAppAdvertBean().getAppMineAdvertLink(), advertImageView);
                advertImageView.setOnClickListener(view -> App.get().startUrl(bean.getAppMineAdvertUrl()));
            }
        });

    }

    @Override
    public void onResume() {

        super.onResume();
        if (App.get().isMemberLogin()) {
            vm.getMember();
            return;
        }
        for (AppCompatTextView appCompatTextView : propertyTextView) {
            appCompatTextView.setVisibility(View.GONE);
        }
        avatarImageView.setImageResource(R.mipmap.ic_default_avatar);
        nameTextView.setText(R.string.tipsLoginAccount);
        levelTextView.setText(R.string.tipsLoginAccount);
        messageDotTextView.setVisibility(View.GONE);

    }

    //自定义方法

    private void setData() {

        ImageHelp.get().displayCircle(App.get().getMemberBean().getAvatar() + "?id=" + TimeUtil.getStampAll() + "", avatarImageView);
        nameTextView.setText(App.get().getMemberBean().getUserName());
        levelTextView.setText(App.get().getMemberBean().getLevelName());
        if (VerifyUtil.isEmpty(App.get().getMemberBean().getOrderNopayCount()) || App.get().getMemberBean().getOrderNopayCount().equals("0")) {
            orderNumTextView[0].setVisibility(View.INVISIBLE);
        } else {
            orderNumTextView[0].setVisibility(View.VISIBLE);
            orderNumTextView[0].setText(App.get().getMemberBean().getOrderNopayCount().length() > 1 ? "+" : App.get().getMemberBean().getOrderNopayCount());
        }
        if (VerifyUtil.isEmpty(App.get().getMemberBean().getOrderNoreceiptCount()) || App.get().getMemberBean().getOrderNoreceiptCount().equals("0")) {
            orderNumTextView[1].setVisibility(View.INVISIBLE);
        } else {
            orderNumTextView[1].setVisibility(View.VISIBLE);
            orderNumTextView[1].setText(App.get().getMemberBean().getOrderNoreceiptCount().length() > 1 ? "+" : App.get().getMemberBean().getOrderNoreceiptCount());
        }
        if (VerifyUtil.isEmpty(App.get().getMemberBean().getOrderNoevalCount()) || App.get().getMemberBean().getOrderNoevalCount().equals("0")) {
            orderNumTextView[2].setVisibility(View.INVISIBLE);
        } else {
            orderNumTextView[2].setVisibility(View.VISIBLE);
            orderNumTextView[2].setText(App.get().getMemberBean().getOrderNoevalCount().length() > 1 ? "+" : App.get().getMemberBean().getOrderNoevalCount());
        }
        if (VerifyUtil.isEmpty(App.get().getMemberBean().getReturnX()) || App.get().getMemberBean().getReturnX().equals("0")) {
            orderNumTextView[3].setVisibility(View.INVISIBLE);
        } else {
            orderNumTextView[3].setVisibility(View.VISIBLE);
            orderNumTextView[3].setText(App.get().getMemberBean().getReturnX().length() > 1 ? "+" : App.get().getMemberBean().getReturnX());
        }
        if (!VerifyUtil.isEmpty(App.get().getXingeToken())) {
            vm.updateXingeToken(App.get().getXingeToken());
        }

    }

    private void getAsset() {

        for (AppCompatTextView appCompatTextView : propertyTextView) {
            appCompatTextView.setVisibility(View.VISIBLE);
        }
        vm.getAsset();
        setData();

    }

}
