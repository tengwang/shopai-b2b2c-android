package top.yokey.shopai.main.activity;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.jeremyliao.liveeventbus.LiveEventBus;

import java.util.ArrayList;
import java.util.List;

import top.yokey.shopai.R;
import top.yokey.shopai.main.fragment.MainCartFragment;
import top.yokey.shopai.main.fragment.MainCateFragment;
import top.yokey.shopai.main.fragment.MainHomeFragment;
import top.yokey.shopai.main.fragment.MainMineFragment;
import top.yokey.shopai.main.fragment.MainSearchFragment;
import top.yokey.shopai.main.viewmodel.MainMainVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.adapter.FragmentAdapter;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zcom.view.FixedViewPager;
import top.yokey.shopai.zsdk.ShopAISdk;

@Route(path = ARoutePath.MAIN_MAIN)
public class MainMainActivity extends BaseActivity {

    private final RelativeLayout[] navigationRelativeLayout = new RelativeLayout[5];
    private final AppCompatTextView[] navigationTextView = new AppCompatTextView[5];
    private final AppCompatTextView[] navigationDotTextView = new AppCompatTextView[5];
    private final Drawable[] drawable = new Drawable[5];
    private final Drawable[] pressDrawable = new Drawable[5];
    @Autowired(name = Constant.DATA_URL)
    String url = "";
    private FixedViewPager mainViewPager = null;
    private MainMainVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_main_main);
        mainViewPager = findViewById(R.id.mainViewPager);
        navigationRelativeLayout[0] = findViewById(R.id.homeRelativeLayout);
        navigationRelativeLayout[1] = findViewById(R.id.cateRelativeLayout);
        navigationRelativeLayout[2] = findViewById(R.id.searchRelativeLayout);
        navigationRelativeLayout[3] = findViewById(R.id.cartRelativeLayout);
        navigationRelativeLayout[4] = findViewById(R.id.mineRelativeLayout);
        navigationTextView[0] = findViewById(R.id.homeTextView);
        navigationTextView[1] = findViewById(R.id.cateTextView);
        navigationTextView[2] = findViewById(R.id.searchTextView);
        navigationTextView[3] = findViewById(R.id.cartTextView);
        navigationTextView[4] = findViewById(R.id.mineTextView);
        navigationDotTextView[0] = findViewById(R.id.homeDotTextView);
        navigationDotTextView[1] = findViewById(R.id.cateDotTextView);
        navigationDotTextView[2] = findViewById(R.id.searchDotTextView);
        navigationDotTextView[3] = findViewById(R.id.cartDotTextView);
        navigationDotTextView[4] = findViewById(R.id.mineDotTextView);

    }

    @Override
    public void initData() {

        drawable[0] = App.get().getDrawables(R.mipmap.ic_navigation_home);
        drawable[1] = App.get().getDrawables(R.mipmap.ic_navigation_cate);
        drawable[2] = App.get().getDrawables(R.mipmap.ic_navigation_search);
        drawable[3] = App.get().getDrawables(R.mipmap.ic_navigation_cart);
        drawable[4] = App.get().getDrawables(R.mipmap.ic_navigation_mine);
        pressDrawable[0] = App.get().getDrawables(R.mipmap.ic_navigation_home_press);
        pressDrawable[1] = App.get().getDrawables(R.mipmap.ic_navigation_cate_press);
        pressDrawable[2] = App.get().getDrawables(R.mipmap.ic_navigation_search_press);
        pressDrawable[3] = App.get().getDrawables(R.mipmap.ic_navigation_cart_press);
        pressDrawable[4] = App.get().getDrawables(R.mipmap.ic_navigation_mine_press);
        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(new MainHomeFragment());
        fragmentList.add(new MainCateFragment());
        fragmentList.add(new MainSearchFragment());
        fragmentList.add(new MainCartFragment());
        fragmentList.add(new MainMineFragment());
        mainViewPager.setAdapter(new FragmentAdapter(getSupportFragmentManager(), fragmentList));
        mainViewPager.setOffscreenPageLimit(navigationTextView.length);
        updateNavigation(0);
        if (!VerifyUtil.isEmpty(url)) App.get().startUrl(url);
        vm = getVM(MainMainVM.class);
        vm.getAppAdvert();
        observeKeyborad(R.id.mainRelativeLayout);
        //App.get().start(ARoutePath.SELLER_MAIN);

    }

    @Override
    public void initEvent() {

        for (int i = 0; i < navigationRelativeLayout.length; i++) {
            int position = i;
            navigationRelativeLayout[i].setOnClickListener(view -> updateNavigation(position));
        }

    }

    @Override
    public void initObserve() {

        vm.getMsgCountLiveData().observe(this, string -> LiveEventBus.get(Constant.DATA_MSG_COUNT).post(string));

        vm.getCartCountLiveData().observe(this, string -> {
            navigationDotTextView[3].setVisibility(View.GONE);
            if (!string.equals("0")) {
                navigationDotTextView[3].setText(string);
                navigationDotTextView[3].setVisibility(View.VISIBLE);
            }
        });

        vm.getSearchKeyLiveData().observe(this, bean -> {
            String data = bean.getList().toString();
            data = data.replace("[", "");
            data = data.replace("]", "");
            data = data.replace("\"", "");
            data = data.replace(",", " ");
            LiveEventBus.get(Constant.DATA_CONTENT).post(data);
        });

        vm.getAppAdvertLiveData().observe(this, bean -> {
            App.get().setAppAdvertBean(bean);
            LiveEventBus.get(Constant.DATA_ADVERT).post(bean);
        });

        vm.getErrorLiveData().observe(this, bean -> {
            switch (bean.getCode()) {
                case 1:
                    LiveEventBus.get(Constant.DATA_MSG_COUNT).post("0");
                    break;
                case 2:
                    navigationDotTextView[3].setVisibility(View.GONE);
                    break;
                case 3:
                    break;
            }
        });

        LiveEventBus.get(Constant.DATA_REFRESH, String.class).observe(this, string -> vm.getCartCount());

    }

    @Override
    public void onReturn(boolean handler) {

        if (handler && isShowKeyboard()) {
            hideKeyboard();
            return;
        }
        if (mainViewPager.getCurrentItem() != 0) {
            updateNavigation(0);
            return;
        }
        App.get().startHome(get());

    }

    @Override
    public void onResume() {

        super.onResume();
        vm.getSearchKey();
        if (App.get().isMemberLogin()) {
            ShopAISdk.get().setKey(App.get().getMemberKey());
            vm.getMsgCount();
            vm.getCartCount();
        } else {
            navigationDotTextView[0].setVisibility(View.GONE);
            navigationDotTextView[1].setVisibility(View.GONE);
            navigationDotTextView[2].setVisibility(View.GONE);
            navigationDotTextView[3].setVisibility(View.GONE);
            navigationDotTextView[4].setVisibility(View.GONE);
        }

    }

    //自定义方法

    private void updateNavigation(int position) {

        for (int i = 0; i < navigationTextView.length; i++) {
            navigationTextView[i].setTextColor(App.get().getColors(R.color.accent));
            navigationTextView[i].setCompoundDrawablesWithIntrinsicBounds(null, drawable[i], null, null);
        }
        navigationTextView[position].setTextColor(App.get().getColors(R.color.accentDark));
        navigationTextView[position].setCompoundDrawablesWithIntrinsicBounds(null, pressDrawable[position], null, null);
        mainViewPager.setCurrentItem(position, false);

    }

}
