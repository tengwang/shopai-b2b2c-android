package top.yokey.shopai.main.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import top.yokey.shopai.R;
import top.yokey.shopai.main.viewmodel.MainRedPacketVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.AnimUtil;
import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;

@Route(path = ARoutePath.MAIN_RED_PACKET)
public class MainRedPacketActivity extends BaseActivity {

    @Autowired(name = Constant.DATA_ID)
    String id = "";
    private AppCompatImageView backImageView = null;
    private AppCompatTextView getTextView = null;
    private AppCompatTextView aroundTextView = null;
    private MainRedPacketVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_main_red_packet);
        backImageView = findViewById(R.id.backImageView);
        getTextView = findViewById(R.id.getTextView);
        aroundTextView = findViewById(R.id.aroundTextView);

    }

    @Override
    public void initData() {

        if (VerifyUtil.isEmpty(id)) {
            ToastHelp.get().showDataError();
            onReturn(false);
            return;
        }
        setFullScreen();
        observeKeyborad(R.id.mainRelativeLayout);
        vm = getVM(MainRedPacketVM.class);
        vm.getDetail(id);
        anim();

    }

    @Override
    public void initEvent() {

        backImageView.setOnClickListener(view -> onReturn(false));

        getTextView.setOnClickListener(view -> {
            if (App.get().isMemberLogin()) {
                getTextView.setText(R.string.handlerIng);
                getTextView.setEnabled(false);
                vm.getRedPack(id);
                return;
            }
            App.get().startMemberLogin();
        });

        aroundTextView.setOnClickListener(view -> onReturn(false));

    }

    @Override
    public void initObserve() {

        vm.getDetailLiveData().observe(this, bean -> {

        });

        vm.getGetLiveData().observe(this, string -> {
            getTextView.setEnabled(true);
            getTextView.setText(R.string.freeGetRedPacket);
            String price = JsonUtil.getString(string, "packet_price");
            price = String.format(getString(R.string.redPacketGetPrice), price);
            DialogHelp.get().query(get(), R.string.confirmSelection, price, view -> onReturn(false), view -> {
                App.get().start(ARoutePath.MEMBER_RED_PACKET);
                onReturn(false);
            });
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.confirmSelection, bean.getReason(), view -> onReturn(false), view -> vm.getDetail(id));
                return;
            }
            if (bean.getCode() == 2) {
                getTextView.setEnabled(true);
                getTextView.setText(R.string.freeGetRedPacket);
                DialogHelp.get().query(get(), R.string.confirmSelection, bean.getReason(), null, view -> {
                    App.get().start(ARoutePath.MEMBER_RED_PACKET);
                    onReturn(false);
                });
            }
        });

    }

    //自定义方法

    private void anim() {

        ObjectAnimator x = ObjectAnimator.ofFloat(getTextView, AnimUtil.SCALE_X, 1f, 1.2f, 1f);
        ObjectAnimator y = ObjectAnimator.ofFloat(getTextView, AnimUtil.SCALE_Y, 1f, 1.2f, 1f);
        x.setDuration(500).start();
        y.setDuration(500).start();
        x.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                anim();
            }
        });

    }

}
