package top.yokey.shopai.main.activity;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.alibaba.android.arouter.facade.annotation.Route;

import top.yokey.shopai.R;
import top.yokey.shopai.main.viewmodel.MainSignVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;

@Route(path = ARoutePath.MAIN_SIGN)
public class MainSignActivity extends BaseActivity {

    private AppCompatImageView backImageView = null;
    private AppCompatTextView signTextView = null;
    private AppCompatTextView shopTextView = null;
    private AppCompatTextView recordTextView = null;
    private MainSignVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_main_sign);
        backImageView = findViewById(R.id.backImageView);
        signTextView = findViewById(R.id.signTextView);
        shopTextView = findViewById(R.id.shopTextView);
        recordTextView = findViewById(R.id.recordTextView);

    }

    @Override
    public void initData() {

        setFullScreen();
        observeKeyborad(R.id.mainRelativeLayout);
        vm = getVM(MainSignVM.class);
        signTextView.setEnabled(false);
        vm.check();

    }

    @Override
    public void initEvent() {

        backImageView.setOnClickListener(view -> onReturn(true));

        signTextView.setOnClickListener(view -> vm.sign());

        shopTextView.setOnClickListener(view -> App.get().start(ARoutePath.POINT_LIST));

        recordTextView.setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_POINT, Constant.DATA_ID, Constant.COMMON_ENABLE));

    }

    @Override
    public void initObserve() {

        vm.getCheckLiveData().observe(this, string -> {
            signTextView.setEnabled(true);
            signTextView.setText(String.format(getString(R.string.signAddPoint), string));
        });

        vm.getSignLiveData().observe(this, string -> {
            ToastHelp.get().showSuccess();
            vm.check();
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                signTextView.setText(bean.getReason());
                signTextView.setEnabled(false);
                return;
            }
            ToastHelp.get().show(bean.getReason());
        });

    }

}
