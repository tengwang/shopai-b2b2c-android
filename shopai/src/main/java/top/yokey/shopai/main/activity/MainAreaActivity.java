package top.yokey.shopai.main.activity;

import android.content.Intent;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.main.adapter.MainAreaAdapter;
import top.yokey.shopai.main.viewmodel.MainAreaVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zsdk.bean.AreaListBean;

@Route(path = ARoutePath.MAIN_AREA)
public class MainAreaActivity extends BaseActivity {

    private final ArrayList<AreaListBean> arrayList = new ArrayList<>();
    private final MainAreaAdapter adapter = new MainAreaAdapter(arrayList);
    private final ArrayList<AreaListBean> cityArrayList = new ArrayList<>();
    private final MainAreaAdapter cityAdapter = new MainAreaAdapter(cityArrayList);
    private final ArrayList<AreaListBean> areaArrayList = new ArrayList<>();
    private final MainAreaAdapter areaAdapter = new MainAreaAdapter(areaArrayList);
    private Toolbar mainToolbar = null;
    private AppCompatTextView provinceTextView = null;
    private AppCompatTextView cityTextView = null;
    private AppCompatTextView areaTextView = null;
    private RecyclerView provinceRecyclerView = null;
    private RecyclerView cityRecyclerView = null;
    private RecyclerView areaRecyclerView = null;
    private String id = "";
    private String name = "";
    private String cityId = "";
    private String cityName = "";
    private String areaId = "";
    private String areaName = "";
    private MainAreaVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_main_area);
        mainToolbar = findViewById(R.id.mainToolbar);
        areaTextView = findViewById(R.id.areaTextView);
        cityTextView = findViewById(R.id.cityTextView);
        provinceTextView = findViewById(R.id.provinceTextView);
        areaRecyclerView = findViewById(R.id.areaRecyclerView);
        cityRecyclerView = findViewById(R.id.cityRecyclerView);
        provinceRecyclerView = findViewById(R.id.provinceRecyclerView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.chooseArea);
        observeKeyborad(R.id.mainLinearLayout);
        App.get().setRecyclerView(provinceRecyclerView, adapter);
        provinceRecyclerView.addItemDecoration(new LineDecoration(1, App.get().getColors(R.color.divider), false));
        App.get().setRecyclerView(cityRecyclerView, cityAdapter);
        cityRecyclerView.addItemDecoration(new LineDecoration(1, App.get().getColors(R.color.divider), false));
        App.get().setRecyclerView(areaRecyclerView, areaAdapter);
        areaRecyclerView.addItemDecoration(new LineDecoration(1, App.get().getColors(R.color.divider), false));
        vm = getVM(MainAreaVM.class);
        vm.getProvince();

    }

    @Override
    public void initEvent() {

        adapter.setOnItemClickListener((position, bean) -> {
            areaId = "";
            cityId = "";
            areaName = "";
            cityName = "";
            id = bean.getAreaId();
            name = bean.getAreaName();
            areaTextView.setText(getString(R.string.area));
            cityTextView.setText(getString(R.string.city));
            provinceTextView.setText(name);
            areaTextView.setTextColor(App.get().getColors(R.color.text));
            cityTextView.setTextColor(App.get().getColors(R.color.text));
            provinceTextView.setTextColor(App.get().getColors(R.color.accent));
            for (int i = 0; i < arrayList.size(); i++) {
                arrayList.get(i).setSelect(false);
            }
            arrayList.get(position).setSelect(true);
            adapter.notifyDataSetChanged();
            vm.getCity(id);
        });

        cityAdapter.setOnItemClickListener((position, bean) -> {
            areaId = "";
            areaName = "";
            cityId = bean.getAreaId();
            cityName = bean.getAreaName();
            areaTextView.setText(getString(R.string.area));
            cityTextView.setText(cityName);
            areaTextView.setTextColor(App.get().getColors(R.color.text));
            cityTextView.setTextColor(App.get().getColors(R.color.accent));
            for (int i = 0; i < cityArrayList.size(); i++) {
                cityArrayList.get(i).setSelect(false);
            }
            cityArrayList.get(position).setSelect(true);
            cityAdapter.notifyDataSetChanged();
            vm.getArea(cityId);
        });

        areaAdapter.setOnItemClickListener((position, bean) -> {
            areaId = bean.getAreaId();
            areaName = bean.getAreaName();
            Intent intent = new Intent();
            intent.putExtra("province_id", id);
            intent.putExtra("city_id", cityId);
            intent.putExtra("area_id", areaId);
            intent.putExtra("province_name", name);
            intent.putExtra("city_name", cityName);
            intent.putExtra("area_name", areaName);
            intent.putExtra("area_info", name + " " + cityName + " " + areaName);
            App.get().finishOk(get(), intent);
        });

    }

    @Override
    public void initObserve() {

        vm.getProvinceLiveData().observe(this, list -> {
            arrayList.clear();
            arrayList.addAll(list);
            adapter.notifyDataSetChanged();
        });

        vm.getCityLiveData().observe(this, list -> {
            cityArrayList.clear();
            cityArrayList.addAll(list);
            cityAdapter.notifyDataSetChanged();
        });

        vm.getAreaLiveData().observe(this, list -> {
            areaArrayList.clear();
            areaArrayList.addAll(list);
            areaAdapter.notifyDataSetChanged();
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getProvince());
            } else {
                ToastHelp.get().show(bean.getReason());
            }
        });

    }

}
