package top.yokey.shopai.main.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.AppAdvertBean;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.SearchKeyBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.AppController;
import top.yokey.shopai.zsdk.controller.IndexController;
import top.yokey.shopai.zsdk.controller.MemberCartController;
import top.yokey.shopai.zsdk.controller.MemberChatController;

public class MainMainVM extends BaseViewModel {

    private final MutableLiveData<String> msgCountLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> cartCountLiveData = new MutableLiveData<>();
    private final MutableLiveData<SearchKeyBean> searchKeyLiveData = new MutableLiveData<>();
    private final MutableLiveData<AppAdvertBean> appAdvertLiveData = new MutableLiveData<>();

    public MainMainVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<String> getMsgCountLiveData() {

        return msgCountLiveData;

    }

    public MutableLiveData<String> getCartCountLiveData() {

        return cartCountLiveData;

    }

    public MutableLiveData<SearchKeyBean> getSearchKeyLiveData() {

        return searchKeyLiveData;

    }

    public MutableLiveData<AppAdvertBean> getAppAdvertLiveData() {

        return appAdvertLiveData;

    }

    public void getMsgCount() {

        MemberChatController.getMsgCount(new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                msgCountLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getCartCount() {

        MemberCartController.cartCount(new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                cartCountLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void getSearchKey() {

        IndexController.searchKeyList(new HttpCallBack<SearchKeyBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, SearchKeyBean bean) {
                searchKeyLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

    public void getAppAdvert() {

        AppController.getAppAdvert(new HttpCallBack<AppAdvertBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, AppAdvertBean bean) {
                appAdvertLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(4, reason));
            }
        });

    }

}
