package top.yokey.shopai.main.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.RedPacketDetailBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberPacketController;
import top.yokey.shopai.zsdk.controller.RedPacketController;

public class MainRedPacketVM extends BaseViewModel {

    private final MutableLiveData<RedPacketDetailBean> detailLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> getLiveData = new MutableLiveData<>();

    public MainRedPacketVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<RedPacketDetailBean> getDetailLiveData() {

        return detailLiveData;

    }

    public MutableLiveData<String> getGetLiveData() {

        return getLiveData;

    }

    public void getDetail(String id) {

        RedPacketController.index(id, new HttpCallBack<RedPacketDetailBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, RedPacketDetailBean bean) {
                detailLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getRedPack(String id) {

        MemberPacketController.getPack(id, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                getLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
