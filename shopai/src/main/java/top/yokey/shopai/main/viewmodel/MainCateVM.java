package top.yokey.shopai.main.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.BrandListBean;
import top.yokey.shopai.zsdk.bean.GoodsClassBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.BrandController;
import top.yokey.shopai.zsdk.controller.GoodsClassController;

public class MainCateVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<BrandListBean>> brandLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<GoodsClassBean>> goodsClassLiveData = new MutableLiveData<>();

    public MainCateVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<BrandListBean>> getBrandLiveData() {

        return brandLiveData;

    }

    public MutableLiveData<ArrayList<GoodsClassBean>> getGoodsClassLiveData() {

        return goodsClassLiveData;

    }

    public void getBrand() {

        BrandController.recommendList(new HttpCallBack<ArrayList<BrandListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<BrandListBean> list) {
                brandLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getGoodsClass() {

        GoodsClassController.getChildAllList(new HttpCallBack<ArrayList<GoodsClassBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<GoodsClassBean> list) {
                goodsClassLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
