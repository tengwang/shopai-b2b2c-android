package top.yokey.shopai.main.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.AreaListBean;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.AreaController;

public class MainAreaVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<AreaListBean>> provinceLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<AreaListBean>> cityLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<AreaListBean>> areaLiveData = new MutableLiveData<>();

    public MainAreaVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<AreaListBean>> getProvinceLiveData() {

        return provinceLiveData;

    }

    public MutableLiveData<ArrayList<AreaListBean>> getCityLiveData() {

        return cityLiveData;

    }

    public MutableLiveData<ArrayList<AreaListBean>> getAreaLiveData() {

        return areaLiveData;

    }

    public void getProvince() {

        AreaController.areaList("0", new HttpCallBack<ArrayList<AreaListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<AreaListBean> list) {
                provinceLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getCity(String provinceId) {

        AreaController.areaList(provinceId, new HttpCallBack<ArrayList<AreaListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<AreaListBean> list) {
                cityLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void getArea(String cityId) {

        AreaController.areaList(cityId, new HttpCallBack<ArrayList<AreaListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<AreaListBean> list) {
                areaLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

}
