package top.yokey.shopai.main.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zsdk.bean.CartBean;

public class MainCartAdapter extends RecyclerView.Adapter<MainCartAdapter.ViewHolder> {

    private final ArrayList<CartBean> arrayList;
    private final LineDecoration lineDecoration = new LineDecoration(App.get().dp2Px(8), false);
    private OnItemClickListener onItemClickListener = null;

    public MainCartAdapter(ArrayList<CartBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        CartBean bean = arrayList.get(position);
        holder.mainCheckBox.setChecked(bean.isSelect());
        holder.nameTextView.setText(bean.getStoreName());
        MainCartGoodsAdapter adapter = new MainCartGoodsAdapter(bean.getGoods());
        App.get().setRecyclerView(holder.mainRecyclerView, adapter);
        holder.mainRecyclerView.removeItemDecoration(lineDecoration);
        holder.mainRecyclerView.addItemDecoration(lineDecoration);
        if (bean.getMansong() == null || bean.getMansong().size() == 0) {
            holder.manSongLinearLayout.setVisibility(View.GONE);
            holder.zengPinLinearLayout.setVisibility(View.GONE);
        } else {
            holder.manSongLinearLayout.setVisibility(View.VISIBLE);
            holder.manSongTextView.setText(bean.getMansong().get(0).getDesc());
            if (bean.getMansong().get(0).getGoodsName() == null || bean.getMansong().get(0).getUrl() == null) {
                holder.zengPinLinearLayout.setVisibility(View.GONE);
            } else {
                holder.zengPinLinearLayout.setVisibility(View.VISIBLE);
                holder.zengPinTextView.setText(bean.getMansong().get(0).getGoodsName());
                ImageHelp.get().displayRadius(bean.getMansong().get(0).getUrl(), holder.zengPinImageView);
            }
        }
        if (VerifyUtil.isEmpty(bean.getFreeFreight())) {
            holder.freeFreightLinearLayout.setVisibility(View.GONE);
        } else {
            LinearLayoutCompat.LayoutParams layoutParams = (LinearLayoutCompat.LayoutParams) holder.freeFreightLinearLayout.getLayoutParams();
            if (holder.manSongLinearLayout.getVisibility() == View.GONE) {
                layoutParams.setMargins(0, App.get().dp2Px(8), 0, 0);
            } else {
                layoutParams.setMargins(0, App.get().dp2Px(4), 0, 0);
            }
            holder.freeFreightLinearLayout.setLayoutParams(layoutParams);
            holder.freeFreightLinearLayout.setVisibility(View.VISIBLE);
            holder.freeFreightTextView.setText(bean.getFreeFreight());
        }

        adapter.setOnItemClickListener(new MainCartGoodsAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position1, CartBean.GoodsBean goodsBean) {
                if (onItemClickListener != null) {
                    onItemClickListener.onClickGoods(position, position1, goodsBean);
                }
            }

            @Override
            public void onDelete(int position1, CartBean.GoodsBean goodsBean) {
                if (onItemClickListener != null) {
                    onItemClickListener.onClickGoodsDelete(position, position1, goodsBean);
                }
            }

            @Override
            public void onAdd(int position1, CartBean.GoodsBean goodsBean) {
                if (onItemClickListener != null) {
                    onItemClickListener.onClickGoodsAdd(position, position1, goodsBean);
                }
            }

            @Override
            public void onSub(int position1, CartBean.GoodsBean goodsBean) {
                if (onItemClickListener != null) {
                    onItemClickListener.onClickGoodsSub(position, position1, goodsBean);
                }
            }

            @Override
            public void onCheck(int position1, boolean select, CartBean.GoodsBean goodsBean) {
                if (onItemClickListener != null) {
                    onItemClickListener.onClickGoodsCheck(position, position1, select, goodsBean);
                }
            }
        });

        holder.mainCheckBox.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickCheck(position, holder.mainCheckBox.isChecked(), bean);
            }
        });

        holder.nameTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickStore(position, bean);
            }
        });

        holder.mainLinearLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_main_cart, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, CartBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickStore(int position, CartBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickCheck(int position, boolean select, CartBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickGoods(int position, int positionGoods, CartBean.GoodsBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickGoodsDelete(int position, int positionGoods, CartBean.GoodsBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickGoodsAdd(int position, int positionGoods, CartBean.GoodsBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickGoodsSub(int position, int positionGoods, CartBean.GoodsBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickGoodsCheck(int position, int positionGoods, boolean select, CartBean.GoodsBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayoutCompat mainLinearLayout;
        private final AppCompatCheckBox mainCheckBox;
        private final AppCompatTextView nameTextView;
        private final LinearLayoutCompat manSongLinearLayout;
        private final AppCompatTextView manSongTextView;
        private final LinearLayoutCompat zengPinLinearLayout;
        private final AppCompatTextView zengPinTextView;
        private final AppCompatImageView zengPinImageView;
        private final LinearLayoutCompat freeFreightLinearLayout;
        private final AppCompatTextView freeFreightTextView;
        private final RecyclerView mainRecyclerView;

        private ViewHolder(View view) {

            super(view);
            mainLinearLayout = view.findViewById(R.id.mainLinearLayout);
            mainCheckBox = view.findViewById(R.id.mainCheckBox);
            nameTextView = view.findViewById(R.id.nameTextView);
            manSongLinearLayout = view.findViewById(R.id.manSongLinearLayout);
            manSongTextView = view.findViewById(R.id.manSongTextView);
            zengPinLinearLayout = view.findViewById(R.id.zengPinLinearLayout);
            zengPinTextView = view.findViewById(R.id.zengPinTextView);
            zengPinImageView = view.findViewById(R.id.zengPinImageView);
            freeFreightLinearLayout = view.findViewById(R.id.freeFreightLinearLayout);
            freeFreightTextView = view.findViewById(R.id.freeFreightTextView);
            mainRecyclerView = view.findViewById(R.id.mainRecyclerView);

        }

    }

}
