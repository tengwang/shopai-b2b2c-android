package top.yokey.shopai.main.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.ArticleBean;
import top.yokey.shopai.zsdk.bean.ArticleClassBean;
import top.yokey.shopai.zsdk.bean.ArticleListBean;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.ArticleClassController;
import top.yokey.shopai.zsdk.controller.ArticleController;

public class MainArticleVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<ArticleClassBean>> classLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<ArticleListBean>> articleLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArticleBean> articleDetailLiveData = new MutableLiveData<>();

    public MainArticleVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<ArticleClassBean>> getClassLiveData() {

        return classLiveData;

    }

    public MutableLiveData<ArrayList<ArticleListBean>> getArticleLiveData() {

        return articleLiveData;

    }

    public MutableLiveData<ArticleBean> getArticleDetailLiveData() {

        return articleDetailLiveData;

    }

    public void getArticleClass() {

        ArticleClassController.index(new HttpCallBack<ArrayList<ArticleClassBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<ArticleClassBean> list) {
                classLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getArticle(String acId, String keyword) {

        ArticleController.articleList(acId, keyword, new HttpCallBack<ArrayList<ArticleListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<ArticleListBean> list) {
                articleLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void getArticleDetail(String id) {

        ArticleController.articleShow(id, new HttpCallBack<ArticleBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArticleBean bean) {
                articleDetailLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

}
