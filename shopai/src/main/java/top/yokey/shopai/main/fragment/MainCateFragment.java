package top.yokey.shopai.main.fragment;

import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;

import com.google.zxing.client.android.MNScanManager;
import com.google.zxing.client.android.model.MNScanConfig;
import com.jeremyliao.liveeventbus.LiveEventBus;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.main.adapter.MainCateBrandAdapter;
import top.yokey.shopai.main.adapter.MainCateOneAdapter;
import top.yokey.shopai.main.adapter.MainCateTwoAdapter;
import top.yokey.shopai.main.viewmodel.MainCateVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseFragment;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.BrandListBean;
import top.yokey.shopai.zsdk.bean.GoodsClassBean;
import top.yokey.shopai.zsdk.data.GoodsSearchData;

public class MainCateFragment extends BaseFragment {

    private final ArrayList<GoodsClassBean> oneArrayList = new ArrayList<>();
    private final MainCateOneAdapter oneAdapter = new MainCateOneAdapter(oneArrayList);
    private final ArrayList<BrandListBean> brandArrayList = new ArrayList<>();
    private final MainCateBrandAdapter brandAdapter = new MainCateBrandAdapter(brandArrayList);
    private final ArrayList<GoodsClassBean.GcListBean> classArrayList = new ArrayList<>();
    private final MainCateTwoAdapter classAdapter = new MainCateTwoAdapter(classArrayList);
    private AppCompatEditText searchEditText = null;
    private AppCompatImageView scanImageView = null;
    private AppCompatImageView messageImageView = null;
    private AppCompatTextView messageDotTextView = null;
    private PullRefreshView onePullRefreshView = null;
    private PullRefreshView brandPullRefreshView = null;
    private PullRefreshView classPullRefreshView = null;
    private MainCateVM vm = null;

    @Override
    public View initView() {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_main_cate, null);
        searchEditText = view.findViewById(R.id.searchEditText);
        scanImageView = view.findViewById(R.id.scanImageView);
        messageImageView = view.findViewById(R.id.messageImageView);
        messageDotTextView = view.findViewById(R.id.messageDotTextView);
        onePullRefreshView = view.findViewById(R.id.onePullRefreshView);
        brandPullRefreshView = view.findViewById(R.id.brandPullRefreshView);
        classPullRefreshView = view.findViewById(R.id.classPullRefreshView);
        return view;

    }

    @Override
    public void initData() {

        onePullRefreshView.setAdapter(oneAdapter);
        onePullRefreshView.setCanRefresh(false);
        onePullRefreshView.setCanLoadMore(false);
        onePullRefreshView.setLoad();
        brandPullRefreshView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        brandPullRefreshView.setAdapter(brandAdapter);
        brandPullRefreshView.setCanRefresh(false);
        brandPullRefreshView.setCanLoadMore(false);
        brandPullRefreshView.setLoad();
        brandPullRefreshView.setVisibility(View.VISIBLE);
        classPullRefreshView.setAdapter(classAdapter);
        classPullRefreshView.setCanRefresh(false);
        classPullRefreshView.setCanLoadMore(false);
        classPullRefreshView.setLoad();
        classPullRefreshView.setVisibility(View.GONE);
        vm = getVM(MainCateVM.class);
        vm.getGoodsClass();
        vm.getBrand();

    }

    @Override
    public void initEvent() {

        searchEditText.setOnClickListener(view -> App.get().start(ARoutePath.MAIN_SEARCH));

        scanImageView.setOnClickListener(view -> {
            MNScanConfig scanConfig = new MNScanConfig.Builder()
                    .isShowVibrate(false)
                    .isShowBeep(true)
                    .isShowPhotoAlbum(true)
                    .setScanHintText(getString(R.string.qrCodeScan))
                    .setZoomControllerLocation(MNScanConfig.ZoomControllerLocation.Bottom)
                    .isShowZoomController(true)
                    .builder();
            MNScanManager.startScan(requireActivity(), scanConfig, (code, data) -> {
                switch (code) {
                    case MNScanManager.RESULT_SUCCESS:
                        String result = data.getStringExtra(MNScanManager.INTENT_KEY_RESULT_SUCCESS);
                        if (result.contains(Constant.URL)) {
                            App.get().startUrl(result);
                            return;
                        }
                        ToastHelp.get().show(data.getStringExtra(MNScanManager.INTENT_KEY_RESULT_SUCCESS));
                    case MNScanManager.RESULT_FAIL:
                        ToastHelp.get().show(data.getStringExtra(MNScanManager.INTENT_KEY_RESULT_ERROR));
                        break;
                }
            });
        });

        messageImageView.setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_MESSAGE));

        onePullRefreshView.setOnClickListener(view -> {
            if (onePullRefreshView.isError() || oneArrayList.size() == 0) {
                vm.getGoodsClass();
            }
        });

        oneAdapter.setOnItemClickListener((position, bean) -> {
            for (int i = 0; i < oneArrayList.size(); i++) {
                oneArrayList.get(i).setSelect(false);
            }
            oneArrayList.get(position).setSelect(true);
            oneAdapter.notifyDataSetChanged();
            if (position == 0) {
                brandPullRefreshView.setVisibility(View.VISIBLE);
                classPullRefreshView.setVisibility(View.GONE);
                return;
            }
            classArrayList.clear();
            classArrayList.addAll(oneArrayList.get(position).getGcList());
            classPullRefreshView.setComplete();
            brandPullRefreshView.setVisibility(View.GONE);
            classPullRefreshView.setVisibility(View.VISIBLE);
        });

        brandAdapter.setOnItemClickListener((position, bean) -> {
            GoodsSearchData data = new GoodsSearchData();
            data.setBrandId(bean.getBrandId());
            data.setGcId("");
            App.get().startGoodsList(data);
        });

        classAdapter.setOnItemClickListener(new MainCateTwoAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, GoodsClassBean.GcListBean bean) {
                GoodsSearchData data = new GoodsSearchData();
                data.setBrandId("");
                data.setGcId(bean.getGcId());
                App.get().startGoodsList(data);
            }

            @Override
            public void onClickItem(int position, int positionItem, GoodsClassBean.GcListBean.ChildBean bean) {
                GoodsSearchData data = new GoodsSearchData();
                data.setBrandId("");
                data.setGcId(bean.getGcId());
                App.get().startGoodsList(data);
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getBrandLiveData().observe(this, list -> {
            brandArrayList.clear();
            brandArrayList.addAll(list);
            brandPullRefreshView.setComplete();
        });

        vm.getGoodsClassLiveData().observe(this, list -> {
            oneArrayList.clear();
            GoodsClassBean goodsClassBean = new GoodsClassBean();
            goodsClassBean.setGcName(getString(R.string.recommendBrand));
            oneArrayList.add(goodsClassBean);
            oneArrayList.addAll(list);
            oneArrayList.get(0).setSelect(true);
            onePullRefreshView.setComplete();
        });

        vm.getErrorLiveData().observe(this, bean -> {
            switch (bean.getCode()) {
                case 1:
                    DialogHelp.get().query(getActivity(), R.string.loadFailure, bean.getReason(), view -> vm.getBrand(), view -> vm.getBrand());
                    break;
                case 2:
                    DialogHelp.get().query(getActivity(), R.string.loadFailure, bean.getReason(), view -> vm.getGoodsClass(), view -> vm.getGoodsClass());
                    break;
            }
        });

        LiveEventBus.get(Constant.DATA_CONTENT, String.class).observe(this, string -> searchEditText.setHint(string));

        LiveEventBus.get(Constant.DATA_MSG_COUNT, String.class).observe(this, string -> {
            messageDotTextView.setVisibility(string.equals("0") ? View.GONE : View.VISIBLE);
            messageDotTextView.setText(string);
        });

    }

}
