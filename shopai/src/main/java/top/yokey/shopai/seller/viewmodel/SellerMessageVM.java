package top.yokey.shopai.seller.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.MessageListBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.SellerChatController;

public class SellerMessageVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<MessageListBean>> messageMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> delMessageMutableLiveData = new MutableLiveData<>();

    public SellerMessageVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<MessageListBean>> getMessageMutableLiveData() {

        return messageMutableLiveData;

    }

    public MutableLiveData<String> getDelMessageMutableLiveData() {

        return delMessageMutableLiveData;

    }

    public void getMessageList() {

        SellerChatController.getUserList(new HttpCallBack<ArrayList<MessageListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<MessageListBean> list) {
                messageMutableLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void delMessage(String tId, int position) {

        SellerChatController.delMsg(tId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                delMessageMutableLiveData.setValue(position + "");
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
