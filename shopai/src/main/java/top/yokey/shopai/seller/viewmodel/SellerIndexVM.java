package top.yokey.shopai.seller.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.SellerIndexBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.SellerChatController;
import top.yokey.shopai.zsdk.controller.SellerIndexController;

public class SellerIndexVM extends BaseViewModel {

    private final MutableLiveData<SellerIndexBean> indexLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> msgCountLiveData = new MutableLiveData<>();

    public SellerIndexVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<SellerIndexBean> getIndexLiveData() {

        return indexLiveData;

    }

    public MutableLiveData<String> getMsgCountLiveData() {

        return msgCountLiveData;

    }

    public void index() {

        SellerIndexController.index(new HttpCallBack<SellerIndexBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, SellerIndexBean bean) {
                indexLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getMsgCount() {

        SellerChatController.getMsgCount(new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                msgCountLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
