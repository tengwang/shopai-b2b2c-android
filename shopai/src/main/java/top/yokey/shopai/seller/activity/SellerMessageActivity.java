package top.yokey.shopai.seller.activity;

import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.seller.adapter.SellerMessageAdapter;
import top.yokey.shopai.seller.viewmodel.SellerMessageVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.util.ConvertUtil;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.MessageListBean;

@Route(path = ARoutePath.SELLER_MESSAGE)
public class SellerMessageActivity extends BaseActivity {

    private final ArrayList<MessageListBean> arrayList = new ArrayList<>();
    private final SellerMessageAdapter adapter = new SellerMessageAdapter(arrayList);
    private Toolbar mainToolbar = null;
    private PullRefreshView mainPullRefreshView = null;
    private SellerMessageVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_pull_refresh_view);
        mainToolbar = findViewById(R.id.mainToolbar);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.messageList);
        observeKeyborad(R.id.mainLinearLayout);
        mainPullRefreshView.setItemDecoration(new LineDecoration(1, App.get().getColors(R.color.divider), false));
        mainPullRefreshView.setAdapter(adapter);
        mainPullRefreshView.setCanLoadMore(false);
        vm = getVM(SellerMessageVM.class);

    }

    @Override
    public void initEvent() {

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                getData();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }

            @Override
            public void onLoadMore() {

            }
        });

        adapter.setOnItemClickListener(new SellerMessageAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, MessageListBean bean) {
                String url = ShopAISdk.get().getSellerChatUrl(bean.getUId());
                App.get().start(ARoutePath.SELLER_CHAT, Constant.DATA_URL, url);
            }

            @Override
            public void onLongClick(int position, MessageListBean bean) {
                DialogHelp.get().query(
                        get(),
                        R.string.confirmSelection,
                        R.string.deleteMessage,
                        null,
                        v -> vm.delMessage(bean.getUId(), position)
                );
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getMessageMutableLiveData().observe(this, list -> {
            arrayList.clear();
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getDelMessageMutableLiveData().observe(this, string -> {
            arrayList.remove(ConvertUtil.string2Int(string));
            mainPullRefreshView.setComplete();
        });

        vm.getErrorLiveData().observe(this, bean -> {
            switch (bean.getCode()) {
                case 1:
                    if (arrayList.size() != 0) {
                        ToastHelp.get().show(bean.getReason());
                    } else {
                        mainPullRefreshView.setError(bean.getReason());
                    }
                    break;
                case 2:
                    ToastHelp.get().show(bean.getReason());
                    break;
            }
        });

    }

    @Override
    public void onResume() {

        super.onResume();
        getData();

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView.setLoad();
        vm.getMessageList();

    }

}
