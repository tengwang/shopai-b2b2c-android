package top.yokey.shopai.seller.adapter;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zsdk.bean.OrderSellerListBean;

public class SellerOrderAdapter extends RecyclerView.Adapter<SellerOrderAdapter.ViewHolder> {

    private final ArrayList<OrderSellerListBean> arrayList;
    private final LineDecoration lineDecoration = new LineDecoration(App.get().dp2Px(12), true);
    private OnItemClickListener onItemClickListener = null;

    public SellerOrderAdapter(ArrayList<OrderSellerListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        OrderSellerListBean bean = arrayList.get(position);
        SellerOrderGoodsAdapter adapter = new SellerOrderGoodsAdapter(bean.getGoodsList());
        App.get().setRecyclerView(holder.mainRecyclerView, adapter);
        holder.mainRecyclerView.removeItemDecoration(lineDecoration);
        holder.mainRecyclerView.addItemDecoration(lineDecoration);
        if (bean.getZengpinList().size() == 0) {
            holder.zengPinLinearLayout.setVisibility(View.GONE);
        } else {
            holder.zengPinLinearLayout.setVisibility(View.VISIBLE);
            holder.zengPinTextView.setText(bean.getZengpinList().get(0).getGoodsName());
            ImageHelp.get().displayRadius(bean.getZengpinList().get(0).getImage240Url(), holder.zengPinImageView);
        }
        holder.snTextView.setText(String.format(App.get().getString(R.string.orderPaySn), bean.getOrderSn()));
        holder.descTextView.setText(bean.getStateDesc());
        int count = 0;
        for (int i = 0; i < bean.getExtendOrderGoods().size(); i++) {
            count += Integer.parseInt(bean.getExtendOrderGoods().get(i).getGoodsNum());
        }
        String temp;
        if (!bean.getShippingFee().equals("0.00") && !bean.getPointsNumber().equals("0")) {
            temp = String.format(App.get().getString(R.string.htmlOrderSettlementShippingFeePoint), count + "", bean.getOrderAmount(), bean.getShippingFee(), bean.getPointsNumber(), bean.getPointsMoney());
        } else if (bean.getShippingFee().equals("0.00") && !bean.getPointsNumber().equals("0")) {
            temp = String.format(App.get().getString(R.string.htmlOrderSettlementPoint), count + "", bean.getOrderAmount(), bean.getPointsNumber(), bean.getPointsMoney());
        } else if (!bean.getShippingFee().equals("0.00") && bean.getPointsNumber().equals("0")) {
            temp = String.format(App.get().getString(R.string.htmlOrderSettlementShippingFee), count + "", bean.getOrderAmount(), bean.getShippingFee());
        } else {
            temp = String.format(App.get().getString(R.string.htmlOrderSettlementNormal), count + "", bean.getOrderAmount());
        }
        holder.totalTextView.setText(Html.fromHtml(App.get().handlerHtml(temp, "#EE0000")));
        holder.timeTextView.setText(bean.getAddTimeText());
        holder.optionTextView.setVisibility(View.GONE);
        holder.operaTextView.setVisibility(View.VISIBLE);
        switch (bean.getOrderState()) {
            case "0":
                holder.operaTextView.setText(R.string.orderDetailed);
                break;
            case "10":
                holder.optionTextView.setVisibility(View.VISIBLE);
                holder.optionTextView.setText(R.string.cancelOrder);
                holder.operaTextView.setText(R.string.modifyPrice);
                break;
            case "20":
                holder.optionTextView.setVisibility(View.VISIBLE);
                holder.optionTextView.setText(R.string.orderDetailed);
                holder.operaTextView.setText(R.string.orderDeliver);
                break;
            case "30":
                if (bean.getIfLock().equals("true")) {
                    holder.descTextView.setText(R.string.refundReturnIng);
                }
                holder.operaTextView.setText(R.string.orderDetailed);
                break;
            case "40":
                holder.operaTextView.setText(R.string.orderDetailed);
                break;
        }

        holder.optionTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onOption(position, bean);
            }
        });

        holder.operaTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onOpera(position, bean);
            }
        });

        holder.mainLinearLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

        adapter.setOnItemClickListener((position1, bean1) -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickGoods(position, position1, bean1);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_seller_order, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, OrderSellerListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onOption(int position, OrderSellerListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onOpera(int position, OrderSellerListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickGoods(int position, int itemPosition, OrderSellerListBean.GoodsListBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayoutCompat mainLinearLayout;
        private final AppCompatTextView snTextView;
        private final AppCompatTextView descTextView;
        private final RecyclerView mainRecyclerView;
        private final LinearLayoutCompat zengPinLinearLayout;
        private final AppCompatTextView zengPinTextView;
        private final AppCompatImageView zengPinImageView;
        private final AppCompatTextView totalTextView;
        private final AppCompatTextView timeTextView;
        private final AppCompatTextView optionTextView;
        private final AppCompatTextView operaTextView;

        private ViewHolder(View view) {

            super(view);
            mainLinearLayout = view.findViewById(R.id.mainLinearLayout);
            snTextView = view.findViewById(R.id.snTextView);
            descTextView = view.findViewById(R.id.descTextView);
            mainRecyclerView = view.findViewById(R.id.mainRecyclerView);
            zengPinLinearLayout = view.findViewById(R.id.zengPinLinearLayout);
            zengPinTextView = view.findViewById(R.id.zengPinTextView);
            zengPinImageView = view.findViewById(R.id.zengPinImageView);
            totalTextView = view.findViewById(R.id.totalTextView);
            timeTextView = view.findViewById(R.id.timeTextView);
            optionTextView = view.findViewById(R.id.optionTextView);
            operaTextView = view.findViewById(R.id.operaTextView);

        }

    }

}
