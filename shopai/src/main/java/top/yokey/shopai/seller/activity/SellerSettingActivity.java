package top.yokey.shopai.seller.activity;

import android.text.InputType;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import top.yokey.shopai.R;
import top.yokey.shopai.seller.viewmodel.SellerSettingVM;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zsdk.data.SellerStoreData;

@Route(path = ARoutePath.SELLER_SETTING)
public class SellerSettingActivity extends BaseActivity {

    private final SellerStoreData data = new SellerStoreData();
    private Toolbar mainToolbar = null;
    private LinearLayoutCompat goodsLinearLayout = null;
    private AppCompatTextView goodsTextView = null;
    private LinearLayoutCompat qqLinearLayout = null;
    private AppCompatTextView qqTextView = null;
    private LinearLayoutCompat wangwangLinearLayout = null;
    private AppCompatTextView wangwangTextView = null;
    private LinearLayoutCompat keywordLinearLayout = null;
    private AppCompatTextView keywordTextView = null;
    private LinearLayoutCompat descriptionLinearLayout = null;
    private AppCompatTextView descriptionTextView = null;
    private LinearLayoutCompat phoneLinearLayout = null;
    private AppCompatTextView phoneTextView = null;
    private AppCompatTextView modifyTextView = null;
    private SellerSettingVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_seller_setting);
        mainToolbar = findViewById(R.id.mainToolbar);
        goodsLinearLayout = findViewById(R.id.goodsLinearLayout);
        goodsTextView = findViewById(R.id.goodsTextView);
        qqLinearLayout = findViewById(R.id.qqLinearLayout);
        qqTextView = findViewById(R.id.qqTextView);
        wangwangLinearLayout = findViewById(R.id.wangwangLinearLayout);
        wangwangTextView = findViewById(R.id.wangwangTextView);
        keywordLinearLayout = findViewById(R.id.keywordLinearLayout);
        keywordTextView = findViewById(R.id.keywordTextView);
        descriptionLinearLayout = findViewById(R.id.descriptionLinearLayout);
        descriptionTextView = findViewById(R.id.descriptionTextView);
        phoneLinearLayout = findViewById(R.id.phoneLinearLayout);
        phoneTextView = findViewById(R.id.phoneTextView);
        modifyTextView = findViewById(R.id.modifyTextView);

    }

    @Override
    public void initData() {

        observeKeyborad(R.id.mainLinearLayout);
        setToolbar(mainToolbar, R.string.storeSetting);
        vm = getVM(SellerSettingVM.class);
        vm.getStore();

    }

    @Override
    public void initEvent() {

        goodsLinearLayout.setOnClickListener(view -> DialogHelp.get().input(get(), InputType.TYPE_CLASS_TEXT, R.string.mainGoods, data.getZy(), null, content -> {
            if (VerifyUtil.isEmpty(content)) {
                ToastHelp.get().show(R.string.pleaseInputContent);
                return;
            }
            data.setZy(content);
            goodsTextView.setText(data.getZy());
        }));

        qqLinearLayout.setOnClickListener(view -> DialogHelp.get().input(get(), InputType.TYPE_CLASS_TEXT, R.string.qq, data.getQq(), null, content -> {
            if (VerifyUtil.isEmpty(content)) {
                ToastHelp.get().show(R.string.pleaseInputContent);
                return;
            }
            data.setQq(content);
            qqTextView.setText(data.getQq());
        }));

        wangwangLinearLayout.setOnClickListener(view -> DialogHelp.get().input(get(), InputType.TYPE_CLASS_TEXT, R.string.wangwang, data.getWw(), null, content -> {
            if (VerifyUtil.isEmpty(content)) {
                ToastHelp.get().show(R.string.pleaseInputContent);
                return;
            }
            data.setWw(content);
            wangwangTextView.setText(data.getWw());
        }));

        keywordLinearLayout.setOnClickListener(view -> DialogHelp.get().input(get(), InputType.TYPE_CLASS_TEXT, R.string.seoKeyword, data.getKeyword(), null, content -> {
            if (VerifyUtil.isEmpty(content)) {
                ToastHelp.get().show(R.string.pleaseInputContent);
                return;
            }
            data.setKeyword(content);
            keywordTextView.setText(data.getKeyword());
        }));

        descriptionLinearLayout.setOnClickListener(view -> DialogHelp.get().input(get(), InputType.TYPE_CLASS_TEXT, R.string.seoStoreDescription, data.getDescription(), null, content -> {
            if (VerifyUtil.isEmpty(content)) {
                ToastHelp.get().show(R.string.pleaseInputContent);
                return;
            }
            data.setDescription(content);
            descriptionTextView.setText(data.getDescription());
        }));

        phoneLinearLayout.setOnClickListener(view -> DialogHelp.get().input(get(), InputType.TYPE_CLASS_TEXT, R.string.storePhone, data.getPhone(), null, content -> {
            if (VerifyUtil.isEmpty(content)) {
                ToastHelp.get().show(R.string.pleaseInputContent);
                return;
            }
            data.setPhone(content);
            phoneTextView.setText(data.getPhone());
        }));

        modifyTextView.setOnClickListener(view -> {
            hideKeyboard();
            vm.modify(data);
            modifyTextView.setEnabled(false);
            modifyTextView.setText(R.string.handlerIng);
        });

    }

    @Override
    public void initObserve() {

        vm.getStoreLiveData().observe(this, bean -> {
            data.setQq(bean.getStoreQq());
            data.setWw(bean.getStoreWw());
            data.setZy(bean.getStoreZy());
            data.setKeyword(bean.getStoreKeywords());
            data.setDescription(bean.getStoreDescription());
            data.setPhone(bean.getStorePhone());
            qqTextView.setText(data.getQq());
            wangwangTextView.setText(data.getWw());
            goodsTextView.setText(data.getZy());
            keywordTextView.setText(data.getKeyword());
            descriptionTextView.setText(data.getDescription());
            phoneTextView.setText(data.getPhone());
        });

        vm.getModifyLiveData().observe(this, string -> {
            ToastHelp.get().showSuccess();
            onReturn(false);
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> vm.getStore(), view -> onReturn(false));
                return;
            }
            modifyTextView.setEnabled(true);
            modifyTextView.setText(R.string.modify);
            ToastHelp.get().show(bean.getReason());
        });

    }

}
