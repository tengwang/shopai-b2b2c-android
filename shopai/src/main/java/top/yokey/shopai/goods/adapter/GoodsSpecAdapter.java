package top.yokey.shopai.goods.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import top.yokey.shopai.R;

public class GoodsSpecAdapter extends RecyclerView.Adapter<GoodsSpecAdapter.ViewHolder> {

    private final ArrayList<HashMap<String, String>> arrayList;
    private onItemClickListener onItemClickListener = null;

    public GoodsSpecAdapter(ArrayList<HashMap<String, String>> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        HashMap<String, String> hashMap = arrayList.get(position);
        holder.mainTextView.setText(hashMap.get("value"));
        if (Objects.requireNonNull(hashMap.get("default")).equals("1")) {
            holder.mainTextView.setTextColor(Color.WHITE);
            holder.mainTextView.setBackgroundResource(R.drawable.selector_accent_12dp);
        } else {
            holder.mainTextView.setTextColor(Color.GRAY);
            holder.mainTextView.setBackgroundResource(R.drawable.selector_hollow_divider_12dp);
        }

        holder.mainLinearLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, hashMap.get("id"), hashMap.get("value"));
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_goods_spec, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(onItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface onItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, String id, String value);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayoutCompat mainLinearLayout;
        private final AppCompatTextView mainTextView;

        private ViewHolder(View view) {

            super(view);
            mainLinearLayout = view.findViewById(R.id.mainLinearLayout);
            mainTextView = view.findViewById(R.id.mainTextView);

        }

    }

}
