package top.yokey.shopai.goods.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zsdk.bean.SearchShowBean;

public class GoodsContractAdapter extends RecyclerView.Adapter<GoodsContractAdapter.ViewHolder> {

    private final ArrayList<SearchShowBean.ContractListBean> arrayList;
    private onItemClickListener onItemClickListener = null;

    public GoodsContractAdapter(ArrayList<SearchShowBean.ContractListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        SearchShowBean.ContractListBean bean = arrayList.get(position);
        holder.mainTextView.setText(bean.getName());
        if (bean.isSelect()) {
            holder.mainTextView.setTextColor(App.get().getColors(R.color.white));
            holder.mainTextView.setBackgroundResource(R.drawable.selector_accent_4dp);
        } else {
            holder.mainTextView.setTextColor(App.get().getColors(R.color.textTwo));
            holder.mainTextView.setBackgroundResource(R.drawable.selector_primary_dark_4dp);
        }

        holder.mainTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_goods_contract, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(onItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface onItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, SearchShowBean.ContractListBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final AppCompatTextView mainTextView;

        private ViewHolder(View view) {

            super(view);
            mainTextView = view.findViewById(R.id.mainTextView);

        }

    }

}
