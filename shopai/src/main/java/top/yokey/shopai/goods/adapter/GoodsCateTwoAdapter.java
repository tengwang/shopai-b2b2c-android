package top.yokey.shopai.goods.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.recycler.GridDecoration;
import top.yokey.shopai.zsdk.bean.GoodsClassBean;

public class GoodsCateTwoAdapter extends RecyclerView.Adapter<GoodsCateTwoAdapter.ViewHolder> {

    private final ArrayList<GoodsClassBean.GcListBean> arrayList;
    private final GridDecoration gridDecoration = new GridDecoration(3, App.get().dp2Px(16), false);
    private OnItemClickListener onItemClickListener = null;

    public GoodsCateTwoAdapter(ArrayList<GoodsClassBean.GcListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        GoodsClassBean.GcListBean bean = arrayList.get(position);
        holder.mainTextView.setText(bean.getGcName());
        holder.mainRecyclerView.removeItemDecoration(gridDecoration);
        GoodsCateThrAdapter adapter = new GoodsCateThrAdapter(bean.getChild());
        App.get().setRecyclerView(holder.mainRecyclerView, adapter);
        holder.mainRecyclerView.addItemDecoration(gridDecoration);
        holder.mainRecyclerView.setLayoutManager(new GridLayoutManager(App.get(), 3));

        adapter.setOnItemClickListener((position1, bean1) -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickItem(position, position1, bean1);
            }
        });

        holder.mainTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_goods_cate_two, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, GoodsClassBean.GcListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickItem(int position, int positionItem, GoodsClassBean.GcListBean.ChildBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final AppCompatTextView mainTextView;
        private final RecyclerView mainRecyclerView;

        private ViewHolder(View view) {

            super(view);
            mainTextView = view.findViewById(R.id.mainTextView);
            mainRecyclerView = view.findViewById(R.id.mainRecyclerView);

        }

    }

}
