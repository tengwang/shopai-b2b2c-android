package top.yokey.shopai.goods.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.GoodsRobbuyBean;
import top.yokey.shopai.zsdk.bean.GoodsXianshiBean;
import top.yokey.shopai.zsdk.bean.SearchShowBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.GoodsController;
import top.yokey.shopai.zsdk.controller.IndexController;
import top.yokey.shopai.zsdk.data.GoodsSearchData;

public class GoodsRobbuyVM extends BaseViewModel {

    private final MutableLiveData<SearchShowBean> searchShowLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<GoodsXianshiBean>> xianshiLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<GoodsRobbuyBean>> robbuyLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();

    public GoodsRobbuyVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<SearchShowBean> getSearchShowLiveData() {

        return searchShowLiveData;

    }

    public MutableLiveData<ArrayList<GoodsXianshiBean>> getXianshiLiveData() {

        return xianshiLiveData;

    }

    public MutableLiveData<ArrayList<GoodsRobbuyBean>> getRobbuyLiveData() {

        return robbuyLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public void getSearchShow() {

        IndexController.searchShow(new HttpCallBack<SearchShowBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, SearchShowBean bean) {
                searchShowLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getXianshi(GoodsSearchData data) {

        GoodsController.goodsDZList(data, new HttpCallBack<ArrayList<GoodsXianshiBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<GoodsXianshiBean> list) {
                xianshiLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void getRobbuy(GoodsSearchData data) {

        GoodsController.goodsGBList(data, new HttpCallBack<ArrayList<GoodsRobbuyBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<GoodsRobbuyBean> list) {
                robbuyLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

}
