package top.yokey.shopai.goods.activity;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import cc.shinichi.library.ImagePreview;
import top.yokey.shopai.R;
import top.yokey.shopai.goods.adapter.GoodsEvaluateAdapter;
import top.yokey.shopai.goods.viewmodel.GoodsEvaluateVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.EvaluateBean;

@Route(path = ARoutePath.GOODS_EVALUATE)
public class GoodsEvaluateActivity extends BaseActivity {

    private final AppCompatTextView[] navigationTextView = new AppCompatTextView[6];
    private final ArrayList<EvaluateBean> arrayList = new ArrayList<>();
    private final GoodsEvaluateAdapter adapter = new GoodsEvaluateAdapter(arrayList);
    @Autowired(name = Constant.DATA_ID)
    String goodsId = "";
    private Toolbar mainToolbar = null;
    private PullRefreshView mainPullRefreshView = null;
    private int page = 1;
    private String type = "";
    private GoodsEvaluateVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_goods_evaluate);
        mainToolbar = findViewById(R.id.mainToolbar);
        navigationTextView[0] = findViewById(R.id.allTextView);
        navigationTextView[1] = findViewById(R.id.goodTextView);
        navigationTextView[2] = findViewById(R.id.inTextView);
        navigationTextView[3] = findViewById(R.id.badTextView);
        navigationTextView[4] = findViewById(R.id.imageTextView);
        navigationTextView[5] = findViewById(R.id.appendTextView);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        if (VerifyUtil.isEmpty(goodsId)) {
            ToastHelp.get().showDataError();
            onReturn(false);
            return;
        }

        setToolbar(mainToolbar, R.string.goodsEvaluate);
        observeKeyborad(R.id.mainLinearLayout);
        mainPullRefreshView.setAdapter(adapter);
        vm = getVM(GoodsEvaluateVM.class);
        getData();

    }

    @Override
    public void initEvent() {

        navigationTextView[0].setOnClickListener(view -> updateNavigation(0));

        navigationTextView[1].setOnClickListener(view -> updateNavigation(1));

        navigationTextView[2].setOnClickListener(view -> updateNavigation(2));

        navigationTextView[3].setOnClickListener(view -> updateNavigation(3));

        navigationTextView[4].setOnClickListener(view -> updateNavigation(4));

        navigationTextView[5].setOnClickListener(view -> updateNavigation(5));

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                getData();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }

            @Override
            public void onLoadMore() {
                vm.getEvaluate(goodsId, type, page + "");
            }
        });

        adapter.setOnItemClickListener(new GoodsEvaluateAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, EvaluateBean bean) {

            }

            @Override
            public void onClickImage(int position, int pos, EvaluateBean bean) {
                ImagePreview.getInstance().setContext(get()).setIndex(pos).setImageList(bean.getGevalImage1024()).start();
            }

            @Override
            public void onClickImageAgain(int position, int pos, EvaluateBean bean) {
                ImagePreview.getInstance().setContext(get()).setIndex(pos).setImageList(bean.getGevalImageAgain1024()).start();
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getEvaluateLiveData().observe(this, list -> {
            if (page == 1) arrayList.clear();
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
            page++;
        });

        vm.getHasMoreLiveData().observe(this, bool -> mainPullRefreshView.setCanLoadMore(bool));

        vm.getErrorLiveData().observe(this, bean -> {
            if (arrayList.size() == 0) {
                mainPullRefreshView.setError(bean.getReason());
            } else {
                ToastHelp.get().show(bean.getReason());
            }
        });

    }

    //自定义方法

    private void getData() {

        page = 1;
        mainPullRefreshView.setLoad();
        vm.getEvaluate(goodsId, type, page + "");

    }

    private void updateNavigation(int position) {

        for (AppCompatTextView appCompatTextView : navigationTextView) {
            appCompatTextView.setTextColor(App.get().getColors(R.color.textTwo));
            appCompatTextView.setBackgroundResource(R.color.transparent);
        }
        navigationTextView[position].setTextColor(App.get().getColors(R.color.accent));
        navigationTextView[position].setBackgroundResource(R.drawable.selector_primary_16dp);
        type = position == 0 ? "" : position + "";
        getData();

    }

}
