package top.yokey.shopai.goods.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zsdk.bean.BuyCouponBean;
import yyydjk.com.library.CouponView;

public class GoodsBuyCouponAdapter extends RecyclerView.Adapter<GoodsBuyCouponAdapter.ViewHolder> {

    private final ArrayList<BuyCouponBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public GoodsBuyCouponAdapter(ArrayList<BuyCouponBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        BuyCouponBean bean = arrayList.get(position);
        holder.descTextView.setText(bean.getDesc());
        holder.limitTextView.setText(String.format(App.get().getString(R.string.voucherPriceLimit), bean.getCouponLimit()));
        holder.priceTextView.setText("￥");
        holder.priceTextView.append(bean.getCouponPrice());

        holder.mainCouponView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_goods_buy_coupon, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, BuyCouponBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final CouponView mainCouponView;
        private final AppCompatTextView descTextView;
        private final AppCompatTextView limitTextView;
        private final AppCompatTextView priceTextView;

        private ViewHolder(View view) {

            super(view);
            mainCouponView = view.findViewById(R.id.mainCouponView);
            descTextView = view.findViewById(R.id.descTextView);
            limitTextView = view.findViewById(R.id.limitTextView);
            priceTextView = view.findViewById(R.id.priceTextView);

        }

    }

}
