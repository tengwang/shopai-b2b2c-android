package top.yokey.shopai.goods.adapter;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.util.ConvertUtil;
import top.yokey.shopai.zcom.view.CountdownTextView;
import top.yokey.shopai.zsdk.bean.GoodsXianshiBean;

public class GoodsXianshiAdapter extends RecyclerView.Adapter<GoodsXianshiAdapter.ViewHolder> {

    private final ArrayList<GoodsXianshiBean> arrayList;
    private onItemClickListener onItemClickListener = null;

    public GoodsXianshiAdapter(ArrayList<GoodsXianshiBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        GoodsXianshiBean bean = arrayList.get(position);
        ImageHelp.get().displayRadius(bean.getImageUrl240(), holder.mainImageView);
        holder.nameTextView.setText(bean.getGoodsName());
        holder.priceTextView.setText("￥");
        holder.priceTextView.append(bean.getXianshiPrice());
        holder.priceMarketTextView.setText("￥");
        holder.priceMarketTextView.append(bean.getGoodsPrice());
        holder.priceMarketTextView.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.countdownTextView.init("", ConvertUtil.string2Long(bean.getEndtime()), App.get().getString(R.string.surplus) + "：", "");
        holder.countdownTextView.start(0);
        holder.tipsTextView.setText(bean.getXianshiTitle());

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_goods_xianshi, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(onItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface onItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, GoodsXianshiBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatImageView mainImageView;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView priceTextView;
        private final AppCompatTextView priceMarketTextView;
        private final CountdownTextView countdownTextView;
        private final AppCompatTextView tipsTextView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainImageView = view.findViewById(R.id.mainImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            priceTextView = view.findViewById(R.id.priceTextView);
            priceMarketTextView = view.findViewById(R.id.priceMarketTextView);
            countdownTextView = view.findViewById(R.id.countdownTextView);
            tipsTextView = view.findViewById(R.id.tipsTextView);

        }

    }

}
