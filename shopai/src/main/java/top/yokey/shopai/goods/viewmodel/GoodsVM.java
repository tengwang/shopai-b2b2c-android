package top.yokey.shopai.goods.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.GoodsCalcBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.GoodsController;
import top.yokey.shopai.zsdk.controller.MemberCartController;
import top.yokey.shopai.zsdk.controller.MemberFavoritesController;

public class GoodsVM extends BaseViewModel {

    private final MutableLiveData<String> goodsLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> bodyLiveData = new MutableLiveData<>();
    private final MutableLiveData<GoodsCalcBean> calcLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> favAddLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> favDelLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> addCartLiveData = new MutableLiveData<>();

    public GoodsVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<String> getGoodsLiveData() {

        return goodsLiveData;

    }

    public MutableLiveData<String> getBodyLiveData() {

        return bodyLiveData;

    }

    public MutableLiveData<GoodsCalcBean> getCalcLiveData() {

        return calcLiveData;

    }

    public MutableLiveData<String> getFavAddLiveData() {

        return favAddLiveData;

    }

    public MutableLiveData<String> getFavDelLiveData() {

        return favDelLiveData;

    }

    public MutableLiveData<String> getAddCartLiveData() {

        return addCartLiveData;

    }

    public void getDetailed(String goodsId) {

        GoodsController.goodsDetailed(goodsId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                goodsLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getBody(String goodsId) {

        GoodsController.goodsBody(goodsId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                bodyLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void calc(String goodsId, String areaId) {

        GoodsController.calc(goodsId, areaId, new HttpCallBack<GoodsCalcBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, GoodsCalcBean bean) {
                calcLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

    public void favoritesAdd(String goodsId) {

        MemberFavoritesController.favoritesAdd(goodsId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                favAddLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(4, reason));
            }
        });

    }

    public void favoritesDel(String goodsId) {

        MemberFavoritesController.favoritesDel(goodsId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                favDelLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(5, reason));
            }
        });

    }

    public void addCart(String goodsId, String quantity) {

        MemberCartController.cartAdd(goodsId, quantity, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                addCartLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(6, reason));
            }
        });

    }

}
