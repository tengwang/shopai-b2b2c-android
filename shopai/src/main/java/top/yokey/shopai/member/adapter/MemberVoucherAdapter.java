package top.yokey.shopai.member.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zsdk.bean.VoucherBean;
import yyydjk.com.library.CouponView;

public class MemberVoucherAdapter extends RecyclerView.Adapter<MemberVoucherAdapter.ViewHolder> {

    private final ArrayList<VoucherBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public MemberVoucherAdapter(ArrayList<VoucherBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        VoucherBean bean = arrayList.get(position);
        ImageHelp.get().display(bean.getVoucherTCustomimg(), holder.mainImageView);
        holder.titleTextView.setText(bean.getStoreName());
        holder.limitTextView.setText(String.format(App.get().getString(R.string.voucherPriceLimit), bean.getVoucherLimit()));
        holder.timeTextView.setText(String.format(App.get().getString(R.string.voucherTimeLimit), bean.getVoucherEndDateText()));
        holder.priceTextView.setText("￥");
        holder.priceTextView.append(bean.getVoucherPrice());
        holder.disableTextView.setVisibility(bean.getVoucherState().equals(Constant.COMMON_ENABLE) ? View.GONE : View.VISIBLE);

        holder.mainCouponView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_member_voucher, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, VoucherBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final CouponView mainCouponView;
        private final AppCompatImageView mainImageView;
        private final AppCompatTextView titleTextView;
        private final AppCompatTextView limitTextView;
        private final AppCompatTextView timeTextView;
        private final AppCompatTextView priceTextView;
        private final AppCompatTextView disableTextView;

        private ViewHolder(View view) {

            super(view);
            mainCouponView = view.findViewById(R.id.mainCouponView);
            mainImageView = view.findViewById(R.id.mainImageView);
            titleTextView = view.findViewById(R.id.titleTextView);
            limitTextView = view.findViewById(R.id.limitTextView);
            timeTextView = view.findViewById(R.id.timeTextView);
            priceTextView = view.findViewById(R.id.priceTextView);
            disableTextView = view.findViewById(R.id.disableTextView);

        }

    }

}
