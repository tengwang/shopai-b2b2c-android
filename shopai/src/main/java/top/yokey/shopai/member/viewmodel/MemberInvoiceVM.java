package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.InvoiceListBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberInvoiceController;

public class MemberInvoiceVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<InvoiceListBean>> invoiceLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> delLiveData = new MutableLiveData<>();

    public MemberInvoiceVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<InvoiceListBean>> getInvoiceLiveData() {

        return invoiceLiveData;

    }

    public MutableLiveData<String> getDelLiveData() {

        return delLiveData;

    }

    public void getInvoice() {

        MemberInvoiceController.invoiceList(new HttpCallBack<ArrayList<InvoiceListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<InvoiceListBean> list) {
                invoiceLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void delInvoice(String invId) {

        MemberInvoiceController.invoiceDel(invId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                delLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
