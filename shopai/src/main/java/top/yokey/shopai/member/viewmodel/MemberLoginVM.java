package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.LoginBean;
import top.yokey.shopai.zsdk.bean.MemberBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.LoginController;
import top.yokey.shopai.zsdk.controller.MemberIndexController;

public class MemberLoginVM extends BaseViewModel {

    private final MutableLiveData<LoginBean> loginLiveData = new MutableLiveData<>();
    private final MutableLiveData<MemberBean> memberLiveData = new MutableLiveData<>();

    public MemberLoginVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<LoginBean> getLoginLiveData() {

        return loginLiveData;

    }

    public MutableLiveData<MemberBean> getMemberLiveData() {

        return memberLiveData;

    }

    public void login(String username, String password) {

        LoginController.index(username, password, new HttpCallBack<LoginBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, LoginBean bean) {
                loginLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getMember() {

        MemberIndexController.index(new HttpCallBack<MemberBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, MemberBean bean) {
                memberLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
