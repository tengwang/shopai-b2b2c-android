package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.MemberAssetBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberIndexController;
import top.yokey.shopai.zsdk.controller.RechargeController;

public class MemberPreDepositVM extends BaseViewModel {

    private final MutableLiveData<MemberAssetBean> assetLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> indexLiveData = new MutableLiveData<>();

    public MemberPreDepositVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<MemberAssetBean> getAssetLiveData() {

        return assetLiveData;

    }

    public MutableLiveData<String> getIndexLiveData() {

        return indexLiveData;

    }

    public void getAsset() {

        MemberIndexController.myAsset(new HttpCallBack<MemberAssetBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, MemberAssetBean bean) {
                assetLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void index(String amount) {

        RechargeController.index(amount, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                indexLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
