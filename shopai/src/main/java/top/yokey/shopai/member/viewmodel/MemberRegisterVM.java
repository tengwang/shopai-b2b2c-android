package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.ConnectController;

public class MemberRegisterVM extends BaseViewModel {

    private final MutableLiveData<String> getCaptchaLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> checkCaptchaLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> registerLiveData = new MutableLiveData<>();

    public MemberRegisterVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<String> getGetCaptchaLiveData() {

        return getCaptchaLiveData;

    }

    public MutableLiveData<String> getCheckCaptchaLiveData() {

        return checkCaptchaLiveData;

    }

    public MutableLiveData<String> getRegisterLiveData() {

        return registerLiveData;

    }

    public void getCaptcha(String mobile, String type) {

        ConnectController.getSmsCaptcha(mobile, type, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                getCaptchaLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void checkCaptcha(String mobile, String type, String captcha) {

        ConnectController.checkSmsCaptcha(mobile, type, captcha, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                checkCaptchaLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void smsRegister(String mobile, String password, String captcha) {

        ConnectController.smsRegister(mobile, password, captcha, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                registerLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

}
