package top.yokey.shopai.member.activity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.member.viewmodel.MemberLoginVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.SharedHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zsdk.ShopAISdk;

@Route(path = ARoutePath.MEMBER_LOGIN)
public class MemberLoginActivity extends BaseActivity {

    private AppCompatImageView closeImageView = null;
    private AppCompatEditText usernameEditText = null;
    private AppCompatImageView usernameClearImageView = null;
    private AppCompatEditText passwordEditText = null;
    private AppCompatImageView passwordClearImageView = null;
    private AppCompatTextView loginTextView = null;
    private AppCompatTextView registerTextView = null;
    private AppCompatTextView findPassTextView = null;
    private AppCompatTextView qqTextView = null;
    private AppCompatTextView wechatTextView = null;
    private AppCompatTextView weiboTextView = null;

    private MemberLoginVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_login);
        closeImageView = findViewById(R.id.closeImageView);
        usernameEditText = findViewById(R.id.usernameEditText);
        usernameClearImageView = findViewById(R.id.usernameClearImageView);
        passwordEditText = findViewById(R.id.passwordEditText);
        passwordClearImageView = findViewById(R.id.passwordClearImageView);
        loginTextView = findViewById(R.id.loginTextView);
        registerTextView = findViewById(R.id.registerTextView);
        findPassTextView = findViewById(R.id.findPassTextView);
        qqTextView = findViewById(R.id.qqTextView);
        wechatTextView = findViewById(R.id.wechatTextView);
        weiboTextView = findViewById(R.id.weiboTextView);

    }

    @Override
    public void initData() {

        observeKeyborad(R.id.mainLinearLayout);
        vm = getVM(MemberLoginVM.class);
        canLogin();

    }

    @Override
    public void initEvent() {

        closeImageView.setOnClickListener(view -> onReturn(true));

        usernameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (VerifyUtil.isEmpty(Objects.requireNonNull(usernameEditText.getText()).toString())) {
                    usernameClearImageView.setVisibility(View.GONE);
                    usernameEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(App.get().getDrawables(R.drawable.ic_hint_mobile), null, null, null);
                } else {
                    usernameClearImageView.setVisibility(View.VISIBLE);
                    usernameEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(App.get().getDrawables(R.drawable.ic_hint_mobile_accent), null, null, null);
                }
                canLogin();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        usernameClearImageView.setOnClickListener(view -> usernameEditText.setText(""));

        passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (VerifyUtil.isEmpty(Objects.requireNonNull(passwordEditText.getText()).toString())) {
                    passwordClearImageView.setVisibility(View.GONE);
                    passwordEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(App.get().getDrawables(R.drawable.ic_hint_password), null, null, null);
                } else {
                    passwordClearImageView.setVisibility(View.VISIBLE);
                    passwordEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(App.get().getDrawables(R.drawable.ic_hint_password_accent), null, null, null);
                }
                canLogin();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        passwordClearImageView.setOnClickListener(view -> passwordEditText.setText(""));

        loginTextView.setOnClickListener(view -> {
            usernameEditText.setEnabled(false);
            usernameClearImageView.setEnabled(false);
            passwordEditText.setEnabled(false);
            passwordClearImageView.setEnabled(false);
            loginTextView.setEnabled(false);
            loginTextView.setText(R.string.tipsLoginIng);
            loginTextView.setTextColor(App.get().getColors(R.color.textThr));
            String username = Objects.requireNonNull(usernameEditText.getText()).toString();
            String password = Objects.requireNonNull(passwordEditText.getText()).toString();
            vm.login(username, password);
        });

        registerTextView.setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_REGISTER));

        findPassTextView.setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_FIND_PASS));

        qqTextView.setOnClickListener(view -> {

        });

        wechatTextView.setOnClickListener(view -> {

        });

        weiboTextView.setOnClickListener(view -> {

        });

    }

    @Override
    public void initObserve() {

        vm.getLoginLiveData().observe(this, bean -> {
            ToastHelp.get().show(R.string.loginSuccess);
            ShopAISdk.get().setKey(bean.getKey());
            SharedHelp.get().putString(Constant.SHARED_MEMBER_KEY, bean.getKey());
            SharedHelp.get().putString(Constant.SHARED_SELLER_KEY, bean.getSell() == null ? "" : bean.getSell().getKey());
            vm.getMember();
        });

        vm.getMemberLiveData().observe(this, bean -> {
            App.get().setMemberBean(bean);
            onReturn(false);
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                ToastHelp.get().show(bean.getReason());
                usernameEditText.setEnabled(true);
                usernameClearImageView.setEnabled(true);
                passwordEditText.setEnabled(true);
                passwordClearImageView.setEnabled(true);
                loginTextView.setEnabled(true);
                loginTextView.setText(R.string.login);
                loginTextView.setTextColor(App.get().getColors(R.color.accent));
            } else {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> vm.getMember(), view -> vm.getMember());
            }
        });

    }

    //自定义方法

    private void canLogin() {

        if (usernameClearImageView.getVisibility() == View.VISIBLE && passwordClearImageView.getVisibility() == View.VISIBLE) {
            loginTextView.setEnabled(true);
            loginTextView.setTextColor(App.get().getColors(R.color.accent));
        } else {
            loginTextView.setEnabled(false);
            loginTextView.setTextColor(App.get().getColors(R.color.textThr));
        }

    }

}
