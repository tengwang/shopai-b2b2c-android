package top.yokey.shopai.member.activity;

import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;

@Route(path = ARoutePath.MEMBER_EMAIL)
public class MemberEmailActivity extends BaseActivity {

    private Toolbar mainToolbar = null;
    private AppCompatImageView checkImageView = null;
    private AppCompatTextView tipsTextView = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_email);
        mainToolbar = findViewById(R.id.mainToolbar);
        checkImageView = findViewById(R.id.checkImageView);
        tipsTextView = findViewById(R.id.tipsTextView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.email);
        observeKeyborad(R.id.mainLinearLayout);
        if (App.get().getMemberBean().getMemberInfoAll().getMemberEmailBind().equals("1")) {
            tipsTextView.setText(String.format(getString(R.string.mineBindEmail), App.get().getMemberBean().getMemberInfoAll().getMemberEmail()));
            checkImageView.setVisibility(View.VISIBLE);
        } else {
            tipsTextView.setText(R.string.tipsUnbindEmail);
            checkImageView.setVisibility(View.GONE);
        }

    }

    @Override
    public void initEvent() {

    }

    @Override
    public void initObserve() {

    }

}
