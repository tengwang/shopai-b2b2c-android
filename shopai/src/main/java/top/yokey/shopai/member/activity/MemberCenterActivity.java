package top.yokey.shopai.member.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.text.InputType;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.entity.LocalMedia;

import java.util.ArrayList;
import java.util.List;

import top.yokey.shopai.R;
import top.yokey.shopai.member.viewmodel.MemberCenterVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.help.SharedHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.ConvertUtil;
import top.yokey.shopai.zcom.util.TimeUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zsdk.ShopAISdk;

@Route(path = ARoutePath.MEMBER_CENTER)
public class MemberCenterActivity extends BaseActivity {

    private Toolbar mainToolbar = null;
    private LinearLayoutCompat avatarLinearLayout = null;
    private AppCompatImageView avatarImageView = null;
    private AppCompatTextView accountTextView = null;
    private LinearLayoutCompat nameLinearLayout = null;
    private AppCompatTextView nameTextView = null;
    private AppCompatTextView levelTextView = null;
    private LinearLayoutCompat genderLinearLayout = null;
    private AppCompatTextView genderTextView = null;
    private LinearLayoutCompat birthdayLinearLayout = null;
    private AppCompatTextView birthdayTextView = null;
    private LinearLayoutCompat qqLinearLayout = null;
    private AppCompatTextView qqTextView = null;
    private LinearLayoutCompat wangwangLinearLayout = null;
    private AppCompatTextView wangwangTextView = null;
    private LinearLayoutCompat areaLinearLayout = null;
    private AppCompatTextView areaTextView = null;
    private LinearLayoutCompat emailLinearLayout = null;
    private AppCompatTextView emailTextView = null;
    private LinearLayoutCompat mobileLinearLayout = null;
    private AppCompatTextView mobileTextView = null;
    private LinearLayoutCompat passwordLinearLayout = null;
    private LinearLayoutCompat payPasswordLinearLayout = null;
    private AppCompatTextView logoutTextView = null;

    private int type = 0;
    private String data = "";
    private MemberCenterVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_center);
        mainToolbar = findViewById(R.id.mainToolbar);
        avatarLinearLayout = findViewById(R.id.avatarLinearLayout);
        avatarImageView = findViewById(R.id.avatarImageView);
        accountTextView = findViewById(R.id.accountTextView);
        nameLinearLayout = findViewById(R.id.nameLinearLayout);
        nameTextView = findViewById(R.id.nameTextView);
        levelTextView = findViewById(R.id.levelTextView);
        genderLinearLayout = findViewById(R.id.genderLinearLayout);
        genderTextView = findViewById(R.id.genderTextView);
        birthdayLinearLayout = findViewById(R.id.birthdayLinearLayout);
        birthdayTextView = findViewById(R.id.birthdayTextView);
        qqLinearLayout = findViewById(R.id.qqLinearLayout);
        qqTextView = findViewById(R.id.qqTextView);
        wangwangLinearLayout = findViewById(R.id.wangwangLinearLayout);
        wangwangTextView = findViewById(R.id.wangwangTextView);
        areaLinearLayout = findViewById(R.id.areaLinearLayout);
        areaTextView = findViewById(R.id.areaTextView);
        emailLinearLayout = findViewById(R.id.emailLinearLayout);
        emailTextView = findViewById(R.id.emailTextView);
        mobileLinearLayout = findViewById(R.id.mobileLinearLayout);
        mobileTextView = findViewById(R.id.mobileTextView);
        passwordLinearLayout = findViewById(R.id.passwordLinearLayout);
        payPasswordLinearLayout = findViewById(R.id.payPasswordLinearLayout);
        logoutTextView = findViewById(R.id.logoutTextView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.personCenter);
        observeKeyborad(R.id.mainLinearLayout);
        vm = getVM(MemberCenterVM.class);

    }

    @Override
    public void initEvent() {

        avatarLinearLayout.setOnClickListener(view -> App.get().startAlbumSignleCrop(get()));

        nameLinearLayout.setOnClickListener(view -> DialogHelp.get().input(get(), InputType.TYPE_CLASS_TEXT, R.string.name, App.get().getMemberBean().getMemberInfoAll().getMemberTruename(), null, content -> {
            type = 1;
            data = content;
            vm.modifyInfo("member_truename", content);
        }));

        genderLinearLayout.setOnClickListener(view -> {
            int select = 1;
            ArrayList<String> arrayList = new ArrayList<>();
            arrayList.add(getString(R.string.boy));
            arrayList.add(getString(R.string.girl));
            String sex = App.get().getMemberBean().getMemberInfoAll().getMemberSex();
            if (VerifyUtil.isEmpty(sex) || sex.equals("1")) select = 0;
            DialogHelp.get().list(get(), R.string.gender, arrayList, select, (position, content) -> {
                type = 2;
                data = (position + 1) + "";
                vm.modifyInfo("member_sex", data);
            }, null);
        });

        birthdayLinearLayout.setOnClickListener(view -> new DatePickerDialog(
                get(),
                (view1, year, month, dayOfMonth) -> {
                    type = 3;
                    data = year + "-";
                    data += (month + 1) < 10 ? "0" + (month + 1) + "-" : (month + 1) + "-";
                    data += dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth + "";
                    vm.modifyInfo("member_birthday", data);
                },
                Integer.parseInt(TimeUtil.getYear()),
                Integer.parseInt(TimeUtil.getMouth()) - 1,
                Integer.parseInt(TimeUtil.getDay()))
                .show()
        );

        qqLinearLayout.setOnClickListener(view -> DialogHelp.get().input(get(), InputType.TYPE_CLASS_TEXT, R.string.qq, App.get().getMemberBean().getMemberInfoAll().getMemberQq(), null, content -> {
            type = 4;
            data = content;
            vm.modifyInfo("member_qq", content);
        }));

        wangwangLinearLayout.setOnClickListener(view -> DialogHelp.get().input(get(), InputType.TYPE_CLASS_TEXT, R.string.wangwang, App.get().getMemberBean().getMemberInfoAll().getMemberWw(), null, content -> {
            type = 5;
            data = content;
            vm.modifyInfo("member_ww", content);
        }));

        areaLinearLayout.setOnClickListener(view -> App.get().start(get(), ARoutePath.MAIN_AREA, Constant.CODE_AREA));

        emailLinearLayout.setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_EMAIL));

        mobileLinearLayout.setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_MOBILE));

        passwordLinearLayout.setOnClickListener(view -> {
            if (App.get().getMemberBean().getMemberInfoAll().getMemberMobileBind().equals("1")) {
                App.get().start(ARoutePath.MEMBER_PASSWORD);
                return;
            }
            App.get().start(ARoutePath.MEMBER_MOBILE_BIND);
        });

        payPasswordLinearLayout.setOnClickListener(view -> {
            if (App.get().getMemberBean().getMemberInfoAll().getMemberMobileBind().equals("1")) {
                App.get().start(ARoutePath.MEMBER_PAY_PASSWORD);
                return;
            }
            App.get().start(ARoutePath.MEMBER_MOBILE_BIND);
        });

        logoutTextView.setOnClickListener(view -> {
            logoutTextView.setEnabled(false);
            logoutTextView.setText(R.string.handlerIng);
            vm.logout(App.get().getMemberBean().getUserName());
        });

    }

    @Override
    public void initObserve() {

        vm.getModifyLiveData().observe(this, string -> {
            ToastHelp.get().showSuccess();
            switch (type) {
                case 0:
                    ImageHelp.get().displayCircle(App.get().getMemberBean().getAvatar() + "?id=" + TimeUtil.getStampAll() + "", avatarImageView);
                    break;
                case 1:
                    nameTextView.setText(data);
                    App.get().getMemberBean().getMemberInfoAll().setMemberTruename(data);
                    break;
                case 2:
                    App.get().getMemberBean().getMemberInfoAll().setMemberSex(data);
                    genderTextView.setText(data.equals("1") ? getString(R.string.boy) : getString(R.string.girl));
                    break;
                case 3:
                    birthdayTextView.setText(data);
                    App.get().getMemberBean().getMemberInfoAll().setMemberBirthday(data);
                    break;
                case 4:
                    qqTextView.setText(data);
                    App.get().getMemberBean().getMemberInfoAll().setMemberQq(data);
                    break;
                case 5:
                    wangwangTextView.setText(data);
                    App.get().getMemberBean().getMemberInfoAll().setMemberWw(data);
                    break;
                case 6:
                    areaTextView.setText(data);
                    App.get().getMemberBean().getMemberInfoAll().setMemberAreainfo(data);
                    break;
            }
        });

        vm.getLogoutLiveData().observe(this, string -> {
            ShopAISdk.get().setKey("");
            ToastHelp.get().showSuccess();
            SharedHelp.get().putString(Constant.SHARED_MEMBER_KEY, "");
            SharedHelp.get().putString(Constant.SHARED_SELLER_KEY, "");
            onReturn(false);
        });

        vm.getErrorLiveData().observe(this, bean -> {
            ToastHelp.get().show(bean.getReason());
            if (bean.getCode() == 2) {
                logoutTextView.setEnabled(false);
                logoutTextView.setText(R.string.logout);
            }
        });

    }

    @Override
    public void onResume() {

        super.onResume();
        ImageHelp.get().displayCircle(App.get().getMemberBean().getAvatar() + "?id=" + TimeUtil.getStampAll() + "", avatarImageView);
        String temp = App.get().getMemberBean().getMemberInfoAll().getMemberName();
        accountTextView.setText(VerifyUtil.isEmpty(temp) ? getString(R.string.unfilled) : temp);
        temp = App.get().getMemberBean().getMemberInfoAll().getMemberTruename();
        nameTextView.setText(VerifyUtil.isEmpty(temp) ? getString(R.string.unfilled) : temp);
        temp = App.get().getMemberBean().getMemberInfoAll().getLevelName();
        levelTextView.setText(VerifyUtil.isEmpty(temp) ? getString(R.string.unfilled) : temp);
        temp = App.get().getMemberBean().getMemberInfoAll().getMemberSex();
        genderTextView.setText(VerifyUtil.isEmpty(temp) ? getString(R.string.unfilled) : temp.equals("1") ? getString(R.string.boy) : getString(R.string.girl));
        temp = App.get().getMemberBean().getMemberInfoAll().getMemberBirthday();
        birthdayTextView.setText(VerifyUtil.isEmpty(temp) ? getString(R.string.unfilled) : temp);
        temp = App.get().getMemberBean().getMemberInfoAll().getMemberQq();
        qqTextView.setText(VerifyUtil.isEmpty(temp) ? getString(R.string.unfilled) : temp);
        temp = App.get().getMemberBean().getMemberInfoAll().getMemberWw();
        wangwangTextView.setText(VerifyUtil.isEmpty(temp) ? getString(R.string.unfilled) : temp);
        temp = App.get().getMemberBean().getMemberInfoAll().getMemberAreainfo();
        areaTextView.setText(VerifyUtil.isEmpty(temp) ? getString(R.string.unfilled) : temp);
        String email = App.get().getMemberBean().getMemberInfoAll().getMemberEmail();
        String emailBind = App.get().getMemberBean().getMemberInfoAll().getMemberEmailBind();
        if (emailBind.equals("1")) {
            emailTextView.setText(email);
        } else if (!VerifyUtil.isEmpty(email)) {
            emailTextView.setText(R.string.waitVerify);
        } else {
            emailTextView.setText(getString(R.string.unbind));
        }
        String mobile = App.get().getMemberBean().getMemberInfoAll().getMemberMobile();
        String mobileBind = App.get().getMemberBean().getMemberInfoAll().getMemberMobileBind();
        if (mobileBind.equals("1")) {
            mobileTextView.setText(mobile);
        } else if (!VerifyUtil.isEmpty(mobile)) {
            mobileTextView.setText(R.string.waitVerify);
        } else {
            mobileTextView.setText(getString(R.string.unbind));
        }

    }

    @Override
    public void onActivityResult(int req, int res, @Nullable Intent intent) {

        super.onActivityResult(req, res, intent);
        if (res == RESULT_OK && req == Constant.CODE_ALBUM && intent != null) {
            List<LocalMedia> list = PictureSelector.obtainMultipleResult(intent);
            vm.modifyAvatar(ConvertUtil.path2Base64(list.get(0).getCompressPath()));
            data = list.get(0).getCompressPath();
            type = 0;
        }
        if (res == RESULT_OK && req == Constant.CODE_AREA && intent != null) {
            String provinceId = intent.getStringExtra("province_id");
            String areaId = intent.getStringExtra("area_id");
            String cityId = intent.getStringExtra("city_id");
            String areaInfo = intent.getStringExtra("area_info");
            vm.modifyArea(provinceId, cityId, areaId, areaInfo);
            data = areaInfo;
            type = 6;
        }

    }

}
