package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.GoodsBrowseBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberGoodsBrowseController;

public class MemberFootprintVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<GoodsBrowseBean>> goodsLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> delLiveData = new MutableLiveData<>();

    public MemberFootprintVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<GoodsBrowseBean>> getGoodsLiveData() {

        return goodsLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public MutableLiveData<String> getDelLiveData() {

        return delLiveData;

    }

    public void getGoods(String page) {

        MemberGoodsBrowseController.browseList(page, new HttpCallBack<ArrayList<GoodsBrowseBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<GoodsBrowseBean> list) {
                goodsLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void clear() {

        MemberGoodsBrowseController.browseClearall(new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                delLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
