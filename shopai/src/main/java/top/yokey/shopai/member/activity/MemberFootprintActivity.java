package top.yokey.shopai.member.activity;

import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.member.adapter.MemberFootprintGoodsAdapter;
import top.yokey.shopai.member.viewmodel.MemberFootprintVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.GoodsBrowseBean;
import top.yokey.shopai.zsdk.data.GoodsData;

@Route(path = ARoutePath.MEMBER_FOOTPRINT)
public class MemberFootprintActivity extends BaseActivity {

    private final ArrayList<GoodsBrowseBean> arrayList = new ArrayList<>();
    private final MemberFootprintGoodsAdapter adapter = new MemberFootprintGoodsAdapter(arrayList);
    private Toolbar mainToolbar = null;
    private AppCompatImageView toolbarImageView = null;
    private PullRefreshView mainPullRefreshView = null;
    private int page = 1;
    private MemberFootprintVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_pull_refresh_view);
        observeKeyborad(R.id.mainLinearLayout);
        mainToolbar = findViewById(R.id.mainToolbar);
        toolbarImageView = findViewById(R.id.toolbarImageView);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.mineFootprint);
        mainPullRefreshView.setLayoutManager(new LinearLayoutManager(get()));
        mainPullRefreshView.setItemDecoration(new LineDecoration(App.get().dp2Px(16), true));
        mainPullRefreshView.setAdapter(adapter);
        toolbarImageView.setImageResource(R.drawable.ic_action_delete);
        toolbarImageView.setVisibility(View.GONE);
        vm = getVM(MemberFootprintVM.class);
        getData();

    }

    @Override
    public void initEvent() {

        toolbarImageView.setOnClickListener(view -> DialogHelp.get().query(get(), R.string.confirmSelection, R.string.tipsClearBrowser, null, v -> {
            ToastHelp.get().show(R.string.handlerIng);
            toolbarImageView.setVisibility(View.GONE);
            vm.clear();
        }));

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                page = 1;
                getData();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getData();
            }

            @Override
            public void onLoadMore() {
                getData();
            }
        });

        adapter.setOnItemClickListener((position, bean) -> App.get().startGoods(new GoodsData(bean.getGoodsId())));

    }

    @Override
    public void initObserve() {

        vm.getGoodsLiveData().observe(this, list -> {
            if (page == 1) arrayList.clear();
            page = page + 1;
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
            toolbarImageView.setVisibility(arrayList.size() == 0 ? View.GONE : View.VISIBLE);
        });

        vm.getHasMoreLiveData().observe(this, bool -> mainPullRefreshView.setCanLoadMore(bool));

        vm.getDelLiveData().observe(this, string -> {
            ToastHelp.get().showSuccess();
            page = 1;
            getData();
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                if (arrayList.size() == 0) {
                    mainPullRefreshView.setError(bean.getReason());
                } else {
                    ToastHelp.get().show(bean.getReason());
                }
                return;
            }
            ToastHelp.get().show(bean.getReason());
        });

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView.setLoad();
        vm.getGoods(page + "");

    }

}
