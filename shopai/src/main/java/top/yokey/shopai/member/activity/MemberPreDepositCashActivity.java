package top.yokey.shopai.member.activity;

import android.text.InputType;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.member.viewmodel.MemberPreDepositCashVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zsdk.data.PreDepositCashData;

@Route(path = ARoutePath.MEMBER_PRE_DEPOSIT_CASH)
public class MemberPreDepositCashActivity extends BaseActivity {

    private final PreDepositCashData data = new PreDepositCashData();
    private Toolbar mainToolbar;
    private AppCompatEditText amountEditText;
    private AppCompatEditText methodEditText;
    private AppCompatEditText accountEditText;
    private AppCompatEditText nameEditText;
    private AppCompatEditText mobileEditText;
    private AppCompatTextView submitTextView;
    private MemberPreDepositCashVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_pre_deposit_cash);
        mainToolbar = findViewById(R.id.mainToolbar);
        amountEditText = findViewById(R.id.amountEditText);
        methodEditText = findViewById(R.id.methodEditText);
        accountEditText = findViewById(R.id.accountEditText);
        nameEditText = findViewById(R.id.nameEditText);
        mobileEditText = findViewById(R.id.mobileEditText);
        submitTextView = findViewById(R.id.submitTextView);

    }

    @Override
    public void initData() {

        App.get().setFocus(amountEditText);
        setToolbar(mainToolbar, R.string.preDepositCash);
        observeKeyborad(R.id.mainLinearLayout);
        vm = getVM(MemberPreDepositCashVM.class);

    }

    @Override
    public void initEvent() {

        submitTextView.setOnClickListener(view -> {
            if (VerifyUtil.isEmpty(Objects.requireNonNull(amountEditText.getText()).toString())) {
                ToastHelp.get().show(R.string.pleaseInputAmount);
                return;
            }
            if (VerifyUtil.isEmpty(Objects.requireNonNull(methodEditText.getText()).toString())) {
                ToastHelp.get().show(R.string.pleaseInputCashMethod);
                return;
            }
            if (VerifyUtil.isEmpty(Objects.requireNonNull(accountEditText.getText()).toString())) {
                ToastHelp.get().show(R.string.pleaseInputCashAccount);
                return;
            }
            if (VerifyUtil.isEmpty(Objects.requireNonNull(nameEditText.getText()).toString())) {
                ToastHelp.get().show(R.string.pleaseInputCashName);
                return;
            }
            if (VerifyUtil.isEmpty(Objects.requireNonNull(mobileEditText.getText()).toString())) {
                ToastHelp.get().show(R.string.pleaseInputMobile);
                return;
            }
            DialogHelp.get().input(
                    get(),
                    InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD,
                    R.string.pleaseInputPayPassword,
                    "",
                    null,
                    content -> {
                        if (VerifyUtil.isEmpty(content)) {
                            ToastHelp.get().show(R.string.pleaseInputPayPassword);
                            return;
                        }
                        data.setAmount(Objects.requireNonNull(amountEditText.getText()).toString());
                        data.setBankName(Objects.requireNonNull(methodEditText.getText()).toString());
                        data.setBankNo(Objects.requireNonNull(accountEditText.getText()).toString());
                        data.setBankUser(Objects.requireNonNull(nameEditText.getText()).toString());
                        data.setMobile(Objects.requireNonNull(mobileEditText.getText()).toString());
                        data.setPassword(content);
                        submitTextView.setEnabled(false);
                        submitTextView.setText(R.string.handlerIng);
                        vm.submit(data);
                    }
            );
        });

    }

    @Override
    public void initObserve() {

        vm.getSubmitLiveData().observe(this, string -> {
            ToastHelp.get().showSuccess();
            submitTextView.setEnabled(true);
            submitTextView.setText(R.string.submit);
            onReturn(false);
        });

        vm.getErrorLiveData().observe(this, bean -> {
            ToastHelp.get().show(bean.getReason());
            submitTextView.setEnabled(true);
            submitTextView.setText(R.string.submit);
        });

    }

}
