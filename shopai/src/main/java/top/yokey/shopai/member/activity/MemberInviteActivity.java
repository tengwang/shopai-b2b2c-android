package top.yokey.shopai.member.activity;

import android.graphics.Bitmap;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import cn.sharesdk.onekeyshare.OnekeyShare;
import top.yokey.shopai.R;
import top.yokey.shopai.member.viewmodel.MemberInviteVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.help.ToastHelp;

@Route(path = ARoutePath.MEMBER_INVITE)
public class MemberInviteActivity extends BaseActivity {

    private Toolbar mainToolbar = null;
    private AppCompatTextView leftTextView = null;
    private AppCompatTextView rightTextView = null;
    private AppCompatImageView qrCodeImageView = null;
    private AppCompatTextView shareTextView = null;
    private AppCompatTextView linkTextView = null;

    private MemberInviteVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_invite);
        mainToolbar = findViewById(R.id.mainToolbar);
        leftTextView = findViewById(R.id.leftTextView);
        rightTextView = findViewById(R.id.rightTextView);
        qrCodeImageView = findViewById(R.id.qrCodeImageView);
        shareTextView = findViewById(R.id.shareTextView);
        linkTextView = findViewById(R.id.linkTextView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar);
        observeKeyborad(R.id.mainLinearLayout);
        leftTextView.setText(R.string.inviteProfit);
        rightTextView.setText(R.string.distribuIncome);
        vm = getVM(MemberInviteVM.class);
        vm.getInvite();

    }

    @Override
    public void initEvent() {

        rightTextView.setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_INVITE_INCOME));

        qrCodeImageView.setOnClickListener(view -> {
            qrCodeImageView.buildDrawingCache(true);
            qrCodeImageView.buildDrawingCache();
            Bitmap bitmap = qrCodeImageView.getDrawingCache();
            ImageHelp.get().saveBitmap(get(), bitmap, "qr_code");
            qrCodeImageView.setDrawingCacheEnabled(false);
            ToastHelp.get().show(String.format(getString(R.string.imageSaveToPath), "qr_code.jpg"));
        });

        shareTextView.setOnClickListener(view -> {
            OnekeyShare onekeyShare = new OnekeyShare();
            onekeyShare.disableSSOWhenAuthorize();
            onekeyShare.setTitleUrl(linkTextView.getText().toString());
            onekeyShare.setImageUrl(App.get().getMemberBean().getAvatar());
            onekeyShare.setTitle(getString(R.string.inviteProfit));
            onekeyShare.setText(getString(R.string.inviteProfit));
            onekeyShare.setUrl(linkTextView.getText().toString());
            onekeyShare.show(get());
        });

        linkTextView.setOnClickListener(view -> {
            ToastHelp.get().showSuccess();
            App.get().setClipboard(linkTextView.getText().toString());
        });

    }

    @Override
    public void initObserve() {

        vm.getInviteLiveData().observe(this, bean -> {
            ImageHelp.get().displayRadius(bean.getMyurlSrc(), qrCodeImageView);
            linkTextView.setText(bean.getMyurl());
        });

    }

}
