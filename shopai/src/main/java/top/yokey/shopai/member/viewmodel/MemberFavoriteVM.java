package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.GoodsFavoriteBean;
import top.yokey.shopai.zsdk.bean.StoreFavoriteBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberFavoritesController;
import top.yokey.shopai.zsdk.controller.MemberFavoritesStoreController;

public class MemberFavoriteVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<GoodsFavoriteBean>> goodsLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<StoreFavoriteBean>> storeLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> delLiveData = new MutableLiveData<>();

    public MemberFavoriteVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<GoodsFavoriteBean>> getGoodsLiveData() {

        return goodsLiveData;

    }

    public MutableLiveData<ArrayList<StoreFavoriteBean>> getStoreLiveData() {

        return storeLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public MutableLiveData<String> getDelLiveData() {

        return delLiveData;

    }

    public void getGoods(String page) {

        MemberFavoritesController.favoritesList(page, new HttpCallBack<ArrayList<GoodsFavoriteBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<GoodsFavoriteBean> list) {
                goodsLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getStore(String page) {

        MemberFavoritesStoreController.favoritesList(page, new HttpCallBack<ArrayList<StoreFavoriteBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<StoreFavoriteBean> list) {
                storeLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void delGoods(String favId) {

        MemberFavoritesController.favoritesDel(favId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                delLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void delStore(String favId) {

        MemberFavoritesStoreController.favoritesDel(favId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                delLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
