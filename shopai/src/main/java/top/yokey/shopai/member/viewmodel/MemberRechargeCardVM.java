package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.MemberAssetBean;
import top.yokey.shopai.zsdk.bean.RechargeCardLogBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberFundController;
import top.yokey.shopai.zsdk.controller.MemberIndexController;
import top.yokey.shopai.zsdk.controller.VercodeController;

public class MemberRechargeCardVM extends BaseViewModel {

    private final MutableLiveData<MemberAssetBean> assetLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> codeKeyLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> rechargeLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<RechargeCardLogBean>> logLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();

    public MemberRechargeCardVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<MemberAssetBean> getAssetLiveData() {

        return assetLiveData;

    }

    public MutableLiveData<String> getCodeKeyLiveData() {

        return codeKeyLiveData;

    }

    public MutableLiveData<String> getRechargeLiveData() {

        return rechargeLiveData;

    }

    public MutableLiveData<ArrayList<RechargeCardLogBean>> getLogLiveData() {

        return logLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public void getAsset() {

        MemberIndexController.myAsset(new HttpCallBack<MemberAssetBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, MemberAssetBean bean) {
                assetLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getCodeKey() {

        VercodeController.makeCodeKey(new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                codeKeyLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void recharge(String rcSn, String captcha, String codeKey) {

        MemberFundController.rechargecardAdd(rcSn, captcha, codeKey, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                rechargeLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

    public void rcbLog(String page) {

        MemberFundController.rcbLog(page, new HttpCallBack<ArrayList<RechargeCardLogBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<RechargeCardLogBean> list) {
                logLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(4, reason));
            }
        });

    }

}
