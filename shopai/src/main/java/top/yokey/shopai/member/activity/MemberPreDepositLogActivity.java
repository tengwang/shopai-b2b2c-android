package top.yokey.shopai.member.activity;

import android.view.View;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import top.yokey.shopai.R;
import top.yokey.shopai.member.adapter.MemberPreDepositCashLogAdapter;
import top.yokey.shopai.member.adapter.MemberPreDepositLogAdapter;
import top.yokey.shopai.member.adapter.MemberPreDepositRechargeLogAdapter;
import top.yokey.shopai.member.viewmodel.MemberPreDepositLogVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.adapter.ViewPagerAdapter;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.PreDepositCashLogBean;
import top.yokey.shopai.zsdk.bean.PreDepositLogBean;
import top.yokey.shopai.zsdk.bean.PreDepositRechargeLogBean;

@Route(path = ARoutePath.MEMBER_PRE_DEPOSIT_LOG)
public class MemberPreDepositLogActivity extends BaseActivity {

    private final ArrayList<PreDepositLogBean> preDepositArrayList = new ArrayList<>();
    private final ArrayList<PreDepositRechargeLogBean> pdRechargeArrayList = new ArrayList<>();
    private final ArrayList<PreDepositCashLogBean> pdCashArrayList = new ArrayList<>();
    private final MemberPreDepositLogAdapter preDepositAdapter = new MemberPreDepositLogAdapter(preDepositArrayList);
    private final MemberPreDepositRechargeLogAdapter pdRechargeAdapter = new MemberPreDepositRechargeLogAdapter(pdRechargeArrayList);
    private final MemberPreDepositCashLogAdapter pdCashAdapter = new MemberPreDepositCashLogAdapter(pdCashArrayList);
    private Toolbar mainToolbar = null;
    private TabLayout mainTabLayout = null;
    private ViewPager mainViewPager = null;
    private PullRefreshView preDepositPullRefreshView = null;
    private PullRefreshView pdRechargePullRefreshView = null;
    private PullRefreshView pdCashPullRefreshView = null;
    private int preDepositPage = 1;
    private int pdRechargePage = 1;
    private int pdCashPage = 1;
    private boolean pdRechargeRefresh = false;
    private MemberPreDepositLogVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_pre_deposit_log);
        mainToolbar = findViewById(R.id.mainToolbar);
        mainTabLayout = findViewById(R.id.mainTabLayout);
        mainViewPager = findViewById(R.id.mainViewPager);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.preDepositDetailed);
        observeKeyborad(R.id.mainLinearLayout);
        List<View> viewList = new ArrayList<>();
        viewList.add(getLayoutInflater().inflate(R.layout.include_pull_refresh_view, null));
        viewList.add(getLayoutInflater().inflate(R.layout.include_pull_refresh_view, null));
        viewList.add(getLayoutInflater().inflate(R.layout.include_pull_refresh_view, null));
        List<String> titleList = new ArrayList<>();
        titleList.add(getString(R.string.depositDetailed));
        titleList.add(getString(R.string.rechargeDetailed));
        titleList.add(getString(R.string.cashDetailed));
        for (int i = 0; i < viewList.size(); i++) {
            mainTabLayout.addTab(mainTabLayout.newTab().setText(titleList.get(i)));
        }
        App.get().setTabLayout(mainTabLayout, mainViewPager, new ViewPagerAdapter(viewList, titleList));
        mainTabLayout.setTabMode(TabLayout.MODE_FIXED);
        mainViewPager.setCurrentItem(0);
        preDepositPullRefreshView = viewList.get(0).findViewById(R.id.mainPullRefreshView);
        preDepositPullRefreshView.setAdapter(preDepositAdapter);
        pdRechargePullRefreshView = viewList.get(1).findViewById(R.id.mainPullRefreshView);
        pdRechargePullRefreshView.setAdapter(pdRechargeAdapter);
        pdCashPullRefreshView = viewList.get(2).findViewById(R.id.mainPullRefreshView);
        pdCashPullRefreshView.setAdapter(pdCashAdapter);
        vm = getVM(MemberPreDepositLogVM.class);
        getPreDepositLog();
        getPdRechargeLog();
        getPdCashLog();

    }

    @Override
    public void initEvent() {

        preDepositPullRefreshView.setOnClickListener(view -> {
            if (preDepositPullRefreshView.isError() || preDepositArrayList.size() == 0) {
                preDepositPage = 1;
                getPreDepositLog();
            }
        });

        preDepositPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                preDepositPage = 1;
                getPreDepositLog();
            }

            @Override
            public void onLoadMore() {
                getPreDepositLog();
            }
        });

        pdRechargePullRefreshView.setOnClickListener(view -> {
            if (pdRechargePullRefreshView.isError() || pdRechargeArrayList.size() == 0) {
                pdRechargePage = 1;
                getPdRechargeLog();
            }
        });

        pdRechargePullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pdRechargePage = 1;
                getPdRechargeLog();
            }

            @Override
            public void onLoadMore() {
                getPdRechargeLog();
            }
        });

        pdCashPullRefreshView.setOnClickListener(view -> {
            if (pdCashPullRefreshView.isError() || pdCashArrayList.size() == 0) {
                pdCashPage = 1;
                getPdCashLog();
            }
        });

        pdCashPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pdCashPage = 1;
                getPdCashLog();
            }

            @Override
            public void onLoadMore() {
                getPdCashLog();
            }
        });

        pdRechargeAdapter.setOnItemClickListener((position, bean) -> {
            if (!bean.getPdrPaymentState().equals(Constant.COMMON_ENABLE)) {
                pdRechargeRefresh = true;
                App.get().start(ARoutePath.MEMBER_PRE_DEPOSIT_PAY, Constant.DATA_SN, bean.getPdrSn());
            }
        });

        pdCashAdapter.setOnItemClickListener((position, bean) -> App.get().start(ARoutePath.MEMBER_PRE_DEPOSIT_CASH_DETAILED, Constant.DATA_ID, bean.getPdcId()));

    }

    @Override
    public void initObserve() {

        vm.getPreDepositLogLiveData().observe(this, list -> {
            if (preDepositPage == 1) preDepositArrayList.clear();
            preDepositPage = preDepositPage + 1;
            preDepositArrayList.addAll(list);
            preDepositPullRefreshView.setComplete();
        });

        vm.getPreDepositLogHasMoreLiveData().observe(this, bool -> preDepositPullRefreshView.setCanLoadMore(bool));

        vm.getPdRechargeLogLiveData().observe(this, list -> {
            if (pdRechargePage == 1) pdRechargeArrayList.clear();
            pdRechargePage = pdRechargePage + 1;
            pdRechargeArrayList.addAll(list);
            pdRechargePullRefreshView.setComplete();
        });

        vm.getPdRechargeLogHasMoreLiveData().observe(this, bool -> pdRechargePullRefreshView.setCanLoadMore(bool));

        vm.getPdCashLogLiveData().observe(this, list -> {
            if (pdCashPage == 1) pdCashArrayList.clear();
            pdCashPage = pdCashPage + 1;
            pdCashArrayList.addAll(list);
            pdCashPullRefreshView.setComplete();
        });

        vm.getPdCashLogHasMoreLiveData().observe(this, bool -> pdCashPullRefreshView.setCanLoadMore(bool));

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                if (preDepositArrayList.size() != 0) {
                    ToastHelp.get().show(bean.getReason());
                } else {
                    preDepositPullRefreshView.setError(bean.getReason());
                }
                return;
            }
            if (bean.getCode() == 2) {
                if (pdRechargeArrayList.size() != 0) {
                    ToastHelp.get().show(bean.getReason());
                } else {
                    pdRechargePullRefreshView.setError(bean.getReason());
                }
                return;
            }
            if (bean.getCode() == 3) {
                if (pdCashArrayList.size() != 0) {
                    ToastHelp.get().show(bean.getReason());
                } else {
                    pdCashPullRefreshView.setError(bean.getReason());
                }
            }
        });

    }

    @Override
    public void onResume() {

        super.onResume();
        if (pdRechargeRefresh) {
            pdRechargeRefresh = false;
            pdRechargePage = 1;
            getPdRechargeLog();
        }

    }

    //自定义方法

    private void getPreDepositLog() {

        preDepositPullRefreshView.setLoad();
        vm.getPreDepositLog(preDepositPage + "");

    }

    private void getPdRechargeLog() {

        pdRechargePullRefreshView.setLoad();
        vm.getPdRechargeLog(pdRechargePage + "");

    }

    private void getPdCashLog() {

        pdCashPullRefreshView.setLoad();
        vm.getPdCashLog(pdCashPage + "");

    }

}
