package top.yokey.shopai.member.activity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.member.viewmodel.MemberRegisterVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.other.CountDown;
import top.yokey.shopai.zcom.util.ConvertUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;

@Route(path = ARoutePath.MEMBER_REGISTER)
public class MemberRegisterActivity extends BaseActivity {

    private Toolbar mainToolbar = null;
    private AppCompatEditText mobileEditText = null;
    private AppCompatImageView mobileClearImageView = null;
    private AppCompatEditText captchaEditText = null;
    private AppCompatTextView captchaTextView = null;
    private AppCompatEditText passwordEditText = null;
    private AppCompatImageView passwordClearImageView = null;
    private AppCompatTextView registerTextView = null;

    private MemberRegisterVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_register);
        mainToolbar = findViewById(R.id.mainToolbar);
        mobileEditText = findViewById(R.id.mobileEditText);
        mobileClearImageView = findViewById(R.id.mobileClearImageView);
        captchaEditText = findViewById(R.id.captchaEditText);
        captchaTextView = findViewById(R.id.captchaTextView);
        passwordEditText = findViewById(R.id.passwordEditText);
        passwordClearImageView = findViewById(R.id.passwordClearImageView);
        registerTextView = findViewById(R.id.registerTextView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.registerAccount);
        observeKeyborad(R.id.mainLinearLayout);
        vm = getVM(MemberRegisterVM.class);
        canGetCaptcha();
        canRegister();

    }

    @Override
    public void initEvent() {

        mobileEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (VerifyUtil.isEmpty(Objects.requireNonNull(mobileEditText.getText()).toString())) {
                    mobileClearImageView.setVisibility(View.GONE);
                    mobileEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(App.get().getDrawables(R.drawable.ic_hint_mobile), null, null, null);
                } else {
                    mobileClearImageView.setVisibility(View.VISIBLE);
                    mobileEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(App.get().getDrawables(R.drawable.ic_hint_mobile_accent), null, null, null);
                }
                canGetCaptcha();
                canRegister();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mobileClearImageView.setOnClickListener(view -> mobileEditText.setText(""));

        captchaEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (VerifyUtil.isEmpty(Objects.requireNonNull(captchaEditText.getText()).toString())) {
                    captchaEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(App.get().getDrawables(R.drawable.ic_hint_code), null, null, null);
                } else {
                    captchaEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(App.get().getDrawables(R.drawable.ic_hint_code_accent), null, null, null);
                }
                canRegister();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        captchaTextView.setOnClickListener(view -> {
            String mobile = Objects.requireNonNull(mobileEditText.getText()).toString();
            if (!VerifyUtil.isMobile(mobile)) {
                ToastHelp.get().show(R.string.tipsMobileError);
                return;
            }
            captchaTextView.setEnabled(false);
            captchaTextView.setText(R.string.handlerIng);
            vm.getCaptcha(mobile, "1");
        });

        passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (VerifyUtil.isEmpty(Objects.requireNonNull(passwordEditText.getText()).toString())) {
                    passwordClearImageView.setVisibility(View.GONE);
                    passwordEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(App.get().getDrawables(R.drawable.ic_hint_password), null, null, null);
                } else {
                    passwordClearImageView.setVisibility(View.VISIBLE);
                    passwordEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(App.get().getDrawables(R.drawable.ic_hint_password_accent), null, null, null);
                }
                canRegister();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        passwordClearImageView.setOnClickListener(view -> passwordEditText.setText(""));

        registerTextView.setOnClickListener(view -> {
            mobileEditText.setEnabled(false);
            mobileClearImageView.setEnabled(false);
            captchaEditText.setEnabled(false);
            captchaTextView.setEnabled(false);
            passwordEditText.setEnabled(false);
            passwordClearImageView.setEnabled(false);
            registerTextView.setEnabled(false);
            registerTextView.setText(R.string.tipsCheckCaptcha);
            registerTextView.setTextColor(App.get().getColors(R.color.textThr));
            String mobile = Objects.requireNonNull(mobileEditText.getText()).toString();
            String captcha = Objects.requireNonNull(captchaEditText.getText()).toString();
            vm.checkCaptcha(mobile, "1", captcha);
        });

    }

    @Override
    public void initObserve() {

        vm.getGetCaptchaLiveData().observe(this, string -> {
            ToastHelp.get().show(R.string.captchaSendSuccess);
            final int time = ConvertUtil.string2Int(string);
            new CountDown(time * 1000, Constant.TIME_TICK) {
                int count = time;

                @Override
                public void onTick(long millisUntilFinished) {
                    super.onTick(millisUntilFinished);
                    count--;
                    String temp = count + " S";
                    captchaTextView.setText(temp);
                    captchaTextView.setTextColor(App.get().getColors(R.color.textThr));
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    captchaTextView.setEnabled(true);
                    captchaTextView.setText(R.string.get);
                    captchaTextView.setTextColor(App.get().getColors(R.color.accent));
                }
            }.start();
        });

        vm.getCheckCaptchaLiveData().observe(this, string -> {
            registerTextView.setText(R.string.handlerIng);
            registerTextView.setTextColor(App.get().getColors(R.color.textThr));
            String mobile = Objects.requireNonNull(mobileEditText.getText()).toString();
            String password = Objects.requireNonNull(passwordEditText.getText()).toString();
            String captcha = Objects.requireNonNull(captchaEditText.getText()).toString();
            vm.smsRegister(mobile, password, captcha);
        });

        vm.getRegisterLiveData().observe(this, string -> {
            ToastHelp.get().show(R.string.registerSuccess);
            onReturn(false);
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                ToastHelp.get().show(bean.getReason());
                captchaTextView.setText(R.string.get);
                captchaTextView.setEnabled(true);
            } else {
                ToastHelp.get().show(bean.getReason());
                mobileEditText.setEnabled(true);
                mobileClearImageView.setEnabled(true);
                captchaEditText.setEnabled(true);
                captchaTextView.setEnabled(true);
                passwordEditText.setEnabled(true);
                passwordClearImageView.setEnabled(true);
                registerTextView.setEnabled(true);
                registerTextView.setTextColor(App.get().getColors(R.color.accent));
                registerTextView.setText(R.string.register);
            }
        });

    }

    //自定义方法

    private void canGetCaptcha() {

        if (mobileClearImageView.getVisibility() == View.VISIBLE) {
            captchaTextView.setEnabled(true);
            captchaTextView.setTextColor(App.get().getColors(R.color.accent));
        } else {
            captchaTextView.setEnabled(false);
            captchaTextView.setTextColor(App.get().getColors(R.color.textThr));
        }

    }

    private void canRegister() {

        if (mobileClearImageView.getVisibility() == View.VISIBLE
                && passwordClearImageView.getVisibility() == View.VISIBLE
                && !VerifyUtil.isEmpty(Objects.requireNonNull(captchaEditText.getText()).toString())) {
            registerTextView.setEnabled(true);
            registerTextView.setTextColor(App.get().getColors(R.color.accent));
        } else {
            registerTextView.setEnabled(false);
            registerTextView.setTextColor(App.get().getColors(R.color.textThr));
        }

    }

}
