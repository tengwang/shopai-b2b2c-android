package top.yokey.shopai.zcom.base;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import com.alibaba.android.arouter.launcher.ARouter;
import com.mob.pushsdk.MobPush;
import com.mob.pushsdk.MobPushCustomMessage;
import com.mob.pushsdk.MobPushNotifyMessage;

import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.LoggerHelp;

@SuppressWarnings("ALL")
public class BaseLifecycle implements LifecycleObserver {

    private BaseActivity baseActivity = null;

    public BaseLifecycle(BaseActivity baseActivity) {

        this.baseActivity = baseActivity;

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    public void onCreate() {

        //颜色修改
        baseActivity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        //组件插入
        ARouter.getInstance().inject(baseActivity);
        //公共方法
        baseActivity.initView();
        baseActivity.initData();
        baseActivity.initEvent();
        baseActivity.initObserve();
        //MOB推送
        MobPush.addPushReceiver(new com.mob.pushsdk.MobPushReceiver() {
            @Override
            public void onCustomMessageReceive(Context context, MobPushCustomMessage mobPushCustomMessage) {
                //接收到自定义消息处理
                LoggerHelp.get().show("接收到自定义消息处理");
            }

            @Override
            public void onNotifyMessageReceive(Context context, MobPushNotifyMessage mobPushNotifyMessage) {
                //接收到通知处理
                LoggerHelp.get().show("接收到通知处理");
            }

            @Override
            public void onNotifyMessageOpenedReceive(Context context, MobPushNotifyMessage mobPushNotifyMessage) {
                //通知被点击事件
                LoggerHelp.get().show("通知被点击事件");
                String type = mobPushNotifyMessage.getExtrasMap().get("type");
                String value = mobPushNotifyMessage.getExtrasMap().get("value");
                App.get().startTypeValue(type, value);
            }

            @Override
            public void onTagsCallback(Context context, String[] strings, int i, int i1) {
                //标签操作回调
                LoggerHelp.get().show("标签操作回调");
            }

            @Override
            public void onAliasCallback(Context context, String s, int i, int i1) {
                //别名操作回调
                LoggerHelp.get().show("别名操作回调");
            }
        });

    }

}
