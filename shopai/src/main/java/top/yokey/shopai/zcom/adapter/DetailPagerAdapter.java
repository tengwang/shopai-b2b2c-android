package top.yokey.shopai.zcom.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

@SuppressWarnings("ALL")
public class DetailPagerAdapter extends FragmentPagerAdapter {

    private View view = null;

    public DetailPagerAdapter(FragmentManager fragmentManager) {

        super(fragmentManager);

    }

    @Override
    public void setPrimaryItem(@NonNull ViewGroup viewGroup, int position, @NonNull Object object) {

        if (object instanceof View) {
            view = (View) object;
        } else if (object instanceof Fragment) {
            Fragment fragment = (Fragment) object;
            view = fragment.getView();
        }

    }

    @Override
    public Fragment getItem(int i) {

        return new Fragment();

    }

    @Override
    public int getCount() {

        return 0;

    }

    public View getPrimaryItem() {

        return view;

    }

}
