package top.yokey.shopai.zcom.base;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

@SuppressWarnings("ALL")
public abstract class BaseViewModel extends AndroidViewModel {

    private final MutableLiveData<BaseErrorBean> errorLiveData = new MutableLiveData<>();

    public BaseViewModel(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<BaseErrorBean> getErrorLiveData() {

        return errorLiveData;

    }

}
