package top.yokey.shopai.zcom.base;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import com.github.anzewei.parallaxbacklayout.ParallaxBack;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.other.CountDown;

@SuppressWarnings("ALL")
@ParallaxBack
public abstract class BaseActivity extends AppCompatActivity {

    private boolean showKeyboard = false;

    @Override
    public void onCreate(Bundle bundle) {

        getLifecycle().addObserver(new BaseLifecycle(this));
        super.onCreate(bundle);

    }

    @Override
    public boolean onKeyDown(int code, KeyEvent event) {

        if (code == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
            onReturn(true);
            return true;
        }
        return super.onKeyDown(code, event);

    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {

        if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
            onReturn(true);
            return true;
        }
        return super.dispatchKeyEvent(keyEvent);

    }

    //必须重载

    public abstract void initView();

    public abstract void initData();

    public abstract void initEvent();

    public abstract void initObserve();

    //公共方法

    public Activity get() {

        return this;

    }

    public void onReturn(boolean handler) {

        if (handler && showKeyboard) {
            showKeyboard = false;
            hideKeyboard();
            return;
        }
        finish();

    }

    public void hideKeyboard() {

        showKeyboard = false;
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (inputMethodManager != null && inputMethodManager.isActive()) {
            inputMethodManager.hideSoftInputFromWindow(getWindow().peekDecorView().getWindowToken(), 0);
        }

    }

    public void setFullScreen() {

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setNavigationBarColor(Color.TRANSPARENT);
        getWindow().setStatusBarColor(Color.TRANSPARENT);

    }

    public void setStateBarWap() {

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(App.get().getColors(R.color.wap));

    }

    public void setStateBarPrimary() {

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(App.get().getColors(R.color.primary));

    }

    public void setStateBarColor(int color) {

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(App.get().getColors(color));

    }

    public void observeKeyborad(int id) {

        findViewById(id).addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            int keyHeight = App.get().getHeight() / 3;
            if (oldBottom != 0 && bottom != 0 && (oldBottom - bottom > keyHeight)) {
                showKeyboard = true;
            } else if (oldBottom != 0 && bottom != 0 && (bottom - oldBottom > keyHeight)) {
                new CountDown(200, 100) {
                    @Override
                    public void onFinish() {
                        super.onFinish();
                        showKeyboard = false;
                    }
                }.start();
            }
        });

    }

    public void setToolbar(Toolbar toolbar) {

        toolbar.setTitle("");
        toolbar.setContentInsetStartWithNavigation(0);
        toolbar.setNavigationIcon(R.drawable.ic_action_back);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onReturn(true));

    }

    public void setToolbar(Toolbar toolbar, int title) {

        toolbar.setTitle("");
        toolbar.setContentInsetStartWithNavigation(0);
        toolbar.setNavigationIcon(R.drawable.ic_action_back);
        AppCompatTextView appCompatTextView = findViewById(R.id.toolbarTextView);
        appCompatTextView.setText(title);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onReturn(true));

    }

    public void setToolbar(Toolbar toolbar, String title) {

        toolbar.setTitle("");
        toolbar.setContentInsetStartWithNavigation(0);
        toolbar.setNavigationIcon(R.drawable.ic_action_back);
        AppCompatTextView appCompatTextView = findViewById(R.id.toolbarTextView);
        appCompatTextView.setText(title);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onReturn(true));

    }

    public void setToolbar(Toolbar toolbar, int icon, int title) {

        toolbar.setTitle("");
        toolbar.setContentInsetStartWithNavigation(0);
        toolbar.setNavigationIcon(icon);
        AppCompatTextView appCompatTextView = findViewById(R.id.toolbarTextView);
        appCompatTextView.setText(title);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onReturn(true));

    }

    public void setToolbar(Toolbar toolbar, int icon, String title) {

        toolbar.setTitle("");
        toolbar.setContentInsetStartWithNavigation(0);
        toolbar.setNavigationIcon(icon);
        AppCompatTextView appCompatTextView = findViewById(R.id.toolbarTextView);
        appCompatTextView.setText(title);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onReturn(true));

    }

    public <T extends ViewModel> T getVM(@NonNull Class<T> cls) {

        return ViewModelProviders.of(this).get(cls);

    }

    public boolean isShowKeyboard() {

        return showKeyboard;

    }

}
