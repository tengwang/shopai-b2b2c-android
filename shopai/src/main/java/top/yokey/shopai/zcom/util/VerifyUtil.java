package top.yokey.shopai.zcom.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("ALL")
public class VerifyUtil {

    public static boolean isUrl(String url) {

        Pattern pattern = Pattern.compile("(https?|ftp|file)://[-A-Za-z0-9+&@#/%?=~_|!:,.;]+[-A-Za-z0-9+&@#/%=~_|]");
        Matcher matcher = pattern.matcher(url);
        return matcher.matches();

    }

    public static boolean isEmail(String email) {

        String reg = "^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\\\.[a-zA-Z0-9_-]+)+$";
        Pattern pattern = Pattern.compile(reg);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();

    }

    public static boolean isEmpty(String content) {

        if (content == null || "".equals(content) || content.length() == 0 || content.equals("null")) {
            return true;
        }
        for (int i = 0; i < content.length(); i++) {
            char c = content.charAt(i);
            if (c != ' ' && c != '\t' && c != '\r' && c != '\n') {
                return false;
            }
        }
        return true;

    }

    public static boolean isNumber(String number) {

        Pattern pattern = Pattern.compile("^[0-9]*$");
        Matcher matcher = pattern.matcher(number);
        return matcher.matches();

    }

    public static boolean isMobile(String mobile) {

        String reg = "^((13[0-9])|(15[0-9])|(17[0-9])|(18[0-9])|(19[0-9]))\\d{8}$";
        Pattern pattern = Pattern.compile(reg);
        Matcher matcher = pattern.matcher(mobile);
        return matcher.matches();

    }

    public static boolean isBankNo(String bankNo) {

        Pattern pattern = Pattern.compile("^[0-9]{16,19}$");
        Matcher matcher = pattern.matcher(bankNo);
        return matcher.matches();

    }

    public static boolean isIPAddress(String ipAddress) {

        Pattern pattern = Pattern.compile("[1-9](\\\\d{1,2})?\\\\.(0|([1-9](\\\\d{1,2})?))\\\\.(0|([1-9](\\\\d{1,2})?))\\\\.(0|([1-9](\\\\d{1,2})?))");
        Matcher matcher = pattern.matcher(ipAddress);
        return matcher.matches();

    }

}
