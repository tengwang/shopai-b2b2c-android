package top.yokey.shopai.zcom.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

@SuppressWarnings("ALL")
public class CenterTextView extends AppCompatTextView {

    public CenterTextView(Context context) {

        super(context);

    }

    public CenterTextView(Context context, AttributeSet attrs) {

        super(context, attrs);

    }

    public CenterTextView(Context context, AttributeSet attrs, int defStyle) {

        super(context, attrs, defStyle);

    }

    @Override
    protected void onDraw(Canvas canvas) {

        Drawable[] drawables = getCompoundDrawables();
        if (drawables != null) {
            Drawable drawable = drawables[0];
            if (drawable != null) {
                float width = getPaint().measureText(getText().toString());
                int padding = getCompoundDrawablePadding();
                int dWidth = 0;
                dWidth = drawable.getIntrinsicWidth();
                float bWidth = width + dWidth + padding;
                canvas.translate((getWidth() - bWidth) / 2, 0);
            }
        }
        super.onDraw(canvas);

    }

}
