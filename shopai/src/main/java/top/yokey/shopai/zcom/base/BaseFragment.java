package top.yokey.shopai.zcom.base;

import static android.content.Context.INPUT_METHOD_SERVICE;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

@SuppressWarnings("ALL")
public abstract class BaseFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle bundle) {

        View view = initView();
        initData();
        initEvent();
        initObserve();
        return view;

    }

    //必须重载

    public abstract View initView();

    public abstract void initData();

    public abstract void initEvent();

    public abstract void initObserve();

    //自定义方法

    public void hideKeyboard() {

        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        if (inputMethodManager != null && inputMethodManager.isActive()) {
            inputMethodManager.hideSoftInputFromWindow(getActivity().getWindow().peekDecorView().getWindowToken(), 0);
        }

    }

    public <T extends ViewModel> T getVM(@NonNull Class<T> cls) {

        return ViewModelProviders.of(this).get(cls);

    }

}
