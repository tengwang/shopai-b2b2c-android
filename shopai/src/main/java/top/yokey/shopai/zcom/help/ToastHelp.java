package top.yokey.shopai.zcom.help;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;

@SuppressWarnings("ALL")
public class ToastHelp {

    private static volatile ToastHelp instance = null;
    private Toast toast = null;
    private Context context = null;
    private String success = "";
    private String dataError = "";

    public static ToastHelp get() {

        if (instance == null) {
            synchronized (ToastHelp.class) {
                if (instance == null) {
                    instance = new ToastHelp();
                }
            }
        }
        return instance;

    }

    public void init(Context context, String success, String dataError) {

        this.toast = null;
        this.context = context;
        this.success = success;
        this.dataError = dataError;

    }

    public void show(String content) {

        if (toast != null) {
            toast.cancel();
        }
        toast = Toasty.info(context, content, Toast.LENGTH_SHORT, true);
        toast.show();

    }

    public void showDataError() {

        if (toast != null) {
            toast.cancel();
        }
        toast = Toasty.error(context, dataError, Toast.LENGTH_SHORT, true);
        toast.show();

    }

    public void showSuccess() {

        if (toast != null) {
            toast.cancel();
        }
        toast = Toasty.success(context, success, Toast.LENGTH_SHORT, true);
        toast.show();

    }

    public void show(int id) {

        if (toast != null) {
            toast.cancel();
        }
        String content = context.getString(id);
        toast = Toasty.info(context, content, Toast.LENGTH_SHORT, true);
        toast.show();

    }

}
